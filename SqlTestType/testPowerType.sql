INSERT INTO at_project_schema."testPower_type" (id, code, name) VALUES 
('0dd8026a-81b3-46ad-bee7-0ba01de95f4b', 'InsulationResistance', 'Insulation resistance'),
('101d5b93-fb82-4a60-8574-4222646d6103', 'DcVoltageOverSheath', 'DC voltage test of oversheath'),
('e441faa7-fc5e-4aa9-a889-6a3d4fefbbe0', 'AcVoltageInsulation', 'AC voltage test of the insulation'),
('e5c29819-226d-4e80-a70e-2d8a1752e3c5', 'DcVoltageInsulation', 'DC voltage test of the insulation'),
('d62eb39d-ac42-4c36-a723-57b2d9c08406', 'VlfTest', 'VLF test'),
('dd3a6049-21b1-492d-aa7b-a9d4bc8f172a', 'TandeltaVlfSource', 'Tan delta measurement with VLF source'),
('f48ffe4e-e813-411e-9cf9-b7ae969627d9', 'TandeltaPowerAcSource', 'Tan delta measurement with power frequency AC source'),
('7835e4b4-770c-4b42-abce-6555ab260c9d', 'ParticalDischarge', 'Partial discharge measurement'),
('89538922-8b04-4789-b3fe-182b613ece1c', 'GeneralInspection', 'General inspection')