INSERT INTO at_project_schema."testSurge_type" (id, code, name) VALUES 
('847b9f63-a309-4a8b-a253-9f9a161c3179', 'InsulationResistance', 'Insulation Resistance'),
('69cadbec-f5d9-447d-9827-0f16d14244fc', 'LeakageCurrent', 'Leakage current at continuous operating voltage'),
('15c95a31-5cbc-4204-8ebf-4676ef40ed33', 'PowerFrequency', 'Power frequency voltage at reference current'),
('cd37afa1-0357-4fe3-b52e-cd960c39a1cf', 'GeneralInspection', 'General inspection')