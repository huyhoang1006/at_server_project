INSERT INTO at_project_schema."testDisconnect_type" (id, code, name) VALUES 
('4327953b-f835-4959-9141-2bccecf6ece0', 'InsulationResistance', 'Insulation Resistance'),
('1f5e6434-571f-4f26-a3b6-3eb6cdd8c295', 'ContactResistance', 'Contact resistance'),
('6edc7aad-ff43-470f-873c-648c49a3cae6', 'InsulationResMotor', 'Insulation resisitance of motor'),
('e34ec797-d49a-45e6-8f89-1a901231a89a', 'DcWindingMotor', 'DC winding resistance of motor'),
('b5b2d09d-a813-439e-8c31-8f5ff4b4cce0', 'OperatingTest', 'Operating Test'),
('c5382eea-9530-48f2-9b7a-84f0da89544c', 'GeneralInspection', 'General inspection'),
('2ecd26f9-ae26-4e5f-b8ed-6f11f2cea027', 'ControlCheck', 'Control cabinet check')