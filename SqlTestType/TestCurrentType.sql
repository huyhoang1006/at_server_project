INSERT INTO at_project_schema."testCurrent_type" (id, code, name) VALUES 
('490aa186-ad49-4138-8abc-0c3adfc8d7b9', 'InsulationResistance', ' Insulation resistance'),
('da78997f-1836-4639-a742-01470144e1f8', 'CTRatio', 'CT ratio'),
('603f40a9-5c74-4da8-8ad9-d0058c498cac', 'CTExcitation', 'CT Excitation'),
('f313e2b4-32cd-48e6-9a1e-5e6d341a2e2e', 'CTWindingRes', 'CT DF & CAP'),
('82af3fc0-8cf7-46a5-8e0b-f56234e6c94c', 'CTDfcap', '3333333333'),
('051adc79-e761-4692-8ded-155ddf16c8ba', 'GeneralInspection', 'General inspection')