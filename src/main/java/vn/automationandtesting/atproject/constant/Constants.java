package vn.automationandtesting.atproject.constant;

import java.nio.file.FileSystems;

public class Constants {
    public static final String USER_ID_CLAIMS_NAME = "user_id";
    public static final String ROLE_CLAIMS_NAME = "role";
    public static final String GROUPS_CLAIMS_NAME = "groups";
    public static final String PERMISSIONS_CLAIMS_NAME = "permissions";

    public static final String ACCESS_TOKEN_ID_CLAIMS_NAME = "ati";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";


    public static final String DATA_TABLE_PARAM = "table";
    public static final String TABLE_CONDITION_INDICATOR_DF_PARAM = "condition_indicator_df";
    public static final String TABLE_CONDITION_INDICATOR_C_PARAM = "condition_indicator_c";
    public static final String TABLE_CONDITION_INDICATOR_PARAM = "condition_indicator";
    public static final String TABLE_DF_MEAS_PARAM = "df_meas";
    public static final String TABLE_DF_CHANGE_PARAM = "df_change";
    public static final String TABLE_C_MEAS_PARAM = "c_meas";
    public  static  final String TABLE_C_MEAS_BUSHING_PARAM = "tri_c_meas";
    public static final String TABLE_DEV_PER_PARAM = "dev_per";
    public static final String TABLE_ERROR_BETWEEN_PHASE_PARAM = "error_between_phase";
    public static final String TABLE_ERROR_R_REF_PARAM = "error_r_ref";
    public static final String TABLE_RATIO_DEV_PARAM = "ratio_dev";

    public static final String DATA_CODE_PARAM = "code";
    public static final String DATA_DF_MEAS_PARAM = "df_meas";
    public static final String DATA_DF_CHANGE_PARAM = "df_change";
    public static final String DATA_C_MEAS_PARAM = "c_meas";
    public static final String DATA_DEV_PER_PARAM = "dev_per";
    public static final String DATA_RATIO_DEV_PARAM = "ratio_dev";
    public static final String DATA_BREAKDOWN_VOLTAGE_PARAM = "breakdown_voltage";
    public static final String DATA_TRI_C_MEAS_PARAM = "tri_c_meas";
    public static final String DATA_RESULT_PARAM = "result";
    public static final String DATA_CONDITION_INDICATOR_DF_PARAM = "condition_indicator_df";
    public static final String DATA_CONDITION_INDICATOR_C_PARAM = "condition_indicator_c";
    public static final String DATA_CONDITION_INDICATOR_SETTING_PARAM = "condition_indicator_setting";
    public static final String DATA_CONDITION_INDICATOR_PARAM = "condition_indicator";
    public static final String DATA_STATUS_PARAM = "status";
    public static final String DATA_GOOD_PARAM = "good";
    public static final String DATA_FAIR_PARAM = "fair";
    public static final String DATA_POOR_PARAM = "poor";
    public static final String DATA_BAD_PARAM = "bad";

    public static final String DATA_H2_PARAM = "h2";
    public static final String DATA_CH4_PARAM = "ch4";
    public static final String DATA_C2H2_PARAM = "c2h2";
    public static final String DATA_C2H4_PARAM = "c2h4";
    public static final String DATA_C2H6_PARAM = "c2h6";
    public static final String DATA_CO_PARAM = "co";
    public static final String DATA_CO2_PARAM = "co2";
    public static final String DATA_TDCG_PARAM = "tdcg";

    public static final String DATA_KHT_PARAM = "kht";
    public static final String DATA_R60S_HVE_PARAM = "r60s_hve";
    public static final String DATA_R60S_LVE_PARAM = "r60s_lve";
    public static final String DATA_R60S_PARAM = "r60s";

    public static final String TYPE_HVE_PARAM = "HV-E";
    public static final String TYPE_LVE_PARAM = "LV-E";
    public static final String TYPE_COMMON = "type";


    public static final String DATA_ERROR_BETWEEN_PHASE_PARAM = "error_between_phase";
    public static final String WEIGHTING_FACTOR = "weighting_factor";

    //Fmeca table
    public static final String FMECA_TABLE_CALCULATION_BUSHING_PF_DF = "b_pf_df";
    public static final String FMECA_TABLE_CALCULATION_BUSHING_C = "b_C1_c";
    public static final String FMECA_TABLE_CALCULATION_MOISTURE_CONTENT = "mc";

    public static final String WORKING_DIRECTORY = System.getProperty("user.dir") + FileSystems.getDefault().getSeparator() + "FileUpload";
}
