package vn.automationandtesting.atproject.util.common;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;


@Component
public class JsonProcessing {
    public ObjectNode getJsonDiff(String json1, String json2) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        // Chuyển chuỗi JSON thành đối tượng JsonNode
        JsonNode node1 = objectMapper.readTree(json1);
        JsonNode node2 = objectMapper.readTree(json2);

        // So sánh hai JsonNode và trả về ObjectNode chứa sự khác biệt
        ObjectNode diffNode =  compareJsonNodes(node1, node2, objectMapper.createObjectNode(), "");
        removeVersionKey(diffNode);
        return diffNode;
    }

    private ObjectNode compareJsonNodes(JsonNode node1, JsonNode node2, ObjectNode diffNode, String path) {
        if (node1.isObject() && node2.isObject()) {
            // So sánh các field trong cả hai đối tượng JSON
            Iterator<Map.Entry<String, JsonNode>> fields1 = node1.fields();
            while (fields1.hasNext()) {
                Map.Entry<String, JsonNode> entry1 = fields1.next();
                String currentPath = path.isEmpty() ? entry1.getKey() : path + "." + entry1.getKey();
                JsonNode node2Value = node2.get(entry1.getKey());

                if (node2Value == null) {
                    // Key có trong JSON 1 nhưng không có trong JSON 2
                    ObjectNode difference = diffNode.putObject(currentPath);
                    difference.put("valueInJson1", entry1.getValue().toString());
                    difference.put("keyMissingInJson2", "true");
                } else {
                    // So sánh giá trị của các field tương ứng
                    compareJsonNodes(entry1.getValue(), node2Value, diffNode, currentPath);
                }
            }

            // Kiểm tra các key có trong JSON 2 nhưng không có trong JSON 1
            Iterator<Map.Entry<String, JsonNode>> fields2 = node2.fields();
            while (fields2.hasNext()) {
                Map.Entry<String, JsonNode> entry2 = fields2.next();
                String currentPath = path.isEmpty() ? entry2.getKey() : path + "." + entry2.getKey();
                if (!node1.has(entry2.getKey())) {
                    ObjectNode difference = diffNode.putObject(currentPath);
                    difference.put("keyMissingInJson1", "true");
                    difference.put("valueInJson2", entry2.getValue().toString());
                }
            }

        } else if (node1.isArray() && node2.isArray()) {
            // So sánh các phần tử trong mảng
            compareJsonArrays(node1, node2, diffNode, path);

        } else if (!node1.equals(node2)) {
            // Nếu là giá trị đơn giản, ghi lại sự khác biệt
            ObjectNode difference = diffNode.putObject(path);
            difference.put("valueInJson1", node1 != null ? node1.toString() : "null");
            difference.put("valueInJson2", node2 != null ? node2.toString() : "null");
        }

        return diffNode;
    }

    private ObjectNode compareJsonArrays(JsonNode array1, JsonNode array2, ObjectNode diffNode, String path) {
        int length1 = array1.size();
        int length2 = array2.size();
        int i = 0, j = 0;

        while (i < length1 && j < length2) {
            String arrayPath = path + "[" + i + "]";

            if (!array1.get(i).equals(array2.get(j))) {
                // So sánh đệ quy để phát hiện thay đổi chi tiết
                compareJsonNodes(array1.get(i), array2.get(j), diffNode, arrayPath);
            }
            i++;
            j++;
        }

        // Kiểm tra các phần tử còn lại trong array1 (bị xóa)
        while (i < length1) {
            String arrayPath = path + "[" + i + "]";
            ObjectNode difference = diffNode.putObject(arrayPath);
            difference.put("valueInJson1", array1.get(i).toString());
            difference.put("elementMissingInJson2", "true");
            i++;
        }

        // Kiểm tra các phần tử còn lại trong array2 (được thêm)
        while (j < length2) {
            String arrayPath = path + "[" + j + "]";
            ObjectNode difference = diffNode.putObject(arrayPath);
            difference.put("elementMissingInJson1", "true");
            difference.put("valueInJson2", array2.get(j).toString());
            j++;
        }

        return diffNode;
    }

    private void removeVersionKey(ObjectNode node) {
        Iterator<String> fieldNames = node.fieldNames();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            if (fieldName.equals("version")) {
                fieldNames.remove();
            } else {
                JsonNode childNode = node.get(fieldName);
                if (childNode.isObject()) {
                    removeVersionKey((ObjectNode) childNode);
                }
            }
        }
    }
}
