package vn.automationandtesting.atproject.util.search;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Location;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 4/9/2022
 * @project at-project-server
 */
@AllArgsConstructor
public class AssetSearchSpecification implements Specification<Asset> {
    private String serial_number;
    private Location location;
    private boolean isDeleted = false;
    private UUID created_by;
    private String collab;

    @Override
    public Predicate toPredicate(Root<Asset> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if (this.serial_number != null) {
            predicates.add(criteriaBuilder.equal(root.get("serial_number"), this.serial_number));
        }
        if (this.location != null) {
            predicates.add(criteriaBuilder.equal(root.get("location"), this.location));
        }
        if(created_by != null && collab != null) {
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("createdBy"), this.created_by),
                    criteriaBuilder.like(root.get("collabs"), "%"+this.collab+"%")
            ));
        }
        predicates.add(criteriaBuilder.equal(root.get("isDeleted"), this.isDeleted));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
