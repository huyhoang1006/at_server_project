package vn.automationandtesting.atproject.config.user_auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.user_auth.handler.MyAuthenticationFailureHandler;
import vn.automationandtesting.atproject.config.user_auth.handler.MyAuthenticationSuccessHandler;
import vn.automationandtesting.atproject.config.user_auth.handler.MyOwnerAuthenticationFailureHandler;
import vn.automationandtesting.atproject.config.user_auth.handler.MyOwnerAuthenticationSuccessHandler;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

@Component
public class MyOwnerUsernamePasswordAuthenticationConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private MyOwnerAuthenticationSuccessHandler myOwnerAuthenticationSuccessHandler;
    @Autowired
    private MyOwnerAuthenticationFailureHandler myOwnerAuthenticationFailureHandler;

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        MyOwnerUsernamePasswordAuthenticationFilter myOwnerUsernamePasswordAuthenticationFilter = new MyOwnerUsernamePasswordAuthenticationFilter(sharedListIdService);
        myOwnerUsernamePasswordAuthenticationFilter.setAuthenticationManager(builder.getSharedObject(AuthenticationManager.class));
        myOwnerUsernamePasswordAuthenticationFilter.setAuthenticationSuccessHandler(myOwnerAuthenticationSuccessHandler);
        myOwnerUsernamePasswordAuthenticationFilter.setAuthenticationFailureHandler(myOwnerAuthenticationFailureHandler);

        builder
                .addFilterAt(myOwnerUsernamePasswordAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
        ;
    }
}
