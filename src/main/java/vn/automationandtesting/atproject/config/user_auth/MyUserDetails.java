package vn.automationandtesting.atproject.config.user_auth;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
public class MyUserDetails extends User {
    private final UUID userID;
    private final String roleName;
    private final List<String> groups;
    private final Set<String> permissions;

    public MyUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, UUID userID, String rolName, List<String> groups, Set<String> permissions) {
        super(username, password, authorities);
        this.userID = userID;
        this.roleName = rolName;
        this.groups = groups;
        this.permissions = permissions;
    }
}
