package vn.automationandtesting.atproject.config.user_auth.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.token.TokenUtils;
import vn.automationandtesting.atproject.config.user_auth.MyOwnerUserDetails;
import vn.automationandtesting.atproject.config.user_auth.MyUserDetails;
import vn.automationandtesting.atproject.controller.dto.JwtResponse;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class MyOwnerAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private TokenUtils tokenUtils;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        MyOwnerUserDetails myOwnerUserDetails = (MyOwnerUserDetails) authentication.getPrincipal();
        JwtResponse jwtResponse = tokenUtils.createJwtResponse(myOwnerUserDetails);

        ResponseObject responseObject = new ResponseObject(true, "Grant access token", jwtResponse);

        ObjectMapper objectMapper = new ObjectMapper();
        log.info(" Login successful ");
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.getWriter().write(objectMapper.writeValueAsString(responseObject));
    }
}
