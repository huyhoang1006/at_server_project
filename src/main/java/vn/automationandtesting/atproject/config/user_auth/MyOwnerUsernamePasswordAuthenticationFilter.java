package vn.automationandtesting.atproject.config.user_auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import vn.automationandtesting.atproject.controller.dto.LoginRequest;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.UUID;

@Slf4j
public class MyOwnerUsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final SharedListIdService sharedListIdService;
    public MyOwnerUsernamePasswordAuthenticationFilter(SharedListIdService sharedListIdService) {
        super(new AntPathRequestMatcher("/auth/owner/login", "POST"));
        this.sharedListIdService = sharedListIdService;
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else {
            LoginRequest loginRequest = getLoginRequest(request);
            String username = loginRequest.getUsername();
            String password = loginRequest.getPassword();
            if (username == null) {
                username = "";
            }

            if (password == null) {
                password = "";
            }
            username = username.trim();
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
            this.setDetails(request, authRequest);
            try {
                Authentication authentication = this.getAuthenticationManager().authenticate(authRequest);
                if(!authentication.getAuthorities().toString().equals("[USER]")){
                    Object principal = authentication.getPrincipal();
                    if (principal instanceof MyOwnerUserDetails) {
                        MyOwnerUserDetails userDetails = (MyOwnerUserDetails) principal;
                        UUID userId = userDetails.getUserID();  // Lấy userId từ UserDetails
                        String currentRole = userDetails.getRoleName();
                        // Gọi hàm initializeCachedIds và truyền userId
                        sharedListIdService.initializeCachedIds(userId.toString(), currentRole);
                    }
                    return authentication;
                } else {
                    throw new BadCredentialsException("Invalid username/password");
                }
            } catch (Exception ex) {
                String errorMessage = ex.getMessage();
                System.out.println(errorMessage);
                throw new BadCredentialsException("Invalid username/password");
            }
        }
    }

    private LoginRequest getLoginRequest(HttpServletRequest request) {
        LoginRequest loginRequest = new LoginRequest();
        BufferedReader reader = null;
        try {
            reader = request.getReader();
            ObjectMapper objectMapper = new ObjectMapper();
            loginRequest = objectMapper.readValue(reader, LoginRequest.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        }
        return loginRequest;
    }

    protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
        authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
    }

}
