package vn.automationandtesting.atproject.config.user_auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.repository.PermissionRepository;
import vn.automationandtesting.atproject.repository.UserRepository;

import java.util.*;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found."));
        if (user.getIsDeleted().booleanValue()) {
            throw new UsernameNotFoundException("User not found.");
        }

        Collection<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole().getRoleName().name());
        String[] groupArray = user.getGroups().split(",");
        List<String> groups = Arrays.asList(groupArray);
        Set<String> permissions = getPermissions(groups);
        return new MyUserDetails(user.getUsername(), user.getPassword(), authorities, user.getId(), user.getRole().getRoleName().name(), groups, permissions);
    }

    private Set<String> getPermissions(List<String> groupNames) {
        Set<String> permissions = new HashSet<>();
        groupNames.forEach(s -> {
            Set<String> permissionNames = permissionRepository.findPermissionNamesByGroupName(s);
            permissions.addAll(permissionNames);
        });

        return permissions;
    }
}
