package vn.automationandtesting.atproject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import vn.automationandtesting.atproject.config.token.JWTTokenFilter;
import vn.automationandtesting.atproject.config.user_auth.MyOwnerUserDetailsService;
import vn.automationandtesting.atproject.config.user_auth.MyOwnerUsernamePasswordAuthenticationConfig;
import vn.automationandtesting.atproject.config.user_auth.MyUserDetailsService;
import vn.automationandtesting.atproject.config.user_auth.MyUsernamePasswordAuthenticationConfig;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyUsernamePasswordAuthenticationConfig myUsernamePasswordAuthenticationConfig;

    @Autowired
    private MyOwnerUsernamePasswordAuthenticationConfig myOwnerUsernamePasswordAuthenticationConfig;
    @Autowired
    private JWTTokenFilter jwtTokenFilter;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private MyOwnerUserDetailsService myOwnerUserDetailsService;

    @Value("${api.prefix}")
    private String apiPrefix;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService).passwordEncoder(passwordEncoder());
        auth.userDetailsService(myOwnerUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
//                .and()
//                .formLogin(form -> form.disable())
                .csrf().disable()
                .authorizeRequests(authorize -> authorize
                        .antMatchers(apiPrefix + "/users/{userId}/update-group").hasAnyAuthority("ADMIN", "OWNER1", "OWNER2", "OWNER3", "OWNER4", "OWNER4")
                        .antMatchers(apiPrefix + "/users/{userId}").access("@webAuthorization.checkPermissionAPIWithUserID(request, #userId)")
                        .antMatchers(HttpMethod.GET, apiPrefix + "/users").hasAnyAuthority("ADMIN", "OWNER1", "OWNER2", "OWNER3", "OWNER4", "OWNER5")
                        .antMatchers(apiPrefix + "/assets/**").access("@webAuthorization.checkAssetPermission(request)")
                        .antMatchers(apiPrefix + "/assetCommon/**").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/circuit/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/current/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/disconnector/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/power/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/surge/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")
                        .regexMatchers(apiPrefix + "/voltage/(?!job/|test/).*").access("@webAuthorization.checkAssetPermission(request)")

                        .antMatchers(apiPrefix + "/jobs/**").access("@webAuthorization.checkJobPermission(request)")
                        .antMatchers(apiPrefix + "/jobsByAssetId/**").access("@webAuthorization.checkJobPermission(request)")
                        .antMatchers(apiPrefix + "/jobsByName/**").access("@webAuthorization.checkJobPermission(request)")
                        .antMatchers(apiPrefix + "/jobsByAssetSerialNo/**").access("@webAuthorization.checkJobPermission(request)")
                        .antMatchers(apiPrefix + "/locations/**").access("@webAuthorization.checkLocationPermission(request)")
                        .antMatchers(apiPrefix + "/groups/**").hasAnyAuthority("ADMIN", "OWNER1", "OWNER2", "OWNER3", "OWNER4", "OWNER5")
//                        .antMatchers("/", "/login", "/js/**", "/css/**", "/images/**", "/webjars/**").permitAll() // Cho phép tất cả mọi người truy cập vào 2 địa chỉ này
                        .antMatchers("/admin/*").hasAuthority("ADMIN")
                        .anyRequest().authenticated()
                )
                .apply(myUsernamePasswordAuthenticationConfig)
                .and()
                .apply(myOwnerUsernamePasswordAuthenticationConfig)
                .and()
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .formLogin() // Cho phép người dùng xác thực bằng form login
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403");
        ;
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}
