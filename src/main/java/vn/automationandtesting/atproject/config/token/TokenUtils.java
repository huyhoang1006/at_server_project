package vn.automationandtesting.atproject.config.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.user_auth.MyOwnerUserDetails;
import vn.automationandtesting.atproject.config.user_auth.MyUserDetails;
import vn.automationandtesting.atproject.constant.ConstantMessages;
import vn.automationandtesting.atproject.constant.Constants;
import vn.automationandtesting.atproject.controller.dto.JwtResponse;
import vn.automationandtesting.atproject.exception.InvalidRequestException;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class TokenUtils {
    @Autowired
    private JWTProperties jwtProperties;

    public Claims getClaimsFromJwtToken(String jwtToken) {
        return Jwts.parserBuilder()
                .setSigningKey(jwtProperties.getSecretKey().getBytes())
                .build()
                .parseClaimsJws(jwtToken)
                .getBody();
    }

    public String generateJwtToken(Claims claims) {
        Key signingKey = new SecretKeySpec(jwtProperties.getSecretKey().getBytes(), SignatureAlgorithm.HS256.getJcaName());

        return Jwts.builder()
                .addClaims(claims)
                .signWith(signingKey)
                .compact();
    }

    public JwtResponse createJwtResponse(MyUserDetails userDetails) {
        Map<String, Object> additionalKeyValue = new HashMap<>();
        additionalKeyValue.put(Constants.USER_ID_CLAIMS_NAME, userDetails.getUserID());
        additionalKeyValue.put(Constants.ROLE_CLAIMS_NAME, userDetails.getRoleName());
        additionalKeyValue.put(Constants.GROUPS_CLAIMS_NAME, userDetails.getGroups());
        additionalKeyValue.put(Constants.PERMISSIONS_CLAIMS_NAME, userDetails.getPermissions());

        long currentMillisecond = System.currentTimeMillis();
        Date accessTokenExpiration = new Date(currentMillisecond + jwtProperties.getAccessTokenValidity() * 1000);
        Date refreshTokenExpiration = new Date(currentMillisecond + jwtProperties.getRefreshTokenValidity() * 1000);

        Claims accessTokenClaims = Jwts.claims();
        accessTokenClaims
                .setId(UUID.randomUUID().toString())
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(currentMillisecond))
                .setExpiration(accessTokenExpiration)
                .putAll(additionalKeyValue)
        ;
        String accessToken = generateJwtToken(accessTokenClaims);

        //Setup value for refresh token
        Claims refreshTokenClaims = Jwts.claims();
        refreshTokenClaims
                .setId(UUID.randomUUID().toString())
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(currentMillisecond))
                .setExpiration(refreshTokenExpiration)
                .put(Constants.ACCESS_TOKEN_ID_CLAIMS_NAME, accessTokenClaims.getId());
        String refreshToken = generateJwtToken(refreshTokenClaims);

        JwtResponse jwtResponse = new JwtResponse(userDetails.getUserID().toString(), userDetails.getRoleName(),
                "Bearer", accessToken, refreshToken, accessTokenClaims.getExpiration());
        return jwtResponse;
    }

    public JwtResponse createJwtResponse(MyOwnerUserDetails userDetails) {
        Map<String, Object> additionalKeyValue = new HashMap<>();
        additionalKeyValue.put(Constants.USER_ID_CLAIMS_NAME, userDetails.getUserID());
        additionalKeyValue.put(Constants.ROLE_CLAIMS_NAME, userDetails.getRoleName());
        additionalKeyValue.put(Constants.GROUPS_CLAIMS_NAME, userDetails.getGroups());
        additionalKeyValue.put(Constants.PERMISSIONS_CLAIMS_NAME, userDetails.getPermissions());

        long currentMillisecond = System.currentTimeMillis();
        Date accessTokenExpiration = new Date(currentMillisecond + jwtProperties.getAccessTokenValidity() * 1000);
        Date refreshTokenExpiration = new Date(currentMillisecond + jwtProperties.getRefreshTokenValidity() * 1000);

        Claims accessTokenClaims = Jwts.claims();
        accessTokenClaims
                .setId(UUID.randomUUID().toString())
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(currentMillisecond))
                .setExpiration(accessTokenExpiration)
                .putAll(additionalKeyValue)
        ;
        String accessToken = generateJwtToken(accessTokenClaims);

        //Setup value for refresh token
        Claims refreshTokenClaims = Jwts.claims();
        refreshTokenClaims
                .setId(UUID.randomUUID().toString())
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(currentMillisecond))
                .setExpiration(refreshTokenExpiration)
                .put(Constants.ACCESS_TOKEN_ID_CLAIMS_NAME, accessTokenClaims.getId());
        String refreshToken = generateJwtToken(refreshTokenClaims);

        JwtResponse jwtResponse = new JwtResponse(userDetails.getUserID().toString(), userDetails.getRoleName(),
                "Bearer", accessToken, refreshToken, accessTokenClaims.getExpiration());
        return jwtResponse;
    }

    public Claims checkValidAccessToken(String accessToken) {
        Claims claims = null;
        try {
            claims = getClaimsFromJwtToken(accessToken);
        } catch (JwtException e) {
            throw new InvalidRequestException(LocalDateTime.now(), ConstantMessages.INVALID_TOKEN,
                    e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return claims;
    }

}
