package vn.automationandtesting.atproject.config.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.constant.Constants;
import vn.automationandtesting.atproject.controller.dto.response.ErrorResponse;
import vn.automationandtesting.atproject.exception.InvalidRequestException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Component
public class JWTTokenFilter extends OncePerRequestFilter {
    @Autowired
    private TokenUtils tokenUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwtToken = getJwtToken(httpServletRequest);
            if (jwtToken != null) {
                //Set auth information to Bearer
                saveToSecurityContext(jwtToken);
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (InvalidRequestException e) {
            ErrorResponse errorResponse = new ErrorResponse(e.getTimestamp().toString(), e.getHttpStatus().value(), e.getError()
                    , e.getError_description(), httpServletRequest.getServletPath());
            httpServletResponse.setStatus(e.getHttpStatus().value());
            httpServletResponse.setContentType("application/json");
            httpServletResponse.getWriter().write(new ObjectMapper().writeValueAsString(errorResponse));
        } finally {
            BearerContextHolder.clearContext();
        }
    }

    private String getJwtToken(HttpServletRequest request) {
        if (request.getHeader(Constants.AUTHORIZATION_HEADER) != null
                && request.getHeader(Constants.AUTHORIZATION_HEADER).startsWith(Constants.TOKEN_PREFIX)) {
            return request.getHeader(Constants.AUTHORIZATION_HEADER).replace(Constants.TOKEN_PREFIX, "");
        }
        return null;
    }

    private void saveToSecurityContext(String jwtToken) {
        //check if the token match with the one on database or not.
        Claims claims = tokenUtils.checkValidAccessToken(jwtToken);

        String userId = (String) claims.get(Constants.USER_ID_CLAIMS_NAME);
        String userName = claims.getSubject();
        String role = (String) claims.get(Constants.ROLE_CLAIMS_NAME);
        List<String> groups = (List<String>) claims.get(Constants.GROUPS_CLAIMS_NAME);
        List<String> permissions = (List<String>) claims.get(Constants.PERMISSIONS_CLAIMS_NAME);

        BearerContext bearerContext = BearerContextHolder.getContext();
        bearerContext.setBearerToken(jwtToken);
        bearerContext.setUserId(userId);
        bearerContext.setUserName(userName);
        bearerContext.setRoleName(role);
        bearerContext.setGroups(groups);
        bearerContext.setPermissions(permissions);

        Collection<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(role);
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userName, null, authorities);
        SecurityContextHolder.getContext().setAuthentication(authToken);
    }
}
