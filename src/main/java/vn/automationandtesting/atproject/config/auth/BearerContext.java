package vn.automationandtesting.atproject.config.auth;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BearerContext implements Serializable {
    private String roleName;
    private List<String> groups = new ArrayList<>();
    private List<String> permissions = new ArrayList<>();
    private String userId;
    private String userName;
    private String bearerToken;
}