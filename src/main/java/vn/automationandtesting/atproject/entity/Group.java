package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Group extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    private String description;

    @Column(name = "group_name", unique = true)
    private String groupName;

    @OneToMany(mappedBy = "group", cascade = CascadeType.REMOVE)
    private Set<GroupPermission> groupPermissions;
}
