package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Fmeca extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    @Column(name = "table_fmeca")
    private String tableFmeca;
    @Column(name = "table_calculate")
    private String tableCalculate;
    private String total;

    public static Fmeca initFmeca() {
        final String INIT_TABLE_FMECA = "[]";
        final String INIT_TABLE_CALCULATE = "{\"dmt\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"otmt\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"ts\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"ir\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"rt\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"dcwr\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"w_pf_df\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"b_pf_df\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"wc\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"b_C1_c\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"scilr\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"frsl\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"sfra\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"mc\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"ec\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"OLTC_s\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"DGA_LTC\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"o_t_LTC\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"DGA_b\":{\"total_rpn\":\"\",\"rpn_proportion\":\"\",\"weighting_factor\":\"\"},\"data_calc\":[],\"display\":{\"dga\":\"none\",\"oil_main_tank\":\"none\",\"thermal\":\"none\",\"insulresistance\":\"none\",\"ratio_test\":\"none\",\"dc_winding_res\":\"none\",\"winding_pf_df\":\"none\",\"bushing_pf_df\":\"none\",\"winding_capa\":\"none\",\"bushing_c1_capa\":\"none\",\"Short_circuit_Leakage\":\"none\",\"FRSL\":\"none\",\"SFRA\":\"none\",\"moisture_content\":\"none\",\"excitation_current\":\"none\",\"OLTC_scan\":\"none\",\"DGA_LTC\":\"none\",\"Oil_test_LTC\":\"none\",\"DGA_Bushing\":\"none\"}}";
        final String INIT_TOTAL = "{\"total_rpn\":0,\"rpn_proportion\":0,\"weighting_factor\":0}";
        return new Fmeca(UUID.randomUUID(), INIT_TABLE_FMECA, INIT_TABLE_CALCULATE, INIT_TOTAL);
    }
}
