package vn.automationandtesting.atproject.entity.owner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "owner")
public class Owner extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID mrid;
    private String name;
    private UUID ref_id;
    private String phone_no;
    private String full_name;
    private String tax_code;
    private String address;
    private String city;
    private String country;
    private String pre;
    private String phone2;
    private String name_person;
    private String phone1;
    private String mode;
    private String fax_contact;
    private UUID user_id;
    private String comment;
    private String state;
    private String position;
    private String fax;
    private String email_contact;
    private String department;
    private String email;
}
