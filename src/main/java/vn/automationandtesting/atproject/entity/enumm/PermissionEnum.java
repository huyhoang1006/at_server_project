package vn.automationandtesting.atproject.entity.enumm;

public enum PermissionEnum {
    CREATE_LOCATION,
    VIEW_LOCATION,
    UPDATE_LOCATION,
    DELETE_LOCATION,
    CREATE_ASSET,
    VIEW_ASSET,
    UPDATE_ASSET,
    DELETE_ASSET,
    CREATE_JOB,
    VIEW_JOB,
    UPDATE_JOB,
    DELETE_JOB
}
