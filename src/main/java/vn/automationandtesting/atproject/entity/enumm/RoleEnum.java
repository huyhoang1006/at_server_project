package vn.automationandtesting.atproject.entity.enumm;

import vn.automationandtesting.atproject.entity.Role;

public enum RoleEnum {
    ADMIN,
    USER,
    OWNER1,
    OWNER2,
    OWNER3,
    OWNER4,
    OWNER5;

    public static int getIntOfRole(String data) {
        switch (data) {
            case "ADMIN":
                return 1;
            case "OWNER1":
                return 2;
            case "OWNER2":
                return 3;
            case "OWNER3":
                return 4;
            case "OWNER4":
                return 5;
            case "OWNER5":
                return 6;
            case "USER":
                return 7;
        }
        return 0;
    }

    public static String getRoleOfString(int data) {
        switch (data) {
            case 1:
                return "ADMIN";
            case 2:
                return "OWNER1";
            case 3:
                return "OWNER2";
            case 4:
                return "OWNER3";
            case 5:
                return "OWNER4";
            case 6:
                return "OWNER5";
            case 7:
                return "USER";
        }
        return "";
    }
}
