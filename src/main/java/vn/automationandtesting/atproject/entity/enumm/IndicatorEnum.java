package vn.automationandtesting.atproject.entity.enumm;

/**
 * @author tridv on 17/9/2022
 * @project at-project-server
 */
public enum IndicatorEnum {
    Good,
    Fair,
    Poor,
    Bad,
    Null,
}
