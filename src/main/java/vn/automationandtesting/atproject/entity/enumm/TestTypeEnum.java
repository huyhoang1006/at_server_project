package vn.automationandtesting.atproject.entity.enumm;

/**
 * @author tridv on 1/9/2022
 * @project at-project-server
 */
public enum TestTypeEnum {
    BUSHINGPRIMC2,
    MEASUREMENTOFOIL,
    BUSHINGPRIMC1,
    DCWINDINGPRIM,
    DCWINDINGSEC,
    DGA,
    EXCITINGCURRENT,
    INSULATIONRESISTANCE,
    RATIOPRIMSEC,
    WINDINGDFCAP
}
