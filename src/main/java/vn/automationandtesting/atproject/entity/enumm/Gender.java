package vn.automationandtesting.atproject.entity.enumm;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
