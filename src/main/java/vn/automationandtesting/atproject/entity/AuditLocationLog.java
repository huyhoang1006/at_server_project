package vn.automationandtesting.atproject.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "audit_location_log")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuditLocationLog extends BaseEntity {
    @Id
    private UUID id;
    private UUID location_staging_id;
    private String content;
    private String old_value;
    private String new_value;
    private UUID sender_id;
    private String sender_name;
    private String version;
    private String status;
}
