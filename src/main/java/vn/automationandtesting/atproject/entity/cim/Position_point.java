package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "position_point")
public class Position_point extends BaseEntity {
    private String alias_name;
    private String description;
    @Id
    @Column(unique = true, nullable = false)
    private UUID mrid;
    private String name;
    private int sequence_number;
    private String x_position;
    private String y_position;
    private String z_position;

    @ManyToOne
    @JoinColumn(name = "location")
    private Location location ;
}
