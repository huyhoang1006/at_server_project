package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;
import java.util.UUID;

/**
 * @author tridv on 1/9/2022
 * @project at-project-server
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Test extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    private String data;
    private String name;
    @Column(name = "average_score")
    private float averageScore;
    @Column(name = "worst_score")
    private float worstScore;
    @Column(name = "worst_score_df")
    private float worstScoreDF;
    @Column(name = "worst_score_c")
    private float worstScoreC;
    @Column(name = "average_score_df")
    private float averageScoreDF;
    @Column(name = "average_score_c")
    private float averageScoreC;
    @Column(name = "create_at_client")
    private BigInteger createAtClient;
    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private TestType testType;
    @Column(name = "weighting_factor")
    private float weightingFactor;
    @Column(name = "weighting_factor_df")
    private float weightingFactorDF;
    @Column(name = "weighting_factor_c")
    private float weightingFactorC;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Test)) return false;
        if (!super.equals(o)) return false;
        Test test = (Test) o;
        return getTestType().getCode().equals(test.getTestType().getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTestType());
    }
}
