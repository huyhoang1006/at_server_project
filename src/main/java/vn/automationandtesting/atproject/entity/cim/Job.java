package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Job extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    @ManyToOne
    @JoinColumn(name = "asset_id")
    private Asset asset;
    private String name;
    private String work_order;
    private String creation_date;
    private String execution_date;
    private String tested_by;
    private String approved_by;
    private String approval_date;
    private String summary;
    private String ambient_condition;
    private String testing_method;
    private Integer standard;
    private boolean locked = false;
    private UUID locked_by;
    private String collabs = "";
    @Column(name = "average_health_index")
    private float averageHealthIndex;
    @Column(name = "worst_health_index")
    private float worstHealthIndex;

    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
    private List<Test> tests = new ArrayList<>();
}
