package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * @author tridv on 1/9/2022
 * @project at-project-server
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class TestType extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    // @Enumerated(EnumType.STRING)
    private String code;
    private String name;
}
