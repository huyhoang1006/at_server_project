package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "manufacturer_custom", schema = "at_project_schema")
public class ManufacturerCustom extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    private String name;
    private String type;
}
