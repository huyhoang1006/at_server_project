package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "attachment")
public class Attachment extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    @Column(name = "id_foreign")
    private UUID id_foreign;
    @Column(name = "type")
    private String type;
    @Column(name = "name")
    private String name;
    @Column(name = "path")
    private String path;
}
