package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class GroupPermissionPK implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "group_id")
    private UUID groupId;

    @Column(name = "permission_id")
    private UUID permissionId;
}
