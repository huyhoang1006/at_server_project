package vn.automationandtesting.atproject.entity.message;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Message extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    @Column(name = "group_id")
    private UUID groupId;

    @Column(name = "content")
    private String content;

    @Column(name = "sender_id")
    private UUID senderId;

    @Column(name = "sender_name")
    private String senderName;

    @Column(name = "status")
    private String status;

    @Column(name = "type")
    private String type;

    @Column(name = "receiver_name")
    private String receiverName;

    @Column(name = "receiver_member")
    private String receiverMember;
}
