package vn.automationandtesting.atproject.entity.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "group_message")
public class GroupList extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "last_message_at")
    private Timestamp lastMessageAt;

    @Column(name = "last_message_created_by")
    private UUID lastMessageCreatedBy;

    @Column(name = "last_message_receiver_member")
    private String lastMessageReceiveMember;

    @Column(name = "last_message")
    private String lastMessage;

    @Column(name = "member")
    private String member;

    @Column(name = "type")
    private String type;
}
