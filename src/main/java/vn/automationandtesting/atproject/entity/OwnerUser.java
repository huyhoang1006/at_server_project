package vn.automationandtesting.atproject.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.cim.Location;
import vn.automationandtesting.atproject.entity.enumm.Gender;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "owner_user", schema = "at_project_schema")
public class OwnerUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    private String username;
    private String password;
    private boolean enabled = true;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String email;
    private String phone;
    private LocalDate birthDate;

    @ManyToOne
    private Role role;
    //String of all groups: group1,group2,group3,...
    private String groups;

    @OneToMany(mappedBy = "ownerUser")
    private List<Location> locations = new ArrayList<>();

    public OwnerUser(String username, String password, boolean enabled, String firstName, String lastName, Gender gender, String email, String phone, LocalDate birthDate, Role role, String groups) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.birthDate = birthDate;
        this.role = role;
        this.groups = groups;
    }
}
