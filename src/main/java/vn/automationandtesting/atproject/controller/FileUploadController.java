package vn.automationandtesting.atproject.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;

@Controller
@RequestMapping("${api.prefix}")
public class FileUploadController {
    String Working_Directory = System.getProperty("user.dir");
    String pathData = Working_Directory + System.getProperty("file.separator") + "FileUpload";
    @PostMapping("/file/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            byte[] bytes;
            bytes = file.getBytes();
            Files.write(Paths.get(pathData + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
            ResponseObject.returnWithData(true);
            return new ResponseEntity<>(ResponseObject.returnWithData(true), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + file.getOriginalFilename() + "!");
        }
    }

    @PostMapping("/file/upload/{name}/{uuid}")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String name, @PathVariable String uuid) {
        try {
            byte[] bytes;
            bytes = file.getBytes();
            Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
            if(!Files.exists(path) || !Files.isDirectory(path)) {
                File theDir = new File(String.valueOf(path));
                theDir.mkdirs();
            }
            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
            ResponseObject.returnWithData(true);
            return new ResponseEntity<>(ResponseObject.returnWithData(true), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + file.getOriginalFilename() + "!");
        }
    }

    @GetMapping("/file/download/{fileName}")
    public ResponseEntity<?> download(@PathVariable String fileName) {
        try {
            String path = pathData + System.getProperty("file.separator") + fileName;
            File file = new File(path);
            FileInputStream dataFile = new FileInputStream(file);
            byte fileData[] = new byte[(int) file.length()];
            dataFile.read(fileData);
            String base64File = Base64.getEncoder().encodeToString(fileData);
            dataFile.close();
            return new ResponseEntity<>(ResponseObject.returnWithData(base64File), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + fileName + "!");
        }
    }

    @GetMapping("/file/download/{fileName}/{name}/{uuid}")
    public ResponseEntity<?> download(@PathVariable String fileName, @PathVariable String name, @PathVariable String uuid) {
        try {
            String path = pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + fileName;
            File file = new File(path);
            FileInputStream dataFile = new FileInputStream(file);
            byte fileData[] = new byte[(int) file.length()];
            dataFile.read(fileData);
            String base64File = Base64.getEncoder().encodeToString(fileData);
            dataFile.close();
            return new ResponseEntity<>(ResponseObject.returnWithData(base64File), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + fileName + "!");
        }
    }

    @PostMapping("/file/update/{name}/{uuid}")
    public ResponseEntity<?> updateFile(@RequestParam(value = "file", required = false) MultipartFile[] files, @RequestParam(value = "deleteFile", required = false) List<String> deleteFile,
                                        @PathVariable String name, @PathVariable String uuid) {
        int indexDelete = 0;
        try {
            if(deleteFile != null) {
                for (String nameFile : deleteFile) {
                    Files.deleteIfExists(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + nameFile));
                    indexDelete = indexDelete + 1;
                }
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                .body("Exception occurred for: " + deleteFile.get(index) + "!");
                    }
                }
            } else {
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                .body("Exception occurred for: " + deleteFile.get(index) + "!");
                    }
                }
            }
            return new ResponseEntity<>(ResponseObject.returnWithData(true), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + files[indexDelete].getOriginalFilename() + "!");
        }
    }

    @PostMapping("/file/updateTemp/{name}/{uuid}")
    public ResponseEntity<?> updateTemp(@RequestParam(value = "file", required = false) MultipartFile[] files, @RequestParam(value = "deleteFile", required = false) List<String> deleteFile,
                                        @PathVariable String name, @PathVariable String uuid) {
        int indexDelete = 0;
        try {
            if(deleteFile != null) {
                for (String nameFile : deleteFile) {
                    Files.deleteIfExists(Paths.get(pathData + FileSystems.getDefault().getSeparator() + uuid + FileSystems.getDefault().getSeparator() + "temp" + FileSystems.getDefault().getSeparator() + nameFile));
                    indexDelete = indexDelete + 1;
                }
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                .body("Exception occurred for: " + deleteFile.get(index) + "!");
                    }
                }
            } else {
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                                .body("Exception occurred for: " + deleteFile.get(index) + "!");
                    }
                }
            }
            return new ResponseEntity<>(ResponseObject.returnWithData(true), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + files[indexDelete].getOriginalFilename() + "!");
        }
    }
}
