package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.service.TapChangerService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class TapChangerController {
    private final TapChangerService tapChangerService;

    @Autowired
    public TapChangerController(TapChangerService tapChangerService) {
        this.tapChangerService = tapChangerService;
    }

    @GetMapping("/tapChangers/{tapChangerId}")
    public ResponseEntity<?> checkTapChangerExisted(@PathVariable String tapChangerId) {
        UUID id = UUID.fromString(tapChangerId);
        boolean tapChangerExisted = tapChangerService.checkTapChangerExisted(id);
        ResponseObject responseObject = new ResponseObject(true, "Update tapChanger", tapChangerExisted);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/tapChangersByAssetId/{assetId}")
    public ResponseEntity<?> findTapChangersByAssetSerialNo(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        List<TapChangerDto> tapChangerDtoList = tapChangerService.findTapChangersByAssetId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get tapChangers by asset id", tapChangerDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/tapChangersByLocationId/{locationId}")
    public ResponseEntity<?> findTapChangersByName(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        List<TapChangerDto> tapChangerDtoList = tapChangerService.findTapChangersByLocationId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get tapChangers by location id", tapChangerDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/tapChangers")
    public ResponseEntity<?> createTapChanger(@RequestBody TapChangerDto tapChangerDto) {
        TapChangerDto savedTapChangerDto = tapChangerService.createNewTapChanger(tapChangerDto);
        ResponseObject responseObject = new ResponseObject(true, "Create tapChanger", savedTapChangerDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/tapChangers/{tapChangerId}")
    public ResponseEntity<?> updateTapChanger(@RequestBody TapChangerDto tapChangerDto, @PathVariable String tapChangerId) {
        UUID id = UUID.fromString(tapChangerId);
        TapChangerDto updatedTapChangerDto = tapChangerService.updateTapChanger(tapChangerDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update tapChanger", updatedTapChangerDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/tapChangers/{tapChangerId}")
    public ResponseEntity<?> deleteTapChanger(@PathVariable String tapChangerId) {
        UUID id = UUID.fromString(tapChangerId);
        tapChangerService.deleteTapChanger(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
