package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.ManufacturerCustomDto;
import vn.automationandtesting.atproject.service.ManufacturerCustomService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class ManufacturerCustomController {
    @Autowired
    private ManufacturerCustomService manufacturerCustomService;

    @PostMapping("/manu/insert")
    public ResponseEntity<?> createManu(@RequestBody ManufacturerCustomDto manufacturerCustomDto) {
        if(manufacturerCustomDto.getId() == null || manufacturerCustomDto.getId().toString().isEmpty()) {
            manufacturerCustomDto.setId(UUID.randomUUID());
        }
        ManufacturerCustomDto savedManu = manufacturerCustomService.insertManu(manufacturerCustomDto);
        ResponseObject responseObject = new ResponseObject(true, "Created manu", savedManu);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/manu/getManuByName/{name}")
    public ResponseEntity<?> getManuByName(@PathVariable String name) {
        List<ManufacturerCustomDto> manufacturerCustomDtoList = manufacturerCustomService.getManuByName(name);
        ResponseObject responseObject = new ResponseObject(true, "Get manu by name", manufacturerCustomDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/manu/getManuByType/{type}")
    public ResponseEntity<?> getManuByType(@PathVariable String type) {
        List<ManufacturerCustomDto> manufacturerCustomDtoList = manufacturerCustomService.getManuByType(type);
        ResponseObject responseObject = new ResponseObject(true, "get manu by type", manufacturerCustomDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/manu/findById/{id}")
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        ManufacturerCustomDto manufacturerCustomDtoList = manufacturerCustomService.findById(id);
        ResponseObject responseObject = new ResponseObject(true, "get manu by id", manufacturerCustomDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/manu/updateManuById/{id}")
    public ResponseEntity<?> updateManuById(@PathVariable UUID id, @RequestBody ManufacturerCustomDto manufacturerCustomDto) {
        ManufacturerCustomDto manufacturerCustomDtoList = manufacturerCustomService.updateManuById(id, manufacturerCustomDto);
        ResponseObject responseObject = new ResponseObject(true, "update manu by id", manufacturerCustomDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/manu/deleteManu")
    public ResponseEntity<?> deleteManu(@RequestBody List<UUID> ids) {
        List<UUID> uuidList = manufacturerCustomService.deleteManu(ids);
        ResponseObject responseObject = new ResponseObject(true, "delete manu by id", uuidList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
