package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.service.BushingService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class BushingController {
    private final BushingService bushingService;

    @Autowired
    public BushingController(BushingService bushingService) {
        this.bushingService = bushingService;
    }

    @GetMapping("/bushingsByLocalId/{localId}")
    public ResponseEntity<?> getAllBushingsByLocalId(@PathVariable String localId) {
        UUID id = UUID.fromString(localId);
        List<BushingDto> bushingDtoList = bushingService.getAllBushingsByLocalid(id);
        ResponseObject responseObject = new ResponseObject(true, "Get all bushings by localID", bushingDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/bushingsByAssetId/{assetId}")
    public ResponseEntity<?> getAllBushingsByAssetId(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        List<BushingDto> bushingDtoList = bushingService.getAllBushingsByAssetid(id);
        ResponseObject responseObject = new ResponseObject(true, "Get all bushings by assetID", bushingDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/bushings")
    public ResponseEntity<?> createBushing(@RequestBody BushingDto bushingDto) {
        BushingDto savedBushingDto = bushingService.createNewBushing(bushingDto);
        ResponseObject responseObject = new ResponseObject(true, "Create bushing", savedBushingDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/bushings/{bushingId}")
    public ResponseEntity<?> updateBushing(@RequestBody BushingDto bushingDto, @PathVariable String bushingId) {
        UUID id = UUID.fromString(bushingId);
        BushingDto updatedBushingDto = bushingService.updateBushing(bushingDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update bushing", updatedBushingDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/bushings/{bushingId}")
    public ResponseEntity<?> deleteBushing(@PathVariable String bushingId) {
        UUID id = UUID.fromString(bushingId);
        bushingService.deleteBushing(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
