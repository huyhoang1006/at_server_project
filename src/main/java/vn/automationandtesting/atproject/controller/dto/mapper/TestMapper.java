package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;
import vn.automationandtesting.atproject.entity.cim.Test;
import vn.automationandtesting.atproject.entity.cim.TestType;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
@Component
public class TestMapper {
    public TestDto testToTestDto(Test test) {
        TestDto testDto = new TestDto();
        BeanUtils.copyProperties(test, testDto);
        testDto.setTestTypeCode(String.valueOf(test.getTestType().getCode()));
        testDto.setJob_id(test.getJob().getId());
        testDto.setTestTypeId(test.getTestType().getId());
        testDto.setTestTypeCode(test.getTestType().getCode());
        testDto.setTestTypeName(test.getTestType().getName());
        testDto.setAverage_score(test.getAverageScore());
        testDto.setWorst_score(test.getWorstScore());
        testDto.setAverage_score_c(test.getAverageScoreC());
        testDto.setAverage_score_df(test.getAverageScoreDF());
        testDto.setWorst_score_df(test.getWorstScoreDF());
        testDto.setWorst_score_c(test.getWorstScoreC());
        testDto.setCreate_at_client(test.getCreateAtClient());
        testDto.setWeighting_factor(test.getWeightingFactor());
        testDto.setWeightingFactorDF(test.getWeightingFactorDF());
        testDto.setWeightingFactorC(test.getWeightingFactorC());
        return testDto;
    }

    public Test testDtoToTest(TestDto testDto) {
        Test test = new Test();
        BeanUtils.copyProperties(testDto, test);
        test.setTestType(new TestType(testDto.getTestTypeId(), testDto.getTestTypeCode(), testDto.getTestTypeName()));
        test.setAverageScore(testDto.getAverage_score());
        test.setWorstScore(testDto.getWorst_score());
        test.setCreateAtClient(testDto.getCreate_at_client());
        test.setWeightingFactor(testDto.getWeighting_factor());
        test.setWeightingFactorDF(testDto.getWeightingFactorDF());
        test.setWeightingFactorC(testDto.getWeightingFactorC());
        return test;
    }

    public Test copyTestDtoToTest(TestDto testDto, Test test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
