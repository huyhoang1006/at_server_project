package vn.automationandtesting.atproject.controller.dto.mapper.voltage;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.voltage.JobVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.JobsVoltage;

@Component
public class JobVoltageMapper {
    public JobVoltageDto JobToJobDto(JobsVoltage job) {
        JobVoltageDto jobDto = new JobVoltageDto();
        BeanUtils.copyProperties(job, jobDto);
        return jobDto;
    }

    public JobsVoltage JobDtoToJob(JobVoltageDto jobDto) {
        JobsVoltage job = new JobsVoltage();
        BeanUtils.copyProperties(jobDto, job, "collabs");
        return job;
    }

    public JobsVoltage copyJobDtoToJob(JobVoltageDto jobDto, JobsVoltage job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
        job.setCollabs(String.join(",", job.getCollabs()));
        return job;
    }
}
