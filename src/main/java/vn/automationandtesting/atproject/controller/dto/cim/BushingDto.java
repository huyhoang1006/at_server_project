package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BushingDto {
    private UUID id;
    private UUID asset_id;
    private String asset_type;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturer_year;
    private String insull_level;
    private String voltage_gr;
    private String max_sys_voltage;
    private String rate_current;
    private String df_c1;
    private String cap_c1;
    private String df_c2;
    private String cap_c2;
    private String insulation_type;
}
