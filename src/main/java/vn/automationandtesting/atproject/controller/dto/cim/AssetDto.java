package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AssetDto {
    private UUID id; //
    private UUID location_id; //
    private String asset;
    private String asset_type; //
    private String serial_no; //
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturing_year;
    private String asset_system_code;
    private String apparatus_id;
    private String feeder;
    private String date_of_warehouse_receipt;
    private String date_of_delivery;
    private String date_of_production_order;
    private String comment;
    private String phases;
    private String vector_group;
    private String vector_group_custom;
    private String unsupported_vector_group;
    private String rated_frequency;
    private String rated_frequency_custom;
    private String voltage_ratings;
    private String voltage_regulation;
    private String power_ratings;
    private String current_ratings;
    private String max_short_circuit_current_ka;
    private String max_short_circuit_current_s;
    private String ref_temp;
    private String prim_sec;
    private String prim_tert;
    private String sec_tert;
    private String base_power;
    private String base_voltage;
    private String zero_percent;
    private String category;
    private String status; //
    private String tank_type;
    private String insulation_medium;
    private String insulation_weight;
    private String insulation_volume;
    private String insulation_key;
    private String total_weight;
    private String winding;
    private String date_of_warehouse_delivery;
    private String progress;
    private String standard;
    private String oil_type;
    private String thermal_meter;
    private boolean locked = false;
    private String extend;
    private Set<String> collabs = new HashSet<>();
}
