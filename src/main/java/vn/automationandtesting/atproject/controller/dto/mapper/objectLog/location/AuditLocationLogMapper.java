package vn.automationandtesting.atproject.controller.dto.mapper.objectLog.location;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.objectLog.location.AuditLocationLogDto;
import vn.automationandtesting.atproject.entity.AuditLocationLog;

@Component
public class AuditLocationLogMapper {
    public AuditLocationLogDto objectToObjectDto(AuditLocationLog auditLocationLog, AuditLocationLogDto auditLocationLogDto, String... ignoreProperties) {
        BeanUtils.copyProperties(auditLocationLog, auditLocationLogDto, ignoreProperties);
        return auditLocationLogDto;
    }

    public AuditLocationLog objectDtoToObject(AuditLocationLogDto auditLocationLogDto, AuditLocationLog auditLocationLog, String... ignoreProperties) {
        BeanUtils.copyProperties(auditLocationLogDto, auditLocationLog, ignoreProperties);
        return auditLocationLog;
    }
}
