package vn.automationandtesting.atproject.controller.dto.mapper.voltage;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.Arrays;
import java.util.List;


@Component
public class VoltageMapper {
    public VoltageDto assetToAssetDto(Voltage asset) {
        VoltageDto assetDto = new VoltageDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public Voltage assetDtoToAsset(VoltageDto assetDto) {
        Voltage asset = new Voltage();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public Voltage copyAssetDtoToAsset(VoltageDto assetDto, Voltage asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
    public Voltage copyAssetDtoToAssetLite(VoltageDto assetDto, Voltage asset, List<String> acceptProperties, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtilsCustom.copyProperties(assetDto, asset, acceptProperties, ignoreProperties);
        return asset;
    }
}
