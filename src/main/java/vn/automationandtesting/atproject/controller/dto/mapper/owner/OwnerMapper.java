package vn.automationandtesting.atproject.controller.dto.mapper.owner;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.entity.owner.Owner;

@Component
public class OwnerMapper {
    public OwnerDto assetToAssetDto(Owner asset, String... ignoreProperties) {
        OwnerDto assetDto = new OwnerDto();
        BeanUtils.copyProperties(asset, assetDto, ignoreProperties);
        assetDto.setId(asset.getMrid());
        assetDto.setCreated_by(asset.getCreatedBy());
        return assetDto;
    }

    public Owner assetDtoToAsset(OwnerDto assetDto, String... ignoreProperties) {
        Owner asset = new Owner();
        BeanUtils.copyProperties(assetDto, asset);
        asset.setMrid(assetDto.getId());
        return asset;
    }

    public Owner updateAssetDtoToAsset(OwnerDto assetDto, Owner asset, String... ignoreProperties) {
        BeanUtils.copyProperties(assetDto, asset);
        asset.setIsDeleted(assetDto.getIsDeleted());
        asset.setMrid(assetDto.getId());
        return asset;
    }
}
