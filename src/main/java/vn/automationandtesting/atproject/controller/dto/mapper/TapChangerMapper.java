package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.entity.cim.TapChanger;

/**
 * @author tund on 8/9/2022
 * @project at-project-server
 */
@Component
public class TapChangerMapper {
    public TapChangerDto tapChangerToTapChangerDto(TapChanger tapChanger) {
        TapChangerDto tapChangerDto = new TapChangerDto();
        BeanUtils.copyProperties(tapChanger, tapChangerDto);
        tapChangerDto.setAsset_id(tapChanger.getAssets().getMrid());
        tapChangerDto.setId(tapChanger.getMrid());
        return tapChangerDto;
    }

    public TapChanger tapChangerDtoToTapChanger(TapChangerDto tapChangerDto) {
        TapChanger tapChanger = new TapChanger();
        BeanUtils.copyProperties(tapChangerDto, tapChanger, "mrid");
        tapChanger.setMrid(tapChangerDto.getId());
        return tapChanger;
    }
    
    public TapChanger copyTapChangerDtoToTapChanger(TapChangerDto tapChangerDto, TapChanger tapChanger, String... ignoredPropertyName){
        BeanUtils.copyProperties(tapChangerDto, tapChanger, ignoredPropertyName);
        return tapChanger;
    }
}
