package vn.automationandtesting.atproject.controller.dto.mapper.surge;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.Arrays;
import java.util.List;


@Component
public class SurgeMapper {
    public SurgeDto assetToAssetDto(Surge asset) {
        SurgeDto assetDto = new SurgeDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public Surge assetDtoToAsset(SurgeDto assetDto) {
        Surge asset = new Surge();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public Surge copyAssetDtoToAsset(SurgeDto assetDto, Surge asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
    public Surge copyAssetDtoToAssetLite(SurgeDto assetDto, Surge asset, List<String> acceptProperties, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtilsCustom.copyProperties(assetDto, asset, acceptProperties, ignoreProperties);
        return asset;
    }
}
