package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.entity.cim.Bushing;

/**
 * @author tund on 3/9/2022
 * @project at-project-server
 */
@Component
public class BushingMapper {
    public BushingDto bushingToBushingDto(Bushing bushing) {
        BushingDto bushingDto = new BushingDto();
        BeanUtils.copyProperties(bushing, bushingDto, "status");
        bushingDto.setAsset_id(bushing.getAsset().getMrid());
        return bushingDto;
    }

    public Bushing bushingDtoToBushing(BushingDto bushingDto) {
        Bushing bushing = new Bushing();
        BeanUtils.copyProperties(bushingDto, bushing, "status");
        return bushing;
    }
    
    public Bushing copyBushingDtoToBushing(BushingDto bushingDto, Bushing bushing, String... ignoredPropertyName){
        BeanUtils.copyProperties(bushingDto, bushing, ignoredPropertyName);
        return bushing;
    }
}
