package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.TestTypeDto;
import vn.automationandtesting.atproject.entity.cim.TestType;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
@Component
public class TestTypeMapper {
    public TestTypeDto testTypeToTestTypeDto(TestType testType) {
        TestTypeDto testTypeDto = new TestTypeDto();
        BeanUtils.copyProperties(testType, testTypeDto);
        // testTypeDto.setCode(testType.getCode());
        return testTypeDto;
    }

    public TestType testTypeDtoToTestType(TestTypeDto testTypeDto) {
        TestType testType = new TestType();
        BeanUtils.copyProperties(testTypeDto, testType);
        // testType.setCode(TestTypeEnum.valueOf(testTypeDto.getCode()));
        return testType;
    }
}
