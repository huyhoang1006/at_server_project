package vn.automationandtesting.atproject.controller.dto.mapper.message;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.message.GroupListDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.message.GroupList;
import vn.automationandtesting.atproject.exception.UserNotFoundException;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.UserRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GroupListMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    public GroupList copyDtoToEntity(GroupListDto source, GroupList destination, String... ignore) {
        String[] newIgnore = Arrays.copyOf(ignore, ignore.length + 2);
        newIgnore[newIgnore.length - 2] = "member";
        newIgnore[newIgnore.length - 1] = "lastMessageReceiveMember";
        BeanUtils.copyProperties(source, destination, newIgnore);
        if(!Arrays.asList(ignore).contains("member")) {
            if(source.getMember() != null) {
                destination.setMember(String.join(",", source.getMember()));
            }
        }
        if(!Arrays.asList(ignore).contains("lastMessageReceiveMember")) {
            if(source.getLastMessageReceiveMember() != null) {
                destination.setLastMessageReceiveMember(String.join(",", source.getLastMessageReceiveMember()));
            }
        }
        if(!Arrays.asList(ignore).contains("isDeleted")) {
            destination.setIsDeleted(false);
        }
        return destination;
    }

    public GroupListDto copyEntityToDto(GroupList source, GroupListDto destination, String... ignore) {
        String[] newIgnore = Arrays.copyOf(ignore, ignore.length + 2);
        newIgnore[newIgnore.length - 2] = "member";
        newIgnore[newIgnore.length - 1] = "lastMessageReceiveMember";
        BeanUtils.copyProperties(source, destination, newIgnore);
        if(!Arrays.asList(ignore).contains("member")) {
            if(source.getMember() != null) {
                destination.setMember(Arrays.stream(source.getMember().split(",")).collect(Collectors.toList()));
            }
        }
        if(!Arrays.asList(ignore).contains("lastMessageReceiveMember")) {
            if(source.getLastMessageReceiveMember() != null) {
                destination.setLastMessageReceiveMember(Arrays.stream(source.getLastMessageReceiveMember().split(",")).collect(Collectors.toList()));
            }
        }
        if(!source.getCreatedBy().toString().isEmpty()) {
            List<User> userList = userRepository.findAllUserById(source.getCreatedBy());
            if(userList.isEmpty()) {
                List<OwnerUser> ownerUserList = ownerUserRepository.findAllOwnerUserById(source.getCreatedBy());
                if(!ownerUserList.isEmpty()) {
                    destination.setCreatedName(ownerUserList.get(0).getUsername());
                }
            } else {
                destination.setCreatedName(userList.get(0).getUsername());
            }
        }
        return destination;
    }
}
