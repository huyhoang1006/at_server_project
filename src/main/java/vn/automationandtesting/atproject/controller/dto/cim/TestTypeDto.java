package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestTypeDto {
    private UUID id;
    private String code;
    private String name;
}
