package vn.automationandtesting.atproject.controller.dto.mapper.message;


import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.message.MessageDto;
import vn.automationandtesting.atproject.entity.message.Message;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class MessageMapper {
    public Message copyDtoToEntity(MessageDto source, Message destination, String... ignore) {
        String[] newIgnore = Arrays.copyOf(ignore, ignore.length + 2);
        newIgnore[newIgnore.length - 1] = "receiverId";
        newIgnore[newIgnore.length - 2] = "receiverName";
        BeanUtils.copyProperties(source, destination, newIgnore);
        if(!Arrays.asList(ignore).contains("receiverId")) {
            if(source.getReceiverMember() != null) {
                destination.setReceiverMember(String.join(",", source.getReceiverMember()));
            }
        }
        if(!Arrays.asList(ignore).contains("receiverName")) {
            if(source.getReceiverName() != null) {
                destination.setReceiverName(String.join(",", source.getReceiverName()));
            }
        }
        if(!Arrays.asList(ignore).contains("isDeleted")) {
            destination.setIsDeleted(false);
        }
        return destination;
    }

    public MessageDto copyEntityToDto(Message source, MessageDto destination, String... ignore) {
        String[] newIgnore = Arrays.copyOf(ignore, ignore.length + 2);
        newIgnore[newIgnore.length - 1] = "receiverId";
        newIgnore[newIgnore.length - 2] = "receiverName";
        BeanUtils.copyProperties(source, destination, newIgnore);
        if(!Arrays.asList(ignore).contains("receiverId")) {
            if(source.getReceiverMember() != null && !source.getReceiverMember().isEmpty()) {
                destination.setReceiverMember(Arrays.stream(source.getReceiverMember().split(",")).collect(Collectors.toList()));
            }
        }
        if(!Arrays.asList(ignore).contains("receiverName")) {
            if(source.getReceiverName() != null && !source.getReceiverName().isEmpty()) {
                destination.setReceiverName(Arrays.stream(source.getReceiverName().split(",")).collect(Collectors.toList()));
            }
        }
        return destination;
    }
}
