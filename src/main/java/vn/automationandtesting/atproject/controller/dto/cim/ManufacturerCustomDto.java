package vn.automationandtesting.atproject.controller.dto.cim;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ManufacturerCustomDto {
    private UUID id;
    private String name;
    private String type;
    private boolean locked = false;
    private UUID createdBy;
    private Timestamp createdOn;
    private UUID updatedBy;
    private Timestamp updatedOn;
}
