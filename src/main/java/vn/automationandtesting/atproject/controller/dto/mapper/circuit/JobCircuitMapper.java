package vn.automationandtesting.atproject.controller.dto.mapper.circuit;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;

@Component
public class JobCircuitMapper {
    public JobCircuitDto JobToJobDto(Jobdata jobdata) {
        JobCircuitDto jobCircuitDto = new JobCircuitDto();
        BeanUtils.copyProperties(jobdata, jobCircuitDto);
        return jobCircuitDto;
    }

    public Jobdata JobDtoToJob(JobCircuitDto jobCircuitDto) {
        Jobdata jobdata = new Jobdata();
        BeanUtils.copyProperties(jobCircuitDto, jobdata, "collabs");
        return jobdata;
    }

    public Jobdata copyJobDtoToJob(JobCircuitDto jobCircuitDto, Jobdata jobdata, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobCircuitDto, jobdata, ignoredPropertyName);
//        jobdata.setCollabs(String.join(",", jobCircuitDto.getCollabs()));
        return jobdata;
    }
}
