package vn.automationandtesting.atproject.controller.dto.mapper.voltage;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.voltage.TestVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.TestsVoltage;

@Component
public class TestVoltageMapper {
    public TestVoltageDto testToTestDto(TestsVoltage test) {
        TestVoltageDto testDto = new TestVoltageDto();
        BeanUtils.copyProperties(test, testDto);
        return testDto;
    }

    public TestsVoltage testDtoToTest(TestVoltageDto testDto) {
        TestsVoltage test = new TestsVoltage();
        BeanUtils.copyProperties(testDto, test);
        return test;
    }

    public TestsVoltage copyTestDtoToTest(TestVoltageDto testDto, TestsVoltage test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
