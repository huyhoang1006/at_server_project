package vn.automationandtesting.atproject.controller.dto.owner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OwnerDto {
    private UUID id;
    private UUID mrid;
    private String name;
    private UUID ref_id;
    private String phone_no;
    private String country;
    private String full_name;
    private String tax_code;
    private String pre;
    private String address;
    private String city;
    private String phone2;
    private String name_person;
    private String phone1;
    private String mode;
    private String fax_contact;
    private UUID user_id;
    private String comment;
    private String state;
    private String position;
    private String fax;
    private String email_contact;
    private String department;
    private String email;
    private Boolean isDeleted;
    private UUID created_by;
}
