package vn.automationandtesting.atproject.controller.dto.circuit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CircuitDto {
    private UUID id;
    private UUID location_id;
    private String properties;
    private String circuitBreaker;
    private String ratings;
    private String contactSys;
    private String others;
    private String operating;
    private String assessmentLimits;
    private String extend;
    private String asset;
    private String asset_type;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturing_year;
    private String asset_system_code;
    private String apparatus_id;
    private String feeder;
    @JsonProperty("created_by")
    private UUID createdBy;
    @JsonProperty("created_on")
    private Timestamp createdOn;
    @JsonProperty("updated_by")
    private UUID updatedBy;
    @JsonProperty("updated_on")
    private Timestamp updatedOn;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
}
