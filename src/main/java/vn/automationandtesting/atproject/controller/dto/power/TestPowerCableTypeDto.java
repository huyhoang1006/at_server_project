package vn.automationandtesting.atproject.controller.dto.power;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestPowerCableTypeDto {
    private UUID id;
    private String name;
    private String code;
}
