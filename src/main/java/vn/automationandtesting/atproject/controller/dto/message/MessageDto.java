package vn.automationandtesting.atproject.controller.dto.message;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {
    private UUID id;
    private UUID groupId;
    private String content;
    private UUID senderId;
    private String senderName;
    private String status;
    private UUID createdBy;
    private Timestamp createdOn;
    private UUID updatedBy;
    private Timestamp updatedOn;
    private Boolean isDeleted;
    private String type;
    private List<String> receiverName;
    private List<String> receiverMember;
}
