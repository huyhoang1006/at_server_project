package vn.automationandtesting.atproject.controller.dto.circuit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestCircuitTypeDto {
    private UUID id;
    private String name;
    private String code;
}
