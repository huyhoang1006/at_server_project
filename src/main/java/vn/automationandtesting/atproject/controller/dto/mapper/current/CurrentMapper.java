package vn.automationandtesting.atproject.controller.dto.mapper.current;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.Arrays;
import java.util.List;


@Component
public class CurrentMapper {
    public CurrentDto assetToAssetDto(Current asset) {
        CurrentDto assetDto = new CurrentDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public Current assetDtoToAsset(CurrentDto assetDto) {
        Current asset = new Current();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public Current copyAssetDtoToAsset(CurrentDto assetDto, Current asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }

    public Current copyAssetDtoToAssetLite(CurrentDto assetDto, Current asset, List<String> acceptProperties, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtilsCustom.copyProperties(assetDto, asset, acceptProperties, ignoreProperties);
        return asset;
    }
}
