package vn.automationandtesting.atproject.controller.dto.mapper.surge;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.surge.JobSurgeDto;
import vn.automationandtesting.atproject.entity.surge.JobsSurge;

@Component
public class JobSurgeMapper {
    public JobSurgeDto JobToJobDto(JobsSurge job) {
        JobSurgeDto jobDto = new JobSurgeDto();
        BeanUtils.copyProperties(job, jobDto);
        return jobDto;
    }

    public JobsSurge JobDtoToJob(JobSurgeDto jobDto) {
        JobsSurge job = new JobsSurge();
        BeanUtils.copyProperties(jobDto, job, "collabs");
        return job;
    }

    public JobsSurge copyJobDtoToJob(JobSurgeDto jobDto, JobsSurge job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
        job.setCollabs(String.join(",", job.getCollabs()));
        return job;
    }
}
