package vn.automationandtesting.atproject.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitoringDto {
    private String id;
    @JsonProperty("ageing_insulation")
    private String ageingInsulation;
    @JsonProperty("moisture_insulation")
    private String moistureInsulation;
    @JsonProperty("bushings_online")
    private String bushingsOnline;
    @JsonProperty("patital_discharge")
    private String patitalDischarge;
    @JsonProperty("dga")
    private String dga;
    @JsonProperty("asset_id")
    private String assetId;

    @JsonProperty("bushing_df_worst")
    private String bushing_df_worst;
    @JsonProperty("bushing_df_average")
    private String bushing_df_average;
    @JsonProperty("bushing_c_worst")
    private String bushing_c_worst;
    @JsonProperty("bushing_c_average")
    private String bushing_c_average;
    @JsonProperty("condition_mois")
    private String condition_mois;
    @JsonProperty("health_index")
    private String health_index;
    @JsonProperty("weight_bushing_df")
    private String weight_bushing_df;
    @JsonProperty("weight_bushing_c")
    private String weight_bushing_c;
    @JsonProperty("weight_mois")
    private String weight_mois;
    private String created_on;
    private String created_by;
    private String updated_on;
    private String update_by;
}
