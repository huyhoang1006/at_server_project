package vn.automationandtesting.atproject.controller.dto.mapper.disconnector;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.disconnector.TestDisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.TestsDisconnector;

@Component
public class TestDisconnectorMapper {
    public TestDisconnectorDto testToTestDto(TestsDisconnector test) {
        TestDisconnectorDto testDto = new TestDisconnectorDto();
        BeanUtils.copyProperties(test, testDto);
        return testDto;
    }

    public TestsDisconnector testDtoToTest(TestDisconnectorDto testDto) {
        TestsDisconnector test = new TestsDisconnector();
        BeanUtils.copyProperties(testDto, test);
        return test;
    }

    public TestsDisconnector copyTestDtoToTest(TestDisconnectorDto testDto, TestsDisconnector test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
