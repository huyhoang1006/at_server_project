package vn.automationandtesting.atproject.controller.dto.mapper.circuit;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;

import java.util.Arrays;
import java.util.List;


@Component
public class CircuitMapper {
    public CircuitDto circuitToCircuitDto(Circuit circuit) {
        CircuitDto circuitDto = new CircuitDto();
        BeanUtils.copyProperties(circuit, circuitDto);
        return circuitDto;
    }

    public Circuit circuitDtoToCircuit(CircuitDto circuitDto) {
        Circuit circuit = new Circuit();
        BeanUtils.copyProperties(circuitDto, circuit, "collabs");
        return circuit;
    }

    public Circuit copyCircuitDtoToCircuit(CircuitDto circuitDto, Circuit circuit, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(circuitDto, circuit, ignoreProperties);
        if (circuitDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            circuit.setId(circuitDto.getId());
        }
        if (circuitDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            circuit.setLocation_id(circuitDto.getLocation_id());
        }
        circuit.setAsset_type(circuitDto.getAsset_type());
        circuit.setSerial_no(circuitDto.getSerial_no());
//        circuit.setCollabs(String.join(",", circuitDto.getCollabs()));
        return circuit;
    }

    public Circuit copyAssetDtoToAssetLite(CircuitDto assetDto, Circuit asset, List<String> acceptProperties, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtilsCustom.copyProperties(assetDto, asset, acceptProperties, ignoreProperties);
        return asset;
    }
}
