package vn.automationandtesting.atproject.controller.dto.cim;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestDto {
    private UUID id;
    private UUID job_id;
    private String name;
    private String data;
    private UUID testTypeId;
    private String testTypeCode;
    private String testTypeName;
    private float average_score;
    private float worst_score;
    private float average_score_df;
    private float worst_score_df;
    private float average_score_c;
    private float worst_score_c;
    @JsonProperty("weighting_factor")
    private float weighting_factor;
    private float total_average_score;
    private float total_worst_score;
    @JsonProperty("created_on")
    private BigInteger create_at_client;
//    @JsonProperty("weighting_factor")
//    private float weightingFactor;
    @JsonProperty("weighting_factor_df")
    private float weightingFactorDF;
    @JsonProperty("weighting_factor_c")
    private float weightingFactorC;
}
