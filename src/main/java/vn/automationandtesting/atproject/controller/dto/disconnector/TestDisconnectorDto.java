package vn.automationandtesting.atproject.controller.dto.disconnector;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class TestDisconnectorDto {
    private UUID id;
    private String name;
    private String data;
    private Float average_score;
    private Float worst_score;
    private Float total_average_score;
    private Float total_worst_score;
    private Float weighting_factor;
    private Float average_score_df;
    private Float average_score_c;
    private Float worst_score_df;
    private Float worst_score_c;
    private Float weighting_factor_df;
    private Float weighting_factor_c;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
    @JsonProperty("testTypeId")
    private UUID type_id;
    private UUID job_id;
    @JsonProperty("created_by")
    private UUID createdBy;
    @JsonProperty("created_on")
    private Timestamp createdOn;
    @JsonProperty("updated_by")
    private UUID updatedBy;
    @JsonProperty("updated_on")
    private Timestamp updatedOn;
}
