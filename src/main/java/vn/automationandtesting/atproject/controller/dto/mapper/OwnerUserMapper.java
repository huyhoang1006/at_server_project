package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.Gender;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.repository.RoleRepository;

import java.util.Arrays;
import java.util.List;


@Component
public class OwnerUserMapper {
    @Autowired
    private RoleRepository roleRepository;

    public OwnerUserDto userOwnerToUserOwnerReqDto(OwnerUser ownerUser) {
        List<String> groups = Arrays.asList(ownerUser.getGroups().split(","));
        OwnerUserDto ownerUserDto = new OwnerUserDto(ownerUser.getId(), ownerUser.getUsername(), null, ownerUser.getFirstName(),
                ownerUser.getLastName(), ownerUser.getGender().name(), ownerUser.getEmail(), ownerUser.getPhone(), ownerUser.getBirthDate(),
                ownerUser.getRole().getRoleName().name(), groups, ownerUser.getIsDeleted());
        return ownerUserDto;
    }

    public OwnerUser userOwnerReqDtoToUserOwner(OwnerUserDto ownerUserDto) {
        Gender gender = Gender.valueOf(ownerUserDto.getGender());
        String roleRaw = ownerUserDto.getRole();
        RoleEnum roleDto = RoleEnum.valueOf(roleRaw);
        Role role = roleRepository.findByRoleName(roleDto);
        OwnerUser ownerUser = new OwnerUser(ownerUserDto.getUsername(), ownerUserDto.getPassword(), true, ownerUserDto.getFirstName(),
                ownerUserDto.getLastName(), gender,
                ownerUserDto.getEmail(), ownerUserDto.getPhone(), ownerUserDto.getBirthDate(), role, "");
        return ownerUser;
    }
}
