package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.common.LocationOwnerDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.entity.cim.Location;
import vn.automationandtesting.atproject.entity.cim.Position_point;
import java.util.stream.IntStream;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
@Component
public class LocationMapper {
    public Location locationDtoToLocation(LocationDto locationDto, String... ignoreProperties) {
        List<String> ignorePropertiesList = Arrays.asList(ignoreProperties);
        if (ignorePropertiesList == null)
            ignorePropertiesList.add("collabs");

        Location location = new Location();
        BeanUtils.copyProperties(locationDto, location, ignorePropertiesList.toArray(String[]::new));
        if (!Arrays.asList(ignoreProperties).contains("id")) {
            location.setMrid(locationDto.getId());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_info_reference")) {
            location.setGeo_info_reference(locationDto.getGeo_coordinates());
        }
        location.setCollabs(String.join(",", locationDto.getCollabs()));
        return location;
    }

    public LocationDto locationToLocationDto(Location location, String... ignoreProperties) {
        List<String> ignorePropertiesList = Arrays.asList(ignoreProperties);
        if (ignorePropertiesList == null)
            ignorePropertiesList.add("collabs");

        if (ignorePropertiesList.contains("positionPoints") || ignorePropertiesList == null)
            ignorePropertiesList.add("positionPoints");

        LocationDto locationDto = new LocationDto();
        BeanUtils.copyProperties(location, locationDto, ignorePropertiesList.toArray(String[]::new));
        if (!Arrays.asList(ignoreProperties).contains("mid")) {
            locationDto.setId(location.getMrid());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_coordinates")) {
            locationDto.setGeo_coordinates(location.getGeo_info_reference());
        }
        if (!Arrays.asList(ignoreProperties).contains("user_id")) {
            if(location.getUser_id() != null) {
                locationDto.setUser_id(location.getUser().getId());
            }
        }
        locationDto.setCollabs(Arrays.stream(location.getCollabs().split(",")).collect(Collectors.toSet()));
        List<HashMap<String, String>> x_position = new ArrayList<>();
        List<HashMap<String, String>> y_position = new ArrayList<>();
        List<HashMap<String, String>> z_position = new ArrayList<>();
        if(location.getPositionPoints() != null) {
            for (Position_point positionPoint : location.getPositionPoints()) {
                HashMap<String, String> mapX = new HashMap<>();
                HashMap<String, String> mapY = new HashMap<>();
                HashMap<String, String> mapZ = new HashMap<>();
                mapX.put("coor", positionPoint.getX_position());
                mapX.put("id", positionPoint.getMrid().toString());
                mapY.put("coor", positionPoint.getY_position());
                mapY.put("id", positionPoint.getMrid().toString());
                mapZ.put("coor", positionPoint.getZ_position());
                mapZ.put("id", positionPoint.getMrid().toString());
                x_position.add(mapX);
                y_position.add(mapY);
                z_position.add(mapZ);
            }


            locationDto.getPositionPoints().put("x", x_position);
            locationDto.getPositionPoints().put("y", y_position);
            locationDto.getPositionPoints().put("z", z_position);
        }
        return locationDto;
    }

    public Location copyLocationDtoToLocation(LocationDto locationDto, Location location, String... ignoreProperties) {
        BeanUtils.copyProperties(locationDto, location, ignoreProperties);
        if (!Arrays.asList(ignoreProperties).contains("id")) {
            location.setMrid(locationDto.getId());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_info_reference")) {
            location.setGeo_info_reference(locationDto.getGeo_coordinates());
        }
        if(!Arrays.asList(ignoreProperties).contains("position_point")) {
            List<Position_point> positionPoints = new ArrayList<>();
            List<Position_point> positionPointList = location.getPositionPoints();
            if(positionPointList != null && !positionPointList.isEmpty() && locationDto.getPositionPoints() != null) {
                for (int i = 0; i < locationDto.getPositionPoints().get("x").size(); i++) {
                    if (locationDto.getPositionPoints().get("x").get(i).get("id").isEmpty()) {
                        Position_point positionPoint = new Position_point();
                        positionPoint.setMrid(UUID.randomUUID());
                        positionPoint.setLocation(location);
                        positionPoint.setX_position(locationDto.getPositionPoints().get("x").get(i).get("coor"));
                        positionPoint.setY_position(locationDto.getPositionPoints().get("y").get(i).get("coor"));
                        positionPoint.setZ_position(locationDto.getPositionPoints().get("z").get(i).get("coor"));
                        positionPoints.add(positionPoint);
                    } else {
                        String idCheck = locationDto.getPositionPoints().get("x").get(i).get("id");
                        int index = IntStream.range(0, positionPointList.size())
                                .filter(j -> positionPointList.get(j).getMrid().toString().equals(idCheck))
                                .findFirst()
                                .orElse(-1);
                        if (index != -1) {
                            Position_point positionPoint = positionPointList.get(index);
                            positionPoint.setX_position(locationDto.getPositionPoints().get("x").get(i).get("coor"));
                            positionPoint.setY_position(locationDto.getPositionPoints().get("y").get(i).get("coor"));
                            positionPoint.setZ_position(locationDto.getPositionPoints().get("z").get(i).get("coor"));
                            positionPoints.add(positionPoint);
                        }
                    }
                }
                location.getPositionPoints().clear();
                location.getPositionPoints().addAll(positionPoints);
            }
        }
//        location.setCollabs(String.join(",", locationDto.getCollabs()));
        return location;
    }

    public Location copyLocationDtoToLocation(LocationDto locationDto, Location location, List<String> acceptProperties, String... ignoreProperties) {
        BeanUtilsCustom.copyProperties(locationDto, location, acceptProperties, ignoreProperties);
        if (!Arrays.asList(ignoreProperties).contains("id")) {
            location.setMrid(locationDto.getId());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_info_reference")) {
            location.setGeo_info_reference(locationDto.getGeo_coordinates());
        }
        return location;
    }

    public Location copyLocationOwnerDtoToLocation(LocationOwnerDto locationOwnerDto, Location location, String... ignoreProperties) {
        BeanUtils.copyProperties(locationOwnerDto, location, ignoreProperties);
        if(location.getMrid() == null) {
            location.setMrid(UUID.randomUUID());
        }
        if(!Arrays.asList(ignoreProperties).contains("position_point")) {
            List<Position_point> positionPoints = new ArrayList<>();
            List<Position_point> positionPointList = location.getPositionPoints();
            for(int i=0; i<locationOwnerDto.getPositionPoints().get("x").size(); i ++) {
                if(locationOwnerDto.getPositionPoints().get("x").get(i).get("id").isEmpty()) {
                    Position_point positionPoint = new Position_point();
                    positionPoint.setMrid(UUID.randomUUID());
                    positionPoint.setLocation(location);
                    positionPoint.setX_position(locationOwnerDto.getPositionPoints().get("x").get(i).get("coor"));
                    positionPoint.setY_position(locationOwnerDto.getPositionPoints().get("y").get(i).get("coor"));
                    positionPoint.setZ_position(locationOwnerDto.getPositionPoints().get("z").get(i).get("coor"));
                    positionPoints.add(positionPoint);
                } else {
                    String idCheck = locationOwnerDto.getPositionPoints().get("x").get(i).get("id");
                    if(positionPointList != null) {
                        int index = IntStream.range(0, positionPointList.size())
                                .filter(j -> positionPointList.get(j).getMrid().toString().equals(idCheck))
                                .findFirst()
                                .orElse(-1);
                        if (index != -1) {
                            Position_point positionPoint = positionPointList.get(index);
                            positionPoint.setX_position(locationOwnerDto.getPositionPoints().get("x").get(i).get("coor"));
                            positionPoint.setY_position(locationOwnerDto.getPositionPoints().get("y").get(i).get("coor"));
                            positionPoint.setZ_position(locationOwnerDto.getPositionPoints().get("z").get(i).get("coor"));
                            positionPoints.add(positionPoint);
                        } else {
                            Position_point positionPoint = new Position_point();
                            positionPoint.setLocation(location);
                            positionPoint.setX_position(locationOwnerDto.getPositionPoints().get("x").get(i).get("coor"));
                            positionPoint.setY_position(locationOwnerDto.getPositionPoints().get("y").get(i).get("coor"));
                            positionPoint.setZ_position(locationOwnerDto.getPositionPoints().get("z").get(i).get("coor"));
                            positionPoint.setMrid(UUID.fromString(locationOwnerDto.getPositionPoints().get("x").get(i).get("id")));
                            positionPoints.add(positionPoint);
                        }
                    } else {
                        Position_point positionPoint = new Position_point();
                        positionPoint.setLocation(location);
                        positionPoint.setX_position(locationOwnerDto.getPositionPoints().get("x").get(i).get("coor"));
                        positionPoint.setY_position(locationOwnerDto.getPositionPoints().get("y").get(i).get("coor"));
                        positionPoint.setZ_position(locationOwnerDto.getPositionPoints().get("z").get(i).get("coor"));
                        positionPoint.setMrid(UUID.fromString(locationOwnerDto.getPositionPoints().get("x").get(i).get("id")));
                        positionPoints.add(positionPoint);
                    }
                }
            }
//            location.getPositionPoints().clear();
            location.setPositionPoints(positionPoints);
        }
//        location.setCollabs(String.join(",", locationDto.getCollabs()));
        return location;
    }

}
