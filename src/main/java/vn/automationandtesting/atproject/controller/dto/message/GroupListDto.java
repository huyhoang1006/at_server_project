package vn.automationandtesting.atproject.controller.dto.message;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupListDto {
    private UUID id;
    private String groupName;
    private Timestamp lastMessageAt;
    private String lastMessage;
    private List<String> member;
    private List<String> lastMessageReceiveMember;
    private UUID lastMessageCreatedBy;
    private UUID createdBy;
    private Timestamp createdOn;
    private UUID updatedBy;
    private Timestamp updatedOn;
    private Boolean isDeleted;
    private String type;
    private String createdName;
}
