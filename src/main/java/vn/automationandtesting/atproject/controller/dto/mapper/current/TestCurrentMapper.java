package vn.automationandtesting.atproject.controller.dto.mapper.current;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.current.TestCurrentDto;
import vn.automationandtesting.atproject.entity.current.TestsCurrent;

@Component
public class TestCurrentMapper {
    public TestCurrentDto testToTestDto(TestsCurrent test) {
        TestCurrentDto testDto = new TestCurrentDto();
        BeanUtils.copyProperties(test, testDto);
        return testDto;
    }

    public TestsCurrent testDtoToTest(TestCurrentDto testDto) {
        TestsCurrent test = new TestsCurrent();
        BeanUtils.copyProperties(testDto, test);
        return test;
    }

    public TestsCurrent copyTestDtoToTest(TestCurrentDto testDto, TestsCurrent test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
