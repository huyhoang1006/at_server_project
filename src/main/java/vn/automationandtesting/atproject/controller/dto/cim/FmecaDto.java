package vn.automationandtesting.atproject.controller.dto.cim;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FmecaDto {
    private UUID id;
    @JsonProperty("table_fmeca")
    private String table_fmeca;
    @JsonProperty("table_calculate")
    private String table_calculate;
    private String total;

}
