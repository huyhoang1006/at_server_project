package vn.automationandtesting.atproject.controller.dto.mapper.voltage;

import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeanUtilsCustom {

    private static final Logger logger = LoggerFactory.getLogger(BeanUtilsCustom.class);

    public static void copyProperties(Object source, Object target, List<String> fields, String... ignoreProperties){
        if(fields.isEmpty()) {
            BeanUtils.copyProperties(source, target, ignoreProperties);
        } else {
            for(String fieldName : fields) {
                try {
                    Field sourceField = source.getClass().getDeclaredField(fieldName);
                    sourceField.setAccessible(true);
                    Object value = sourceField.get(source);
                    Field targetField = target.getClass().getDeclaredField(fieldName);
                    targetField.setAccessible(true);
                    targetField.set(target, value);
                } catch ( Exception e) {
                    logger.error("Error copying property: {}", fieldName, e);
                }
            }
        }
    }
}
