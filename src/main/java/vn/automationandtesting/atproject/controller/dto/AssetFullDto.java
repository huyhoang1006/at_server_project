package vn.automationandtesting.atproject.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssetFullDto {
    private AssetDto asset;
    private BushingDto bushing;
    private TapChangerDto tapChanger;
}
