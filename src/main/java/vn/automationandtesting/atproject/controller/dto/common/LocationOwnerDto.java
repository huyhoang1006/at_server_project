package vn.automationandtesting.atproject.controller.dto.common;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LocationOwnerDto {
    private String person_phone_no1;
    private String person_phone_no2;
    private String person_email;
    private String company_email;
    private String person_name;
    private String person_position;
    private HashMap< String, List<HashMap<String, String>>> positionPoints = new HashMap<>();
    private String comment;
    private String name;
    private String city;
    private String state_province;
    private String location_system_code;
    private String full_name;
    private String address;
    private String mrid;
}
