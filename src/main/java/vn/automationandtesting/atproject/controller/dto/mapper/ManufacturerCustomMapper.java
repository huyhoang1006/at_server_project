package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.ManufacturerCustomDto;
import vn.automationandtesting.atproject.entity.ManufacturerCustom;

import java.util.ArrayList;
import java.util.List;

@Component
public class ManufacturerCustomMapper {
    public ManufacturerCustomDto manuToManuDto(ManufacturerCustom manufacturerCustom, String... ignoreProperties) {
        ManufacturerCustomDto manufacturerCustomDto = new ManufacturerCustomDto();
        BeanUtils.copyProperties(manufacturerCustom, manufacturerCustomDto, ignoreProperties);
        return manufacturerCustomDto;
    }

    public ManufacturerCustom manuDtoToManu(ManufacturerCustomDto manufacturerCustomDto, String... ignoreProperties) {
        ManufacturerCustom manufacturerCustom = new ManufacturerCustom();
        BeanUtils.copyProperties(manufacturerCustomDto, manufacturerCustom, ignoreProperties);
        return manufacturerCustom;
    }

    public ManufacturerCustom copyManuDtoToManuCustom(ManufacturerCustomDto manufacturerCustomDto, ManufacturerCustom manufacturerCustom, String... ignoreProperties) {
        BeanUtils.copyProperties(manufacturerCustomDto, manufacturerCustom, ignoreProperties);
        return manufacturerCustom;
    }

    public ManufacturerCustom copyManuDtoToManu(ManufacturerCustomDto manufacturerCustomDto, ManufacturerCustom manufacturerCustom) {
        List<String> ignoreProperties = new ArrayList<>();
        ignoreProperties.add("createdBy");
        ignoreProperties.add("createdOn");
        ignoreProperties.add("updatedBy");
        ignoreProperties.add("updatedOn");
        BeanUtils.copyProperties(manufacturerCustomDto, manufacturerCustom, ignoreProperties.toArray(new String[0]));
        return manufacturerCustom;
    }
}
