package vn.automationandtesting.atproject.controller.dto.mapper.staging.location;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.staging.location.LocationStagingDto;
import vn.automationandtesting.atproject.entity.LocationStaging;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.UUID;

@Component
public class LocationStagingMapper {
    public LocationStagingDto objectToObjectDto(LocationStaging locationStaging, LocationStagingDto locationStagingDto, String... ignoreProperties) {
        BeanUtils.copyProperties(locationStaging, locationStagingDto, ignoreProperties);
        return locationStagingDto;
    }

    public LocationStaging objectDtoToObject(LocationStagingDto locationStagingDto, LocationStaging locationStaging, String... ignoreProperties) {
        BeanUtils.copyProperties(locationStagingDto, locationStaging, ignoreProperties);
        return locationStaging;
    }

    public LocationStaging LocationToStaging(Location location) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String content = objectMapper.writeValueAsString(location);
        LocationStaging locationStaging = new LocationStaging();
        locationStaging.setId(UUID.randomUUID());
        locationStaging.setLocation_id(location.getMrid());
        locationStaging.setContent(content);
        locationStaging.setSender_id(UUID.fromString(BearerContextHolder.getContext().getUserId()));
        locationStaging.setSender_name(BearerContextHolder.getContext().getUserName());
        return locationStaging;
    }

    public LocationStaging LocationDtoToStaging(LocationDto locationDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String content = objectMapper.writeValueAsString(locationDto);
        LocationStaging locationStaging = new LocationStaging();
        locationStaging.setId(UUID.randomUUID());
        locationStaging.setLocation_id(locationDto.getId());
        locationStaging.setContent(content);
        locationStaging.setSender_id(UUID.fromString(BearerContextHolder.getContext().getUserId()));
        locationStaging.setSender_name(BearerContextHolder.getContext().getUserName());
        return locationStaging;
    }
}
