package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.Gender;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.repository.RoleRepository;

import java.util.Arrays;
import java.util.List;

@Component
public class UserMapper {
    @Autowired
    private RoleRepository roleRepository;

    public UserReqDto userToUserReqDto(User user) {
        List<String> groups = Arrays.asList(user.getGroups().split(","));
        UserReqDto userReqDto = new UserReqDto(user.getId(), user.getUsername(), null, user.getFirstName(),
                user.getLastName(), user.getGender().name(), user.getEmail(), user.getPhone(), user.getBirthDate(),
                user.getRole().getRoleName().name(), groups, user.getIsDeleted());
        return userReqDto;
    }

    public User userReqDtoToUser(UserReqDto userReqDto) {
        Gender gender = Gender.valueOf(userReqDto.getGender());
        String roleRaw = userReqDto.getRole();
        RoleEnum roleDto = RoleEnum.valueOf(roleRaw);
        Role role = roleRepository.findByRoleName(roleDto);
        User user = new User(userReqDto.getUsername(), userReqDto.getPassword(), true, userReqDto.getFirstName(),
                userReqDto.getLastName(), gender,
                userReqDto.getEmail(), userReqDto.getPhone(), userReqDto.getBirthDate(), role, "");
        return user;
    }
}
