package vn.automationandtesting.atproject.controller.dto.objectLog.location;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuditLocationLogDto {
    private UUID id;
    private UUID location_staging_id;
    private String content;
    private String old_value;
    private String new_value;
    private UUID sender_id;
    private String sender_name;
    private String version;
    private String status;
    private Timestamp createdOn;
    private UUID createdBy;
}
