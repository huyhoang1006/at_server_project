package vn.automationandtesting.atproject.controller.dto.mapper.disconnector;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.disconnector.JobDisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.JobsDisconnector;

@Component
public class JobDisconnectorMapper {
    public JobDisconnectorDto JobToJobDto(JobsDisconnector job) {
        JobDisconnectorDto jobDto = new JobDisconnectorDto();
        BeanUtils.copyProperties(job, jobDto);
        return jobDto;
    }

    public JobsDisconnector JobDtoToJob(JobDisconnectorDto jobDto) {
        JobsDisconnector job = new JobsDisconnector();
        BeanUtils.copyProperties(jobDto, job, "collabs");
        return job;
    }

    public JobsDisconnector copyJobDtoToJob(JobDisconnectorDto jobDto, JobsDisconnector job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
        job.setCollabs(String.join(",", job.getCollabs()));
        return job;
    }
}
