package vn.automationandtesting.atproject.controller.owner;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.owner.OwnerMapper;
import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.entity.owner.Owner;
import vn.automationandtesting.atproject.service.owner.OwnerService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class OwnerController {
    Logger logger = LoggerFactory.getLogger(OwnerController.class);

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private OwnerMapper ownerMapper;

    @GetMapping("/owner/findById/{ownerId}")
    public ResponseEntity<?> getOwnerByID(@PathVariable String ownerId) {
        UUID id = UUID.fromString(ownerId);
        OwnerDto ownerDtoList = ownerService.findById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get owner by ID", ownerDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/owner/findAll")
    public ResponseEntity<?> getAllOwnerByUserId() {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID id = UUID.fromString(userId);
        List<OwnerDto> ownerDtoList = ownerService.findAll(id);
        ResponseObject responseObject = new ResponseObject(true, "Get Owner by User Id", ownerDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/owner/create")
    public ResponseEntity<?> createOwner(@RequestBody List<OwnerDto> ownerDtoList) {
        List<Owner> data = ownerService.saveOwner(ownerDtoList);
        ResponseObject responseObject = new ResponseObject(true, "Create Owner by User Id", data);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/owner/remove")
    public ResponseEntity<?> removeOwner(@RequestBody OwnerDto ownerDto) {
        try {
            Boolean check = ownerService.removeOwner(ownerDto);
            ResponseObject responseObject = new ResponseObject(true, "delete Owner by User Id", check);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "delete fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/owner/delete/{sign}")
    public ResponseEntity<?> deleteOwner(@RequestBody OwnerDto ownerDto, @PathVariable Boolean sign) {
        try {
            ownerService.deleteOwner(ownerDto, sign);
            ResponseObject responseObject = new ResponseObject(true, "delete Owner by User Id", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "delete fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/owner/update")
    public ResponseEntity<?> updateOwner(@RequestBody OwnerDto ownerDto) {
        try {
            Owner owner = ownerService.updateOwner(ownerDto);
            ResponseObject responseObject = new ResponseObject(true, "Update Owner by User Id", owner);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "Update fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/owner/changeOwner/{ownerId}/{id}")
    public ResponseEntity<?> changeOwner(@PathVariable String ownerId, @PathVariable String id) {
        try {
            UUID user_id = UUID.fromString(id);
            UUID owner_id = UUID.fromString(ownerId);
            ownerService.changeOwner(owner_id, user_id);
            ResponseObject responseObject = new ResponseObject(true, "Update Owner by User Id", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "Update fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/getAllOwnerAndParent")
    public ResponseEntity<?> getAllOwnerAndParent() {
        try {
            UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
            List<Owner> ownerList = ownerService.getAllOwnerAndParent(id);
            ResponseObject responseObject = new ResponseObject(true, "get Owner by User and parent", ownerList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findOwnerParentById")
    public ResponseEntity<?> findOwnerParentById() {
        try {
            UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
            List<Object> ownerList = ownerService.findOwnerParentById(id);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/getOwnerByParentId/{idParent}")
    public ResponseEntity<?> getOwnerByParentId(@PathVariable UUID idParent) {
        try {
            List<OwnerDto> ownerList = ownerService.getOwnerByParentId(idParent);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/countOwnerByRole/{role}")
    public ResponseEntity<?> countOwnerByRole(@PathVariable String role) {
        try {
            int count = ownerService.countOwnerByRole(role);
            ResponseObject responseObject = new ResponseObject(true, "count owner by role", count);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "count owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/getOwnerByRole/{role}/{stt}/{sl}")
    public ResponseEntity<?> getOwnerByRole(@PathVariable String role, @PathVariable int stt, @PathVariable int sl) {
        try {
            int first = (stt - 1) * sl;
            List<OwnerDto> ownerDtoList = ownerService.getOwnerByRole(role, first, sl);
            ResponseObject responseObject = new ResponseObject(true, "get owner by role", ownerDtoList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "count owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findOwnerParentById/page/{stt}/{sl}")
    public ResponseEntity<?> findOwnerParentByIdAndPage(@PathVariable int stt, @PathVariable int sl) {
        try {
            int first = (stt - 1) * sl;
            UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
            List<Owner> ownerList = ownerService.findOwnerParentAndPreById(id, "1", first, sl);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/count")
    public ResponseEntity<?> countOwnerPre() {
        try {
            String id = BearerContextHolder.getContext().getUserId();
            UUID user_id = UUID.fromString(id);
            int ownerListCount = ownerService.countByCreatedAndPre(user_id, "1");
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerListCount);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/getOwnerByRefAndCreated/{userId}/{refId}/{stt}/{sl}")
    public ResponseEntity<?> getOwnerByRefAndCreated(@PathVariable UUID userId, @PathVariable UUID refId, @PathVariable int stt, @PathVariable int sl) {
        try {
            int first = (stt - 1) * sl;
            List<Owner> owners = ownerService.getOwnerByRefAndCreated(refId, userId, first, sl);
            ResponseObject responseObject = new ResponseObject(true, "get Owner child user", owners);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/countByRef/{id}/{ref_id}")
    public ResponseEntity<?> countByRef(@PathVariable String id, @PathVariable String ref_id) {
        try {
            UUID user_id = UUID.fromString(id);
            UUID refId = UUID.fromString(ref_id);
            int ownerListCount = ownerService.countByRef(refId, user_id);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerListCount);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/countByCreated/{created_by}")
    public ResponseEntity<?> countByCreated(@PathVariable String created_by) {
        try {
            UUID id = UUID.fromString(created_by);
            int ownerListCount = ownerService.countByCreated(id);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerListCount);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findByCreated/{created_by}/{stt}/{sl}")
    public ResponseEntity<?> findByCreated(@PathVariable String created_by, @PathVariable int stt, @PathVariable int sl) {
        try {
            UUID id = UUID.fromString(created_by);
            int first = (stt - 1) * sl;
            List<OwnerDto> ownerDtoList = ownerService.findByCreated(id, first, sl);
            ResponseObject responseObject = new ResponseObject(true, "get Owner successful", ownerDtoList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/countByRefId/{ref_id}")
    public ResponseEntity<?> countByRefId(@PathVariable String ref_id) {
        try {
            UUID id = UUID.fromString(ref_id);
            int ownerListCount = ownerService.countByRefId(id);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerListCount);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findByRef/{ref_id}/{stt}/{sl}")
    public ResponseEntity<?> findByRef(@PathVariable String ref_id, @PathVariable int stt, @PathVariable int sl) {
        try {
            UUID id = UUID.fromString(ref_id);
            int first = (stt - 1) * sl;
            List<OwnerDto> ownerDtoList = ownerService.findByRef(id, first, sl);
            ResponseObject responseObject = new ResponseObject(true, "get Owner", ownerDtoList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findByCreatedAndPre/{pre}/{stt}/{sl}")
    public ResponseEntity<?> findByCreatedAndPre(@PathVariable int stt, @PathVariable int sl, @PathVariable String pre) {
        try {
            int first = (stt - 1) * sl;
            UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
            List<OwnerDto> ownerDtoList = ownerService.findByCreatedAndPre(id, first, sl, pre);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", ownerDtoList);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/owner/findParent/{role}")
    public ResponseEntity<?> findParent(@PathVariable String role) {
        try {
            UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
            List<OwnerDto> parentOwner = ownerService.findOwnerParent(id, role);
            ResponseObject responseObject = new ResponseObject(true, "get Owner of parent parent by user", parentOwner);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "get Owner fail", null);
            return new ResponseEntity<>(responseObject, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/owner/getDataByIncludes/{st}/{sl}")
    public ResponseEntity<?> getDataByIncludes(@RequestBody OwnerDto ownerDto, @PathVariable String st, @PathVariable String sl) {
        int first = (Integer.parseInt(st) - 1) * Integer.parseInt(sl);
        List<OwnerDto> ownerDtoList = ownerService.getDataByIncludes(ownerDto, first, Integer.parseInt(sl));
        ResponseObject responseObject = new ResponseObject(true, "Find owner user successfully", ownerDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/owner/countDataByIncludes")
    public ResponseEntity<?> countDataByIncludes(@RequestBody OwnerDto ownerDto) {
        Long count = ownerService.countAllOwnerUserByField(ownerDto);
        ResponseObject responseObject = new ResponseObject(true, "Find owner user successfully", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/owner/countAllOwnerByIds")
    public ResponseEntity<?> countAllOwnerByIds() {
        int count = ownerService.countAllOwnerByIds();
        ResponseObject responseObject = new ResponseObject(true, "count owner successfully", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/owner/countAllOwnerByIdsAndMode/{mode}")
    public ResponseEntity<?> countAllOwnerByIdsAndMode(@PathVariable String mode) {
        int count = ownerService.countAllOwnerByIdsAndMode(mode);
        ResponseObject responseObject = new ResponseObject(true, "count owner by mode successfully", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
