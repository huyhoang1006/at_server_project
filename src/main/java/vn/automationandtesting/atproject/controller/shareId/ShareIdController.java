package vn.automationandtesting.atproject.controller.shareId;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

@RestController
@RequestMapping("${api.prefix}")
public class ShareIdController {
    Logger logger = LoggerFactory.getLogger(ShareIdController.class);
    @Autowired
    private SharedListIdService sharedListIdService;

    @GetMapping("/shareId")
    public ResponseEntity<?> generateShareId() {
        sharedListIdService.reloadCachedIds(BearerContextHolder.getContext().getUserId(), BearerContextHolder.getContext().getRoleName());
        ResponseObject responseObject = new ResponseObject(true, "update shared id", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
