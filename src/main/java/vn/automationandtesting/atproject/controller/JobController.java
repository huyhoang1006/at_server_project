package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.JobDto;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;
import vn.automationandtesting.atproject.controller.dto.response.HealthIndexResponseDto;
import vn.automationandtesting.atproject.entity.Fmeca;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.service.AttachmentService;
import vn.automationandtesting.atproject.service.FmecaService;
import vn.automationandtesting.atproject.service.JobService;
import vn.automationandtesting.atproject.service.TestService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${api.prefix}")
public class JobController {
    private final JobService jobService;
    private final FmecaService fmecaService;

    @Autowired
    public AttachmentService attachmentService;

    @Autowired
    private TestService testService;

    @Autowired
    public JobController(JobService jobService, FmecaService fmecaService) {
        this.jobService = jobService;
        this.fmecaService = fmecaService;
    }

    //    @GetMapping("/jobsByLocalId/{localId}")
//    public ResponseEntity<?> getAllJobsByLocalId(@PathVariable String localId) {
//        UUID id = UUID.fromString(localId);
//        List<JobDto> jobDtoList = jobService.getAllJobsByLocalid(id);
//        ResponseObject responseObject = new ResponseObject(true, "Get all jobs by localID", jobDtoList);
//        return new ResponseEntity<>(responseObject, HttpStatus.OK);
//    }
    @GetMapping("/jobs/{jobId}")
    public ResponseEntity<?> checkJobExisted(@PathVariable String jobId) {
         List<JobDto> jobDtoList = jobService.getAllJobs();
         JobDto job = jobDtoList.stream().filter(jobDto -> jobDto.getId().toString().equals(jobId))
                .collect(Collectors.toList()).get(0);
        ResponseObject responseObject = new ResponseObject(true, "", job);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/jobsByAssetId/{assetId}")
    public ResponseEntity<?> findJobsByAssetId(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        List<JobDto> jobDtoList = jobService.findJobsByAssetId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get jobs by asset id", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobsByAssetSerialNo/{assetSerialNo}")
    public ResponseEntity<?> findJobsByAssetSerialNo(@PathVariable String assetSerialNo) {
        List<JobDto> jobDtoList = jobService.findJobsByAssetSerialNo(assetSerialNo);
        ResponseObject responseObject = new ResponseObject(true, "Get jobs by asset serial number", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobsByName/{jobName}")
    public ResponseEntity<?> findJobsByName(@PathVariable String jobName) {
        List<JobDto> jobDtoList = jobService.findJobsByName(jobName);
        ResponseObject responseObject = new ResponseObject(true, "Get jobs by name", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/all")
    public ResponseEntity<?> getAllJobs() {
        List<JobDto> jobDtoList = jobService.getAllJobs();
        ResponseObject responseObject = new ResponseObject(true, "Get all jobs", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/jobs")
    public ResponseEntity<?> createJob(@RequestBody JobDto jobDto) {
        JobDto savedJobDto = jobService.createNewJob(jobDto);
        ResponseObject responseObject = new ResponseObject(true, "Create job", savedJobDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/jobs/{jobId}")
    public ResponseEntity<?> updateJob(@RequestBody JobDto jobDto, @PathVariable String jobId) {
        UUID id = UUID.fromString(jobId);
        JobDto updatedJobDto = jobService.updateJob(jobDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update job", updatedJobDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/jobs/{jobId}")
    public ResponseEntity<?> deleteJob(@PathVariable String jobId) {
        UUID id = UUID.fromString(jobId);
        jobService.deleteJob(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/jobs/delete-multiple")
    public ResponseEntity<?> deleteMultiple(@RequestParam List<UUID> listId) {
        for (UUID id : listId) {
            try {
                List<TestDto> testDtoList = testService.findAllByJobId(id.toString());
                jobService.deleteJob(id);
                for(TestDto testDto : testDtoList) {
                    attachmentService.deleteAttachmentById_foreign(testDto.getId().toString());
                }
            } catch (Exception e) {
                System.out.println(e);
                // TODO: handle exception
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/jobs/lock")
    public ResponseEntity<?> lock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = jobService.lock(true, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/jobs/unlock")
    public ResponseEntity<?> unlock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = jobService.lock(false, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/jobs/upload")
    public ResponseEntity<?> upload(@RequestBody ResourceFullDto resourceFullDto) {
        ResponseObject responseObject = jobService.upload(resourceFullDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/download")
    public ResponseEntity<?> download(@RequestParam List<UUID> listId) {
        ResponseObject responseObject = jobService.download(listId);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/{jobId}/getHeathIndex/{fmecaId}")
    public ResponseEntity<?> getHealthIndex(@PathVariable UUID jobId, @PathVariable UUID fmecaId) {
        Fmeca fmeca = fmecaService.getFmecaTableById(fmecaId);
        Job job = jobService.getJobWithHealthIndex(jobId, fmeca.getTableCalculate());
        float averageHealthIndex = job.getAverageHealthIndex();
        float worstHealthIndex = job.getWorstHealthIndex();
        ResponseObject responseObject = new ResponseObject(true, "Get health index",
                new HealthIndexResponseDto(averageHealthIndex, worstHealthIndex));
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/jobs/{jobId}/updateCollabs")
    public ResponseEntity<?> updateJobCollabs(@PathVariable String jobId, @RequestBody ListIdDto listIdDto) {
        jobService.updateJobCollabs(jobId, listIdDto);
        ResponseObject responseObject = new ResponseObject(true, "Update collaborators successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/findJobByAssetId/{assetId}/{stt}/{sl}")
    public ResponseEntity<?> findJobByAssetId(@PathVariable String assetId, @PathVariable int stt, @PathVariable int sl) {
        UUID asset_id = UUID.fromString(assetId);
        int first = (stt - 1) * sl;
        List<JobDto> assetDtoList = jobService.findJobByAssetId(asset_id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get Job by asset ID", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/jobs/countJobByAssetId/{assetId}")
    public ResponseEntity<?> countJobByAssetId(@PathVariable String assetId) {
        UUID asset_id = UUID.fromString(assetId);
        int count = jobService.countJobByAssetId(asset_id);
        ResponseObject responseObject = new ResponseObject(true, "Count job by asset id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
