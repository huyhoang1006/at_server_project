package vn.automationandtesting.atproject.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.OwnerUserMapper;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.service.OwnerUserService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class OwnerUserController {

    private final OwnerUserService ownerUserService;

    @Autowired
    public OwnerUserController(OwnerUserService ownerUserService) {
        this.ownerUserService = ownerUserService;
    }
    @Autowired
    private OwnerUserMapper ownerUserMapper;

    @PostMapping("/ownerUser/created")
    public ResponseEntity<?> createOwnerUser(@RequestBody OwnerUserDto ownerUserDto) {
        ResponseObject responseObject = ownerUserService.saveNewOwnerUser(ownerUserDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/ownerUser/{id}")
    public ResponseEntity<?> getOwnerUserById(@PathVariable String id) {
        UUID owner_user_id = UUID.fromString(id);
        OwnerUserDto ownerUserDto = ownerUserService.getOwnerUserById(owner_user_id);
        ResponseObject responseObject = new ResponseObject(true, "Get owner user by id", ownerUserDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/ownerUser/findOwnerUserByCreated/{ownerUserId}/{st}/{sl}")
    public ResponseEntity<?> findOwnerUserByCreated(@PathVariable String ownerUserId, @PathVariable String st, @PathVariable String sl) {
        UUID id = UUID.fromString(ownerUserId);
        int first = (Integer.parseInt(st) - 1) * Integer.parseInt(sl);
        List<OwnerUserDto> ownerUserDtos = ownerUserService.findOwnerUserByCreated(id, first, Integer.parseInt(sl));
        ResponseObject responseObject = new ResponseObject(true, "Get owner user by id", ownerUserDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/ownerUser/countOwnerUserByCreated/{ownerUserId}")
    public ResponseEntity<?> countOwnerUserByCreated(@PathVariable String ownerUserId) {
        UUID id = UUID.fromString(ownerUserId);
        int count = ownerUserService.countOwnerUserByCreated(id);
        ResponseObject responseObject = new ResponseObject(true, "Get owner user by id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/ownerUser/ownerUserAllInfo/{ownerUserId}")
    public ResponseEntity<?> updateOwnerUserAllInfo(@RequestBody OwnerUserDto ownerUserDto, @PathVariable String ownerUserId) {
        UUID id = UUID.fromString(ownerUserId);
        OwnerUserDto updatedOwnerUserReqDto = ownerUserService.updateOwnerUserAllInfo(ownerUserDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update owner user", updatedOwnerUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/ownerUser/{ownerUserId}")
    public ResponseEntity<?> getOwnerUser(@PathVariable String ownerUserId) {
        UUID id = UUID.fromString(ownerUserId);
        ownerUserService.deleteOwnerUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete owner user successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/ownerUser/remove/{ownerUserId}")
    public ResponseEntity<?> removeOwnerUser(@PathVariable String ownerUserId) {
        UUID id = UUID.fromString(ownerUserId);
        ownerUserService.removeOwnerUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Remove owner user successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/ownerUser/getDataByIncludes/{st}/{sl}")
    public ResponseEntity<?> getDataByIncludes(@RequestBody OwnerUserDto ownerUserDto, @PathVariable String st, @PathVariable String sl) {
        int first = (Integer.parseInt(st) - 1) * Integer.parseInt(sl);
        List<OwnerUserDto> ownerUserDtos = ownerUserService.getDataByIncludes(ownerUserDto, first, Integer.parseInt(sl));
        ResponseObject responseObject = new ResponseObject(true, "Find owner user successfully", ownerUserDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/ownerUser/countDataByIncludes")
    public ResponseEntity<?> countDataByIncludes(@RequestBody OwnerUserDto ownerUserDto) {
        Long count = ownerUserService.countAllOwnerUserByField(ownerUserDto);
        ResponseObject responseObject = new ResponseObject(true, "Find owner user successfully", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
