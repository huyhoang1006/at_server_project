package vn.automationandtesting.atproject.controller.current;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.current.TestCurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.current.TestCurrentMapper;
import vn.automationandtesting.atproject.entity.current.TestsCurrent;
import vn.automationandtesting.atproject.service.current.TestCurrentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestCurrentController {
    @Autowired
    private TestCurrentMapper mapper;

    @Autowired
    private TestCurrentService service;

    @GetMapping("/current/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestCurrentDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestCurrentDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestCurrentDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestCurrentDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/current/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestCurrentDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestCurrentDto> testDto = service.findAllTestByJobId(job_id);
        List<TestCurrentDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestCurrentDto item : testDto) {
                boolean sign = false;
                for (TestCurrentDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestsCurrent> testDelete = new ArrayList<>();
            for(TestCurrentDto item : testDeleteDto) {
                TestsCurrent itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestsCurrent> tests = new ArrayList<>();
        for(TestCurrentDto itemDto : listTest) {
            TestsCurrent test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/current/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestCurrentDto> listTest) {
        List<TestsCurrent> tests = new ArrayList<>();
        for(TestCurrentDto itemDto : listTest) {
            TestsCurrent test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
