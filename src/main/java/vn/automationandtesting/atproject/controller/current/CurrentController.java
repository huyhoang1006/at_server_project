package vn.automationandtesting.atproject.controller.current;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.circuit.CircuitController;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.current.CurrentMapper;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.service.current.CurrentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class CurrentController {
    Logger logger = LoggerFactory.getLogger(CircuitController.class);

    @Autowired
    private CurrentService service;

    @Autowired
    private CurrentMapper mapper;

    @GetMapping("/current/findById/{_id}")
    public ResponseEntity<?> getAssetByID(@PathVariable String _id) {
        UUID id = UUID.fromString(_id);
        List<CurrentDto> dtoList = service.findAssetById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/getAssetById/{id}")
    public ResponseEntity<?> getAssetById(@PathVariable String id) {
        UUID assetId = UUID.fromString(id);
        try {
            CurrentDto assetDto = service.getAssetById(assetId, BearerContextHolder.getContext().getUserId());
            ResponseObject responseObject = new ResponseObject(true, "Get current by ID", assetDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "Get current by ID", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/current/findAll")
    public ResponseEntity<?> getAll() {
        List<CurrentDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all asset", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findByLocationId/{locationId}")
    public ResponseEntity<?> findAllByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        List<CurrentDto> dtoList = service.findAllByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findAssetByLocationId/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetByLocationId(@PathVariable String locationId, @PathVariable int stt, @PathVariable int sl) {
        UUID location_Id = UUID.fromString(locationId);
        int first = (stt - 1) * sl;
        List<CurrentDto> currentDtoList = service.findAssetByLocationId(location_Id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", currentDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/countAssetByLocationId/{locationId}")
    public ResponseEntity<?> countAssetByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        int count = service.countAssetByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Count curent by location id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/countAssetList")
    public ResponseEntity<?> countAssetList() {
        int count = service.countAssetList();
        ResponseObject responseObject = new ResponseObject(true, "Count current transformer", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findAssetList/{stt}/{sl}")
    public ResponseEntity<?> findAssetList(@PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<CurrentDto> assetDtoList = service.findAssetList(first, sl);
        ResponseObject responseObject = new ResponseObject(true, "get current transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findAssetListOffset/{stt}/{sl}")
    public ResponseEntity<?> findAssetListOffset(@PathVariable int stt, @PathVariable int sl) {
        List<CurrentDto> assetDtoList = service.findAssetList(stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get current transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId,@PathVariable int stt, @PathVariable int sl) {
        List<CurrentDto> assetDtoList = service.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get current", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        int count = service.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count current", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findByLocationIdAndCollab/{locationId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        List<CurrentDto> dtoList = service.findByLocationIdAndCollabsContaining(location_Id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findBySerial/{serial}")
    public ResponseEntity<?> findBySerial(@PathVariable String serial) {
        List<CurrentDto> dtoList = service.findBySerial(serial);
        ResponseObject responseObject = new ResponseObject(true, "Get asset serial", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/current/findBySerial/{serial}/{locationId}")
    public ResponseEntity<?> findBySerialAndLocation(@PathVariable(name = "serial") String serial, @PathVariable(name = "locationId") String locationId) {
        UUID location_id = UUID.fromString(locationId);
        List<CurrentDto> dtoList = service.findBySerialAndLocation(serial, location_id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by serial and location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/current/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        CurrentDto assets = service.findAssetById(UUID.fromString(id)).get(0);
        List<Current> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            assets.setLocked(true);
            assets.setLocked_by(UUID.fromString(userId));
            Current item = mapper.assetDtoToAsset(assets);
            item.setCollabs(assets.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(assets.getLocked_by().toString().equals(userId)) {
                assets.setLocked(false);
                assets.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                Current item = mapper.assetDtoToAsset(assets);
                item.setCollabs(assets.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }

        ResponseObject responseObject = new ResponseObject(true, "sign lock asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/current/save")
    public ResponseEntity<?> insertAsset(@RequestBody List<CurrentDto> dtoList) {
        List<Current> assets = new ArrayList<>();
        for(CurrentDto items : dtoList) {
            List<CurrentDto> item = new ArrayList<>();
            if(items.getId() != null && items.getId().toString().isEmpty()) {
                item = service.findAssetById(items.getId());
            }
            Current asset = new Current();
            if(item.isEmpty()) {
                asset = mapper.assetDtoToAsset(items);
                if(asset.getId() == null || asset.getId().toString().isEmpty()) {
                    asset.setId(UUID.randomUUID());
                }
            } else {
                asset = mapper.copyAssetDtoToAsset(item.get(0), asset);
                asset = mapper.copyAssetDtoToAsset(items, asset, "createdOn", "createdBy", "locked", "collabs");
            }

            if(!asset.isLocked()) {
                assets.add(asset);
            }
        }
        if(!assets.isEmpty()) {
            service.saveAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "save asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/current/updateAssetLite/{assetId}")
    public ResponseEntity<?> updateAssetLite(@RequestBody CurrentDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        CurrentDto updatedAssetDto = service.updateAssetLite(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update current", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/current/delete")
    public ResponseEntity<?> deleteAsset(@RequestBody List<CurrentDto> dtoList) {
        List<Current> assets = new ArrayList<>();
        for(CurrentDto items : dtoList) {
            List<CurrentDto> item = service.findAssetById(items.getId());
            if(!item.get(0).isLocked()) {
                Current asset = mapper.assetDtoToAsset(items);
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.deleteAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
