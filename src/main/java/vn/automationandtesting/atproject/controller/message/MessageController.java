package vn.automationandtesting.atproject.controller.message;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.message.GroupListDto;
import vn.automationandtesting.atproject.controller.dto.message.MessageDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserMessageDto;
import vn.automationandtesting.atproject.controller.dto.request.UserMessageDto;
import vn.automationandtesting.atproject.entity.message.GroupList;
import vn.automationandtesting.atproject.service.OwnerUserService;
import vn.automationandtesting.atproject.service.UserService;
import vn.automationandtesting.atproject.service.message.FirebaseService;
import vn.automationandtesting.atproject.service.message.GroupListService;
import vn.automationandtesting.atproject.service.message.MessageService;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("${api.prefix}")
public class MessageController {
    @Autowired
    private UserService userService;

    @Autowired
    private OwnerUserService ownerUserService;

    @Autowired
    private GroupListService groupListService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private FirebaseService firebaseService;


    @GetMapping("/message/{data}")
    public ResponseEntity<?> getUserByNameAndEmail(@PathVariable String data) {
        List<UserMessageDto> userMessageDtoList = userService.searchMessageUser(data);
        List<OwnerUserMessageDto> ownerUserMessageDtoList = ownerUserService.searchMessageUser(data);
        List<Object> dataList = new ArrayList<>();
        dataList.addAll(userMessageDtoList);
        dataList.addAll(ownerUserMessageDtoList);
        ResponseObject responseObject = new ResponseObject(true, "Get data by name and email", dataList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/message/insert")
    public ResponseEntity<?> insertGroupList(@RequestBody GroupListDto groupListDto) {
        GroupListDto groupListDto1 = groupListService.saveGroupList(groupListDto);
        ResponseObject responseObject = new ResponseObject(true, "insert group list", groupListDto1);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/firebase/markAsRead/{id}")
    public ResponseEntity<?> markAsRead(@PathVariable String id) {
        try {
            firebaseService.markAsRead(BearerContextHolder.getContext().getUserId(), id);
            ResponseObject responseObject = new ResponseObject(true, "mark as read", true);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "mark as read", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @PostMapping("/message/firebase/markAsRead")
    public ResponseEntity<?> markAsReadArr(@RequestBody List<String> ids) {
        try {
            firebaseService.markAsReadArr(BearerContextHolder.getContext().getUserId(), ids);
            ResponseObject responseObject = new ResponseObject(true, "mark as read", true);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "mark as read", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @PostMapping("/message/markAsSeenArr")
    public ResponseEntity<?> markAsSeenArr(@RequestBody List<UUID> ids) {
        try {
            messageService.markAsSeenArr(ids);
            ResponseObject responseObject = new ResponseObject(true, "mark as read", true);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            ResponseObject responseObject = new ResponseObject(true, "mark as read", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @PostMapping("/message/content/insert")
    public ResponseEntity<?> insertMessage(@RequestBody MessageDto messageDto) {
        MessageDto saveMessage = messageService.saveMessage(messageDto);
        ResponseObject responseObject = new ResponseObject(true, "insert message", saveMessage);
        BearerContext bearerContext = BearerContextHolder.getContext();
        UUID userId = UUID.fromString(bearerContext.getUserId());
        CompletableFuture.runAsync(() -> {
            try {
                List<GroupListDto> groupListDto = groupListService.getGroupListById(saveMessage.getGroupId());
                if (!groupListDto.isEmpty()) {
                    GroupListDto groupListDtoData = groupListDto.get(0);
                    groupListDtoData.setLastMessage(saveMessage.getContent());
                    groupListDtoData.setLastMessageAt(saveMessage.getCreatedOn());
                    groupListDtoData.setLastMessageReceiveMember(new ArrayList<>());
                    groupListDtoData.setLastMessageCreatedBy(userId);
                    groupListService.saveGroupList(groupListDtoData);
                    List<String> uuidList = groupListDtoData.getMember();
                    if (!uuidList.isEmpty()) {
                        for (String member : uuidList) {
                            if (!member.equals(bearerContext.getUserId())) {
                                firebaseService.insertNotification(member, "New message", "New message", "message", false, saveMessage, bearerContext);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        });
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/content/getMessage/{groupId}/{stt}/{sl}")
    public ResponseEntity<?> getMessage(@PathVariable String groupId, @PathVariable int stt, @PathVariable int sl) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        UUID group_id = UUID.fromString(groupId);
        List<MessageDto> messageDtoList = messageService.getMessage(user_id, group_id, stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get message list", messageDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/content/getAllMessageByUnread/{groupId}")
    public ResponseEntity<?> getAllMessageByUnread(@PathVariable String groupId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        UUID group_id = UUID.fromString(groupId);
        List<MessageDto> messageDtoList = messageService.getAllMessageByUnread(user_id, group_id);
        ResponseObject responseObject = new ResponseObject(true, "get message list", messageDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/content/getMessageById/{id}/{groupId}")
    public ResponseEntity<?> getMessageById(@PathVariable String id, @PathVariable String groupId) {
        String userId = BearerContextHolder.getContext().getUserId();
        MessageDto messageDto = messageService.getMessageById(groupId, id, userId);
        ResponseObject responseObject = new ResponseObject(true, "get message ", messageDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/getMessage/{stt}/{sl}")
    public ResponseEntity<?> getGroupList(@PathVariable int stt, @PathVariable int sl) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<GroupListDto> groupListDtoList = groupListService.getGroupList(user_id, stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get group list", groupListDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/message/updateMessage/{lastMessage}/{id}")
    public ResponseEntity<?> updateMessage(@RequestParam String lastMessageAt ,@PathVariable String lastMessage, @PathVariable String id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        UUID group_id = UUID.fromString(id);
        Instant instantTime = Instant.parse(lastMessageAt);
        Timestamp lastMessageAtTime = Timestamp.from(instantTime);
        GroupListDto groupListDtoList = groupListService.updateGroup(group_id, user_id, lastMessage, lastMessageAtTime);
        ResponseObject responseObject = new ResponseObject(true, "update group list", groupListDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/deleteGroup/{id}")
    public ResponseEntity<?> deleteGroup(@PathVariable String id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        UUID group_id = UUID.fromString(id);
        Boolean data = groupListService.deleteGroup(group_id, user_id);
        ResponseObject responseObject = new ResponseObject(true, "delete group", data);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/message/rename/{id}/{name}")
    public ResponseEntity<?> rename(@PathVariable String id, @PathVariable String name) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID group_id = UUID.fromString(id);
        GroupListDto data = groupListService.renameGroup(group_id, userId, name);
        ResponseObject responseObject = new ResponseObject(true, "rename group", data);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/firebase/checkIfExist/{name}")
    public ResponseEntity<?> checkIfExistCollection(@PathVariable String name) {
        String userId = BearerContextHolder.getContext().getUserId();
        Boolean check;
        if(userId.equals(name)) {
            check = firebaseService.checkCollection(name);
            ResponseObject responseObject = new ResponseObject(true, "check exist collection", check);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } else {
            ResponseObject responseObject = new ResponseObject(true, "check exist collection", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/message/firebase/insert/{name}")
    public ResponseEntity<?> insertCollection(@PathVariable String name) {
        String userId = BearerContextHolder.getContext().getUserId();
        Boolean check;
        if(userId.equals(name)) {
            check = firebaseService.checkCollection(name);
            ResponseObject responseObject = new ResponseObject(true, "check exist collection", check);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } else {
            ResponseObject responseObject = new ResponseObject(true, "check exist collection", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/message/getSingleGroupListByTypeAndId/{type}/{other}")
    public ResponseEntity<?> getSingleGroupListByTypeAndId(@PathVariable String type, @PathVariable String other) {
        String userId = BearerContextHolder.getContext().getUserId();
        List<GroupListDto> groupListDtoList = groupListService.getSingleGroupListByTypeAndId(type, userId, other);
        ResponseObject responseObject = new ResponseObject(true, "get group list", groupListDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/getGroupListByTypeAndId/{type}")
    public ResponseEntity<?> getGroupListByTypeAndId(@PathVariable String type) {
        String userId = BearerContextHolder.getContext().getUserId();
        List<GroupListDto> groupListDtoList = groupListService.getGroupListByTypeAndId(type, userId);
        ResponseObject responseObject = new ResponseObject(true, "get group list", groupListDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/message/getMemberOfGroup/{group}")
    public ResponseEntity<?> getMemberOfGroup(@PathVariable String group) {
        UUID userId = UUID.fromString(BearerContextHolder.getContext().getUserId());
        UUID groupId = UUID.fromString(group);
        List<Object> member = groupListService.getMemberOfGroupById(groupId, userId);
        ResponseObject responseObject = new ResponseObject(true, "get member list", member);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/message/addMemberToGroup/{groupId}")
    public ResponseEntity<?> addMemberToGroup(@RequestBody List<UUID> ids, @PathVariable String groupId) {
        UUID userId = UUID.fromString(BearerContextHolder.getContext().getUserId());
        GroupListDto groupListDto = groupListService.addMemberToGroup(ids, userId, groupId);
        ResponseObject responseObject = new ResponseObject(true, "add member to group", groupListDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/message/deleteMember/{groupId}/{id}")
    public ResponseEntity<?> deleteMember(@PathVariable UUID groupId, @PathVariable UUID id) {
        UUID userId = UUID.fromString(BearerContextHolder.getContext().getUserId());
        GroupListDto groupListDto = groupListService.deleteMember(id, userId, groupId);
        ResponseObject responseObject = new ResponseObject(true, "delete member in group", groupListDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
