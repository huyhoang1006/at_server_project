package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.FmecaDto;
import vn.automationandtesting.atproject.service.FmecaService;

import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@RestController
@RequestMapping("${api.prefix}")
public class FmecaController {
    private final FmecaService fmecaService;

    @Autowired
    public FmecaController(FmecaService fmecaService) {
        this.fmecaService = fmecaService;
    }

    @PostMapping("/fmeca")
    public ResponseEntity<?> createFmecaTable(@RequestBody FmecaDto fmecaDto) {
        FmecaDto savedFmecaDto = fmecaService.createFmeca(fmecaDto);
        ResponseObject responseObject = new ResponseObject(true, "Create fmeca", savedFmecaDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/fmeca/GetOrInit")
    public ResponseEntity<?> getOrInit() {
        ResponseObject responseObject = fmecaService.getOrInit();
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/fmeca/{fmecaId}")
    public ResponseEntity<?> createFmecaTable(@PathVariable String fmecaId, @RequestBody FmecaDto fmecaDto) {
        UUID id = UUID.fromString(fmecaId);
        FmecaDto updatedFmecaDto = fmecaService.updateFmeca(id, fmecaDto);
        ResponseObject responseObject = new ResponseObject(true, "Create fmeca", updatedFmecaDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
