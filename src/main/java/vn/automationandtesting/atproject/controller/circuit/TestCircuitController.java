package vn.automationandtesting.atproject.controller.circuit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.circuit.TestCircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.TestCircuitMapper;
import vn.automationandtesting.atproject.entity.circuit.TestCircuit;
import vn.automationandtesting.atproject.service.circuit.TestCircuitService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestCircuitController {
    @Autowired
    private TestCircuitMapper mapper;

    @Autowired
    private TestCircuitService service;

    @GetMapping("/circuit/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestCircuitDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestCircuitDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestCircuitDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestCircuitDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestCircuitDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestCircuitDto> testDto = service.findAllTestByJobId(job_id);
        List<TestCircuitDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestCircuitDto item : testDto) {
                boolean sign = false;
                for (TestCircuitDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestCircuit> testDelete = new ArrayList<>();
            for(TestCircuitDto item : testDeleteDto) {
                TestCircuit itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestCircuit> tests = new ArrayList<>();
        for(TestCircuitDto itemDto : listTest) {
            TestCircuit test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestCircuitDto> listTest) {
        List<TestCircuit> tests = new ArrayList<>();
        for(TestCircuitDto itemDto : listTest) {
            TestCircuit test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
