package vn.automationandtesting.atproject.controller.circuit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.CircuitMapper;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.service.circuit.CircuitService;

import java.util.*;

@Controller
@RequestMapping("${api.prefix}")
public class CircuitController {
    Logger logger = LoggerFactory.getLogger(CircuitController.class);

    @Autowired
    private CircuitService circuitService;

    @Autowired CircuitMapper circuitMapper;

    @GetMapping("/circuit/findById/{circuitId}")
    public ResponseEntity<?> getCircuitByID(@PathVariable String circuitId) {
        UUID id = UUID.fromString(circuitId);
        List<CircuitDto> circuitDtoList = circuitService.findAssetById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by ID", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/getAssetById/{id}")
    public ResponseEntity<?> getAssetById(@PathVariable String id) {
        UUID assetId = UUID.fromString(id);
        try {
            CircuitDto assetDto = circuitService.getAssetById(assetId, BearerContextHolder.getContext().getUserId());
            ResponseObject responseObject = new ResponseObject(true, "Get circuit by ID", assetDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "Get circuit by ID", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/circuit/findAll")
    public ResponseEntity<?> getAll() {
        List<CircuitDto> circuitDtoList = circuitService.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all circuit", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findByLocationId/{locationId}")
    public ResponseEntity<?> findAllByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        List<CircuitDto> circuitDtoList = circuitService.findAllByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findAssetByLocationId/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetByLocationId(@PathVariable String locationId, @PathVariable int stt, @PathVariable int sl) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        int first = (stt - 1) * sl;
        List<CircuitDto> circuitDtoList = circuitService.findAssetByLocationId(location_Id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/countAssetByLocationId/{locationId}")
    public ResponseEntity<?> countAssetByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        int count = circuitService.countAssetByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Count circuit by location id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/countAssetList")
    public ResponseEntity<?> countAssetList() {
        int count = circuitService.countAssetList();
        ResponseObject responseObject = new ResponseObject(true, "Count circuit breaker", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findAssetList/{stt}/{sl}")
    public ResponseEntity<?> findAssetList(@PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<CircuitDto> assetDtoList = circuitService.findAssetList(first, sl);
        ResponseObject responseObject = new ResponseObject(true, "get circuit breaker", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findAssetListOffset/{stt}/{sl}")
    public ResponseEntity<?> findAssetListOffset(@PathVariable int stt, @PathVariable int sl) {
        List<CircuitDto> assetDtoList = circuitService.findAssetList(stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get circuit breaker", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }


    @GetMapping("/circuit/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId,@PathVariable int stt, @PathVariable int sl) {
        List<CircuitDto> assetDtoList = circuitService.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get circuit", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        int count = circuitService.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count circuit", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findByLocationIdAndCollab/{locationId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        List<CircuitDto> circuitDtoList = circuitService.findByLocationIdAndCollabsContaining(location_Id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findBySerial/{serial}")
    public ResponseEntity<?> findBySerial(@PathVariable String serial) {
        List<CircuitDto> circuitDtoList = circuitService.findBySerial(serial);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit serial", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/findBySerial/{serial}/{locationId}")
    public ResponseEntity<?> findBySerialAndLocation(@PathVariable(name = "serial") String serial, @PathVariable(name = "locationId") String locationId) {
        UUID location_id = UUID.fromString(locationId);
        List<CircuitDto> circuitDtoList = circuitService.findBySerialAndLocation(serial, location_id);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by serial and location id", circuitDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        CircuitDto assets = circuitService.findAssetById(UUID.fromString(id)).get(0);
        List<Circuit> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            assets.setLocked(true);
            assets.setLocked_by(UUID.fromString(userId));
            Circuit item = circuitMapper.circuitDtoToCircuit(assets);
            item.setCollabs(assets.getCollabs());
            assetItem.add(item);
            circuitService.saveAll(assetItem);
        } else {
            if(assets.getLocked_by().toString().equals(userId)) {
                assets.setLocked(false);
                assets.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                Circuit item = circuitMapper.circuitDtoToCircuit(assets);
                item.setCollabs(assets.getCollabs());
                assetItem.add(item);
                circuitService.saveAll(assetItem);
            }
        }

        ResponseObject responseObject = new ResponseObject(true, "sign lock circuit", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/circuit/updateAssetLite/{assetId}")
    public ResponseEntity<?> updateAssetLite(@RequestBody CircuitDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        CircuitDto updatedAssetDto = circuitService.updateAssetLite(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update circuit", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/save")
    public ResponseEntity<?> insertCircuit(@RequestBody List<CircuitDto> circuitDtoList) {
        List<Circuit> circuits = new ArrayList<>();
        for(CircuitDto circuitDto : circuitDtoList) {
            List<CircuitDto> item = new ArrayList<>();
            if(circuitDto.getId() != null && circuitDto.getId().toString().isEmpty()) {
                item = circuitService.findAssetById(circuitDto.getId());
            }
            Circuit circuit = new Circuit();
            if(item.isEmpty()) {
                circuit = circuitMapper.circuitDtoToCircuit(circuitDto);
                if(circuit.getId() == null || circuit.getId().toString().isEmpty()) {
                    circuit.setId(UUID.randomUUID());
                }
            } else {
                circuit = circuitMapper.copyCircuitDtoToCircuit(item.get(0), circuit);
                circuit = circuitMapper.copyCircuitDtoToCircuit(circuitDto, circuit, "createdOn", "createdBy", "locked", "collabs");
            }
            if(!circuit.isLocked()) {
                circuits.add(circuit);
            }
        }
        if(!circuits.isEmpty()) {
            circuitService.saveAll(circuits);
        }
        ResponseObject responseObject = new ResponseObject(true, "save circuit", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/delete")
    public ResponseEntity<?> deleteCircuit(@RequestBody List<CircuitDto> circuitDtoList) {
        List<Circuit> circuits = new ArrayList<>();
        for(CircuitDto circuitDto : circuitDtoList) {
            List<CircuitDto> asset = circuitService.findAssetById(circuitDto.getId());
            if(!asset.get(0).isLocked()) {
                Circuit circuit = circuitMapper.circuitDtoToCircuit(circuitDto);
                circuits.add(circuit);
            }
        }
        if(circuits.size() != 0) {
            circuitService.deleteAll(circuits);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete circuit", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
