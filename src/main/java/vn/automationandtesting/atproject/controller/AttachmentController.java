package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.AttachmentMapper;
import vn.automationandtesting.atproject.entity.Attachment;
import vn.automationandtesting.atproject.service.AttachmentService;

import java.util.List;
import java.util.UUID;


@Controller
@RequestMapping("${api.prefix}")
public class AttachmentController {

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private AttachmentMapper attachmentMapper;

    @PostMapping("/attachment/upload")
    public ResponseEntity<?> insertAttachment(@RequestBody List<AttachmentDto> attachmentDtos) {
        for (AttachmentDto attachmentDto : attachmentDtos) {
            Attachment attachment = attachmentMapper.AttachmentDtoToAttachment(attachmentDto);
            Boolean check = attachmentService.findById(attachment.getId_foreign());
            if(check) {
                attachmentService.updateNameById_foreign(attachment);
            } else {
                attachmentService.insertAttachment(attachment);
            }
        }
        ResponseObject responseObject = ResponseObject.returnWithData(true);;
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/attachment/delete")
    public ResponseEntity<?> deleteAttachment(@RequestBody List<AttachmentDto> attachmentDtos) {
        for (AttachmentDto attachmentDto : attachmentDtos) {
            attachmentService.deleteAttachmentById_foreign(attachmentDto.getId_foreign());
        }
        ResponseObject responseObject = ResponseObject.returnWithData(true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
    @GetMapping("/attachment/getAll/{id_foreign}")
    public ResponseEntity<?> getAttachment(@PathVariable String id_foreign) {
        List<AttachmentDto> attachmentDto = attachmentService.findAllById_foreign(id_foreign);
        ResponseObject responseObject = ResponseObject.returnWithData(attachmentDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/attachment/getName/{id_foreign}")
    public ResponseEntity<?> getNameAttachment(@PathVariable String id_foreign) {
       String name = attachmentService.findNamebyId_foreign(UUID.fromString(id_foreign));
       System.out.println(name);
        ResponseObject responseObject = ResponseObject.returnWithData(name);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
