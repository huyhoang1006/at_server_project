package vn.automationandtesting.atproject.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.MonitoringDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.service.MonitoringService;

import java.util.List;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
@RestController
@RequestMapping("${api.prefix}")
public class MonitoringController {
    private final MonitoringService monitoringService;

    @Autowired
    public MonitoringController(MonitoringService monitoringService) {
        this.monitoringService = monitoringService;
    }

    @GetMapping("/monitoring")
    public ResponseEntity<?> searchMonitoringByAssetId(@RequestParam(required = false) String assetId) {
        List<MonitoringDto> monitoringDtoList = null;
        if(assetId != null) {
            monitoringDtoList = monitoringService.findMonitoringByAssetId(assetId);
        } else {
            monitoringDtoList = monitoringService.getAllMonitoring();
        }
        ResponseObject responseObject = new ResponseObject(true, "Search monitoring by asset ID", monitoringDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/monitoring/getAll/{assetId}")
    public ResponseEntity<?> searchMonitoringDescByAssetId(@PathVariable String assetId) {
        List<MonitoringDto> monitoringDtoList = null;
        if(assetId != null) {
            monitoringDtoList = monitoringService.findMonitoringdescByAssetId(assetId);
        }
        ResponseObject responseObject = new ResponseObject(true, "Search monitoring by asset ID", monitoringDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/monitoring/getLast/{assetId}")
    public ResponseEntity<?> searchLastMonitoringAscByAssetId(@PathVariable String assetId) {
        List<MonitoringDto> monitoringDtoList = null;
        if(assetId != null) {
            monitoringDtoList = monitoringService.findLastMonitoringdescByAssetId(assetId);
        }
        ResponseObject responseObject = new ResponseObject(true, "Search monitoring by asset ID", monitoringDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/monitoring/{id}")
    public ResponseEntity<?> searchMonitoringById(@PathVariable String id) {
        MonitoringDto monitoringDto = monitoringService.findMonitoringById(id);
        ResponseObject responseObject = new ResponseObject(true, "Search monitoring by ID", monitoringDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/monitoring")
    public ResponseEntity<?> createMonitoring(@RequestBody Object value) throws JsonProcessingException, JSONException {
        ObjectMapper mapper = new ObjectMapper();
        String monitorString = mapper.writeValueAsString(value);
        JSONObject monitorObject = new JSONObject(monitorString);
        MonitoringDto savedMonitoringDto = monitoringService.createMonitoring(monitorObject);
        ResponseObject responseObject = new ResponseObject(true, "Create monitoring", savedMonitoringDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/monitoring/{id}")
    public ResponseEntity<?> updateMonitoring(@PathVariable String id, @RequestBody MonitoringDto monitoringDto) {
        MonitoringDto updatedMonitoringDto = monitoringService.updateMonitoring(id, monitoringDto);
        ResponseObject responseObject = new ResponseObject(true, "Create monitoring", updatedMonitoringDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

        @DeleteMapping("/monitoring/{id}")
    public ResponseEntity<?> deleteMonitoringById(@PathVariable String id) {
        monitoringService.deleteMonitoringById(id);
        ResponseObject responseObject = new ResponseObject(true, "Successfully deleted", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
