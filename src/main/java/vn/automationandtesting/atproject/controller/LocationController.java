package vn.automationandtesting.atproject.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.common.LocationOwnerDto;
import vn.automationandtesting.atproject.controller.dto.message.GroupListDto;
import vn.automationandtesting.atproject.controller.dto.objectLog.location.AuditLocationLogDto;
import vn.automationandtesting.atproject.controller.dto.staging.location.LocationStagingDto;
import vn.automationandtesting.atproject.entity.LocationStaging;
import vn.automationandtesting.atproject.service.AttachmentService;
import vn.automationandtesting.atproject.service.LocationService;
import vn.automationandtesting.atproject.service.message.FirebaseService;
import vn.automationandtesting.atproject.util.common.JsonProcessing;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${api.prefix}")
public class LocationController {
    private final LocationService locationService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @Autowired
    private FirebaseService firebaseService;

    @Autowired
    private JsonProcessing jsonProcessing;

    @PostMapping("/locations")
    public ResponseEntity<?> createLocation(@RequestBody LocationDto locationDto) {
        LocationDto savedLocationDto = locationService.createLocation(locationDto);
        ResponseObject responseObject = new ResponseObject(true, "Create location", savedLocationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/getLocationInList")
    public ResponseEntity<?> getListLocation(@RequestBody List<UUID> listId) {
        List<Object> savedLocationDto = locationService.getListLocation(listId);
        ResponseObject responseObject = new ResponseObject(true, "Get location", savedLocationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/download")
    public ResponseEntity<?> download(@RequestParam List<UUID> listId) {
        ResponseObject responseObject = locationService.download(listId);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/upload")
    public ResponseEntity<?> upload(@RequestBody ResourceFullDto resourceFullDto) {
        ResponseObject responseObject = locationService.upload(resourceFullDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/lock")
    public ResponseEntity<?> lock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = locationService.lock(true, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/unlock")
    public ResponseEntity<?> unlock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = locationService.lock(false, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/{locationId}")
    public ResponseEntity<?> updateLocation(@PathVariable String locationId, @RequestBody LocationDto locationDto) {
        UUID id = UUID.fromString(locationId);
        try {
            Map<String, Object> mapData = locationService.updateLocation(id, locationDto);
            LocationDto updatedLocationDto = (LocationDto) mapData.get("location");
            LocationStagingDto locationStagingDto = (LocationStagingDto) mapData.get("location_staging");
            AuditLocationLogDto auditLocationLogDto = (AuditLocationLogDto) mapData.get("audit_location_log");
            BearerContext bearerContext = BearerContextHolder.getContext();
            CompletableFuture.runAsync(() -> {
                try {
                    List<String> uuidList = new ArrayList<>(updatedLocationDto.getCollabs());
                    if (!uuidList.isEmpty()) {
                        for (String member : uuidList) {
                            if(!member.isBlank()) {
                                if (!member.equals(bearerContext.getUserId())) {
                                    firebaseService.insertNotificationNoMessage(member, "New notification", "New notification",
                                            "Location", updatedLocationDto.getId().toString(), locationStagingDto.getId().toString(),
                                            auditLocationLogDto.getId().toString(), false, bearerContext);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            ResponseObject responseObject = new ResponseObject(true, "update location", updatedLocationDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "update location", false);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @PutMapping("/locations/updateLocationTemp/{locationId}")
    public ResponseEntity<?> updateLocationTemp(@PathVariable UUID locationId, @RequestParam(value = "locationDto") LocationDto locationDto,
                                                @RequestParam(value = "attachmentData") List<AttachmentDto> attachmentDto,
                                                @RequestParam(value = "deleteFile", required = false) List<String> deleteFile,
                                                @RequestParam(value = "file", required = false) MultipartFile[] files) {
        try {
            LocationDto locationDto2 = null;
            LocationDto locationDto1 = locationService.getListLocationById(locationId);
            if(locationDto1 == null) {
                locationDto.setVersion("1");
                locationDto2 = locationService.insertLocationData(locationId, locationDto, attachmentDto, deleteFile, files);
            } else {
                if(locationDto1.getVersion() == null || locationDto1.getVersion().isEmpty()) {
                    locationDto.setVersion("1");
                    locationDto2 = locationService.insertLocationData(locationId, locationDto, attachmentDto, deleteFile, files);
                } else {
                    if(locationDto1.getVersion().equals(locationDto.getVersion())) {
                        int version = Integer.parseInt(locationDto.getVersion());
                        version = version + 1;
                        locationDto.setVersion(String.valueOf(version));
                        locationDto2 = locationService.insertLocationData(locationId, locationDto, attachmentDto, deleteFile, files);
                    } else {
                        locationService.updateLocation(locationDto.getId(), locationDto);
                        ResponseObject responseObject = new ResponseObject(false, "update location", false);
                        return new ResponseEntity<>(responseObject, HttpStatus.OK);
                    }
                }
            }
            ResponseObject responseObject = new ResponseObject(true, "update location", locationDto2);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(false, "update location", false);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @PostMapping("/locations/updateLite/{locationId}")
    public ResponseEntity<?> updateLiteLocation(@PathVariable String locationId, @RequestBody LocationDto locationDto) {
        UUID id = UUID.fromString(locationId);
        try {
            LocationDto updatedLocationDto = locationService.updateLocationLite(id, locationDto);
            ResponseObject responseObject = new ResponseObject(true, "update location", updatedLocationDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "update location", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @DeleteMapping("/locations/{locationId}")
    public ResponseEntity<?> deleteLocation(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        locationService.deleteLocation(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete location successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
    @DeleteMapping("/locations/delete/{locationId}")
    public ResponseEntity<?> deleteLocationData(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        locationService.deleteLocationData(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete location successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/{locationId}")
    public ResponseEntity<?> getLocationById(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        LocationDto locationDto = locationService.getLocationById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get location by ID", locationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations")
    public ResponseEntity<?> searchLocations(
            @RequestParam(required = false, name = "user_id") String userId,
            Pageable pageable
    ) {
        UUID id = null;
        if (userId != null) {
            id = UUID.fromString(userId);
        }
        List<LocationDto> locationDtoList = locationService.searchLocations(id, pageable);
        ResponseObject responseObject = new ResponseObject(true, "Search locations", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/locations/delete-multiple")
    @Transactional
    public ResponseEntity<?> deleteMultiple(@RequestParam List<UUID> listId) {
        List<UUID> deletedIds = new ArrayList<>();
        try {
            // Lặp qua từng ID trong danh sách để xử lý xóa
            for (UUID id : listId) {
                List<LocationDto> listIdDelete = new ArrayList<>();
                LocationDto location = locationService.getLocationById(id);

                if (location == null) {
                    throw new RuntimeException("Location with id " + id + " not found");
                }

                if (location.getMode().equals("location")) {
                    List<LocationDto> listVoltage = locationService.getLocationByRefId(location.getId());
                    listIdDelete.add(location);
                    listIdDelete.addAll(listVoltage);

                    for (LocationDto locationDto : listVoltage) {
                        List<LocationDto> listFeeder = locationService.getLocationByRefId(locationDto.getId());
                        listIdDelete.addAll(listFeeder);
                    }
                } else if (location.getMode().equals("voltage")) {
                    List<LocationDto> listFeeder = locationService.getLocationByRefId(location.getId());
                    listIdDelete.add(location);
                    listIdDelete.addAll(listFeeder);
                } else {
                    listIdDelete.add(location);
                }

                listIdDelete.forEach(element -> {
                    System.out.println(element.getName());
                });

                if (!listIdDelete.isEmpty()) {
                    // Kiểm tra nếu có location nào bị khóa
                    List<LocationDto> locationLock = listIdDelete.stream().filter(item -> item.isLocked() == true).collect(Collectors.toList());

                    // Nếu không có location bị khóa thì thực hiện xóa
                    if (locationLock.isEmpty()) {
                        List<UUID> listLocationId = listIdDelete.stream().map(LocationDto::getId).collect(Collectors.toList());

                        for (UUID dataId : listLocationId) {
                            locationService.deleteLocation(dataId);
                            attachmentService.deleteAttachmentById_foreign(dataId.toString());
                        }

                        // Nếu xóa thành công, thêm ID vào danh sách đã xóa
                        deletedIds.addAll(listLocationId);
                    } else {
                        // Nếu có location bị khóa, bỏ qua
                        throw new RuntimeException("Some locations are locked and cannot be deleted");
                    }
                }
            }

            // Kiểm tra nếu không có gì được xóa thì trả về null
            if (deletedIds.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.OK);
            }

            // Nếu tất cả xóa thành công, trả về danh sách ID đã xóa
            ResponseObject responseObject = new ResponseObject(true, "Delete successfully", deletedIds);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu có lỗi, rollback toàn bộ giao dịch và trả về lỗi
            System.out.println(e);
            ResponseObject responseObject = new ResponseObject(false, "Error during deletion", null);
            return new ResponseEntity<>(responseObject, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/locations/{locationId}/updateCollabs")
    public ResponseEntity<?> updateJobCollabs(@PathVariable String locationId, @RequestBody ListIdDto listIdDto) {
        locationService.updateJobCollabs(locationId, listIdDto);
        ResponseObject responseObject = new ResponseObject(true, "Update collaborators successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/findLocationByRefId/{ref_id}/{stt}/{sl}")
    public ResponseEntity<?> findLocationByOwner(@PathVariable String ref_id, @PathVariable int stt, @PathVariable int sl) {
        UUID id = UUID.fromString(ref_id);
        int first = (stt - 1) * sl;
        List<LocationDto> locationDtoList = locationService.findLocationByRefId(id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get location by ref Id", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/findAllLocationByRefId/{ref_id}")
    public ResponseEntity<?> findAllLocationByRefId(@PathVariable String ref_id) {
        UUID id = UUID.fromString(ref_id);
        List<LocationDto> locationDtoList = locationService.getLocationByRefId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get location by ref Id", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countLocationByRefId/{ref_id}")
    public ResponseEntity<?> countLocationByRefId(@PathVariable String ref_id) {
        UUID id = UUID.fromString(ref_id);
        int  count = locationService.countLocationByRefId(id);
        ResponseObject responseObject = new ResponseObject(true, "count location by ref Id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/findLocationByRefIdAndUser/{ref_id}/{stt}/{sl}")
    public ResponseEntity<?> findLocationByRefIdAndCreatedAndCollab(@PathVariable String ref_id, @PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<LocationDto> locationDtoList = locationService.findLocationByRefIdAndCreatedAndCollab(ref_id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get location by ref Id and user", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countLocationByRefIdAndUser/{ref_id}")
    public ResponseEntity<?> countLocationByRefIdAndCreatedAndCollab(@PathVariable String ref_id) {
        int count = locationService.countLocationByRefIdAndCreatedAndCollab(ref_id);
        ResponseObject responseObject = new ResponseObject(true, "Count location by ref Id and user", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/findAllLocationByCreatedAndCollab/{mode}/{stt}/{sl}")
    public ResponseEntity<?> findAllLocationByCreatedAndCollab(@PathVariable String mode, @PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        String user_id = BearerContextHolder.getContext().getUserId();
        List<LocationDto> locationDtoList = locationService.findAllLocationByCreatedAndCollab(mode, UUID.fromString(user_id), first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get location", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countAllLocationByCreatedAndCollab/{mode}")
    public ResponseEntity<?> countAllLocationByCreatedAndCollab(@PathVariable String mode) {
        String user_id = BearerContextHolder.getContext().getUserId();
        int count = locationService.countAllLocationByCreatedAndCollab(mode, UUID.fromString(user_id));
        ResponseObject responseObject = new ResponseObject(true, "Count location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/findAllLocationByField/{stt}/{sl}")
    public ResponseEntity<?> findAllLocationByField(@RequestBody LocationDto locationDto, @PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        String user_id = BearerContextHolder.getContext().getUserId();
        List<LocationDto> locationDtoList = locationService.findAllLocationByField(locationDto, first, sl,user_id);
        ResponseObject responseObject = new ResponseObject(true, "Get location", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/countAllLocationByField")
    public ResponseEntity<?> countAllLocationByField(@RequestBody LocationDto locationDto) {
        String user_id = BearerContextHolder.getContext().getUserId();
        Long count = locationService.countAllLocationByField(locationDto, user_id);
        ResponseObject responseObject = new ResponseObject(true, "Get location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countLocationsByRefIdOwnerAndModeAndCreatedBy/{mode}")
    public ResponseEntity<?> countLocationsByRefIdOwnerAndModeAndCreatedBy(@PathVariable String mode) {
        String user_id = BearerContextHolder.getContext().getUserId();
        UUID userId = UUID.fromString(user_id);
        int count = locationService.countLocationsByRefIdOwnerAndModeAndCreatedBy(mode, userId);
        ResponseObject responseObject = new ResponseObject(true, "count location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countLocationsByCreatedByAndCollab")
    public ResponseEntity<?> countLocationsByCreatedByAndCollab() {
        String user_id = BearerContextHolder.getContext().getUserId();
        UUID userId = UUID.fromString(user_id);
        int count = locationService.countLocationsByCreatedByAndCollab(userId);
        ResponseObject responseObject = new ResponseObject(true, "count location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/countLocationsModeByCreatedByAndCollab/{mode}")
    public ResponseEntity<?> countLocationsModeByCreatedByAndCollab(@PathVariable String mode) {
        String user_id = BearerContextHolder.getContext().getUserId();
        UUID userId = UUID.fromString(user_id);
        int count = locationService.countLocationsModeByCreatedByAndCollab(mode, userId);
        ResponseObject responseObject = new ResponseObject(true, "count location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/addLocationByOwnerFullName")
    public ResponseEntity<?> addLocationByOwnerFullName(@RequestBody List<LocationOwnerDto> locationOwnerDtoList) {
        List<LocationOwnerDto> count = locationService.addLocationByOwnerFullName(locationOwnerDtoList);
        ResponseObject responseObject = new ResponseObject(true, "Get location", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
