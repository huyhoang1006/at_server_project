package vn.automationandtesting.atproject.controller.disconnector;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.disconnector.TestDisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.TestDisconnectorMapper;
import vn.automationandtesting.atproject.entity.disconnector.TestsDisconnector;
import vn.automationandtesting.atproject.service.disconnector.TestDisconnectorService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestDisconnectorController {
    @Autowired
    private TestDisconnectorMapper mapper;

    @Autowired
    private TestDisconnectorService service;

    @GetMapping("/disconnector/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestDisconnectorDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/disconnector/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestDisconnectorDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/disconnector/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestDisconnectorDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/disconnector/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestDisconnectorDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/disconnector/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestDisconnectorDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestDisconnectorDto> testDto = service.findAllTestByJobId(job_id);
        List<TestDisconnectorDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestDisconnectorDto item : testDto) {
                boolean sign = false;
                for (TestDisconnectorDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestsDisconnector> testDelete = new ArrayList<>();
            for(TestDisconnectorDto item : testDeleteDto) {
                TestsDisconnector itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestsDisconnector> tests = new ArrayList<>();
        for(TestDisconnectorDto itemDto : listTest) {
            TestsDisconnector test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/disconnector/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestDisconnectorDto> listTest) {
        List<TestsDisconnector> tests = new ArrayList<>();
        for(TestDisconnectorDto itemDto : listTest) {
            TestsDisconnector test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
