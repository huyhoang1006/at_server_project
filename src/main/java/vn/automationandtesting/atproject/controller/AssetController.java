package vn.automationandtesting.atproject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.service.AssetService;
import vn.automationandtesting.atproject.service.BushingService;
import vn.automationandtesting.atproject.service.TapChangerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class AssetController {
    Logger logger = LoggerFactory.getLogger(AssetController.class);


    private final AssetService assetService;

    @Autowired
    private BushingService bushingService;

    @Autowired
    private TapChangerService tapChangerService;

    @Autowired
    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

//    @GetMapping("/assets")
//    public ResponseEntity<?> getAllAssets() {
//        List<AssetDto> assetDtoList = assetService.getAllAssets();
//        ResponseObject responseObject = new ResponseObject(true, "Get all assets", assetDtoList);
//        return new ResponseEntity<>(responseObject, HttpStatus.OK);
//    }

    @GetMapping("/assets/{assetId}")
    public ResponseEntity<?> getAssetByID(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        AssetDto assetDto = assetService.getAssetByIdAndUserIdAndCollabId(id, UUID.fromString(BearerContextHolder.getContext().getUserId()));
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", assetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/getAssetById/{id}")
    public ResponseEntity<?> getAssetById(@PathVariable String id) {
        UUID assetId = UUID.fromString(id);
        AssetDto assetDto = assetService.getAssetById(assetId, BearerContextHolder.getContext().getUserId());
        List<BushingDto> bushingDtos = bushingService.getAllBushingsByAssetid(assetDto.getId());
        List<TapChangerDto> tapChangerDtos = tapChangerService.findTapChangersByAssetId(assetDto.getId());
        Map<String, Object> data = new HashMap<>();
        try {
            data.put("asset", assetDto);
            if(!bushingDtos.isEmpty()) {
                data.put("bushings", bushingDtos.get(0));
            } else {
                BushingDto bushingDto = new BushingDto();
                data.put("bushings", bushingDto);
            }
            if(!tapChangerDtos.isEmpty()) {
                data.put("tap_changer", tapChangerDtos.get(0));
            } else {
                TapChangerDto tapChangerDto = new TapChangerDto();
                data.put("tap_changer", tapChangerDto);
            }
            ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", data);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/assets/getAssetByLocation/{locationId}")
    public ResponseEntity<?> getAssetByLocationId(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        List<AssetDto> assetDtoList = assetService.getAssetByLocationIdAndUserIdAndCollabId(id, UUID.fromString(BearerContextHolder.getContext().getUserId()));
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location ID", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/findAssetByLocationId/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetByLocationId(@PathVariable String locationId, @PathVariable int stt, @PathVariable int sl) {
        UUID id = UUID.fromString(locationId);
        int first = (stt - 1) * sl;
        List<AssetDto> assetDtoList = assetService.findAssetByLocationId(id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location ID", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/countAssetByLocationId/{locationId}")
    public ResponseEntity<?> countAssetByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        int count = assetService.countAssetByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Count transformer by location id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/countAssetList")
    public ResponseEntity<?> countAssetList() {
        int count = assetService.countAssetList();
        ResponseObject responseObject = new ResponseObject(true, "Count transformer", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/findAssetList/{stt}/{sl}")
    public ResponseEntity<?> findAssetList(@PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<AssetDto> assetDtoList = assetService.findAssetList(first, sl);
        ResponseObject responseObject = new ResponseObject(true, "get transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/findAssetListOffset/{stt}/{sl}")
    public ResponseEntity<?> findAssetListOffset(@PathVariable int stt, @PathVariable int sl) {
        List<AssetDto> assetDtoList = assetService.findAssetList(stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId,@PathVariable int stt, @PathVariable int sl) {
        List<AssetDto> assetDtoList = assetService.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        int count = assetService.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count transformer", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assets")
    public ResponseEntity<?> createAsset(@RequestBody AssetDto assetDto) {
        AssetDto savedAssetDto = assetService.createNewAsset(assetDto);
        ResponseObject responseObject = new ResponseObject(true, "Create asset", savedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/{assetId}")
    public ResponseEntity<?> updateAsset(@RequestBody AssetDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        AssetDto updatedAssetDto = assetService.updateAsset(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update asset", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/updateAssetLite/{assetId}")
    public ResponseEntity<?> updateAssetLite(@RequestBody AssetDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        AssetDto updatedAssetDto = assetService.updateAssetLite(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update asset", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/assets/{assetId}")
    public ResponseEntity<?> deleteAsset(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        assetService.deleteAsset(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets")
    public ResponseEntity<?> searchAssets(
            @RequestParam(required = false, name = "location_id") String locationId,
            @RequestParam(required = false, name = "serial_no") String serialNo,
            Pageable pageable
    ) {
        List<AssetDto> assetDtos = assetService.searchAssets(locationId, serialNo, pageable);
        ResponseObject responseObject = new ResponseObject(true, "Search assets", assetDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/lock")
    public ResponseEntity<?> lock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = assetService.lock(true, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/unlock")
    public ResponseEntity<?> unlock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = assetService.lock(false, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/download")
    public ResponseEntity<?> download(@RequestParam List<UUID> listId) {
        ResponseObject responseObject = assetService.download(listId);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assets/upload")
    public ResponseEntity<?> upload(@RequestBody ResourceFullDto resourceFullDto) {
        ResponseObject responseObject = assetService.upload(resourceFullDto);
        logger.info(String.valueOf(resourceFullDto));
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/assets/delete-multiple")
    public ResponseEntity<?> deleteMultiple(@RequestParam List<UUID> listId) {
        for (UUID id : listId) {
            try {
                assetService.deleteAsset(id);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
