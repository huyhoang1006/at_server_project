package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.service.UserService;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 1/9/2022
 * @project at-project-server
 */
@Controller
@RequestMapping("${api.prefix}")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    private UserMapper userMapper;

    @PostMapping("/users")
    public ResponseEntity<?> createUser(@RequestBody UserReqDto userReqDto) {
        ResponseObject responseObject = userService.saveNewUser(userReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        List<UserReqDto> userReqDtoList = userService.getAllUsers();
        ResponseObject responseObject = new ResponseObject(true, "Get all users", userReqDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getUserById(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto userReqDto = userService.getUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get user by id", userReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users/findUserByCreated/{userId}/{st}/{sl}")
    public ResponseEntity<?> findUserByCreated(@PathVariable String userId, @PathVariable String st, @PathVariable String sl) {
        UUID id = UUID.fromString(userId);
        int first = (Integer.parseInt(st) - 1) * Integer.parseInt(sl);
        List<UserReqDto> userReqDtos = userService.findUserByCreated(id, first, Integer.parseInt(sl));
        ResponseObject responseObject = new ResponseObject(true, "Get user by id", userReqDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users/countUserByCreated/{userId}")
    public ResponseEntity<?> countUserByCreated(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        int count = userService.countUserByCreated(id);
        ResponseObject responseObject = new ResponseObject(true, "Get user by id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<?> updateUser(@RequestBody UserReqDto userReqDto, @PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto updatedUserReqDto = userService.updateUser(userReqDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update user", updatedUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/users/usersAllInfo/{userId}")
    public ResponseEntity<?> updateUserAllInfo(@RequestBody UserReqDto userReqDto, @PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto updatedUserReqDto = userService.updateUserAllInfo(userReqDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update user", updatedUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> getUser(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        userService.deleteUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete user successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/users/remove/{userId}")
    public ResponseEntity<?> removeUser(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        userService.removeUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Remove user successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/users/{userId}/update-group")
    public ResponseEntity<?> updateUserGroups(@PathVariable("userId") String userId, @RequestParam List<String> groupNames) {
        UUID id = UUID.fromString(userId);
        UserReqDto updatedUserReqDto = userService.updateUserGroupsForAdmin(groupNames, id);
        ResponseObject responseObject = new ResponseObject(true, "Update user", updatedUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/users/getDataByIncludes/{st}/{sl}")
    public ResponseEntity<?> getDataByIncludes(@RequestBody UserReqDto userReqDto, @PathVariable String st, @PathVariable String sl) {
        int first = (Integer.parseInt(st) - 1) * Integer.parseInt(sl);
        List<UserReqDto> userReqDtos = userService.getDataByIncludes(userReqDto, first, Integer.parseInt(sl));
        ResponseObject responseObject = new ResponseObject(true, "Find user successfully", userReqDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/users/countDataByIncludes")
    public ResponseEntity<?> countDataByIncludes(@RequestBody UserReqDto userReqDto) {
        Long count = userService.countAllUserByField(userReqDto);
        ResponseObject responseObject = new ResponseObject(true, "Find user successfully", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
