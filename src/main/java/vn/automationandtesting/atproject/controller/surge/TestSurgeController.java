package vn.automationandtesting.atproject.controller.surge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.TestSurgeMapper;
import vn.automationandtesting.atproject.controller.dto.surge.TestSurgeDto;
import vn.automationandtesting.atproject.entity.surge.TestsSurge;
import vn.automationandtesting.atproject.service.surge.TestSurgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestSurgeController {
    @Autowired
    private TestSurgeMapper mapper;

    @Autowired
    private TestSurgeService service;

    @GetMapping("/surge/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestSurgeDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestSurgeDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestSurgeDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestSurgeDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestSurgeDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestSurgeDto> testDto = service.findAllTestByJobId(job_id);
        List<TestSurgeDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestSurgeDto item : testDto) {
                boolean sign = false;
                for (TestSurgeDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestsSurge> testDelete = new ArrayList<>();
            for(TestSurgeDto item : testDeleteDto) {
                TestsSurge itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestsSurge> tests = new ArrayList<>();
        for(TestSurgeDto itemDto : listTest) {
            TestsSurge test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestSurgeDto> listTest) {
        List<TestsSurge> tests = new ArrayList<>();
        for(TestSurgeDto itemDto : listTest) {
            TestsSurge test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
