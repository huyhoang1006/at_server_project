package vn.automationandtesting.atproject.controller.surge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.JobSurgeMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.SurgeMapper;
import vn.automationandtesting.atproject.controller.dto.power.JobPowerCableDto;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.JobSurgeDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.power.JobsPowerCable;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.entity.surge.JobsSurge;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.service.surge.JobSurgeService;
import vn.automationandtesting.atproject.service.surge.SurgeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class JobSurgeController {

    @Autowired
    private JobSurgeService service;

    @Autowired
    private JobSurgeMapper mapper;

    @Autowired
    private SurgeService assetService;

    @Autowired
    private SurgeMapper assetMapper;

    @GetMapping("/surge/job/findAllJobByAssetId/{asset_id}")
    public ResponseEntity<?> findAllJobByAssetId(@PathVariable String asset_id) {
        UUID id = UUID.fromString(asset_id);
        List<JobSurgeDto> dtoList = service.findAllJobByAssetId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findJobById/{id}")
    public ResponseEntity<?> findJobById(@PathVariable String id) {
        UUID uuid = UUID.fromString(id);
        List<JobSurgeDto> dtoList = service.findJobById(uuid);
        ResponseObject responseObject = new ResponseObject(true, "Get by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findJobByName/{name}")
    public ResponseEntity<?> findJobByName(@PathVariable String name) {
        List<JobSurgeDto> dtoList = service.findJobByName(name);
        ResponseObject responseObject = new ResponseObject(true, "Get by name", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findJobByNameAndAsset/{name}/{asset_id}")
    public ResponseEntity<?> findJobByNameAndAsset(@PathVariable(name = "name") String name, @PathVariable(name = "asset_id") String asset_id) {
        UUID id = UUID.fromString(asset_id);
        List<JobSurgeDto> dtoList = service.findJobByNameAndAsset(name, id);
        ResponseObject responseObject = new ResponseObject(true, "Get by name and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findAll")
    public ResponseEntity<?> findAll() {
        List<JobSurgeDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findByAssetIdAndCollab/{assetId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String assetId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID asset_id = UUID.fromString(assetId);
        List<JobSurgeDto> jobDtoList = service.findByAssetIdAndCollabsContaining(asset_id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get job by asset id and collab", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/job/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        JobSurgeDto job = service.findJobById(UUID.fromString(id)).get(0);
        List<JobsSurge> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            job.setLocked(true);
            job.setLocked_by(UUID.fromString(userId));
            JobsSurge item = mapper.JobDtoToJob(job);
            item.setCollabs(job.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(job.getLocked_by().toString().equals(userId)) {
                job.setLocked(false);
                job.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                JobsSurge item = mapper.JobDtoToJob(job);
                item.setCollabs(job.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "sign lock job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/job/share/{jobId}")
    public ResponseEntity<?> share(@RequestBody List<String> userIds, @PathVariable String jobId) {
        JobSurgeDto jobDto = service.findJobById(UUID.fromString(jobId)).get(0);
        SurgeDto assetDto = assetService.findAssetById(jobDto.getAsset_id()).get(0);

        String collabAsset = assetDto.getCollabs();
        List<String> collabAssetData = new ArrayList<>();
        if(collabAsset != null) {
            collabAssetData = Arrays.asList(collabAsset.split(","));
            collabAssetData = new ArrayList<>(collabAssetData);
        }
        for (String user : userIds) {
            if(!collabAssetData.contains(user)) {
                collabAssetData.add(user);
            }
        }
        collabAsset = String.join(",", collabAssetData);
        List<Surge> assets = new ArrayList<>();
        Surge asset = assetMapper.assetDtoToAsset(assetDto);
        asset.setCollabs(collabAsset);
        assets.add(asset);
        assetService.saveAll(assets);

        String collab = jobDto.getCollabs();
        List<String> collabData = new ArrayList<>();
        if(collab != null) {
            collabData = Arrays.asList(collab.split(","));
            collabData = new ArrayList<>(collabData);
        }
        for (String user : userIds) {
            if(!collabData.contains(user)) {
                collabData.add(user);
            }
        }
        collab = String.join(",", collabData);
        List<JobsSurge> jobs = new ArrayList<>();
        JobsSurge job = mapper.JobDtoToJob(jobDto);
        job.setCollabs(collab);
        jobs.add(job);
        service.saveAll(jobs);
        ResponseObject responseObject = new ResponseObject(true, "share job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/job/save")
    public ResponseEntity<?> insertJob(@RequestBody List<JobSurgeDto> jobDtoList) {
        List<JobsSurge> jobs = new ArrayList<>();
        for(JobSurgeDto item : jobDtoList) {
            List<JobSurgeDto> itemDtoList = service.findJobById(item.getId());
            JobsSurge job = new JobsSurge();
            if(itemDtoList.size() == 0) {
                job = mapper.JobDtoToJob(item);
            } else {
                job = mapper.copyJobDtoToJob(itemDtoList.get(0), job);
                job = mapper.copyJobDtoToJob(item, job, "createdOn", "createdBy", "locked", "collabs");
            }
            System.out.println(job.getAverage_health_index());
            if(!job.isLocked()) {
                jobs.add(job);
            }
        }
        if(jobs.size() != 0) {
            service.saveAll(jobs);
        }
        ResponseObject responseObject = new ResponseObject(true, "save circuit", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/surge/job/delete")
    public ResponseEntity<?> deleteJob(@RequestBody List<JobSurgeDto> jobDtoList) {
        List<JobsSurge> jobs = new ArrayList<>();
        for(JobSurgeDto item : jobDtoList) {
            List<JobSurgeDto> itemDto = service.findJobById(item.getId());
            if(!itemDto.get(0).isLocked()) {
                JobsSurge job = mapper.JobDtoToJob(item);
                jobs.add(job);
            }
        }
        if(jobs.size() != 0) {
            service.deleteAll(jobs);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/findJobByAssetId/{assetId}/{stt}/{sl}")
    public ResponseEntity<?> findJobByAssetId(@PathVariable String assetId, @PathVariable int stt, @PathVariable int sl) {
        UUID asset_id = UUID.fromString(assetId);
        int first = (stt - 1) * sl;
        List<JobSurgeDto> jobDtoList = service.findJobByAssetId(asset_id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get Job by asset ID", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/surge/job/countJobByAssetId/{assetId}")
    public ResponseEntity<?> countJobByAssetId(@PathVariable String assetId) {
        UUID asset_id = UUID.fromString(assetId);
        int count = service.countJobByAssetId(asset_id);
        ResponseObject responseObject = new ResponseObject(true, "Count job by asset id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
