package vn.automationandtesting.atproject.controller.power;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.circuit.CircuitController;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.power.PowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.service.power.PowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class PowerController {
    Logger logger = LoggerFactory.getLogger(CircuitController.class);

    @Autowired
    private PowerCableService service;

    @Autowired
    private PowerCableMapper mapper;

    @GetMapping("/power/findById/{_id}")
    public ResponseEntity<?> getAssetByID(@PathVariable String _id) {
        UUID id = UUID.fromString(_id);
        List<PowerCableDto> dtoList = service.findAssetById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/getAssetById/{id}")
    public ResponseEntity<?> getAssetById(@PathVariable String id) {
        UUID assetId = UUID.fromString(id);
        try {
            PowerCableDto assetDto = service.getAssetById(assetId, BearerContextHolder.getContext().getUserId());
            ResponseObject responseObject = new ResponseObject(true, "Get power by ID", assetDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "Get power by ID", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/power/findAll")
    public ResponseEntity<?> getAll() {
        List<PowerCableDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all asset", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findByLocationId/{locationId}")
    public ResponseEntity<?> findAllByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findAllByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findAssetByLocationId/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetByLocationId(@PathVariable String locationId, @PathVariable int stt, @PathVariable int sl) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        int first = (stt - 1) * sl;
        List<PowerCableDto> powerCableDtoList = service.findAssetByLocationId(location_Id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", powerCableDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/countAssetByLocationId/{locationId}")
    public ResponseEntity<?> countAssetByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        int count = service.countAssetByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Count power by location id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/countAssetList")
    public ResponseEntity<?> countAssetList() {
        int count = service.countAssetList();
        ResponseObject responseObject = new ResponseObject(true, "Count power cable", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findAssetList/{stt}/{sl}")
    public ResponseEntity<?> findAssetList(@PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<PowerCableDto> assetDtoList = service.findAssetList(first, sl);
        ResponseObject responseObject = new ResponseObject(true, "get power cable", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findAssetListOffset/{stt}/{sl}")
    public ResponseEntity<?> findAssetListOffset(@PathVariable int stt, @PathVariable int sl) {
        List<PowerCableDto> assetDtoList = service.findAssetList(stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get power cable", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId,@PathVariable int stt, @PathVariable int sl) {
        List<PowerCableDto> assetDtoList = service.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get power cable", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        int count = service.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count power", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
    @GetMapping("/power/findByLocationIdAndCollab/{locationId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findByLocationIdAndCollabsContaining(location_Id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findBySerial/{serial}")
    public ResponseEntity<?> findBySerial(@PathVariable String serial) {
        List<PowerCableDto> dtoList = service.findBySerial(serial);
        ResponseObject responseObject = new ResponseObject(true, "Get asset serial", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findBySerial/{serial}/{locationId}")
    public ResponseEntity<?> findBySerialAndLocation(@PathVariable(name = "serial") String serial, @PathVariable(name = "locationId") String locationId) {
        UUID location_id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findBySerialAndLocation(serial, location_id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by serial and location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        PowerCableDto assets = service.findAssetById(UUID.fromString(id)).get(0);
        List<PowerCable> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            assets.setLocked(true);
            assets.setLocked_by(UUID.fromString(userId));
            PowerCable item = mapper.assetDtoToAsset(assets);
            item.setCollabs(assets.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(assets.getLocked_by().toString().equals(userId)) {
                assets.setLocked(false);
                assets.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                PowerCable item = mapper.assetDtoToAsset(assets);
                item.setCollabs(assets.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }

        ResponseObject responseObject = new ResponseObject(true, "sign lock asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/save")
    public ResponseEntity<?> insertAsset(@RequestBody List<PowerCableDto> dtoList) {
        List<PowerCable> assets = new ArrayList<>();
        for(PowerCableDto items : dtoList) {
            List<PowerCableDto> item = new ArrayList<>();
            if(items.getId() != null && items.getId().toString().isEmpty()) {
                item = service.findAssetById(items.getId());
            }
            PowerCable asset = new PowerCable();
            if(item.size() == 0) {
                asset = mapper.assetDtoToAsset(items);
                if(asset.getId() == null || asset.getId().toString().isEmpty()) {
                    asset.setId(UUID.randomUUID());
                }
            } else {
                asset = mapper.copyAssetDtoToAsset(item.get(0), asset);
                asset = mapper.copyAssetDtoToAsset(items, asset, "createdOn", "createdBy", "locked", "collabs");
            }

            if(!asset.isLocked()) {
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.saveAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "save asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/power/updateAssetLite/{assetId}")
    public ResponseEntity<?> updateAssetLite(@RequestBody PowerCableDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        PowerCableDto updatedAssetDto = service.updateAssetLite(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update power cable", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/delete")
    public ResponseEntity<?> deleteAsset(@RequestBody List<PowerCableDto> dtoList) {
        List<PowerCable> assets = new ArrayList<>();
        for(PowerCableDto items : dtoList) {
            List<PowerCableDto> item = service.findAssetById(items.getId());
            if(!item.get(0).isLocked()) {
                PowerCable asset = mapper.assetDtoToAsset(items);
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.deleteAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
