package vn.automationandtesting.atproject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.service.AssetCommonService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class AssetCommonController {

    Logger logger = LoggerFactory.getLogger(AssetController.class);
    @Autowired
    private AssetCommonService assetCommonService;

    @GetMapping("/assetCommon/countAsset")
    public ResponseEntity<?> countAsset() {
        JSONObject count = assetCommonService.countAsset();
        ResponseObject responseObject = new ResponseObject(true, "Count asset", count.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assetCommon/findAllAssetByField/{stt}/{sl}")
    public ResponseEntity<?> findAllAssetByField(@RequestBody Object asset, @PathVariable int stt, @PathVariable int sl) {
        Object assetList = assetCommonService.findAllAssetByField(asset ,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get asset", assetList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assetCommon/countAllAssetByField")
    public ResponseEntity<?> countAllAssetByField(@RequestBody Object asset) {
        JSONObject count = assetCommonService.countAllAssetByField(asset);
        ResponseObject responseObject = new ResponseObject(true, "get asset", count.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assetCommon/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId, @PathVariable int stt, @PathVariable int sl) {
        Object assetDtoList = assetCommonService.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get asset", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assetCommon/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        JSONObject object = assetCommonService.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count asset", object.toString());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
