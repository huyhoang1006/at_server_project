package vn.automationandtesting.atproject.controller.voltage;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.TestVoltageMapper;
import vn.automationandtesting.atproject.controller.dto.voltage.TestVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.TestsVoltage;
import vn.automationandtesting.atproject.service.voltage.TestVoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestVoltageController {
    @Autowired
    private TestVoltageMapper mapper;

    @Autowired
    private TestVoltageService service;

    @GetMapping("/voltage/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestVoltageDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestVoltageDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestVoltageDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestVoltageDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/voltage/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestVoltageDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestVoltageDto> testDto = service.findAllTestByJobId(job_id);
        List<TestVoltageDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestVoltageDto item : testDto) {
                boolean sign = false;
                for (TestVoltageDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestsVoltage> testDelete = new ArrayList<>();
            for(TestVoltageDto item : testDeleteDto) {
                TestsVoltage itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestsVoltage> tests = new ArrayList<>();
        for(TestVoltageDto itemDto : listTest) {
            TestsVoltage test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/voltage/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestVoltageDto> listTest) {
        List<TestsVoltage> tests = new ArrayList<>();
        for(TestVoltageDto itemDto : listTest) {
            TestsVoltage test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
