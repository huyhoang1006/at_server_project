package vn.automationandtesting.atproject.controller.voltage;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.circuit.CircuitController;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.VoltageMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.voltage.Voltage;
import vn.automationandtesting.atproject.service.voltage.VoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class VoltageController {
    Logger logger = LoggerFactory.getLogger(CircuitController.class);

    @Autowired
    private VoltageService service;

    @Autowired
    VoltageMapper mapper;

    @GetMapping("/voltage/findById/{_id}")
    public ResponseEntity<?> getAssetByID(@PathVariable String _id) {
        UUID id = UUID.fromString(_id);
        List<VoltageDto> dtoList = service.findAssetById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/getAssetById/{id}")
    public ResponseEntity<?> getAssetById(@PathVariable String id) {
        UUID assetId = UUID.fromString(id);
        try {
            VoltageDto assetDto = service.getAssetById(assetId, BearerContextHolder.getContext().getUserId());
            ResponseObject responseObject = new ResponseObject(true, "Get voltage by ID", assetDto);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            ResponseObject responseObject = new ResponseObject(true, "Get voltage by ID", null);
            return new ResponseEntity<>(responseObject, HttpStatus.OK);
        }
    }

    @GetMapping("/voltage/findAll")
    public ResponseEntity<?> getAll() {
        List<VoltageDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all asset", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findByLocationId/{locationId}")
    public ResponseEntity<?> findAllByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        List<VoltageDto> dtoList = service.findAllByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findAssetByLocationId/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetByLocationId(@PathVariable String locationId, @PathVariable int stt, @PathVariable int sl) {
        UUID location_Id = UUID.fromString(locationId);
        int first = (stt - 1) * sl;
        List<VoltageDto> voltageDtoList = service.findAssetByLocationId(location_Id, first, sl);
        ResponseObject responseObject = new ResponseObject(true, "Get circuit by location id", voltageDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/countAssetByLocationId/{locationId}")
    public ResponseEntity<?> countAssetByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        int count = service.countAssetByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Count voltage by location id", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/countAssetList")
    public ResponseEntity<?> countAssetList() {
        int count = service.countAssetList();
        ResponseObject responseObject = new ResponseObject(true, "Count voltage transformer", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findAssetList/{stt}/{sl}")
    public ResponseEntity<?> findAssetList(@PathVariable int stt, @PathVariable int sl) {
        int first = (stt - 1) * sl;
        List<VoltageDto> assetDtoList = service.findAssetList(first, sl);
        ResponseObject responseObject = new ResponseObject(true, "get voltage transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findAssetListOffset/{stt}/{sl}")
    public ResponseEntity<?> findAssetListOffset(@PathVariable int stt, @PathVariable int sl) {
        List<VoltageDto> assetDtoList = service.findAssetList(stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get voltage transformer", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findAssetListByLocationIdOffset/{locationId}/{stt}/{sl}")
    public ResponseEntity<?> findAssetListByLocationIdOffset(@PathVariable UUID locationId,@PathVariable int stt, @PathVariable int sl) {
        List<VoltageDto> assetDtoList = service.findAssetListByLocationId(locationId,stt, sl);
        ResponseObject responseObject = new ResponseObject(true, "get voltage", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/countAssetListByLocationIdOffset/{locationId}")
    public ResponseEntity<?> countAssetListByLocationIdOffset(@PathVariable UUID locationId) {
        int count = service.countAssetListByLocationId(locationId);
        ResponseObject responseObject = new ResponseObject(true, "count voltage", count);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findByLocationIdAndCollab/{locationId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        List<VoltageDto> dtoList = service.findByLocationIdAndCollabsContaining(location_Id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findBySerial/{serial}")
    public ResponseEntity<?> findBySerial(@PathVariable String serial) {
        List<VoltageDto> dtoList = service.findBySerial(serial);
        ResponseObject responseObject = new ResponseObject(true, "Get asset serial", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/voltage/findBySerial/{serial}/{locationId}")
    public ResponseEntity<?> findBySerialAndLocation(@PathVariable(name = "serial") String serial, @PathVariable(name = "locationId") String locationId) {
        UUID location_id = UUID.fromString(locationId);
        List<VoltageDto> dtoList = service.findBySerialAndLocation(serial, location_id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by serial and location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/voltage/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        VoltageDto assets = service.findAssetById(UUID.fromString(id)).get(0);
        List<Voltage> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            assets.setLocked(true);
            assets.setLocked_by(UUID.fromString(userId));
            Voltage item = mapper.assetDtoToAsset(assets);
            item.setCollabs(assets.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(assets.getLocked_by().toString().equals(userId)) {
                assets.setLocked(false);
                assets.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                Voltage item = mapper.assetDtoToAsset(assets);
                item.setCollabs(assets.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }

        ResponseObject responseObject = new ResponseObject(true, "sign lock asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/voltage/save")
    public ResponseEntity<?> insertAsset(@RequestBody List<VoltageDto> dtoList) {
        List<Voltage> assets = new ArrayList<>();
        for(VoltageDto items : dtoList) {
            List<VoltageDto> item = new ArrayList<>();
            if(items.getId() != null && items.getId().toString().isEmpty()) {
                item = service.findAssetById(items.getId());
            }
            Voltage asset = new Voltage();
            if(item.size() == 0) {
                asset = mapper.assetDtoToAsset(items);
                if(asset.getId() == null || asset.getId().toString().isEmpty()) {
                    asset.setId(UUID.randomUUID());
                }
            } else {
                asset = mapper.copyAssetDtoToAsset(item.get(0), asset);
                asset = mapper.copyAssetDtoToAsset(items, asset, "createdOn", "createdBy", "locked", "collabs");
            }

            if(!asset.isLocked()) {
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.saveAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "save asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/voltage/updateAssetLite/{assetId}")
    public ResponseEntity<?> updateAssetLite(@RequestBody VoltageDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        VoltageDto updatedAssetDto = service.updateAssetLite(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update voltage", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/voltage/delete")
    public ResponseEntity<?> deleteAsset(@RequestBody List<VoltageDto> dtoList) {
        List<Voltage> assets = new ArrayList<>();
        for(VoltageDto items : dtoList) {
            List<VoltageDto> item = service.findAssetById(items.getId());
            if(!item.get(0).isLocked()) {
                Voltage asset = mapper.assetDtoToAsset(items);
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.deleteAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
