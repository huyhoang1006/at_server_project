package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(reason = "Group name already existed", value = HttpStatus.BAD_REQUEST)
public class ExistedGroupNameException extends RuntimeException {
}
