package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tridv on 15/9/2022
 * @project at-project-server
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AssetLockedException extends RuntimeException{
    public AssetLockedException(String message) {
        super(message);
    }
}
