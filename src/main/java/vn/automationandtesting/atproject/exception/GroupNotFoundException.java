package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class GroupNotFoundException extends RuntimeException {
    public GroupNotFoundException() {
        super("Group not found.");
    }

    public GroupNotFoundException(String message) {
        super(message);
    }
}
