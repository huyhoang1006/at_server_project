package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tridv on 18/9/2022
 * @project at-project-server
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTestException extends RuntimeException{
    public InvalidTestException(String message) {
        super(message);
    }
}
