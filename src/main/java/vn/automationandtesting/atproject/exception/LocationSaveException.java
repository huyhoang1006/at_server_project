package vn.automationandtesting.atproject.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class LocationSaveException extends RuntimeException {
    public LocationSaveException(String message) {
        super(message);
    }
}
