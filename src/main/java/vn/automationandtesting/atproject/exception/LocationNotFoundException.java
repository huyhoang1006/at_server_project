package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class LocationNotFoundException extends RuntimeException {
    public LocationNotFoundException(String message) {
        super(message);
    }
}
