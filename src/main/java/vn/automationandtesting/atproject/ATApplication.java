package vn.automationandtesting.atproject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class ATApplication {

	public static void main(String[] args) {
		SpringApplication.run(ATApplication.class, args);
		String Working_Directory = System.getProperty("user.dir");
		String pathData = Working_Directory + System.getProperty("file.separator") + "FileUpload";
		File theDir = new File(pathData);
		if (!theDir.exists()){
			theDir.mkdirs();
		}
	}

}
