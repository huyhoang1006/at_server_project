package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Job;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface JobRepository extends JpaRepository<Job, UUID> {
    Optional<Job> findByIdAndIsDeleted(UUID jobId, boolean isDeleted);
    List<Job> findAllByIsDeleted(boolean isDeleted);
    List<Job> findAllByNameAndIsDeleted(String jobName, boolean isDeleted);

    @Query(
            value = "select * from job j "+
                    "left join asset a on j.asset_id = a.mrid "+
                    "where a.serial_number = ?1 and j.is_deleted = ?2", nativeQuery = true
    )
    List<Job> findByAssetSerialNo(String assetSerailNo, boolean isDeleted);

    @Query(
            value = "select * from job j "+
                    "left join asset a on j.asset_id = a.mrid "+
                    "where a.mrid = ?1 and j.is_deleted = ?2", nativeQuery = true
    )
    List<Job> findByAssetId(UUID id, boolean isDeleted);

    Optional<Job> findByIdAndIsDeletedAndCreatedBy(UUID jobId, boolean b, UUID loggedInUserId);

    Optional<Job> findByIdAndIsDeletedAndCollabsContains(UUID jobId, boolean b, String toString);
    List<Job> findAllByIsDeletedAndIdInAndCreatedByOrIsDeletedAndIdInAndCollabsContaining(
            boolean isDeleted1,
            List<UUID> jobId1,
            UUID createdById,
            boolean isDeleted2,
            List<UUID> jobId2,
            UUID collabId
    );

    @Query(
            value = "select * from job j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%) offset ?4 limit ?5 ", nativeQuery = true
    )
    List<Job> findJobByAssetId(UUID id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(id) from job j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%)", nativeQuery = true
    )
    int countJobByAssetId(UUID id, UUID user_id, String userId);

    @Query(
            value = "select count(id) from job j "+
                    "where j.created_by = ?1 or collabs like %?2%", nativeQuery = true
    )
    int countJob(UUID user_id, String userId);

    @Query(
            value = "select * from job j "+
                    "where j.created_by = ?1 or collabs like %?2% offset ?3 limit ?4 order by j.created_on DESC", nativeQuery = true
    )
    List<Job> findJob(UUID user_id, String userId, int first, int limit);
}
