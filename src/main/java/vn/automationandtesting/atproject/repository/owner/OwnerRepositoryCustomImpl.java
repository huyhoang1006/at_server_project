package vn.automationandtesting.atproject.repository.owner;

import org.springframework.beans.factory.annotation.Autowired;
import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.entity.owner.Owner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

public class OwnerRepositoryCustomImpl implements OwnerRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    /**
     * @param ownerUserList
     * @param role
     * @return
     */
    @Override
    public List<Owner> getAllParentByRole(List<UUID> ownerUserList, String role) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM Owner t WHERE 1=1");
        queryStr.append(" AND t.mode LIKE :mode");
        queryStr.append(" AND t.createdBy IN :createdBy");
        TypedQuery<Owner> query = entityManager.createQuery(queryStr.toString(), Owner.class);
        query.setParameter("mode", role);
        query.setParameter("createdBy", ownerUserList);
        query.getResultList();
        return query.getResultList();
    }

    /**
     * @param ownerDto
     * @param first
     * @param limit
     * @param ids
     * @return
     */
    @Override
    public List<Owner> findAllOwnerByField(OwnerDto ownerDto, int first, int limit, List<UUID> ids) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM Owner t WHERE 1=1");

        if (ownerDto.getId() != null) {
            queryStr.append(" AND t.mrid LIKE :mrid");
        }
        if (ownerDto.getFull_name() != null && !ownerDto.getFull_name().isEmpty()) {
            queryStr.append(" AND t.full_name LIKE :full_name");
        }
        if (ownerDto.getName() != null && !ownerDto.getName().isEmpty()) {
            queryStr.append(" AND t.name LIKE :name");
        }
        if (ownerDto.getAddress() != null && !ownerDto.getAddress().isEmpty()) {
            queryStr.append(" AND t.address LIKE :address");
        }
        if (ownerDto.getCity() != null && !ownerDto.getCity().isEmpty()) {
            queryStr.append(" AND t.city LIKE :city");
        }
        if (ownerDto.getPhone_no() != null && !ownerDto.getPhone_no().isEmpty()) {
            queryStr.append(" AND t.phone_no = :phone_no");
        }
        if (ownerDto.getEmail() != null && !ownerDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (ownerDto.getIsDeleted() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (ownerDto.getMode() != null && !ownerDto.getMode().isEmpty()) {
            queryStr.append(" AND t.mode = :mode");
        }

        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<Owner> query = entityManager.createQuery(queryStr.toString(), Owner.class);

        query.setParameter("createdBy", ids);

        if (ownerDto.getId() != null) {
            query.setParameter("mrid", "%" + ownerDto.getId().toString() + "%");
        }
        if (ownerDto.getFull_name() != null && !ownerDto.getFull_name().isEmpty()) {
            query.setParameter("full_name", "%" + ownerDto.getFull_name() + "%");
        }
        if (ownerDto.getName() != null && !ownerDto.getName().isEmpty()) {
            query.setParameter("name", "%" + ownerDto.getName() + "%");
        }
        if (ownerDto.getAddress() != null && !ownerDto.getAddress().isEmpty()) {
            query.setParameter("address", "%" + ownerDto.getAddress() + "%");
        }
        if (ownerDto.getCity() != null && !ownerDto.getCity().isEmpty()) {
            query.setParameter("city", "%" + ownerDto.getCity() + "%");
        }
        if (ownerDto.getPhone_no() != null && !ownerDto.getPhone_no().isEmpty()) {
            query.setParameter("phone_no", ownerDto.getPhone_no());
        }
        if (ownerDto.getEmail() != null && !ownerDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + ownerDto.getEmail() + "%");
        }
        if (ownerDto.getIsDeleted() != null) {
            query.setParameter("isDeleted", ownerDto.getIsDeleted());
        }
        if (ownerDto.getMode() != null && !ownerDto.getMode().isEmpty()) {
            query.setParameter("mode", ownerDto.getMode());
        }

        query.setFirstResult(first);
        query.setMaxResults(limit);

        return query.getResultList();
    }

    /**
     * @param ownerDto
     * @return
     */
    @Override
    public Long countAllOwnerByField(OwnerDto ownerDto, List<UUID> ids) {
        StringBuilder queryStr = new StringBuilder("SELECT count(t) FROM Owner t WHERE 1=1");

        if (ownerDto.getId() != null) {
            queryStr.append(" AND t.mrid LIKE :mrid");
        }
        if (ownerDto.getFull_name() != null && !ownerDto.getFull_name().isEmpty()) {
            queryStr.append(" AND t.full_name LIKE :full_name");
        }
        if (ownerDto.getName() != null && !ownerDto.getName().isEmpty()) {
            queryStr.append(" AND t.name LIKE :name");
        }
        if (ownerDto.getAddress() != null && !ownerDto.getAddress().isEmpty()) {
            queryStr.append(" AND t.address LIKE :address");
        }
        if (ownerDto.getCity() != null && !ownerDto.getCity().isEmpty()) {
            queryStr.append(" AND t.city LIKE :city");
        }
        if (ownerDto.getPhone_no() != null && !ownerDto.getPhone_no().isEmpty()) {
            queryStr.append(" AND t.phone_no = :phone_no");
        }
        if (ownerDto.getEmail() != null && !ownerDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (ownerDto.getIsDeleted() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (ownerDto.getMode() != null && !ownerDto.getMode().isEmpty()) {
            queryStr.append(" AND t.mode = :mode");
        }

        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<Long> query = entityManager.createQuery(queryStr.toString(), Long.class);

        query.setParameter("createdBy", ids);

        if (ownerDto.getId() != null) {
            query.setParameter("mrid", "%" + ownerDto.getId().toString() + "%");
        }
        if (ownerDto.getFull_name() != null && !ownerDto.getFull_name().isEmpty()) {
            query.setParameter("full_name", "%" + ownerDto.getFull_name() + "%");
        }
        if (ownerDto.getName() != null && !ownerDto.getName().isEmpty()) {
            query.setParameter("name", "%" + ownerDto.getName() + "%");
        }
        if (ownerDto.getAddress() != null && !ownerDto.getAddress().isEmpty()) {
            query.setParameter("address", "%" + ownerDto.getAddress() + "%");
        }
        if (ownerDto.getCity() != null && !ownerDto.getCity().isEmpty()) {
            query.setParameter("city", "%" + ownerDto.getCity() + "%");
        }
        if (ownerDto.getPhone_no() != null && !ownerDto.getPhone_no().isEmpty()) {
            query.setParameter("phone_no", ownerDto.getPhone_no());
        }
        if (ownerDto.getEmail() != null && !ownerDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + ownerDto.getEmail() + "%");
        }
        if (ownerDto.getIsDeleted() != null) {
            query.setParameter("isDeleted", ownerDto.getIsDeleted());
        }
        if (ownerDto.getMode() != null && !ownerDto.getMode().isEmpty()) {
            query.setParameter("mode", ownerDto.getMode());
        }

        return query.getSingleResult();
    }
}
