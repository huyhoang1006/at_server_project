package vn.automationandtesting.atproject.repository.owner;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.owner.Owner;
import vn.automationandtesting.atproject.repository.OwnerUserRepositoryCustom;

import java.util.List;
import java.util.UUID;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, UUID>, OwnerRepositoryCustom {

    @Query(
            value = "select count(created_by) from owner " +
                    "where owner.created_by = ?1 and pre = ?2", nativeQuery = true
    )
    int countByCreatedAndPre(UUID id, String pre);

    @Query(
            value = "select count(created_by) from owner " +
                    "where owner.ref_id = ?1 and created_by = ?2", nativeQuery = true
    )
    int countByRef(UUID ref_id, UUID user_id);

    @Query(
            value = "select * from owner " +
                    "where owner.ref_id = ?1 and created_by = ?2 ORDER BY created_on offset ?3 limit ?4", nativeQuery = true
    )
    List<Owner> getOwnerByRefAndCreated(UUID ref_id, UUID user_id, int first, int limit);

    @Query(
            value = "select * from owner " +
                    "where owner.created_by = ?1 and pre = ?2 ORDER BY created_on offset ?3 limit ?4", nativeQuery = true
    )
    List<Owner> findByCreatedAndPre(UUID id, String pre, int first, int limit);

    @Query(
            value = "select * from owner " +
                    "where owner.created_by = ?1 ORDER BY created_on offset ?2 limit ?3", nativeQuery = true
    )
    List<Owner> findByCreated(UUID id, int first, int limit);

    @Query(
            value = "select * from owner " +
                    "where owner.ref_id = ?1 ORDER BY created_on", nativeQuery = true
    )
    List<Owner> getOwnerByParentId(UUID refId);

    @Query(
            value = "select count(*) from owner " +
                    "where owner.mode = ?1", nativeQuery = true
    )
    int countOwnerByRole(String mode);

    @Query(
            value = "select * from owner " +
                    "where owner.mode = ?1 ORDER BY created_on offset ?2 limit ?3", nativeQuery = true
    )
    List<Owner> getOwnerByRole(String mode, int first, int limit);

    @Query(
            value = "select count(created_by) from owner " +
                    "where owner.created_by = ?1", nativeQuery = true
    )
    int countByCreated(UUID id);

    @Query(
            value = "select count(created_by) from owner " +
                    "where owner.ref_id = ?1", nativeQuery = true
    )
    int countByRef(UUID id);

    @Query(
            value = "select * from owner " +
                    "where owner.ref_id = ?1 ORDER BY created_on offset ?2 limit ?3", nativeQuery = true
    )
    List<Owner> findByRef(UUID id, int first, int limit);

    @Query(
            value = "select * from owner " +
                    "where owner.created_by = ?1", nativeQuery = true
    )
    List<Owner> findByCreated(UUID id);

    @Query(
            value = "select * from owner " +
                    "where owner.created_by = ?1 and mode = ?2 ORDER BY created_on offset ?3 limit ?4", nativeQuery = true
    )
    List<Owner> findByCreated(UUID id, String mode, int first, int limit);

    @Query(
            value = "select * from owner " +
                    "where owner.mrid = ?1", nativeQuery = true
    )
    Owner findByMrid(UUID id);

    @Query(
            value = "select * from owner " +
                    "where owner.user_id = ?1 and name = ?2", nativeQuery = true
    )
    Owner findByUserIdAndName(UUID id, String name);


    @Query(
            value = "select * from owner " +
                    "where owner.created_by = ?1 and mode = ?2", nativeQuery = true
    )
    List<Owner> findParentByCreated(UUID id, String mode);

    @Query(
            value = "select count(*) from owner " +
                    "where owner.created_by IN ?1", nativeQuery = true
    )
    int countAllOwnerByIds(List<UUID> ids);

    @Query(
            value = "select count(*) from owner " +
                    "where owner.created_by IN ?1 and mode = ?2 ", nativeQuery = true
    )
    int countAllOwnerByIdsAndMode(List<UUID> ids, String mode);

    @Query(
            value = "select * from owner " +
                    "where owner.full_name = ?1", nativeQuery = true
    )
    List<Owner> findByFullName(String name);
}
