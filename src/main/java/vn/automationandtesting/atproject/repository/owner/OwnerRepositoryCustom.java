package vn.automationandtesting.atproject.repository.owner;

import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.owner.Owner;
import java.util.List;
import java.util.UUID;

public interface OwnerRepositoryCustom {
    List<Owner> getAllParentByRole(List<UUID> ownerUserList, String role);

    List<Owner> findAllOwnerByField(OwnerDto ownerDto,int first,int limit,List<UUID> ids);

    Long countAllOwnerByField(OwnerDto ownerDto, List<UUID> ids);
}
