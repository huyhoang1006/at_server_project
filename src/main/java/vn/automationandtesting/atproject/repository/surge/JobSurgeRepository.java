package vn.automationandtesting.atproject.repository.surge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.surge.JobsSurge;

import java.util.List;
import java.util.UUID;

@Repository
public interface JobSurgeRepository extends JpaRepository<JobsSurge, UUID> {
    @Query(
            value = "select * from jobssurge at " +
                    "where at.asset_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<JobsSurge> findAllJobByAssetId(UUID asset_id, UUID user_id, String userId);

    @Query(
            value = "select * from jobssurge at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<JobsSurge> findJobById(UUID id);

    @Query(
            value = "select * from jobssurge at " +
                    "where at.name = ?1", nativeQuery = true
    )
    List<JobsSurge> findJobByName(String name);

    @Query(
            value = "select * from jobssurge at " +
                    "where at.name = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<JobsSurge> findJobByNameAndAsset(String name, UUID asset_id);

    @Query(
            value = "select * from jobssurge", nativeQuery = true
    )
    List<JobsSurge> findAll();

    @Query(
            value = "select * from jobssurge at where at.asset_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<JobsSurge> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    @Query(
            value = "select * from jobssurge j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%) offset ?4 limit ?5 ", nativeQuery = true
    )
    List<JobsSurge> findJobByAssetId(UUID id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(created_by) from jobssurge j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%)", nativeQuery = true
    )
    int countJobByAssetId(UUID id, UUID user_id, String userId);
}
