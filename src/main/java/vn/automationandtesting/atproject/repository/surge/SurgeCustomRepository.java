package vn.automationandtesting.atproject.repository.surge;

import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.List;
import java.util.UUID;

public interface SurgeCustomRepository {
    List<Surge> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
