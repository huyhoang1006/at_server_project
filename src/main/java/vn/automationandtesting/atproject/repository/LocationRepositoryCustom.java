package vn.automationandtesting.atproject.repository;

import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.List;
import java.util.UUID;

public interface LocationRepositoryCustom {
    List<Location> findAllLocationByField(LocationDto locationDto, int first, int limit, List<UUID> createdByList, String user_id);

    Long countAllLocationByField(LocationDto locationDto, List<UUID> createdByList, String user_id);

}
