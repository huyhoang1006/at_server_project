package vn.automationandtesting.atproject.repository.objectLog.location;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.AuditLocationLog;

import java.util.UUID;

@Repository
public interface AuditLocationLogRepository extends JpaRepository<AuditLocationLog, UUID> {
}
