package vn.automationandtesting.atproject.repository.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.automationandtesting.atproject.entity.message.Message;

import java.util.List;
import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {
    @Query(
            value = "select * from message at " +
                    "where group_id = ?1 order by at.created_on desc offset ?2 limit ?3", nativeQuery = true
    )
    List<Message> findGroupByCreatedIdAndGroupId(UUID group_id, int first, int limit);

    @Query(
            value = "select * from message as at " +
                    "where at.group_id = ?1 and at.receiver_member LIKE CONCAT('%', ?2, '%') order by at.created_on desc", nativeQuery = true
    )
    List<Message> getAllMessageByUnread(UUID group_id, String id);

    @Query(
            value = "select * from message at " +
                    "where group_id = ?1 and id = ?2 order by at.created_on desc", nativeQuery = true
    )
    List<Message> findMessageByGroupIdAndId(UUID group_id, UUID id);

    @Query(
            value = "select * from message at " +
                    "where id IN ?1", nativeQuery = true
    )
    List<Message> getAllMessageById(List<UUID> ids);
}
