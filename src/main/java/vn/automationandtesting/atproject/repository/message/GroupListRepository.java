package vn.automationandtesting.atproject.repository.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.message.GroupList;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupListRepository extends JpaRepository<GroupList, UUID> {
    @Query(
            value = "select * from group_message at " +
                    "where at.created_by = ?1 or (at.member like %?2%) order by at.updated_on desc offset ?3 limit ?4", nativeQuery = true
    )
    List<GroupList> findGroupByCreatedIdAndMember(UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select * from group_message at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<GroupList> findGroupListById(UUID groupId);

    @Query(
            value = "SELECT * FROM group_message at " +
                    "WHERE at.type = ?1 AND (at.member LIKE %?2% AND at.member LIKE %?3%)",
            nativeQuery = true
    )
    List<GroupList> findSingleGroupListByTypeAndId(String type, String user_id, String other_id);

    @Query(
            value = "select * from group_message at " +
                    "where at.type = ?1 and at.member like %?2% ", nativeQuery = true
    )
    List<GroupList> findGroupListByTypeAndId(String type, String user_id);
}
