package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.UUID;

@Repository
public interface AssetCommonRepository extends JpaRepository<Asset, UUID>, AssetCommonCustom  {
}
