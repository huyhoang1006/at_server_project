package vn.automationandtesting.atproject.repository.current;

import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.List;
import java.util.UUID;

public interface CurrentCustomRepository {
    List<Current> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
