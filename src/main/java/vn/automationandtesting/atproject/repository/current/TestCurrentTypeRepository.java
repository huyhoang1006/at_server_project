package vn.automationandtesting.atproject.repository.current;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.current.TestCurrentType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestCurrentTypeRepository extends JpaRepository<TestCurrentType, UUID> {
    @Query(
            value = "select * from testcurrent_type", nativeQuery = true
    )
    List<TestCurrentType> findAll();

    @Query(
            value = "select * from testcurrent_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestCurrentType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testcurrent_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestCurrentType> findTestTypeById(String code);
}
