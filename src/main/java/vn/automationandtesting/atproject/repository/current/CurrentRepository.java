package vn.automationandtesting.atproject.repository.current;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.List;
import java.util.UUID;

@Repository
public interface CurrentRepository extends JpaRepository<Current, UUID>, CurrentCustomRepository {
    @Query(
            value = "select * from current_voltage at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Current> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%) offset ?4 limit ?5", nativeQuery = true
    )
    List<Current> findAssetByLocationId(UUID location_id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(id) from current_voltage at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    int countAssetByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Current> findAssetById(UUID id);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.id = ?1 and (at.created_by IN ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    Current findAssetById(UUID id, List<UUID> ids, String userId);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Current> findBySerial(String serial);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Current> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from current_voltage", nativeQuery = true
    )
    List<Current> findAll();

    @Query(
            value = "select * from current_voltage at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Current> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    @Query(
            value = "select count(id) from current_voltage at " +
                    "where at.created_by IN ?1 or at.collabs like %?2%", nativeQuery = true
    )
    int countAssetList(List<UUID> ids, String userId);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.created_by IN ?1 or at.collabs like %?2% order by at.created_on DESC offset ?3 limit ?4", nativeQuery = true
    )
    List<Current> findAssetList(List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select * from current_voltage at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% ) order by at.created_on DESC offset ?4 limit ?5", nativeQuery = true
    )
    List<Current> findAssetListByLocationId(UUID location_id, List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select count(*) from current_voltage at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% )", nativeQuery = true
    )
    int countAssetListByLocationId(UUID location_id, List<UUID> ids, String userId);
}
