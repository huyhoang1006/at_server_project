package vn.automationandtesting.atproject.repository.current;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.current.TestsCurrent;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestCurrentRepository extends JpaRepository<TestsCurrent, UUID> {
    @Query(
            value = "select * from testscurrent at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestsCurrent> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testscurrent at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestsCurrent> findTestById(UUID id);

    @Query(
            value = "select * from testscurrent at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestsCurrent> findTestByType(UUID type_id);

    @Query(
            value = "select * from testscurrent at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestsCurrent> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
