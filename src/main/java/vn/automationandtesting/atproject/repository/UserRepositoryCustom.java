package vn.automationandtesting.atproject.repository;

import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserRepositoryCustom {
    List<User> findAllUserByField(UserReqDto userReqDto, int first, int limit, List<UUID> createdByList);

    Long countAllUserByField(UserReqDto userReqDto, List<UUID> createdByList);
}
