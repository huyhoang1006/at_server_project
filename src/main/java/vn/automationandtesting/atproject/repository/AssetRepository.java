package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AssetRepository extends JpaRepository<Asset, UUID>, JpaSpecificationExecutor<Asset>, AssetCustomRepository {
    List<Asset> findAllByIsDeleted(boolean isDeleted);

    Optional<Asset> findByMridAndIsDeleted(UUID assetId, boolean isDeleted);

    Optional<Asset> findByMridAndIsDeletedAndCreatedBy(UUID assetId, boolean isDeleted, UUID createdById);

    Optional<Asset> findByMridAndIsDeletedAndCollabsContains(UUID assetId, boolean isDeleted, String collabId);

    List<Asset> findAllByIsDeletedAndMridInAndCreatedByOrIsDeletedAndMridInAndCollabsContaining(
            boolean isDeleted1,
            List<UUID> assetId1,
            UUID createdById,
            boolean isDeleted2,
            List<UUID> assetId2,
            UUID collabId
    );

    @Query(
            value = "select  * from asset at " +
                    "where at.location_id = ?1 and at.is_deleted = ?2 and at.created_by = ?3 order by at.created_on DESC", nativeQuery = true
    )
    List<Asset> findAllByLocation_idAndIsDeletedAndCreatedBy(UUID location_id, boolean isDeleted, UUID createdById);

    @Query(
            value = "select  * from asset at " +
                    "where at.location_id = ?1 and at.is_deleted = ?2 and at.collabs Like %?3% order by at.created_on DESC", nativeQuery = true
    )
    List<Asset> findAllByLocation_idAndIsDeletedAndCollabs(UUID location_id, boolean isDeleted, String createdById);

    @Query(
            value = "select * from asset at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%) offset ?4 limit ?5", nativeQuery = true
    )
    List<Asset> findAssetByLocationId(UUID location_id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select * from asset at " +
                    "where at.mrid = ?1 and (at.created_by IN ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    Asset findAssetById(UUID id, List<UUID> ids, String userId);

    @Query(
            value = "select count(*) from asset at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    int countAssetByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select count(mrid) from asset at " +
                    "where at.created_by IN ?1 or at.collabs like %?2%", nativeQuery = true
    )
    int countAssetList(List<UUID> ids, String userId);

    @Query(
            value = "select * from asset at " +
                    "where at.created_by IN ?1 or at.collabs like %?2% order by at.created_on DESC offset ?3 limit ?4", nativeQuery = true
    )
    List<Asset> findAssetList(List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select * from asset at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% ) order by at.created_on DESC offset ?4 limit ?5", nativeQuery = true
    )
    List<Asset> findAssetListByLocationId(UUID location_id, List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select count(*) from asset at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% )", nativeQuery = true
    )
    int countAssetListByLocationId(UUID location_id, List<UUID> ids, String userId);
}
