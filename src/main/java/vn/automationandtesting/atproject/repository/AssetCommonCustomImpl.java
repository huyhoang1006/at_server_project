package vn.automationandtesting.atproject.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.AssetMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.entity.voltage.Voltage;
import vn.automationandtesting.atproject.repository.circuit.CircuitRepository;
import vn.automationandtesting.atproject.repository.current.CurrentRepository;
import vn.automationandtesting.atproject.repository.disconnector.DisconnectorRepository;
import vn.automationandtesting.atproject.repository.power.PowerRepository;
import vn.automationandtesting.atproject.repository.surge.SurgeRepository;
import vn.automationandtesting.atproject.repository.voltage.VoltageRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;
import java.util.stream.Collectors;

public class AssetCommonCustomImpl implements AssetCommonCustom {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AssetMapper assetMapper;

    @Autowired
    private CircuitRepository circuitRepository;

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private CurrentRepository currentRepository;

    @Autowired
    private VoltageRepository voltageRepository;

    @Autowired
    private DisconnectorRepository disconnectorRepository;

    @Autowired
    private PowerRepository powerRepository;

    @Autowired
    private SurgeRepository surgeRepository;

    /**
     * @param asset
     * @param ids
     * @return
     */
    @Override
    public int countAsset(String asset, String userId, List<UUID> ids) {
        // Tạo chuỗi truy vấn SQL động
        StringBuilder queryStr = new StringBuilder("SELECT COUNT(created_by) FROM ");
        queryStr.append(asset).append(" at ").append("WHERE ( at.collabs LIKE :userId ");
        if (ids != null && !ids.isEmpty()) {
            queryStr.append("OR at.created_by IN :ids");
        }
        queryStr.append(" ) ");
        // Tạo truy vấn native
        Query query = entityManager.createNativeQuery(queryStr.toString());
        // Đặt tham số cho `userId`
        query.setParameter("userId", "%" + userId + "%");
        query.setParameter("ids", ids);
        return ((Number) query.getSingleResult()).intValue();
    }

    /**
     * @param asset
     * @param userId
     * @param ids
     * @return
     */
    @Override
    public JSONObject countAssetInclude(Object asset, String userId, List<UUID> ids) {
        HashMap<String, String> assetData = (HashMap<String, String>) asset;
        JSONObject jsonObject = new JSONObject();
        if(assetData.get("asset").isEmpty()) {
            assetData.replace("asset", "asset");
            int dataTransformer = assetRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "circuit_breaker");
            int dataCircuit = circuitRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "current_voltage");
            int dataCurrent = currentRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "voltage_trans");
            int dataVoltage = voltageRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "disconnector");
            int dataDisconnector = disconnectorRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "surge_arrester");
            int dataSurge = surgeRepository.countAssetInclude(asset, userId, ids);
            assetData.replace("asset", "power_cable");
            int dataPower = powerRepository.countAssetInclude(asset, userId, ids);

            try {
                jsonObject.put("Transformer", dataTransformer);
                jsonObject.put("Circuit breaker", dataCircuit);
                jsonObject.put("Current transformer", dataCurrent);
                jsonObject.put("Voltage transformer", dataVoltage);
                jsonObject.put("Disconnector", dataDisconnector);
                jsonObject.put("Surge arrester", dataSurge);
                jsonObject.put("Power cable", dataPower);
                jsonObject.put("All", dataTransformer + dataCircuit + dataCurrent + dataVoltage + dataDisconnector + dataSurge + dataPower);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("asset")) {
            try {
                int dataTransformer = assetRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Transformer", dataTransformer);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("circuit_breaker")) {
            try {
                int dataCircuit = circuitRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Circuit breaker", dataCircuit);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("current_voltage")) {
            try {
                int dataCurrent = currentRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Current transformer", dataCurrent);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("voltage_trans")) {
            try {
                int dataVoltage = voltageRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Voltage transformer", dataVoltage);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("disconnector")) {
            try {
                int dataDisconnector = disconnectorRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Disconnector", dataDisconnector);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("surge_arrester")) {
            try {
                int dataSurge = surgeRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Surge arrester", dataSurge);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else if (assetData.get("asset").equals("power_cable")) {
            try {
                int dataPower = powerRepository.countAssetInclude(asset, userId, ids);
                jsonObject.put("Transformer", dataPower);
                return jsonObject;
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else {
            return null;
        }
    }

    /**
     * @param data
     * @param first
     * @param sl
     * @return
     */
    @Override
    public Object getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl) {
        HashMap<String, String> dataAsset = (HashMap<String, String>) data;

        if(dataAsset.get("asset").equals("Transformer")) {
            dataAsset.replace("asset", "asset");
        } else if(dataAsset.get("asset").equals("Circuit breaker")) {
            dataAsset.replace("asset", "circuit_breaker");
        } else if(dataAsset.get("asset").equals("Current transformer")) {
            dataAsset.replace("asset", "current_voltage");
        } else if(dataAsset.get("asset").equals("Voltage transformer")) {
            dataAsset.replace("asset", "voltage_trans");
        } else if(dataAsset.get("asset").equals("Disconnector")) {
            dataAsset.replace("asset", "disconnector");
        } else if(dataAsset.get("asset").equals("Surge arrester")) {
            dataAsset.replace("asset", "surge_arrester");
        } else if(dataAsset.get("asset").equals("Power cable")) {
            dataAsset.replace("asset", "power_cable");
        }

        if(dataAsset.get("asset").equals("circuit_breaker")) {
            List<Circuit> circuits = circuitRepository.getAssetInclude(data, userId, ids, first, sl);
            return circuits;
        } else if(dataAsset.get("asset").equals("asset")) {
            List<Asset> assets = assetRepository.getAssetInclude(data, userId, ids, first, sl);
            List<AssetDto> assetDtos = assets.stream().map(asset -> assetMapper.assetToAssetDto(asset)).collect(Collectors.toList());
            return assetDtos;
        } else if(dataAsset.get("asset").equals("current_voltage")) {
            List<Current> currents = currentRepository.getAssetInclude(data, userId, ids, first, sl);
            return currents;
        } else if(dataAsset.get("asset").equals("voltage_trans")) {
            List<Voltage> voltages = voltageRepository.getAssetInclude(data, userId, ids, first, sl);
            return voltages;
        } else if(dataAsset.get("asset").equals("disconnector")) {
            List<Disconnector> disconnectors = disconnectorRepository.getAssetInclude(data, userId, ids, first, sl);
            return disconnectors;
        } else if(dataAsset.get("asset").equals("surge_arrester")) {
            List<Surge> surges = surgeRepository.getAssetInclude(data, userId, ids, first, sl);
            return surges;
        } else if(dataAsset.get("asset").equals("power_cable")) {
            List<PowerCable> powerCables = powerRepository.getAssetInclude(data, userId, ids, first, sl);
            return powerCables;
        } else {
            return null;
        }
    }

    /**
     * @param location_id
     * @param userId
     * @param ids
     * @return
     */
    @Override
    public JSONObject countAssetListOffset(UUID location_id, String userId, List<UUID> ids) {
        JSONObject jsonObject = new JSONObject();
        int dataTransformer = assetRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataCircuit = circuitRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataCurrent = currentRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataDisconnector = disconnectorRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataVoltage = voltageRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataSurge = surgeRepository.countAssetListByLocationId(location_id, ids, userId);
        int dataPower = powerRepository.countAssetListByLocationId(location_id, ids, userId);

        try {
            jsonObject.put("Transformer", dataTransformer);
            jsonObject.put("Circuit breaker", dataCircuit);
            jsonObject.put("Current transformer", dataCurrent);
            jsonObject.put("Voltage transformer", dataVoltage);
            jsonObject.put("Disconnector", dataDisconnector);
            jsonObject.put("Surge arrester", dataSurge);
            jsonObject.put("Power cable", dataPower);
            jsonObject.put("All", dataTransformer + dataCircuit + dataCurrent + dataVoltage + dataDisconnector + dataSurge + dataPower);
            return jsonObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }
}
