package vn.automationandtesting.atproject.repository;

import java.util.List;

public interface CommonRepositoryCustom {
    void updateLocationRef(String tableName, String locationOldId, String locationNewId);

    List<Object[]> findReferencingTables(String tableName, String columnName);
}
