package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.controller.dto.request.UserMessageDto;
import vn.automationandtesting.atproject.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID>, UserRepositoryCustom {
    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);

    List<User> findAllByIsDeleted(boolean isDeleted);

    Optional<User> findByIdAndIsDeleted(UUID userId, boolean isDeleted);

    @Query(
        value = "select * from at_project_schema.user as t " +
            "where t.id = ?1", nativeQuery = true
    )
    List<User> findAllUserById(UUID id);

    @Query(
            value = "select * from at_project_schema.user as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    List<User> findAllUserByCreated(UUID createdById);

    @Query(
            value = "select * from at_project_schema.user as t " +
                    "where t.created_by = ?1 offset ?2 limit ?3", nativeQuery = true
    )
    List<User> findUserByCreated(UUID createdById, int first, int limit);

    @Query(
            value = "select count(id) from at_project_schema.user as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    int countUserByCreated(UUID createdById);

    @Query(
            value = "select * from at_project_schema.user as t " +
                    "where ( t.username like %?1% or t.email like %?1% ) and is_deleted = false", nativeQuery = true
    )
    List<User> findAllUserByEmailOrName(String data);
}
