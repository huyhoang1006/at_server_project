package vn.automationandtesting.atproject.repository.voltage;

import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.List;
import java.util.UUID;

public interface VoltageCustomRepository {
    List<Voltage> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
