package vn.automationandtesting.atproject.repository.voltage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.voltage.TestsVoltage;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestVoltageRepository extends JpaRepository<TestsVoltage, UUID> {
    @Query(
            value = "select * from testsvoltage at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestsVoltage> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testsvoltage at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestsVoltage> findTestById(UUID id);

    @Query(
            value = "select * from testsvoltage at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestsVoltage> findTestByType(UUID type_id);

    @Query(
            value = "select * from testsvoltage at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestsVoltage> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
