package vn.automationandtesting.atproject.repository.voltage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.List;
import java.util.UUID;

@Repository
public interface VoltageRepository extends JpaRepository<Voltage, UUID>, VoltageCustomRepository {
    @Query(
            value = "select * from voltage_trans at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Voltage> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%) offset ?4 limit ?5", nativeQuery = true
    )
    List<Voltage> findAssetByLocationId(UUID location_id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(created_by) from voltage_trans at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    int countAssetByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Voltage> findAssetById(UUID id);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.id = ?1 and (at.created_by IN ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    Voltage findAssetById(UUID id, List<UUID> ids, String userId);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Voltage> findBySerial(String serial);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Voltage> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from voltage_trans", nativeQuery = true
    )
    List<Voltage> findAll();

    @Query(
            value = "select * from voltage_trans at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Voltage> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    @Query(
            value = "select count(id) from voltage_trans at " +
                    "where at.created_by IN ?1 or at.collabs like %?2%", nativeQuery = true
    )
    int countAssetList(List<UUID> ids, String userId);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.created_by IN ?1 or at.collabs like %?2% order by at.created_on DESC offset ?3 limit ?4", nativeQuery = true
    )
    List<Voltage> findAssetList(List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select * from voltage_trans at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% ) order by at.created_on DESC offset ?4 limit ?5", nativeQuery = true
    )
    List<Voltage> findAssetListByLocationId(UUID location_id, List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select count(*) from voltage_trans at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% )", nativeQuery = true
    )
    int countAssetListByLocationId(UUID location_id, List<UUID> ids, String userId);
}
