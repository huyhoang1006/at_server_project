package vn.automationandtesting.atproject.repository.disconnector;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;

import java.util.List;
import java.util.UUID;

@Repository
public interface DisconnectorRepository extends JpaRepository<Disconnector, UUID>, DisconnectorCustomRepository {
    @Query(
            value = "select * from disconnector at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Disconnector> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from disconnector at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%) offset ?4 limit ?5", nativeQuery = true
    )
    List<Disconnector> findAssetByLocationId(UUID location_id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(id) from disconnector at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    int countAssetByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from disconnector at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Disconnector> findAssetById(UUID id);

    @Query(
            value = "select * from disconnector at " +
                    "where at.id = ?1 and (at.created_by IN ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    Disconnector findAssetById(UUID id, List<UUID> ids, String userId);

    @Query(
            value = "select * from disconnector at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Disconnector> findBySerial(String serial);

    @Query(
            value = "select * from disconnector at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Disconnector> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from disconnector", nativeQuery = true
    )
    List<Disconnector> findAll();

    @Query(
            value = "select * from disconnector at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Disconnector> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    @Query(
            value = "select * from jobspower j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like ?3) offset ?4 limit ?5 ", nativeQuery = true
    )
    List<Job> findJobByAssetId(UUID id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(id) from jobspower j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like ?3)", nativeQuery = true
    )
    int countJobByAssetId(UUID id, UUID user_id, String userId);

    @Query(
            value = "select count(id) from disconnector at " +
                    "where at.created_by IN ?1 or at.collabs like %?2%", nativeQuery = true
    )
    int countAssetList(List<UUID> ids, String userId);

    @Query(
            value = "select * from disconnector at " +
                    "where at.created_by IN ?1 or at.collabs like %?2% order by at.created_on DESC offset ?3 limit ?4", nativeQuery = true
    )
    List<Disconnector> findAssetList(List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select * from disconnector at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% ) order by at.created_on DESC offset ?4 limit ?5", nativeQuery = true
    )
    List<Disconnector> findAssetListByLocationId(UUID location_id, List<UUID> ids, String userId, int first, int limit);

    @Query(
            value = "select count(*) from disconnector at " +
                    "where location_id = ?1 and ( at.created_by IN ?2 or at.collabs like %?3% )", nativeQuery = true
    )
    int countAssetListByLocationId(UUID location_id, List<UUID> ids, String userId);
}
