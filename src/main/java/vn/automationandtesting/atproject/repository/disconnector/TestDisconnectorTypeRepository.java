package vn.automationandtesting.atproject.repository.disconnector;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.disconnector.TestDisconnectorType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestDisconnectorTypeRepository extends JpaRepository<TestDisconnectorType, UUID> {
    @Query(
            value = "select * from testdisconnect_type", nativeQuery = true
    )
    List<TestDisconnectorType> findAll();

    @Query(
            value = "select * from testdisconnect_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestDisconnectorType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testdisconnect_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestDisconnectorType> findTestTypeById(String code);
}
