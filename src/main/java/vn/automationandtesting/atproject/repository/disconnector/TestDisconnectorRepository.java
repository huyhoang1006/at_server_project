package vn.automationandtesting.atproject.repository.disconnector;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.disconnector.TestsDisconnector;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestDisconnectorRepository extends JpaRepository<TestsDisconnector, UUID> {
    @Query(
            value = "select * from testsdisconnect at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestsDisconnector> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testsdisconnect at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestsDisconnector> findTestById(UUID id);

    @Query(
            value = "select * from testsdisconnect at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestsDisconnector> findTestByType(UUID type_id);

    @Query(
            value = "select * from testsdisconnect at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestsDisconnector> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
