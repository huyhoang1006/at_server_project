package vn.automationandtesting.atproject.repository.disconnector;

import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;

import java.util.List;
import java.util.UUID;

public interface DisconnectorCustomRepository {
    List<Disconnector> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
