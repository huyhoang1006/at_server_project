package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.automationandtesting.atproject.entity.Group;
import vn.automationandtesting.atproject.entity.GroupPermission;
import vn.automationandtesting.atproject.entity.GroupPermissionPK;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupPermissionRepository extends JpaRepository<GroupPermission, GroupPermissionPK> {
    void deleteByGroup(Group group);

    @Query(
            value = "select Cast(permission_id as varchar) from group_permission gp " +
                    "where gp.group_id = ?1", nativeQuery = true
    )
    List<String> findPermissionByGroupId(UUID groupId);

    @Modifying
    @Query(
            value = "delete from group_permission " +
                    "where group_id = :groupId", nativeQuery = true
    )
    @Transactional
    void deltePermissionByGroupId(@Param("groupId") UUID groupId);

    @Modifying
    @Query(
            value = "insert into group_permission(permission_id, group_id) " +
                    "values (:permissionId, :groupId)", nativeQuery = true
    )
    @Transactional
    void insertPermission(@Param("permissionId") UUID permissionId, @Param("groupId") UUID groupId);
}
