package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, UUID>, JpaSpecificationExecutor<Location>, LocationRepositoryCustom {
    Optional<Location> findByMridAndIsDeleted(UUID locationId, boolean isDeleted);

    Optional<Location> findByMridAndIsDeletedAndCreatedBy(UUID locationId, boolean isDeleted, UUID createdById);

    Optional<Location> findByMridAndIsDeletedAndCollabsContains(UUID locationId, boolean isDeleted, String collabId);
    List<Location> findAllByIsDeletedAndMridInAndCreatedByOrIsDeletedAndMridInAndCollabsContaining(
            boolean isDeleted1,
            List<UUID> locationId1,
            UUID createdById,
            boolean isDeleted2,
            List<UUID> locationId2,
            UUID collabId
    );

    @Query(
            value = "select * from location l where l.ref_id = ?1 ORDER BY created_on", nativeQuery = true
    )
    List<Location> findAllByRefId(String id);


    @Query(
            value = "select * from location l where l.mrid = ?1", nativeQuery = true
    )
    List<Location> getLocationById(UUID id);

    @Query(
            value = "select * from location l where l.ref_id = ?1 ORDER BY created_on offset ?2 limit ?3", nativeQuery = true
    )
    List<Location> findLocationByRefId(String id, int first, int limit);

    @Query(
            value = "select count(created_by) from location l where l.ref_id = ?1 ", nativeQuery = true
    )
    int countLocationByRefId(String id);

    @Query(
            value = "select * from location l where l.ref_id = ?1 and (l.created_by = ?2 or l.collabs like CAST(?2 AS TEXT)) ORDER BY created_on offset ?3 limit ?4", nativeQuery = true
    )
    List<Location> findLocationByRefIdAndCreatedAndCollab(String ref_id, UUID user_id, int offset, int limit);

    @Query(
            value = "select count(created_by) from location l where l.ref_id = ?1 and (l.created_by = ?2 or l.collabs like CAST(?2 AS TEXT))", nativeQuery = true
    )
    int countLocationByRefIdAndCreatedAndCollab(String ref_id, UUID user_id);

    @Query(
            value = "select * from location l where ?1 like CONCAT('%', l.mode, '%') and (l.created_by IN ?2 or l.collabs like CAST(?3 AS TEXT)) ORDER BY created_on offset ?4 limit ?5", nativeQuery = true
    )
    List<Location> findAllLocationByCreatedAndCollab(String mode, List<UUID> userList, UUID user_id, int offset, int limit);

    @Query(
            value = "select count(created_by) from location l where ?1 like CONCAT('%', l.mode, '%') and (l.created_by IN ?2 or l.collabs like CAST(?3 AS TEXT))", nativeQuery = true
    )
    int countAllLocationByCreatedAndCollab(String mode, List<UUID> userList, UUID user_id);

    @Query(value = "SELECT COUNT(*) FROM Location l " +
            "JOIN Owner o ON CAST(l.ref_id AS UUID) = o.mrid " +
            "WHERE o.mode = ?1 AND (o.created_by IN ?2 or l.collabs like CAST(?3 AS TEXT))", nativeQuery = true
    )
    int countLocationsByRefIdOwnerAndModeAndCreatedBy(String mode, List<UUID> ids, UUID userId);

    @Query(value = "SELECT COUNT(*) FROM Location l " +
            "WHERE l.created_by IN ?1 or l.collabs like CAST(?2 AS TEXT)", nativeQuery = true
    )
    int countLocationsByCreatedByAndCollab(List<UUID> ids, UUID userId);

    @Query(value = "SELECT COUNT(*) FROM Location l " +
            "WHERE l.mode = ?1 and (l.created_by IN ?2 or l.collabs like CAST(?3 AS TEXT))", nativeQuery = true
    )
    int countLocationsModeByCreatedByAndCollab(String mode, List<UUID> ids, UUID userId);

    @Query(
            value = "select * from location l where l.name = ?1 ", nativeQuery = true
    )
    List<Location> findLocationByName(String name);
}

