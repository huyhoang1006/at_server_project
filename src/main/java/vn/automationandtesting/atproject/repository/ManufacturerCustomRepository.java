package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.ManufacturerCustom;
import java.util.List;
import java.util.UUID;

@Repository
public interface ManufacturerCustomRepository extends JpaRepository<ManufacturerCustom, UUID> {

    @Query(
            value = "select * from manufacturer_custom mt " +
                    "where mt.type like %?1% order by mt.created_on DESC", nativeQuery = true
    )
    List<ManufacturerCustom> findByType(String type);

    @Query(
            value = "select * from manufacturer_custom mt " +
                    "where mt.name = ?1 order by mt.created_on DESC", nativeQuery = true
    )
    List<ManufacturerCustom> findByName(String name);

    @Query(
            value = "select * from manufacturer_custom mt " +
                    "where mt.id = ?1 order by mt.created_on DESC", nativeQuery = true
    )
    List<ManufacturerCustom> getById(UUID id);
}
