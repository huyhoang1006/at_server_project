package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserMessageDto;
import vn.automationandtesting.atproject.controller.dto.request.UserMessageDto;
import vn.automationandtesting.atproject.entity.OwnerUser;

import java.util.List;
import java.util.UUID;

@Repository
public interface OwnerUserRepository extends JpaRepository<OwnerUser, UUID>, OwnerUserRepositoryCustom {
    @Query(
            value = "select * from at_project_schema.owner_user as t " +
                    "where t.username = ?1", nativeQuery = true
    )
    OwnerUser findAllOwnerUserByUserName(String username);

    boolean existsByUsername(String username);

    @Query(
            value = "select * from at_project_schema.owner_user as t " +
                    "where t.id = ?1", nativeQuery = true
    )
    List<OwnerUser> findAllOwnerUserById(UUID id);

    @Query(
            value = "select * from at_project_schema.owner_user as t " +
                    "where t.created_by = ?1 offset ?2 limit ?3", nativeQuery = true
    )
    List<OwnerUser> findOwnerUserByCreated(UUID createdById, int first, int limit);

    @Query(
            value = "select count(id) from at_project_schema.owner_user as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    int countOwnerUserByCreated(UUID createdById);

    @Query(
            value = "select * from at_project_schema.owner_user as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    List<OwnerUser> findAllOwnerUserByCreated(UUID createdById);

    @Query(
            value = "select * from at_project_schema.owner_user as t " +
                    "where ( t.username like %?1% or t.email like %?1% ) and is_deleted = false", nativeQuery = true
    )
    List<OwnerUser> findAllUserByEmailOrName(String data);

}
