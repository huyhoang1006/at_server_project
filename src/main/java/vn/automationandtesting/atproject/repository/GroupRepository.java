package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.Group;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {
    List<Group> findByIsDeleted(boolean isDeleted);
    boolean existsByGroupNameIgnoreCase(String groupName);
    Group findByIdAndIsDeleted(UUID id, boolean isDeleted);

    @Query(
            value = "select * from at_project_schema.group as t " +
                    "where t.created_by = ?1 offset ?2 limit ?3", nativeQuery = true
    )
    List<Group> findGroupByCreated(UUID createdById, int first, int limit);

    @Query(
            value = "select * from at_project_schema.group as t " +
                    "offset ?1 limit ?2", nativeQuery = true
    )
    List<Group> findGroup(int first, int limit);

    @Query(
            value = "select * from at_project_schema.group as t " +
                    "where t.group_name = ?1", nativeQuery = true
    )
    List<Group> findGroupByName(String name);

    @Query(
            value = "select * from at_project_schema.group as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    List<Group> findAllGroupByCreated(UUID createdById);

    @Query(
            value = "select count(id) from at_project_schema.group as t " +
                    "where t.created_by = ?1", nativeQuery = true
    )
    int countGroupByCreated(UUID createdById);

    @Query(
            value = "select count(id) from at_project_schema.group", nativeQuery = true
    )
    int countGroup();
}
