package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AssetCustomRepository {
    List<Asset> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
