package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.automationandtesting.atproject.entity.Attachment;

import java.util.List;
import java.util.UUID;


@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    @Query(
            value = "select * from attachment at " +
                    "where at.id_foreign = ?1", nativeQuery = true
    )
    List<Attachment> findAllById_foreign(UUID id_foreign);

    @Modifying
    @Query(
            value = "delete from attachment " +
                    "where id_foreign = ?1", nativeQuery = true
    )
    @Transactional
    void deleteAttachmentById_foreign(UUID id_foreign);


    @Modifying
    @Query(
            value = "update attachment set name=?1 " +
                    "where id_foreign = ?2", nativeQuery = true
    )
    @Transactional
    void updateNameById_foreign(String name, UUID id_foreign);

    @Query(
            value = "select name from attachment at " +
                    "where at.id_foreign = ?1", nativeQuery = true
    )
    String findNamebyId_foreign(UUID id_foreign);

}
