package vn.automationandtesting.atproject.repository;

import org.springframework.beans.factory.annotation.Autowired;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.Gender;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.exception.UserNotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class UserRepositoryCustomImpl implements UserRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private RoleRepository roleRepository;


    /**
     * @param userReqDto
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<User> findAllUserByField(UserReqDto userReqDto, int first, int limit, List<UUID> createdByList) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM User t WHERE 1=1");

        if (userReqDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (userReqDto.getUsername() != null && !userReqDto.getUsername().isEmpty()) {
            queryStr.append(" AND t.username LIKE :username");
        }
        if (userReqDto.getFirstName() != null && !userReqDto.getFirstName().isEmpty()) {
            queryStr.append(" AND t.firstName LIKE :firstName");
        }
        if (userReqDto.getLastName() != null && !userReqDto.getLastName().isEmpty()) {
            queryStr.append(" AND t.lastName LIKE :lastName");
        }
        if (userReqDto.getGender() != null && !userReqDto.getGender().isEmpty()) {
            queryStr.append(" AND t.gender = :gender");
        }
        if (userReqDto.getEmail() != null && !userReqDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (userReqDto.getPhone() != null && !userReqDto.getPhone().isEmpty()) {
            queryStr.append(" AND t.phone LIKE :phone");
        }
        if (userReqDto.getBirthDate() != null) {
            queryStr.append(" AND t.birthDate = :birthDate");
        }
        if (userReqDto.isDelete() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (userReqDto.getRole() != null && !userReqDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(userReqDto.getRole()));
            if (role != null) {
                queryStr.append(" AND t.role.id = :roleId");
            }
        }

        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<User> query = entityManager.createQuery(queryStr.toString(), User.class);

        query.setParameter("createdBy", createdByList);

        if (userReqDto.getId() != null) {
            query.setParameter("id", "%" + userReqDto.getId().toString() + "%");
        }
        if (userReqDto.getUsername() != null && !userReqDto.getUsername().isEmpty()) {
            query.setParameter("username", "%" + userReqDto.getUsername() + "%");
        }
        if (userReqDto.getFirstName() != null && !userReqDto.getFirstName().isEmpty()) {
            query.setParameter("firstName", "%" + userReqDto.getFirstName() + "%");
        }
        if (userReqDto.getLastName() != null && !userReqDto.getLastName().isEmpty()) {
            query.setParameter("lastName", "%" + userReqDto.getLastName() + "%");
        }
        if (userReqDto.getGender() != null && !userReqDto.getGender().isEmpty()) {
            query.setParameter("gender", Gender.valueOf(userReqDto.getGender()));
        }
        if (userReqDto.getEmail() != null && !userReqDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + userReqDto.getEmail() + "%");
        }
        if (userReqDto.getPhone() != null && !userReqDto.getPhone().isEmpty()) {
            query.setParameter("phone", "%" + userReqDto.getPhone() + "%");
        }
        if (userReqDto.getBirthDate() != null) {
            query.setParameter("birthDate", userReqDto.getBirthDate());
        }
        if (userReqDto.getIsDelete() != null) {
            query.setParameter("isDeleted", userReqDto.getIsDelete());
        }
        if (userReqDto.getRole() != null && !userReqDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(userReqDto.getRole()));
            if (role != null) {
                query.setParameter("roleId", role.getId());
            }
        }

        query.setFirstResult(first);
        query.setMaxResults(limit);

        return query.getResultList();
    }

    /**
     * @param userReqDto
     * @return
     */
    @Override
    public Long countAllUserByField(UserReqDto userReqDto, List<UUID> createdByList) {

        StringBuilder queryStr = new StringBuilder("SELECT COUNT(*) FROM User t WHERE 1=1");

        if (userReqDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (userReqDto.getUsername() != null && !userReqDto.getUsername().isEmpty()) {
            queryStr.append(" AND t.username LIKE :username");
        }
        if (userReqDto.getFirstName() != null && !userReqDto.getFirstName().isEmpty()) {
            queryStr.append(" AND t.firstName LIKE :firstName");
        }
        if (userReqDto.getLastName() != null && !userReqDto.getLastName().isEmpty()) {
            queryStr.append(" AND t.lastName LIKE :lastName");
        }
        if (userReqDto.getGender() != null && !userReqDto.getGender().isEmpty()) {
            queryStr.append(" AND t.gender = :gender");
        }
        if (userReqDto.getEmail() != null && !userReqDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (userReqDto.getPhone() != null && !userReqDto.getPhone().isEmpty()) {
            queryStr.append(" AND t.phone LIKE :phone");
        }
        if (userReqDto.getBirthDate() != null) {
            queryStr.append(" AND t.birthDate = :birthDate");
        }
        if (userReqDto.isDelete() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (userReqDto.getRole() != null && !userReqDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(userReqDto.getRole()));
            if (role != null) {
                queryStr.append(" AND t.role.id = :roleId");
            }
        }
        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<Long> query = entityManager.createQuery(queryStr.toString(), Long.class);

        query.setParameter("createdBy", createdByList);

        if (userReqDto.getId() != null) {
            query.setParameter("id", "%" + userReqDto.getId().toString() + "%");
        }
        if (userReqDto.getUsername() != null && !userReqDto.getUsername().isEmpty()) {
            query.setParameter("username", "%" + userReqDto.getUsername() + "%");
        }
        if (userReqDto.getFirstName() != null && !userReqDto.getFirstName().isEmpty()) {
            query.setParameter("firstName", "%" + userReqDto.getFirstName() + "%");
        }
        if (userReqDto.getLastName() != null && !userReqDto.getLastName().isEmpty()) {
            query.setParameter("lastName", "%" + userReqDto.getLastName() + "%");
        }
        if (userReqDto.getGender() != null && !userReqDto.getGender().isEmpty()) {
            query.setParameter("gender", Gender.valueOf(userReqDto.getGender()));
        }
        if (userReqDto.getEmail() != null && !userReqDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + userReqDto.getEmail() + "%");
        }
        if (userReqDto.getPhone() != null && !userReqDto.getPhone().isEmpty()) {
            query.setParameter("phone", "%" + userReqDto.getPhone() + "%");
        }
        if (userReqDto.getBirthDate() != null) {
            query.setParameter("birthDate", userReqDto.getBirthDate());
        }
        if (userReqDto.getIsDelete() != null) {
            query.setParameter("isDeleted", userReqDto.getIsDelete());
        }
        if (userReqDto.getRole() != null && !userReqDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(userReqDto.getRole()));
            if (role != null) {
                query.setParameter("roleId", role.getId());
            }
        }

        return query.getSingleResult();
    }
}
