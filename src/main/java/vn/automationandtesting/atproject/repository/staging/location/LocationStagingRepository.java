package vn.automationandtesting.atproject.repository.staging.location;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.LocationStaging;

import java.util.UUID;

@Repository
public interface LocationStagingRepository extends JpaRepository<LocationStaging, UUID> {
}
