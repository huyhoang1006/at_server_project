package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.Monitoring;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
@Repository
public interface MonitoringRepository extends JpaRepository<Monitoring, UUID> {
    List<Monitoring> findByIsDeleted(boolean isDeleted);

    List<Monitoring> findByAssetAndIsDeleted(Asset asset, boolean isDeleted);

    Monitoring findByIdAndIsDeleted(UUID id, boolean isDeleted);

    @Query(
            value = "select * from monitoring mt " +
                    "where mt.asset_mrid = ?1 and mt.is_deleted = ?2 order by mt.created_on DESC", nativeQuery = true
    )
    List<Monitoring> findByAssetAndIsDeletedDesc(Asset asset, boolean isDeleted);

    @Query(
            value = "select  * from monitoring mt " +
                    "where mt.asset_mrid = ?1 and mt.is_deleted = ?2 order by mt.created_on DESC LIMIT 1", nativeQuery = true
    )
    List<Monitoring> findLastByAssetAndIsDeletedDesc(Asset asset, boolean isDeleted);
}
