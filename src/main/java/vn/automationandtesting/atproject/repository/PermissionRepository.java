package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.Permission;
import vn.automationandtesting.atproject.entity.enumm.PermissionEnum;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, UUID> {
    Permission findByPermissionName(PermissionEnum permissionName);


    List<Permission> findAllByIsDeleted(boolean isDeleted);

    @Query(value = "select p.permission_name from permission p " +
            "left join group_permission gp on gp.permission_id = p.id " +
            "left join at_project_schema.group g on g.id = gp.group_id " +
            "where g.group_name = ?1", nativeQuery = true)
    Set<String> findPermissionNamesByGroupName(String groupName);

}
