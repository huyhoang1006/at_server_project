package vn.automationandtesting.atproject.repository.power;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.power.JobsPowerCable;

import java.util.List;
import java.util.UUID;

@Repository
public interface JobPowerRepository extends JpaRepository<JobsPowerCable, UUID> {
    @Query(
            value = "select * from jobspower at " +
                    "where at.asset_id = ?1", nativeQuery = true
    )
    List<JobsPowerCable> findAllJobByAssetId(UUID asset_id, UUID user_id, String userId);

    @Query(
            value = "select * from jobspower at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<JobsPowerCable> findJobById(UUID id);

    @Query(
            value = "select * from jobspower at " +
                    "where at.name = ?1", nativeQuery = true
    )
    List<JobsPowerCable> findJobByName(String name);

    @Query(
            value = "select * from jobspower at " +
                    "where at.name = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<JobsPowerCable> findJobByNameAndAsset(String name, UUID asset_id);

    @Query(
            value = "select * from jobspower", nativeQuery = true
    )
    List<JobsPowerCable> findAll();

    @Query(
            value = "select * from jobspower at where at.asset_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<JobsPowerCable> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    @Query(
            value = "select * from jobspower j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%) offset ?4 limit ?5 ", nativeQuery = true
    )
    List<JobsPowerCable> findJobByAssetId(UUID id, UUID user_id, String userId, int first, int limit);

    @Query(
            value = "select count(created_by) from jobspower j "+
                    "where j.asset_id = ?1 and (j.created_by = ?2 or collabs like %?3%)", nativeQuery = true
    )
    int countJobByAssetId(UUID id, UUID user_id, String userId);
}
