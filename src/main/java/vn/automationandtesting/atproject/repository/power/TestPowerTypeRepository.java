package vn.automationandtesting.atproject.repository.power;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.power.TestPowerCableType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestPowerTypeRepository extends JpaRepository<TestPowerCableType, UUID> {
    @Query(
            value = "select * from testpower_type", nativeQuery = true
    )
    List<TestPowerCableType> findAll();

    @Query(
            value = "select * from testpower_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestPowerCableType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testpower_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestPowerCableType> findTestTypeById(String code);
}
