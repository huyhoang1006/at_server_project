package vn.automationandtesting.atproject.repository.power;

import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import java.util.List;
import java.util.UUID;

public interface PowerCustomRepository {
    List<PowerCable> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
