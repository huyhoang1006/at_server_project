package vn.automationandtesting.atproject.repository.power;

import org.springframework.beans.factory.annotation.Autowired;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;

public class PowerCustomRepositoryImpl implements PowerCustomRepository {
    @Autowired
    private EntityManager entityManager;
    /**
     * @param data
     * @param userId
     * @param ids
     * @param first
     * @param sl
     * @return
     */
    @Override
    public List getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl) {
        HashMap<String, String> dataAsset = (HashMap<String, String>) data;

        StringBuilder queryStr = new StringBuilder("SELECT * FROM ");
        queryStr.append("at_project_schema.").append(dataAsset.get("asset")).append(" as at ").append("WHERE ( at.collabs LIKE :userId ");
        if (ids != null && !ids.isEmpty()) {
            queryStr.append("OR at.created_by IN :ids");
        }
        queryStr.append(" ) ");
        for (Map.Entry<String, String> entry : dataAsset.entrySet()) {
            if(!entry.getKey().equals("asset") && !entry.getKey().equals("option")) {
                if(!Objects.equals(entry.getValue(), "")) {
                    queryStr.append(" AND at.").append(entry.getKey()).append(" LIKE :").append(entry.getKey());
                }
            }
        }
        queryStr.append(" offset :offset");
        queryStr.append(" limit :limit");
        // Tạo truy vấn native
        Query query = entityManager.createNativeQuery(queryStr.toString(), PowerCable.class);
        // Đặt tham số cho `userId`
        query.setParameter("userId", "%" + userId + "%");
        query.setParameter("ids", ids);
        for (Map.Entry<String, String> entry : dataAsset.entrySet()) {
            if(!entry.getKey().equals("asset") && !entry.getKey().equals("option")) {
                if(!Objects.equals(entry.getValue(), "")) {
                    query.setParameter(entry.getKey(), "%" + entry.getValue() + "%");
                }
            }
        }
        query.setParameter("offset", first);
        query.setParameter("limit", sl);
        return query.getResultList();
    }

    /**
     * @param data
     * @param userId
     * @param ids
     * @return
     */
    @Override
    public int countAssetInclude(Object data, String userId, List<UUID> ids) {
        HashMap<String, String> dataAsset = (HashMap<String, String>) data;

        StringBuilder queryStr = new StringBuilder("SELECT count(created_by) FROM ");
        queryStr.append("at_project_schema.").append(dataAsset.get("asset")).append(" as at ").append("WHERE ( at.collabs LIKE :userId ");
        if (ids != null && !ids.isEmpty()) {
            queryStr.append("OR at.created_by IN :ids");
        }
        queryStr.append(" ) ");
        for (Map.Entry<String, String> entry : dataAsset.entrySet()) {
            if(!entry.getKey().equals("asset") && !entry.getKey().equals("option")) {
                if(!Objects.equals(entry.getValue(), "")) {
                    queryStr.append(" AND at.").append(entry.getKey()).append(" LIKE :").append(entry.getKey());
                }
            }
        }
        // Tạo truy vấn native
        Query query = entityManager.createNativeQuery(queryStr.toString());
        // Đặt tham số cho `userId`
        query.setParameter("userId", "%" + userId + "%");
        query.setParameter("ids", ids);
        for (Map.Entry<String, String> entry : dataAsset.entrySet()) {
            if(!entry.getKey().equals("asset") && !entry.getKey().equals("option")) {
                if(!Objects.equals(entry.getValue(), "")) {
                    query.setParameter(entry.getKey(), "%" + entry.getValue() + "%");
                }
            }
        }
        return ((Number) query.getSingleResult()).intValue();
    }
}
