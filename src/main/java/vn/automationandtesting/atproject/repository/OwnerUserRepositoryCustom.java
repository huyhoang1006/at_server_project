package vn.automationandtesting.atproject.repository;

import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;

import java.util.List;
import java.util.UUID;

public interface OwnerUserRepositoryCustom {
    List<OwnerUser> findAllOwnerUserByField(OwnerUserDto ownerUserDto, int first, int limit, List<UUID> createdByList);

    Long countAllOwnerUserByField(OwnerUserDto ownerUserDto, List<UUID> createdByList);

    List<UUID> findAllIdByCreated( List<UUID> createdList);

    List<OwnerUser> findAllOwnerUserByCreated( List<UUID> createdList);

    List<OwnerUser> findAllOwnerUserById( List<UUID> ids);
}
