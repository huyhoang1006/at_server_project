package vn.automationandtesting.atproject.repository;

import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public interface AssetCommonCustom {
    int countAsset(String asset, String userId, List<UUID> ids);

    JSONObject countAssetInclude(Object asset, String userId, List<UUID> ids);

    Object getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    JSONObject countAssetListOffset(UUID location_id, String userId, List<UUID> ids);
}
