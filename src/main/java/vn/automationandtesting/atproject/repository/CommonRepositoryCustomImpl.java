package vn.automationandtesting.atproject.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CommonRepositoryCustomImpl implements CommonRepositoryCustom {

    @Autowired
    private EntityManager entityManager;
    /**
     * @param tableName
     * @param locationOldId
     * @param locationNewId
     */
    @Override
    @Transactional
    public void updateLocationRef(String tableName, String locationOldId, String locationNewId) {
        String sql = "UPDATE " + tableName + " SET location_id = :newLocationId WHERE location_id = :oldLocationId";
        entityManager.createNativeQuery(sql)
                .setParameter("newLocationId", locationNewId)
                .setParameter("oldLocationId", locationOldId)
                .executeUpdate();
    }

    /**
     * @param tableName
     * @param columnName
     * @return
     */
    @Override
    public List<Object[]> findReferencingTables(String tableName, String columnName) {
        String sql = "SELECT tc.table_name AS referencing_table, " +
                "kcu.column_name AS referencing_column " +
                "FROM information_schema.table_constraints AS tc " +
                "JOIN information_schema.key_column_usage AS kcu " +
                "ON tc.constraint_name = kcu.constraint_name " +
                "JOIN information_schema.referential_constraints AS rc " +
                "ON tc.constraint_name = rc.constraint_name " +
                "JOIN information_schema.key_column_usage AS ref_kcu " +
                "ON rc.unique_constraint_name = ref_kcu.constraint_name " +
                "WHERE ref_kcu.table_name = ?1 " +
                "AND ref_kcu.column_name = ?2 " +
                "AND tc.constraint_type = 'FOREIGN KEY'";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, tableName);
        query.setParameter(2, columnName);
        return (List<Object[]>) query.getResultList();
    }
}
