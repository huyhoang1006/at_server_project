package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Test;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TestRepository extends JpaRepository<Test, UUID> {
    Optional<Test> findByIdAndIsDeleted(UUID testId, boolean isDeleted);
    List<Test> findAllByIsDeleted(boolean isDeleted);

    @Query(
            value = "select * from test t "+
                    "left join job j on t.job_id = j.id "+
                    "where j.name = ?1 and j.is_deleted = ?2 and t.is_deleted = ?2", nativeQuery = true
    )
    List<Test> findByJobName(String jobName, boolean isDeleted);

    @Query(
            value = "select * from test t "+
                    "left join test_type tt on t.type_id = tt.id "+
                    "where tt.code = ?1 and t.is_deleted = ?2 and tt.is_deleted = ?2", nativeQuery = true
    )
    List<Test> findByCodeType(String codeType, boolean isDeleted);

    @Query(
            value = "select * from test t "+
                    "left join job j on t.job_id = j.id "+
                    "left join asset a on j.asset_id = a.mrid "+
                    "where a.serial_number = ?1 and t.is_deleted = ?2 and j.is_deleted = ?2 and a.is_deleted =?2", nativeQuery = true
    )
    List<Test> findByAssetSerialNo(String assetSerailNo, boolean isDeleted);

    @Query(value = "select * from test t " +
                    "where t.job_id = ?1 and t.is_deleted = false", nativeQuery = true)
    List<Test> findAllByJobId(UUID jobId);
}
