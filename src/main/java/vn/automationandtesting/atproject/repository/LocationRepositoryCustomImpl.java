package vn.automationandtesting.atproject.repository;

import org.springframework.beans.factory.annotation.Autowired;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.entity.cim.Location;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

public class LocationRepositoryCustomImpl implements LocationRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    /**
     * @param locationDto
     * @param first
     * @param limit
     * @param createdByList
     * @param user_id
     * @return
     */
    @Override
    public List<Location> findAllLocationByField(LocationDto locationDto, int first, int limit, List<UUID> createdByList, String user_id) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM Location t WHERE 1=1");
        if (locationDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (locationDto.getName() != null && !locationDto.getName().isEmpty()) {
            queryStr.append(" AND t.name LIKE :name");
        }
        if (locationDto.getAddress() != null && !locationDto.getAddress().isEmpty()) {
            queryStr.append(" AND t.address LIKE :address");
        }
        if (locationDto.getState_province() != null && !locationDto.getState_province().isEmpty()) {
            queryStr.append(" AND t.state_province = :state_province");
        }
        if (locationDto.getCity() != null && !locationDto.getCity().isEmpty()) {
            queryStr.append(" AND t.city LIKE :city");
        }
        if (locationDto.getMode() != null && !locationDto.getMode().isEmpty()) {
            queryStr.append(" AND t.mode LIKE :mode");
        }
        if (locationDto.getPerson_email() != null && !locationDto.getPerson_email().isEmpty()) {
            queryStr.append(" AND t.person_email LIKE :person_email");
        }
        if (locationDto.getPerson_phone_no1() != null && !locationDto.getPerson_phone_no1().isEmpty()) {
            queryStr.append(" AND t.person_phone_no1 LIKE :person_phone_no1");
        }

        queryStr.append(" AND ( t.createdBy IN :createdBy");
        queryStr.append(" OR t.collabs LIKE :collabs )");

        TypedQuery<Location> query = entityManager.createQuery(queryStr.toString(), Location.class);

        query.setParameter("createdBy", createdByList);
        query.setParameter("collabs", "%" + user_id + "%");

        if (locationDto.getId() != null) {
            query.setParameter("id", "%" + locationDto.getId().toString() + "%");
        }
        if (locationDto.getName() != null && !locationDto.getName().isEmpty()) {
            query.setParameter("name", "%" + locationDto.getName() + "%");
        }
        if (locationDto.getAddress() != null && !locationDto.getAddress().isEmpty()) {
            query.setParameter("address", "%" + locationDto.getAddress() + "%");
        }
        if (locationDto.getCity() != null && !locationDto.getCity().isEmpty()) {
            query.setParameter("city", "%" + locationDto.getCity() + "%");
        }
        if (locationDto.getState_province() != null && !locationDto.getState_province().isEmpty()) {
            query.setParameter("state_province", "%" + locationDto.getState_province() + "%");
        }
        if (locationDto.getMode() != null && !locationDto.getMode().isEmpty()) {
            query.setParameter("mode", "%" + locationDto.getMode() + "%");
        }
        if (locationDto.getPerson_phone_no1() != null && !locationDto.getPerson_phone_no1().isEmpty()) {
            query.setParameter("person_phone_no1", "%" + locationDto.getPerson_phone_no1() + "%");
        }
        if (locationDto.getPerson_email() != null && !locationDto.getPerson_email().isEmpty()) {
            query.setParameter("person_email", "%" + locationDto.getPerson_email() + "%");
        }


        query.setFirstResult(first);
        query.setMaxResults(limit);

        return query.getResultList();
    }

    /**
     * @param locationDto
     * @param createdByList
     * @param user_id
     * @return
     */
    @Override
    public Long countAllLocationByField(LocationDto locationDto, List<UUID> createdByList, String user_id) {
        StringBuilder queryStr = new StringBuilder("SELECT COUNT(t) FROM Location t WHERE 1=1");
        if (locationDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (locationDto.getName() != null && !locationDto.getName().isEmpty()) {
            queryStr.append(" AND t.name LIKE :name");
        }
        if (locationDto.getAddress() != null && !locationDto.getAddress().isEmpty()) {
            queryStr.append(" AND t.address LIKE :address");
        }
        if (locationDto.getState_province() != null && !locationDto.getState_province().isEmpty()) {
            queryStr.append(" AND t.state_province = :state_province");
        }
        if (locationDto.getCity() != null && !locationDto.getCity().isEmpty()) {
            queryStr.append(" AND t.city LIKE :city");
        }
        if (locationDto.getMode() != null && !locationDto.getMode().isEmpty()) {
            queryStr.append(" AND t.mode LIKE :mode");
        }
        if (locationDto.getPerson_email() != null && !locationDto.getPerson_email().isEmpty()) {
            queryStr.append(" AND t.person_email LIKE :person_email");
        }
        if (locationDto.getPerson_phone_no1() != null && !locationDto.getPerson_phone_no1().isEmpty()) {
            queryStr.append(" AND t.person_phone_no1 LIKE :person_phone_no1");
        }

        queryStr.append(" AND ( t.createdBy IN :createdBy");
        queryStr.append(" OR t.collabs LIKE :collabs )");

        TypedQuery<Long> query = entityManager.createQuery(queryStr.toString(), Long.class);

        query.setParameter("createdBy", createdByList);
        query.setParameter("collabs", "%" + user_id + "%");

        if (locationDto.getId() != null) {
            query.setParameter("id", "%" + locationDto.getId().toString() + "%");
        }
        if (locationDto.getName() != null && !locationDto.getName().isEmpty()) {
            query.setParameter("name", "%" + locationDto.getName() + "%");
        }
        if (locationDto.getAddress() != null && !locationDto.getAddress().isEmpty()) {
            query.setParameter("address", "%" + locationDto.getAddress() + "%");
        }
        if (locationDto.getCity() != null && !locationDto.getCity().isEmpty()) {
            query.setParameter("city", "%" + locationDto.getCity() + "%");
        }
        if (locationDto.getState_province() != null && !locationDto.getState_province().isEmpty()) {
            query.setParameter("state_province", "%" + locationDto.getState_province() + "%");
        }
        if (locationDto.getMode() != null && !locationDto.getMode().isEmpty()) {
            query.setParameter("mode", "%" + locationDto.getMode() + "%");
        }
        if (locationDto.getPerson_phone_no1() != null && !locationDto.getPerson_phone_no1().isEmpty()) {
            query.setParameter("person_phone_no1", "%" + locationDto.getPerson_phone_no1() + "%");
        }
        if (locationDto.getPerson_email() != null && !locationDto.getPerson_email().isEmpty()) {
            query.setParameter("person_email", "%" + locationDto.getPerson_email() + "%");
        }

        return query.getSingleResult();
    }
}
