package vn.automationandtesting.atproject.repository;

import org.springframework.beans.factory.annotation.Autowired;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.enumm.Gender;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;


public class OwnerUserRepositoryCustomImpl implements OwnerUserRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private RoleRepository roleRepository;


    /**
     * @param ownerUserDto
     * @param first
     * @param limit
     * @param createdByList
     * @return
     */
    @Override
    public List<OwnerUser> findAllOwnerUserByField(OwnerUserDto ownerUserDto, int first, int limit, List<UUID> createdByList) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM OwnerUser t WHERE 1=1");

        if (ownerUserDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (ownerUserDto.getUsername() != null && !ownerUserDto.getUsername().isEmpty()) {
            queryStr.append(" AND t.username LIKE :username");
        }
        if (ownerUserDto.getFirstName() != null && !ownerUserDto.getFirstName().isEmpty()) {
            queryStr.append(" AND t.firstName LIKE :firstName");
        }
        if (ownerUserDto.getLastName() != null && !ownerUserDto.getLastName().isEmpty()) {
            queryStr.append(" AND t.lastName LIKE :lastName");
        }
        if (ownerUserDto.getGender() != null && !ownerUserDto.getGender().isEmpty()) {
            queryStr.append(" AND t.gender = :gender");
        }
        if (ownerUserDto.getEmail() != null && !ownerUserDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (ownerUserDto.getPhone() != null && !ownerUserDto.getPhone().isEmpty()) {
            queryStr.append(" AND t.phone LIKE :phone");
        }
        if (ownerUserDto.getBirthDate() != null) {
            queryStr.append(" AND t.birthDate = :birthDate");
        }
        if (ownerUserDto.isDelete() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (ownerUserDto.getRole() != null && !ownerUserDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(ownerUserDto.getRole()));
            if (role != null) {
                queryStr.append(" AND t.role.id = :roleId");
            }
        }

        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<OwnerUser> query = entityManager.createQuery(queryStr.toString(), OwnerUser.class);

        query.setParameter("createdBy", createdByList);

        if (ownerUserDto.getId() != null) {
            query.setParameter("id", "%" + ownerUserDto.getId().toString() + "%");
        }
        if (ownerUserDto.getUsername() != null && !ownerUserDto.getUsername().isEmpty()) {
            query.setParameter("username", "%" + ownerUserDto.getUsername() + "%");
        }
        if (ownerUserDto.getFirstName() != null && !ownerUserDto.getFirstName().isEmpty()) {
            query.setParameter("firstName", "%" + ownerUserDto.getFirstName() + "%");
        }
        if (ownerUserDto.getLastName() != null && !ownerUserDto.getLastName().isEmpty()) {
            query.setParameter("lastName", "%" + ownerUserDto.getLastName() + "%");
        }
        if (ownerUserDto.getGender() != null && !ownerUserDto.getGender().isEmpty()) {
            query.setParameter("gender", Gender.valueOf(ownerUserDto.getGender()));
        }
        if (ownerUserDto.getEmail() != null && !ownerUserDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + ownerUserDto.getEmail() + "%");
        }
        if (ownerUserDto.getPhone() != null && !ownerUserDto.getPhone().isEmpty()) {
            query.setParameter("phone", "%" + ownerUserDto.getPhone() + "%");
        }
        if (ownerUserDto.getBirthDate() != null) {
            query.setParameter("birthDate", ownerUserDto.getBirthDate());
        }
        if (ownerUserDto.getIsDelete() != null) {
            query.setParameter("isDeleted", ownerUserDto.getIsDelete());
        }
        if (ownerUserDto.getRole() != null && !ownerUserDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(ownerUserDto.getRole()));
            if (role != null) {
                query.setParameter("roleId", role.getId());
            }
        }

        System.out.println(queryStr);

        query.setFirstResult(first);
        query.setMaxResults(limit);

        return query.getResultList();
    }

    /**
     * @param ownerUserDto
     * @param createdByList
     * @return
     */
    @Override
    public Long countAllOwnerUserByField(OwnerUserDto ownerUserDto, List<UUID> createdByList) {

        StringBuilder queryStr = new StringBuilder("SELECT count(t) FROM OwnerUser t WHERE 1=1");

        if (ownerUserDto.getId() != null) {
            queryStr.append(" AND t.id LIKE :id");
        }
        if (ownerUserDto.getUsername() != null && !ownerUserDto.getUsername().isEmpty()) {
            queryStr.append(" AND t.username LIKE :username");
        }
        if (ownerUserDto.getFirstName() != null && !ownerUserDto.getFirstName().isEmpty()) {
            queryStr.append(" AND t.firstName LIKE :firstName");
        }
        if (ownerUserDto.getLastName() != null && !ownerUserDto.getLastName().isEmpty()) {
            queryStr.append(" AND t.lastName LIKE :lastName");
        }
        if (ownerUserDto.getGender() != null && !ownerUserDto.getGender().isEmpty()) {
            queryStr.append(" AND t.gender = :gender");
        }
        if (ownerUserDto.getEmail() != null && !ownerUserDto.getEmail().isEmpty()) {
            queryStr.append(" AND t.email LIKE :email");
        }
        if (ownerUserDto.getPhone() != null && !ownerUserDto.getPhone().isEmpty()) {
            queryStr.append(" AND t.phone LIKE :phone");
        }
        if (ownerUserDto.getBirthDate() != null) {
            queryStr.append(" AND t.birthDate = :birthDate");
        }
        if (ownerUserDto.isDelete() != null) {
            queryStr.append(" AND t.isDeleted = :isDeleted");
        }
        if (ownerUserDto.getRole() != null && !ownerUserDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(ownerUserDto.getRole()));
            if (role != null) {
                queryStr.append(" AND t.role.id = :roleId");
            }
        }

        queryStr.append(" AND t.createdBy IN :createdBy");

        TypedQuery<Long> query = entityManager.createQuery(queryStr.toString(), Long.class);

        query.setParameter("createdBy", createdByList);

        if (ownerUserDto.getId() != null) {
            query.setParameter("id", "%" + ownerUserDto.getId().toString() + "%");
        }
        if (ownerUserDto.getUsername() != null && !ownerUserDto.getUsername().isEmpty()) {
            query.setParameter("username", "%" + ownerUserDto.getUsername() + "%");
        }
        if (ownerUserDto.getFirstName() != null && !ownerUserDto.getFirstName().isEmpty()) {
            query.setParameter("firstName", "%" + ownerUserDto.getFirstName() + "%");
        }
        if (ownerUserDto.getLastName() != null && !ownerUserDto.getLastName().isEmpty()) {
            query.setParameter("lastName", "%" + ownerUserDto.getLastName() + "%");
        }
        if (ownerUserDto.getGender() != null && !ownerUserDto.getGender().isEmpty()) {
            query.setParameter("gender", Gender.valueOf(ownerUserDto.getGender()));
        }
        if (ownerUserDto.getEmail() != null && !ownerUserDto.getEmail().isEmpty()) {
            query.setParameter("email", "%" + ownerUserDto.getEmail() + "%");
        }
        if (ownerUserDto.getPhone() != null && !ownerUserDto.getPhone().isEmpty()) {
            query.setParameter("phone", "%" + ownerUserDto.getPhone() + "%");
        }
        if (ownerUserDto.getBirthDate() != null) {
            query.setParameter("birthDate", ownerUserDto.getBirthDate());
        }
        if (ownerUserDto.getIsDelete() != null) {
            query.setParameter("isDeleted", ownerUserDto.getIsDelete());
        }
        if (ownerUserDto.getRole() != null && !ownerUserDto.getRole().isEmpty()) {
            Role role = roleRepository.findByRoleName(RoleEnum.valueOf(ownerUserDto.getRole()));
            if (role != null) {
                query.setParameter("roleId", role.getId());
            }
        }
        return query.getSingleResult();
    }

    /**
     * @param createdList
     * @return
     */
    @Override
    public List<UUID> findAllIdByCreated(List<UUID> createdList) {
        StringBuilder queryStr = new StringBuilder("SELECT id FROM OwnerUser t WHERE 1=1");
        queryStr.append(" AND t.createdBy IN :createdBy");
        TypedQuery<UUID> query = entityManager.createQuery(queryStr.toString(), UUID.class);
        query.setParameter("createdBy", createdList);
        return query.getResultList();
    }

    /**
     * @param createdList
     * @return
     */
    @Override
    public List<OwnerUser> findAllOwnerUserByCreated(List<UUID> createdList) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM OwnerUser t WHERE 1=1");
        queryStr.append(" AND t.createdBy IN :createdBy");
        TypedQuery<OwnerUser> query = entityManager.createQuery(queryStr.toString(), OwnerUser.class);
        query.setParameter("createdBy", createdList);
        return query.getResultList();
    }

    /**
     * @param ids
     * @return
     */
    @Override
    public List<OwnerUser> findAllOwnerUserById(List<UUID> ids) {
        StringBuilder queryStr = new StringBuilder("SELECT t FROM OwnerUser t WHERE 1=1");
        queryStr.append(" AND t.id IN :id");
        TypedQuery<OwnerUser> query = entityManager.createQuery(queryStr.toString(), OwnerUser.class);
        query.setParameter("id", ids);
        return query.getResultList();
    }

}
