package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.TestType;

import java.util.UUID;

@Repository
public interface TestTypeRepository extends JpaRepository<TestType, UUID> {
    TestType findByCodeAndIsDeleted(String codeType, boolean isDeleted);

    TestType findByCode(String testTypeCode);
}
