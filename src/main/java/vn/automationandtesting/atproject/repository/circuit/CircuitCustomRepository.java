package vn.automationandtesting.atproject.repository.circuit;

import vn.automationandtesting.atproject.entity.circuit.Circuit;

import java.util.List;
import java.util.UUID;

public interface CircuitCustomRepository {
    List<Circuit> getAssetInclude(Object data, String userId, List<UUID> ids, int first, int sl);

    int countAssetInclude(Object data, String userId, List<UUID> ids);
}
