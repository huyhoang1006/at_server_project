package vn.automationandtesting.atproject.repository.circuit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.TestCircuit;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestCircuitRepository extends JpaRepository<TestCircuit, UUID> {
    @Query(
            value = "select * from testdatas at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestCircuit> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testdatas at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestCircuit> findTestById(UUID id);

    @Query(
            value = "select * from testdatas at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestCircuit> findTestByType(UUID type_id);

    @Query(
            value = "select * from testdatas at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestCircuit> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
