package vn.automationandtesting.atproject.repository.circuit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.TestCircuitType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestCircuitTypeRepository extends JpaRepository<TestCircuitType, UUID> {
    @Query(
            value = "select * from testcircuit_type", nativeQuery = true
    )
    List<TestCircuitType> findAll();

    @Query(
            value = "select * from testcircuit_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestCircuitType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testcircuit_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestCircuitType> findTestTypeById(String code);
}
