package vn.automationandtesting.atproject.update;


import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UpdateData implements CommandLineRunner {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Boolean checkTableName(String name) {
        String query = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'at_project_schema' AND table_name = ?) ";
        return jdbcTemplate.queryForObject(query, Boolean.class, name);
    }

    public void renameColumn(String tableName, String oldColumnName, String newColumnName) {
        String query = "ALTER TABLE " + tableName + " RENAME COLUMN " + oldColumnName + " TO " + newColumnName;
        jdbcTemplate.execute(query);
    }

    public boolean isColumnExist(String columnName, String tableName) {
        String query = "SELECT EXISTS (" +
                "    SELECT 1 " +
                "    FROM information_schema.columns " +
                "    WHERE table_name = ? AND column_name = ?" +
                ")";
        return jdbcTemplate.queryForObject(query, Boolean.class, tableName, columnName);
    }

    public void addColumnToTable(String columnName, String tableName) {
        String schemaName = "at_project_schema";
        String query = "ALTER TABLE " + schemaName + "." + tableName + " ADD COLUMN " + columnName + " Text";
        jdbcTemplate.execute(query);
    }

    public void addColumnToTable(String columnName, String tableName, String type) {
        String schemaName = "at_project_schema";
        String query = "ALTER TABLE " + schemaName + "." + tableName + " ADD COLUMN " + columnName + " " + type;
        jdbcTemplate.execute(query);
    }

    /**
     *
     * @param columnName tên cột
     * @param tableName tên bảng chứa cột
     * @param sign dùng để xác nhận là set cho cột đó có thể chứa giá trị null hay not null, nếu set là null thì không thể chứa giá trị null
     */
    public void updateColumnToTableNullOrNot(String columnName, String tableName, String sign) {
        String query;
        if(sign.equalsIgnoreCase("null")) {
            query = "ALTER TABLE at_project_schema." + tableName + " ALTER COLUMN " + columnName + " SET NOT NULL";
        } else {
            query = "ALTER TABLE at_project_schema." + tableName + " ALTER COLUMN " + columnName + " DROP NOT NULL";
        }
        jdbcTemplate.execute(query);

    }

    public void updateValueColumnTable(String tableName, Map<String, String> data,String id) {
        String query = "UPDATE at_project_schema." + tableName +
                " Set ";
        for(Map.Entry<String, String> entry : data.entrySet()) {
            query = query + entry.getKey() + " = '" + entry.getValue() + "',";
        }
        query = query.substring(0, query.length() - 1);
        query = query + " WHERE mrid = '" + id + "'";
        System.out.println(query);
        jdbcTemplate.execute(query);
    }

    public boolean isColumnIndexed(@NotNull String tableName, @NotNull String columnName) {
        String query = "SELECT EXISTS ( SELECT 1 " +
                "FROM pg_indexes " +
                "WHERE schemaname = ? AND tablename = ? AND indexdef ILIKE ? )";
        boolean check = jdbcTemplate.queryForObject(query, Boolean.class, "at_project_schema", tableName,"%" + columnName + "%");
        return check;
    }

    public void indexColumn(String tableName, String columnName) {
        String createIndexQuery = "CREATE INDEX idx_" + columnName + " ON " + tableName + "(" + columnName + ")";
        jdbcTemplate.execute(createIndexQuery);
    }

    public  List<Map<String, Object>> getDataFromTable(String table_name, Map<String, String> condition) {
        String query = "SELECT * FROM at_project_schema." + table_name;
        if(!condition.isEmpty()) {
            query = query + " where ";
            for(Map.Entry<String, String> entry : condition.entrySet()) {
                if(entry.getValue().charAt(0) == 'i' && entry.getValue().charAt(1) == 'n') {
                    query = query + entry.getKey() + " in (" + entry.getValue().substring(2) + ") and ";
                } else {
                    if(entry.getValue().charAt(0) == '!') {
                        query = query + entry.getKey() + " <> '" + entry.getValue().substring(1) + "' and ";
                    } else {
                        query = query + entry.getKey() + " = '" + entry.getValue() + "' and ";
                    }
                }
            }
        }
        query = query.substring(0, query.length() - 5);
        return jdbcTemplate.queryForList(query);
    }

    public void processAllOwners() {
        String query = "SELECT * FROM at_project_schema.owner";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(query);
        for (Map<String, Object> row : rows) {
            Object ref_id = row.get("ref_id");
            Object user_id = row.get("created_by");
            for (Map<String, Object> rowParent : rows) {
                if(rowParent.get("mrid").equals(ref_id)) {
                    if(!rowParent.get("created_by").equals(user_id)) {
                        Map<String, String> data = new HashMap<>();
                        data.put("pre", "1");
                        updateValueColumnTable("owner", data, row.get("mrid").toString());
                    } else {
                        Map<String, String> data = new HashMap<>();
                        data.put("pre", "");
                        updateValueColumnTable("owner", data, row.get("mrid").toString());
                    }
                }
            }
        }
    }

    public void  copyDataFromTableToTable(String table_source, String table_target, Map<String, String> condition, List<String> ignore, Boolean deleteOld) {
        String query = "SELECT * FROM at_project_schema." + table_source;
        String queryInsert = "INSERT INTO at_project_schema." + table_target + " (";
        if(!condition.isEmpty()) {
            query = query + " where ";
            for(Map.Entry<String, String> entry : condition.entrySet()) {
                if(entry.getValue().charAt(0) == 'i' && entry.getValue().charAt(1) == 'n') {
                    query = query + entry.getKey() + " in (" + entry.getValue().substring(2) + ") and ";
                } else {
                    if(entry.getValue().charAt(0) == '!') {
                        query = query + entry.getKey() + " <> '" + entry.getValue().substring(1) + "' and ";
                    } else {
                        query = query + entry.getKey() + " = '" + entry.getValue() + "' and ";
                    }
                }
            }
            query = query.substring(0, query.length() - 5);
        }
        List<Map<String, Object>> dataOld = jdbcTemplate.queryForList(query);
        String queryColumn = "SELECT column_name FROM information_schema.columns WHERE table_name = " + "'" + table_target + "'";
        List<Map<String, Object>> rowsColumn = jdbcTemplate.queryForList(queryColumn);
        List<String> columnNameList = new ArrayList<>();
        for (Map<String, Object> column : rowsColumn) {
            String columnName = (String) column.get("column_name");
            columnNameList.add(columnName);
            if(!ignore.contains(columnName)) {
                queryInsert = queryInsert + columnName + ",";
            }
        }
        queryInsert = queryInsert.substring(0, queryInsert.length() - 1);
        queryInsert = queryInsert + ") VALUES ";
        for (Map<String, Object> row : dataOld) {
            queryInsert = queryInsert + "(";
            for(String columnName : columnNameList) {
                for (Map.Entry<String, Object> entry : row.entrySet()) {
                    if (columnName.equals(entry.getKey())) {
                        if (!ignore.contains(entry.getKey())) {
                            if (entry.getValue() != null) {
                                queryInsert = queryInsert + "'" + entry.getValue() + "',";
                            } else {
                                queryInsert = queryInsert + "NULL" + ",";
                            }
                        }
                    }
                }
            }
            queryInsert = queryInsert.substring(0, queryInsert.length() - 1);
            queryInsert = queryInsert + "),";
        }
        queryInsert = queryInsert.substring(0, queryInsert.length() - 1);
        jdbcTemplate.update(queryInsert);
        if(deleteOld) {
            List<String> ids = dataOld.stream()
                    .map(record -> record.get("id").toString())
                    .collect(Collectors.toList());
            System.out.println(ids);
            removeRow(ids, table_source);
        }
    }

    public Boolean checkingValueInTable(String name, HashMap<String, String> key_value) {
        String queString = "Select count(*) from at_project_schema." + name + " where ";
        for(Map.Entry<String, String> entry : key_value.entrySet()) {
            queString = queString + entry.getKey() + " = '" + entry.getValue() + "' or ";
        }
        queString = queString.substring(0, queString.length() - 4);
        Long count = jdbcTemplate.queryForObject(queString, Long.class);
        if(count == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void createTable(String name, HashMap<String, String> column_name) {
        String queString = "CREATE TABLE at_project_schema." + name + " (";
        for(Map.Entry<String, String> entry : column_name.entrySet()) {
            if(entry.getKey().equals("id")) {
                queString = queString + entry.getKey() + " " + entry.getValue() + " PRIMARY KEY ,";
            } else {
                queString = queString + entry.getKey() + " " + entry.getValue() + " ,";
            }
        }
        queString = queString.substring(0, queString.length() - 1);
        queString = queString + ")";
        queString = queString + " INHERITS (at_project_schema.identified_object) TABLESPACE pg_default";
        jdbcTemplate.execute(queString);
    }

    public void createTableRaw(String name, HashMap<String, String> column_name, String primary, String foreign_name, String column_to_foreign, String foreign_column, String foreign_table) {
        String queString = "CREATE TABLE at_project_schema." + name + " (";
        for(Map.Entry<String, String> entry : column_name.entrySet()) {
            if(entry.getKey().equals(primary)) {
                queString = queString + entry.getKey() + " " + entry.getValue() + " PRIMARY KEY ,";
            } else {
                queString = queString + entry.getKey() + " " + entry.getValue() + " ,";
            }
        }
        queString = queString + "CONSTRAINT " + foreign_name + " FOREIGN KEY (" + column_to_foreign + ")" + "REFERENCES " + foreign_table + " (" + foreign_column + "),";
        queString = queString.substring(0, queString.length() - 1);
        queString = queString + ")";
        queString = queString + " TABLESPACE pg_default";
        jdbcTemplate.execute(queString);
    }

    public void createTableRawInherits(String name, HashMap<String, String> column_name, String primary, String foreign_name, String column_to_foreign, String foreign_column, String foreign_table, Boolean inherits) {
        String queString = "CREATE TABLE at_project_schema." + name + " (";
        for(Map.Entry<String, String> entry : column_name.entrySet()) {
            if(entry.getKey().equals(primary)) {
                queString = queString + entry.getKey() + " " + entry.getValue() + " PRIMARY KEY ,";
            } else {
                queString = queString + entry.getKey() + " " + entry.getValue() + " ,";
            }
        }
        queString = queString + "CONSTRAINT " + foreign_name + " FOREIGN KEY (" + column_to_foreign + ")" + "REFERENCES " + foreign_table + " (" + foreign_column + ") ON DELETE CASCADE,";
        queString = queString.substring(0, queString.length() - 1);
        queString = queString + ")";
        if (inherits) {
            queString = queString + " INHERITS (at_project_schema.identified_object)";
        }
        queString = queString + " TABLESPACE pg_default";
        jdbcTemplate.execute(queString);
    }


    public void addValueToTable(String name, HashMap<String, String> key_value) {
        String queString = "INSERT INTO at_project_schema." + name + " (";
        for(Map.Entry<String, String> entry : key_value.entrySet()) {
            queString = queString + entry.getKey() + ",";
        }
        queString = queString.substring(0, queString.length() - 1);
        queString = queString + ")";
        queString = queString + " VALUES (";
        for(Map.Entry<String, String> entry : key_value.entrySet()) {
            queString = queString + "'" + entry.getValue() + "'" + ",";
        }
        queString = queString.substring(0, queString.length() - 1);
        queString = queString + ")";
        jdbcTemplate.execute(queString);
    }

    public void removeRow(List<String> ids, String table_name) {
        String sql = "DELETE FROM at_project_schema." + table_name + " WHERE id IN (";
        for(String id : ids) {
            sql = sql + "'" + id + "',";
        }
        sql = sql.substring(0, sql.length() - 1);
        sql = sql + ")";
        System.out.println(sql);
        jdbcTemplate.execute(sql);
    }

    public boolean checkRowExist(String table_name, Map<String, String> condition) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM " + table_name + " WHERE ");

        // Thêm các điều kiện
        boolean first = true;
        for (Map.Entry<String, String> entry : condition.entrySet()) {
            if (!first) {
                sql.append(" AND ");
            }
            String key = entry.getKey();
            String value = entry.getValue();

            if (value.startsWith("!")) {
                sql.append(key).append(" != ?");
                value = value.substring(1); // Loại bỏ dấu '!' để lấy giá trị thực tế
            } else {
                sql.append(key).append(" = ?");
            }

            first = false;
        }

        // Chuyển đổi giá trị điều kiện thành mảng Object
        Object[] params = condition.values().stream()
                .map(value -> value.startsWith("!") ? value.substring(1) : value)
                .toArray();

        // Thực hiện câu lệnh select và lấy kết quả
        int count = jdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
        return count > 0;
    }

    public void addForeignKey(String foreign_name,String column_source,String table_source,String column_dest,String table_dest) {
        String sql = "ALTER TABLE " + table_source + " ADD CONSTRAINT " + foreign_name + " FOREIGN KEY " +
                "(" + column_source + ") REFERENCES " + table_dest + "(" + column_dest + ") ON DELETE CASCADE";

        jdbcTemplate.execute(sql);
    }

    @Override
    public void run(String... args) throws Exception {
        updateColumnToTableNullOrNot("birth_date", "user", "");
        boolean ownerChecking = checkTableName("owner");
        if(!ownerChecking) {
            HashMap<String, String> data = new HashMap<>();
            data.put("user_id", "UUID");
            data.put("address", "text");
            data.put("full_name", "text");
            data.put("tax_code", "text");
            data.put("city", "text");
            data.put("country", "text");
            data.put("state", "text");
            data.put("phone_no", "text");
            data.put("mode", "text");
            data.put("ref_id", "UUID");
            data.put("fax", "text");
            data.put("email", "text");
            data.put("name_person", "text");
            data.put("phone1", "text");
            data.put("phone2", "text");
            data.put("fax_contact", "text");
            data.put("email_contact", "text");
            data.put("department", "text");
            data.put("position", "text");
            data.put("comment", "text");
            try {
                createTable("owner", data);
            } catch (Exception e) {
                System.out.println("Error create owner");
            }
        }
        else {
            HashMap<String, String> valueRoot = new HashMap<String, String>();
            valueRoot.put("user_id", "00000000-0000-0000-0000-000000000000");
            Boolean checkValue = checkingValueInTable("owner", valueRoot);
            if(!checkValue) {
                HashMap<String, String> data = new HashMap<String, String>();
                data.put("user_id", "00000000-0000-0000-0000-000000000000");
                data.put("name", "root");
                data.put("mrid", UUID.randomUUID().toString());
                data.put("created_by", "3e0ce75b-e915-4cfb-b25e-bc5a23bb425a");
                addValueToTable("owner", data);
            }
            boolean checkColumn = isColumnExist("tax_code", "owner");
            if(!checkColumn) {
                renameColumn("owner", "address", "full_name");
                renameColumn("owner", "city", "address");
                renameColumn("owner", "country", "tax_code");

                addColumnToTable("city", "owner");
                addColumnToTable("country", "owner");

            }
        }

        HashMap<String, String> value = new HashMap<String,String>();
        value.put("role_name", "OWNER4");
        value.put("description", "owner5");
        boolean checkValue = checkingValueInTable("role", value);
        if(!checkValue) {
            HashMap<String, String> data = new HashMap<String, String>();
            data.put("id", UUID.randomUUID().toString());
            data.put("role_name", "OWNER4");
            data.put("description", "owner4");
            data.put("created_by", "3e0ce75b-e915-4cfb-b25e-bc5a23bb425a");
            addValueToTable("role",data);
            data.put("id", UUID.randomUUID().toString());
            data.put("role_name", "OWNER5");
            data.put("description", "owner5");
            data.put("created_by", "3e0ce75b-e915-4cfb-b25e-bc5a23bb425a");
            addValueToTable("role",data);
        } else {}
        boolean checkIndexModeOwner = isColumnIndexed("owner", "mode");
        if(!checkIndexModeOwner) {
            System.out.println(checkIndexModeOwner);
            indexColumn("owner", "mode");
        }
        Boolean checkPreColumn = isColumnExist("pre", "owner");
        if(!checkPreColumn) {
            addColumnToTable("pre", "owner");
            processAllOwners();
        }

        boolean ownerUserChecking = checkTableName("owner_user");
        if(!ownerUserChecking) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "UUID");
            data.put("username", "text");
            data.put("password", "text");
            data.put("enabled", "boolean");
            data.put("first_name", "text");
            data.put("last_name", "text");
            data.put("gender", "text");
            data.put("email", "text");
            data.put("phone", "text");
            data.put("role_id", "UUID");
            data.put("groups", "text");
            data.put("birth_date", "date");
            data.put("created_by", "UUID");
            data.put("created_on", "TIMESTAMP");
            data.put("updated_by", "UUID");
            data.put("updated_on", "TIMESTAMP");
            data.put("is_deleted", "boolean");
            try {
                createTableRaw("owner_user", data, "id", "fk_owner_user", "role_id", "id", "role");
                Map<String, String> conditionRole = new HashMap<>();
                Map<String, String> condition = new HashMap<>();
                conditionRole.put("role_name", "!USER");
                List<Map<String, Object>> dataRole = getDataFromTable("role", conditionRole);
                StringBuilder roleIds = new StringBuilder();
                for(Map<String, Object> row : dataRole) {
                    Object roleId = row.get("id");
                    if (roleIds.length() > 0) {
                        roleIds.append(",");
                    }
                    roleIds.append("'").append(roleId.toString()).append("'");
                }
                String concatenatedRoleIds = roleIds.toString();
                condition.put("role_id", "in" + concatenatedRoleIds);
                List<String> ignore = new ArrayList<>();
                copyDataFromTableToTable("user", "owner_user", condition, ignore, true);
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Error create owner user");
            }
        } else {
            try {
                boolean checkColumnExist = isColumnExist("time_off", "owner_user");
                if (!checkColumnExist) {
                    addColumnToTable("time_off", "owner_user", "TIMESTAMP");
                    addColumnToTable("option_off", "owner_user", "text");
                }
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Error update owner user");
            }
        }

        boolean checkColumnExist = isColumnExist("user_owner_id", "location");
        boolean checkColumnExistData = isColumnExist("person_department", "location");

        if(!checkColumnExistData) {
            addColumnToTable("person_department", "location", "Text");
            addColumnToTable("person_position", "location", "Text");
        }

        if(!checkColumnExist) {
            addColumnToTable("user_owner_id", "location", "uuid");
            updateColumnToTableNullOrNot("user_owner_id", "location", "");
            updateColumnToTableNullOrNot("user_id", "location", "");
            addForeignKey("fk_user_owner_id", "user_owner_id", "location", "id", "owner_user");
        }

        boolean positionChecking = checkTableName("position_point");
        if(positionChecking) {
            updateColumnToTableNullOrNot("sequence_number", "position_point", "");
            updateColumnToTableNullOrNot("x_position", "position_point", "");
            updateColumnToTableNullOrNot("y_position", "position_point", "");
            updateColumnToTableNullOrNot("z_position", "position_point", "");
        }

        boolean checkingManu = checkTableName("manufacturer_custom");
        if(!checkingManu) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "uuid");
            data.put("type", "text");
            createTable("manufacturer_custom", data);
        }

        Boolean checkGroupMessage = checkTableName("group_message");
        if(!checkGroupMessage) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "uuid");
            data.put("group_name", "text");
            data.put("last_message", "text");
            data.put("last_message_created_by", "uuid");
            data.put("last_message_at", "TIMESTAMP WITHOUT TIME ZONE");
            data.put("last_message_receiver_member", "text");
            data.put("member", "text");
            data.put("type", "text");
            createTable("group_message", data);
        }

        Boolean checkMessageInGroup = checkTableName("message");
        if(!checkMessageInGroup) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "uuid");
            data.put("group_id", "uuid");
            data.put("content", "text");
            data.put("sender_id", "uuid");
            data.put("sender_name", "text");
            data.put("status", "text");
            data.put("type", "text");
            data.put("receiver_name", "text");
            data.put("receiver_member", "text");
            createTableRawInherits("message", data, "id", "fk_group_message", "group_id", "id","group_message", true);
        }

        boolean checkColumnRatedFrequencyCustom = isColumnExist("rated_frequency_custom", "asset");
        if(!checkColumnRatedFrequencyCustom) {
            addColumnToTable("rated_frequency_custom", "asset", "text");
        }
        boolean checkColumnOtherInsulationKey = isColumnExist("insulation_key", "asset");
        if(!checkColumnOtherInsulationKey) {
            addColumnToTable("insulation_key", "asset", "text");
        }
        boolean checkVersionLocation = isColumnExist("version", "location");
        if(!checkVersionLocation) {
            addColumnToTable("version", "location", "text");
        }
        boolean checkTableLocationStaging = checkTableName("location_staging");
        if(!checkTableLocationStaging) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "uuid");
            data.put("location_id", "uuid");
            data.put("content", "text");
            data.put("sender_id", "uuid");
            data.put("sender_name", "text");
            data.put("version", "text");
            data.put("status", "text");
            createTableRawInherits("location_staging", data, "id", "fk_location_staging", "location_id", "mrid","location", true);
        }
        boolean checkTableAuditLocationLog = checkTableName("audit_location_log");
        if(!checkTableAuditLocationLog) {
            HashMap<String, String> data = new HashMap<>();
            data.put("id", "uuid");
            data.put("location_staging_id", "uuid");
            data.put("content", "text");
            data.put("old_value", "text");
            data.put("new_value", "text");
            data.put("sender_id", "uuid");
            data.put("sender_name", "text");
            data.put("status", "text");
            data.put("version", "text");
            createTableRawInherits("audit_location_log", data, "id", "fk_location_log", "location_staging_id", "id","location_staging", true);
        }
    }
}
