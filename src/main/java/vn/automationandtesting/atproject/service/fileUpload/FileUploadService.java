package vn.automationandtesting.atproject.service.fileUpload;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileUploadService {
    public boolean updateFile(MultipartFile[] files, List<String> deleteFile, String uuid);
}
