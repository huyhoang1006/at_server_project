package vn.automationandtesting.atproject.service.current;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.JobCurrentDto;
import vn.automationandtesting.atproject.entity.current.JobsCurrent;

import java.util.List;
import java.util.UUID;

public interface JobCurrentService {

    List<JobCurrentDto> findAllJobByAssetId(UUID asset_id);

    List<JobCurrentDto> findJobById(UUID id);

    List<JobCurrentDto> findJobByName(String name);

    List<JobCurrentDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobCurrentDto> findAll();

    List<JobCurrentDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<JobsCurrent> jobdataList);

    void deleteAll(List<JobsCurrent> jobdataList);

    List<JobCurrentDto> findJobByAssetId(UUID asset_id, int first, int limit);

    int countJobByAssetId(UUID asset_id);
}
