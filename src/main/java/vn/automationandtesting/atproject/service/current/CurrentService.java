package vn.automationandtesting.atproject.service.current;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.List;
import java.util.UUID;

public interface CurrentService {
    List<CurrentDto> findAllByLocationId(UUID location_id);

    List<CurrentDto> findAssetById(UUID id);

    List<CurrentDto> findBySerial(String serial);

    List<CurrentDto> findBySerialAndLocation(String serial, UUID location_id);

    List<CurrentDto> findAll();

    List<CurrentDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Current> currents);

    CurrentDto updateAssetLite(CurrentDto currentDto, UUID id);

    void deleteAll(List<Current> currents);

    List<CurrentDto> findAssetByLocationId(UUID id, int first, int limit);

    int countAssetByLocationId(UUID locationId);

    int countAssetList();
    List<CurrentDto> findAssetList(int stt, int sl);

    List<CurrentDto> findAssetListByLocationId(UUID location_id, int stt, int sl);

    int countAssetListByLocationId(UUID location_id);

    CurrentDto getAssetById(UUID asset_id, String user_id);
}
