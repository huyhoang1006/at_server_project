package vn.automationandtesting.atproject.service.current;

import vn.automationandtesting.atproject.controller.dto.current.TestCurrentDto;
import vn.automationandtesting.atproject.entity.current.TestsCurrent;

import java.util.List;
import java.util.UUID;

public interface TestCurrentService {
    List<TestCurrentDto> findAllTestByJobId(UUID job_id);
    List<TestCurrentDto> findTestById(UUID id);
    List<TestCurrentDto> findTestByType(UUID type_id);
    List<TestCurrentDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestsCurrent> tests);

    void deleteAll(List<TestsCurrent> tests);
}
