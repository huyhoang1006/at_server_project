package vn.automationandtesting.atproject.service.power;

import vn.automationandtesting.atproject.controller.dto.power.TestPowerCableDto;
import vn.automationandtesting.atproject.entity.power.TestsPowerCable;

import java.util.List;
import java.util.UUID;

public interface TestPowerCableService {
    List<TestPowerCableDto> findAllTestByJobId(UUID job_id);
    List<TestPowerCableDto> findTestById(UUID id);
    List<TestPowerCableDto> findTestByType(UUID type_id);
    List<TestPowerCableDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestsPowerCable> tests);

    void deleteAll(List<TestsPowerCable> test);
}
