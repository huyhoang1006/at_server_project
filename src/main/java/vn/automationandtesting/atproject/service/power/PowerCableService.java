package vn.automationandtesting.atproject.service.power;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import java.util.List;
import java.util.UUID;

public interface PowerCableService {
    List<PowerCableDto> findAllByLocationId(UUID location_id);

    List<PowerCableDto> findAssetById(UUID id);

    List<PowerCableDto> findBySerial(String serial);

    List<PowerCableDto> findBySerialAndLocation(String serial, UUID location_id);

    List<PowerCableDto> findAll();

    List<PowerCableDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<PowerCable> assets);

    PowerCableDto updateAssetLite(PowerCableDto powerCableDto, UUID id);

    void deleteAll(List<PowerCable> assets);

    List<PowerCableDto> findAssetByLocationId(UUID id, int first, int limit);

    int countAssetByLocationId(UUID locationId);

    int countAssetList();
    List<PowerCableDto> findAssetList(int stt, int sl);

    List<PowerCableDto> findAssetListByLocationId(UUID location_id, int stt, int sl);

    int countAssetListByLocationId(UUID location_id);

    PowerCableDto getAssetById(UUID asset_id, String user_id);

}
