package vn.automationandtesting.atproject.service.power;

import vn.automationandtesting.atproject.controller.dto.power.TestPowerCableTypeDto;
import vn.automationandtesting.atproject.entity.power.TestPowerCableType;

import java.util.List;

public interface TestPowerCableTypeService {
    List<TestPowerCableTypeDto> findAll();
    List<TestPowerCableTypeDto> findTestTypeByCode(String code);
    List<TestPowerCableTypeDto> findTestTypeById(String code);
    void saveAll(List<TestPowerCableType> testTypes);

    void deleteAll(List<TestPowerCableType> testTypes);
}
