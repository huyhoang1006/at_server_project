package vn.automationandtesting.atproject.service.sharedListId;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Setter
@Getter
public class SharedListIdService {
    private List<UUID> cachedIds;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    UserRepository userRepository;

    public void clearIds() {
        this.cachedIds = null;
    }
    @Async
    public void initializeCachedIds(String userId, String currentRole) {
        List<UUID> ids = getAllCreatedIdByRoleAndCreated(userId, "USER", currentRole);
        cachedIds = ids;
    }

    public void reloadCachedIds(String userId, String currentRole) {
        List<UUID> ids = getAllCreatedIdByRoleAndCreated(userId, "USER", currentRole);
        cachedIds = ids;
    }

    public List<UUID> getAllCreatedIdByRoleAndCreated(String createdBy, String Role, String currentRole) {
        if(RoleEnum.getIntOfRole(Role) >= RoleEnum.getIntOfRole(currentRole)) {
            List<UUID> ids = new ArrayList<>();
            ids.add(UUID.fromString(createdBy));
            List<UUID> idsLocal = new ArrayList<>();
            idsLocal.add(UUID.fromString(createdBy));
            int i = RoleEnum.getIntOfRole(Role) - RoleEnum.getIntOfRole(currentRole);
            for(int j=0 ; j <i ; j++) {
                List<UUID> data = ownerUserRepository.findAllIdByCreated(idsLocal);
                if (data.isEmpty()) {
                    break;
                } else {
                    idsLocal = data;
                    ids.addAll(data);
                }

            }
            for(int k = 0; k<ids.size(); k++) {
                List<User> userList = userRepository.findAllUserByCreated(ids.get(k));
                if(!userList.isEmpty()) {
                    List<UUID> idU = userList.stream().map(user -> user.getId()).collect(Collectors.toList());
                    ids.addAll(idU);
                }
            }
            return ids;
        } else {
            return null;
        }
    }

}
