package vn.automationandtesting.atproject.service.surge;

import vn.automationandtesting.atproject.controller.dto.surge.TestSurgeTypeDto;
import vn.automationandtesting.atproject.entity.surge.TestSurgeType;

import java.util.List;

public interface TestSurgeTypeService {
    List<TestSurgeTypeDto> findAll();
    List<TestSurgeTypeDto> findTestTypeByCode(String code);
    List<TestSurgeTypeDto> findTestTypeById(String code);
    void saveAll(List<TestSurgeType> testTypes);

    void deleteAll(List<TestSurgeType> testTypes);
}
