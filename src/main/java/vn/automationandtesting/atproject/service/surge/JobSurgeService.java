package vn.automationandtesting.atproject.service.surge;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.power.JobPowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.JobSurgeDto;
import vn.automationandtesting.atproject.entity.surge.JobsSurge;

import java.util.List;
import java.util.UUID;

public interface JobSurgeService {

    List<JobSurgeDto> findAllJobByAssetId(UUID asset_id);

    List<JobSurgeDto> findJobById(UUID id);

    List<JobSurgeDto> findJobByName(String name);

    List<JobSurgeDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobSurgeDto> findAll();

    List<JobSurgeDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<JobsSurge> jobdataList);

    void deleteAll(List<JobsSurge> jobdataList);

    List<JobSurgeDto> findJobByAssetId(UUID asset_id, int first, int limit);

    int countJobByAssetId(UUID asset_id);
}
