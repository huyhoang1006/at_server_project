package vn.automationandtesting.atproject.service.surge;

import vn.automationandtesting.atproject.controller.dto.surge.TestSurgeDto;
import vn.automationandtesting.atproject.entity.surge.TestsSurge;

import java.util.List;
import java.util.UUID;

public interface TestSurgeService {
    List<TestSurgeDto> findAllTestByJobId(UUID job_id);
    List<TestSurgeDto> findTestById(UUID id);
    List<TestSurgeDto> findTestByType(UUID type_id);
    List<TestSurgeDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestsSurge> tests);

    void deleteAll(List<TestsSurge> tests);
}
