package vn.automationandtesting.atproject.service.surge;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.List;
import java.util.UUID;

public interface SurgeService {
    List<SurgeDto> findAllByLocationId(UUID location_id);

    List<SurgeDto> findAssetById(UUID id);

    List<SurgeDto> findBySerial(String serial);

    List<SurgeDto> findBySerialAndLocation(String serial, UUID location_id);

    List<SurgeDto> findAll();

    List<SurgeDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Surge> assets);
    SurgeDto updateAssetLite(SurgeDto surgeDto, UUID id);

    void deleteAll(List<Surge> assets);

    List<SurgeDto> findAssetByLocationId(UUID id, int first, int limit);

    int countAssetByLocationId(UUID locationId);

    int countAssetList();
    List<SurgeDto> findAssetList(int stt, int sl);

    List<SurgeDto> findAssetListByLocationId(UUID location_id, int stt, int sl);

    int countAssetListByLocationId(UUID location_id);

    SurgeDto getAssetById(UUID asset_id, String user_id);

}
