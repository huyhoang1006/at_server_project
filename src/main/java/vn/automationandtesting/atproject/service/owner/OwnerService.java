package vn.automationandtesting.atproject.service.owner;


import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.entity.owner.Owner;

import java.util.List;
import java.util.UUID;

public interface OwnerService {
    OwnerDto findById(UUID ownerId);
    List<OwnerDto> findAll(UUID userId);

    List<Owner> saveOwner(List<OwnerDto> ownerDtoList);

    List<Owner> getAllOwnerAndParent(UUID id);

    List<Owner> findOwnerParentAndPreById(UUID id, String pre, int first, int limit);

    Boolean removeOwner(OwnerDto ownerDto);

    Owner updateOwner(OwnerDto ownerDto);

    Owner deleteOwner(OwnerDto ownerDto, Boolean sign);

    List<Object> findOwnerParentById(UUID id);

    List<Object> findOwnerParentById(UUID id, String mode, int first, int limit);

    void changeOwner(UUID owner_id, UUID user_id);

    int countByCreatedAndPre(UUID id, String pre);

    int countByRef(UUID ref_id, UUID user_id);

    List<Owner> getOwnerByRefAndCreated(UUID ref_id, UUID user_id, int first, int limit);

    int countByCreated(UUID created_by);

    List<OwnerDto> findByCreated(UUID created_by, int first, int limit);

    int countByRefId(UUID ref_id);

    List<OwnerDto> findByRef(UUID ref_id, int first, int limit);

    List<OwnerDto> findByCreatedAndPre(UUID created_by, int first, int limit, String pre);

    List<OwnerDto> findOwnerParent(UUID id, String role);

    List<OwnerDto> getDataByIncludes(OwnerDto ownerDto, int first, int limit);

    Long countAllOwnerUserByField(OwnerDto ownerDto);

    int countAllOwnerByIds();

    int countAllOwnerByIdsAndMode(String mode);

    List<OwnerDto> getOwnerByParentId(UUID id);

    int countOwnerByRole(String role);

    List<OwnerDto> getOwnerByRole(String role, int first, int limit);
}
