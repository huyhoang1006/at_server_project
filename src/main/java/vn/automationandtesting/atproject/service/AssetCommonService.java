package vn.automationandtesting.atproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;

import java.util.List;
import java.util.UUID;

public interface AssetCommonService {

    public JSONObject countAsset();

    public Object getAsset(String asset, int stt, int sl);

    Object findAllAssetByField(Object data, int first, int limit);

    JSONObject countAllAssetByField(Object data);

    Object findAssetListByLocationId(UUID location_id, int stt, int sl);

    JSONObject countAssetListByLocationId(UUID location_id);
}
