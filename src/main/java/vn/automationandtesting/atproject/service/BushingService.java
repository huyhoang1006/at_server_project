package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;

import java.util.List;
import java.util.UUID;

/**
 * @author tund on 3/9/2022
 * @project at-project-server
 */
public interface BushingService {

    BushingDto createNewBushing(BushingDto bushingDto);

    BushingDto updateBushing(BushingDto bushingDto, UUID id);

    void deleteBushing(UUID id);

    List<BushingDto> getAllBushingsByLocalid(UUID id);

    List<BushingDto> getAllBushingsByAssetid(UUID id);
}
