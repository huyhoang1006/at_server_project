package vn.automationandtesting.atproject.service.disconnector;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;

import java.util.List;
import java.util.UUID;

public interface DisconnectorService {
    List<DisconnectorDto> findAllByLocationId(UUID location_id);

    List<DisconnectorDto> findAssetById(UUID id);

    List<DisconnectorDto> findBySerial(String serial);

    List<DisconnectorDto> findBySerialAndLocation(String serial, UUID location_id);

    List<DisconnectorDto> findAll();

    List<DisconnectorDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Disconnector> assets);
    DisconnectorDto updateAssetLite(DisconnectorDto disconnectorDto, UUID id);
    void deleteAll(List<Disconnector> assets);

    List<DisconnectorDto> findAssetByLocationId(UUID id, int first, int limit);

    int countAssetByLocationId(UUID locationId);

    int countAssetList();
    List<DisconnectorDto> findAssetList(int stt, int sl);

    List<DisconnectorDto> findAssetListByLocationId(UUID location_id, int stt, int sl);

    int countAssetListByLocationId(UUID location_id);

    DisconnectorDto getAssetById(UUID asset_id, String user_id);
}
