package vn.automationandtesting.atproject.service.disconnector;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.current.JobCurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.JobDisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.JobsDisconnector;

import java.util.List;
import java.util.UUID;

public interface JobDisconnectorService {

    List<JobDisconnectorDto> findAllJobByAssetId(UUID asset_id);

    List<JobDisconnectorDto> findJobById(UUID id);

    List<JobDisconnectorDto> findJobByName(String name);

    List<JobDisconnectorDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobDisconnectorDto> findAll();

    List<JobDisconnectorDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<JobsDisconnector> jobdataList);

    void deleteAll(List<JobsDisconnector> jobdataList);

    List<JobDisconnectorDto> findJobByAssetId(UUID asset_id, int first, int limit);

    int countJobByAssetId(UUID asset_id);
}
