package vn.automationandtesting.atproject.service;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.common.LocationOwnerDto;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
public interface LocationService {
    LocationDto createLocation(LocationDto locationDto);

    Map<String, Object> updateLocation(UUID id, LocationDto locationDto);

    LocationDto updateLocationLite(UUID id, LocationDto locationDto);

    void deleteLocation(UUID id);

    void deleteLocationData(UUID id);

    LocationDto getLocationById(UUID id);

    LocationDto getListLocationById(UUID id);

    List<Object> getListLocation(List<UUID> ids);

    List<LocationDto> getLocationByRefId(UUID id);

    List<LocationDto> searchLocations(UUID id, Pageable pageable);

    ResponseObject lock(Boolean locked, List<UUID> listId);

    ResponseObject download(List<UUID> listId);

    ResponseObject upload(ResourceFullDto resourceFullDto);

    Location insertIfNotExist(LocationDto locationDto);

    void updateJobCollabs(String locationId, ListIdDto listIdDto);

    List<LocationDto> findLocationByRefId(UUID ref_id, int first, int limit);

    int countLocationByRefId(UUID id);

    List<LocationDto> findLocationByRefIdAndCreatedAndCollab(String ref_id, int first, int limit);

    int countLocationByRefIdAndCreatedAndCollab(String ref_id);

    List<LocationDto> findAllLocationByCreatedAndCollab(String mode, UUID user_id, int offset, int limit);

    int countAllLocationByCreatedAndCollab(String mode, UUID user_id);

    List<LocationDto> findAllLocationByField(LocationDto locationDto, int first, int limit, String user_id);

    Long countAllLocationByField(LocationDto locationDto, String user_id);

    LocationDto insertLocation(LocationDto locationDto);

    LocationDto insertLocationData(UUID locationId, LocationDto locationDto, List<AttachmentDto> attachmentDto, List<String> deleteFile, MultipartFile[] files);

    int countLocationsByRefIdOwnerAndModeAndCreatedBy(String mode, UUID userId);

    int countLocationsByCreatedByAndCollab(UUID userId);

    int countLocationsModeByCreatedByAndCollab(String mode, UUID userId);

    List<LocationOwnerDto> addLocationByOwnerFullName(List<LocationOwnerDto> locationOwnerDtoList);
}
