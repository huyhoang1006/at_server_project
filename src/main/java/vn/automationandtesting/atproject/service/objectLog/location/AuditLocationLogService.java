package vn.automationandtesting.atproject.service.objectLog.location;

import vn.automationandtesting.atproject.controller.dto.objectLog.location.AuditLocationLogDto;

public interface AuditLocationLogService {
    public AuditLocationLogDto insert(AuditLocationLogDto auditLocationLogDto);
}
