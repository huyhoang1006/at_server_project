package vn.automationandtesting.atproject.service.voltage;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.surge.JobSurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.JobVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.JobsVoltage;

import java.util.List;
import java.util.UUID;

public interface JobVoltageService {

    List<JobVoltageDto> findAllJobByAssetId(UUID asset_id);

    List<JobVoltageDto> findJobById(UUID id);

    List<JobVoltageDto> findJobByName(String name);

    List<JobVoltageDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobVoltageDto> findAll();

    List<JobVoltageDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<JobsVoltage> jobdataList);

    void deleteAll(List<JobsVoltage> jobdataList);

    List<JobVoltageDto> findJobByAssetId(UUID asset_id, int first, int limit);

    int countJobByAssetId(UUID asset_id);
}
