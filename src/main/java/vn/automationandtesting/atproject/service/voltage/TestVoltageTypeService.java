package vn.automationandtesting.atproject.service.voltage;

import vn.automationandtesting.atproject.controller.dto.voltage.TestVoltageTypeDto;
import vn.automationandtesting.atproject.entity.voltage.TestVoltageType;

import java.util.List;

public interface TestVoltageTypeService {
    List<TestVoltageTypeDto> findAll();
    List<TestVoltageTypeDto> findTestTypeByCode(String code);
    List<TestVoltageTypeDto> findTestTypeById(String code);
    void saveAll(List<TestVoltageType> testTypes);

    void deleteAll(List<TestVoltageType> testTypes);
}
