package vn.automationandtesting.atproject.service;


import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.entity.Attachment;

import java.util.List;
import java.util.UUID;

public interface AttachmentService {
    List<AttachmentDto> findAllById_foreign(String id_foreign);
    void deleteAttachmentById_foreign(String id_foreign);

    void insertAttachment(Attachment attachment);

    void updateNameById_foreign(Attachment attachment);

    Boolean findById(UUID id);

    String findNamebyId_foreign(UUID id_foreign);

    boolean insertAttachment(List<AttachmentDto> attachmentDtos);
}
