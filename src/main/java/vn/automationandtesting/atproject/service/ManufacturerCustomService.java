package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.cim.ManufacturerCustomDto;

import java.util.List;
import java.util.UUID;

public interface ManufacturerCustomService {
    List<ManufacturerCustomDto> getManuByName(String name);
    List<ManufacturerCustomDto> getManuByType(String type);
    ManufacturerCustomDto findById(UUID id);
    ManufacturerCustomDto updateManuById(UUID id, ManufacturerCustomDto manufacturerCustomDto);
    ManufacturerCustomDto insertManu(ManufacturerCustomDto manufacturerCustomDto);
    List<UUID> deleteManu(List<UUID> ids);
}
