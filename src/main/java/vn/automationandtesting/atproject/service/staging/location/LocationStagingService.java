package vn.automationandtesting.atproject.service.staging.location;

import vn.automationandtesting.atproject.controller.dto.staging.location.LocationStagingDto;
import vn.automationandtesting.atproject.entity.LocationStaging;

public interface LocationStagingService {
    public LocationStagingDto insert(LocationStagingDto locationStagingDto);
}
