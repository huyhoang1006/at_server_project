package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserMessageDto;
import vn.automationandtesting.atproject.controller.dto.request.UserMessageDto;
import vn.automationandtesting.atproject.entity.OwnerUser;

import java.util.List;
import java.util.UUID;

public interface OwnerUserService {
    int countOwnerUserByCreated(UUID id);

    void removeOwnerUserById(UUID id);

    void deleteOwnerUserById(UUID id);

    OwnerUserDto updateOwnerUserAllInfo(OwnerUserDto ownerUserDto, UUID id);

    OwnerUserDto getOwnerUserById(UUID id);

    List<OwnerUserDto> findOwnerUserByCreated(UUID created_by, int first, int limit);

    ResponseObject saveNewOwnerUser(OwnerUserDto ownerUserDto);

    ResponseObject changePassword(UUID userID, String oldPassword, String newPassword);

    OwnerUser getOwnerUserByUsername(String username);

    List<OwnerUserDto> getDataByIncludes(OwnerUserDto ownerUser, int first, int limit);

    Long countAllOwnerUserByField(OwnerUserDto ownerUserDto);

    List<UUID> getAllCreatedIdByRoleAndCreated(String createdBy, String Role);

    List<OwnerUserMessageDto> searchMessageUser(String data);
}
