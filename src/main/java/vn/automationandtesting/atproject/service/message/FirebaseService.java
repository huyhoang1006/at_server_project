package vn.automationandtesting.atproject.service.message;

import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.controller.dto.message.MessageDto;

import java.util.List;

public interface FirebaseService {
    public void insertNotification(String name, String data, String title, String type, Boolean sign, MessageDto messageDto, BearerContext bearerContext);

    public void insertNotificationNoMessage(String name, String data, String title, String type, String updatedId, String stagingId, String id, Boolean sign, BearerContext bearerContext);

    public Boolean checkCollection(String collectionName);

    public void insertCollection(String name);

    public void markAsRead(String name, String id);

    public void markAsReadArr(String name, List<String> ids);
}
