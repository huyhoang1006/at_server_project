package vn.automationandtesting.atproject.service.message;

import vn.automationandtesting.atproject.controller.dto.message.MessageDto;

import java.util.List;
import java.util.UUID;

public interface MessageService {
    public MessageDto saveMessage(MessageDto messageDto);
    public List<MessageDto> getMessage(UUID user_id, UUID group_id, int first, int sl);

    public List<MessageDto> getAllMessageByUnread(UUID user_id, UUID group_id);

    public MessageDto getMessageById(String groupId, String id, String userId);

    public void markAsSeenArr(List<UUID> ids);
}
