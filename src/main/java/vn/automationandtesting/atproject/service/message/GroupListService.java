package vn.automationandtesting.atproject.service.message;

import vn.automationandtesting.atproject.controller.dto.message.GroupListDto;
import vn.automationandtesting.atproject.entity.message.GroupList;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface GroupListService {
    public GroupListDto saveGroupList(GroupListDto groupListDto);

    public List<GroupListDto> getGroupList(UUID user_id, int first, int sl);

    public List<GroupListDto> getGroupListById(UUID id);

    public List<GroupListDto> getSingleGroupListByTypeAndId(String type, String user_id, String other_id);

    public List<GroupListDto> getGroupListByTypeAndId(String type, String user_id);

    public GroupListDto updateGroup(UUID group_id, UUID user_id, String lastMessage, Timestamp timestamp);

    public Boolean deleteGroup(UUID group_id, UUID user_id);

    public GroupListDto renameGroup(UUID group_id, String user_id, String name);

    List<Object> getMemberOfGroupById(UUID groupId, UUID userId);

    public GroupListDto addMemberToGroup(List<UUID> ids,UUID userId,String groupId);

    public GroupListDto deleteMember(UUID id, UUID userId, UUID groupId);
}
