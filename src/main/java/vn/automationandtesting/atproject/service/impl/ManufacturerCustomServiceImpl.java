package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.ManufacturerCustomDto;
import vn.automationandtesting.atproject.controller.dto.mapper.ManufacturerCustomMapper;
import vn.automationandtesting.atproject.entity.ManufacturerCustom;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.repository.ManufacturerCustomRepository;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.service.ManufacturerCustomService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class ManufacturerCustomServiceImpl implements ManufacturerCustomService {

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private ManufacturerCustomRepository manufacturerCustomRepository;

    @Autowired
    private ManufacturerCustomMapper mapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    /**
     * @param name
     * @return
     */
    @Override
    public List<ManufacturerCustomDto> getManuByName(String name) {
        List<ManufacturerCustom> manufacturerCustomList = manufacturerCustomRepository.findByName(name);
        List<ManufacturerCustomDto> manufacturerCustomDtoList = new ArrayList<>();
        if(!manufacturerCustomList.isEmpty()) {
            for(ManufacturerCustom manufacturerCustom : manufacturerCustomList) {
                ManufacturerCustomDto manufacturerCustomDto = mapper.manuToManuDto(manufacturerCustom);
                manufacturerCustomDtoList.add(manufacturerCustomDto);
            }
        }
        return manufacturerCustomDtoList;
    }

    /**
     * @param type
     * @return
     */
    @Override
    public List<ManufacturerCustomDto> getManuByType(String type) {
        List<ManufacturerCustom> manufacturerCustomList = manufacturerCustomRepository.findByType(type);
        List<ManufacturerCustomDto> manufacturerCustomDtoList = new ArrayList<>();
        if(!manufacturerCustomList.isEmpty()) {
            for(ManufacturerCustom manufacturerCustom : manufacturerCustomList) {
                ManufacturerCustomDto manufacturerCustomDto = mapper.manuToManuDto(manufacturerCustom);
                manufacturerCustomDtoList.add(manufacturerCustomDto);
            }
        }
        return manufacturerCustomDtoList;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ManufacturerCustomDto findById(UUID id) {
        ManufacturerCustom manufacturerCustom = manufacturerCustomRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Manufacturer not found"));
        return mapper.manuToManuDto(manufacturerCustom);
    }

    /**
     * @param id
     * @param manufacturerCustomDto
     * @return
     */
    @Override
    public ManufacturerCustomDto updateManuById(UUID id, ManufacturerCustomDto manufacturerCustomDto) {
        ManufacturerCustom manufacturerCustom = manufacturerCustomRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Manufacturer not found"));
        List<UUID> ids = sharedListIdService.getCachedIds();
        if(ids.contains(manufacturerCustom.getCreatedBy())) {
            manufacturerCustom = mapper.copyManuDtoToManu(manufacturerCustomDto, manufacturerCustom);
            manufacturerCustomRepository.save(manufacturerCustom);
            return manufacturerCustomDto;
        } else {
            return null;
        }
    }

    /**
     * @param manufacturerCustomDto
     * @return
     */
    @Override
    public ManufacturerCustomDto insertManu(ManufacturerCustomDto manufacturerCustomDto) {
        ManufacturerCustom manufacturerCustom = mapper.manuDtoToManu(manufacturerCustomDto);
        manufacturerCustomRepository.save(manufacturerCustom);
        return manufacturerCustomDto;
    }

    /**
     * @param ids
     * @return
     */
    @Override
    public List<UUID> deleteManu(List<UUID> ids) {
        List<UUID> uuidList = new ArrayList<>();
        List<UUID> idUuids = sharedListIdService.getCachedIds();
        for(UUID id : ids) {
            List<ManufacturerCustom> manufacturerCustomList = manufacturerCustomRepository.getById(id);
            if(!manufacturerCustomList.isEmpty()) {
                if(idUuids.contains(manufacturerCustomList.get(0).getCreatedBy())) {
                    manufacturerCustomRepository.delete(manufacturerCustomList.get(0));
                } else {
                    uuidList.add(id);
                }
            } else {
                uuidList.add(id);
            }
        }
        return uuidList;
    }

    public List<UUID> getAllCreatedIdByRoleAndCreated(String createdBy, String Role) {
        if(RoleEnum.getIntOfRole(Role) >= RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName())) {
            List<UUID> ids = new ArrayList<>();
            ids.add(UUID.fromString(createdBy));
            List<UUID> idsLocal = new ArrayList<>();
            idsLocal.add(UUID.fromString(createdBy));
            int i = RoleEnum.getIntOfRole(Role) - RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
            for(int j=0 ; j <i ; j++) {
                List<UUID> data = ownerUserRepository.findAllIdByCreated(idsLocal);
                if (data.isEmpty()) {
                    break;
                } else {
                    idsLocal = data;
                    ids.addAll(data);
                }

            }
            for(int k = 0; k<ids.size(); k++) {
                List<User> userList = userRepository.findAllUserByCreated(ids.get(k));
                if(!userList.isEmpty()) {
                    List<UUID> idU = userList.stream().map(user -> user.getId()).collect(Collectors.toList());
                    ids.addAll(idU);
                }
            }
            return ids;
        } else {
            return null;
        }
    }
}
