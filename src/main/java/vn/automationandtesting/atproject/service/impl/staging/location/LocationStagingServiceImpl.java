package vn.automationandtesting.atproject.service.impl.staging.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.mapper.staging.location.LocationStagingMapper;
import vn.automationandtesting.atproject.controller.dto.staging.location.LocationStagingDto;
import vn.automationandtesting.atproject.entity.LocationStaging;
import vn.automationandtesting.atproject.repository.staging.location.LocationStagingRepository;
import vn.automationandtesting.atproject.service.staging.location.LocationStagingService;

@Service
public class LocationStagingServiceImpl implements LocationStagingService {

    @Autowired
    private LocationStagingRepository locationStagingRepository;

    @Autowired
    private LocationStagingMapper locationStagingMapper;

    /**
     * @param locationStagingDto
     * @return
     */
    @Override
    public LocationStagingDto insert(LocationStagingDto locationStagingDto) {
        try {
            LocationStaging locationStaging = new LocationStaging();
            locationStaging = locationStagingMapper.objectDtoToObject(locationStagingDto, locationStaging);
            locationStagingRepository.save(locationStaging);
            return locationStagingDto;
        } catch (Exception e) {
            return null;
        }
    }
}
