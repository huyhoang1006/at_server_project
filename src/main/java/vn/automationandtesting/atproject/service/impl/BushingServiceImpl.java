package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.controller.dto.mapper.BushingMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Bushing;
import vn.automationandtesting.atproject.exception.AssetNotFoundException;
import vn.automationandtesting.atproject.exception.BushingNotFoundException;
import vn.automationandtesting.atproject.repository.AssetRepository;
import vn.automationandtesting.atproject.repository.BushingRepository;
import vn.automationandtesting.atproject.repository.LocationRepository;
import vn.automationandtesting.atproject.service.BushingService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author tund on 3/9/2022
 * @project at-project-server
 */
@Service
public class BushingServiceImpl implements BushingService{

    @Autowired
    private BushingRepository bushingRepository;

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private BushingMapper bushingMapper;

    @Autowired
    private LocationRepository locationRepository;


    @Override
    public BushingDto createNewBushing(BushingDto bushingDto) {
        Asset asset = assetRepository.findByMridAndIsDeleted(bushingDto.getAsset_id(), false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        Bushing bushing = bushingMapper.bushingDtoToBushing(bushingDto);
        if(bushing.getId() == null) {
            bushing.setId(UUID.randomUUID());
        }
        bushing.setAsset(asset);
        bushing = bushingRepository.save(bushing);
        return bushingMapper.bushingToBushingDto(bushing);
    }

    @Override
    public BushingDto updateBushing(BushingDto bushingDto, UUID id) {
        Bushing bushing = bushingRepository.findById(id)
                .orElseThrow(() -> new BushingNotFoundException("Bushing id not existed"));
        bushing = bushingMapper.copyBushingDtoToBushing(bushingDto, bushing, "id", "asset_id");
        Bushing updateBushing = bushingRepository.save(bushing);
        return bushingMapper.bushingToBushingDto(updateBushing);
    }

    @Override
    public void deleteBushing(UUID id) {
        Bushing bushing = bushingRepository.findByIdAndIsDeleted(id, false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        bushing.setIsDeleted(true);
        bushingRepository.save(bushing);
    }

    @Override
    public List<BushingDto> getAllBushingsByLocalid(UUID id) {
        List<BushingDto> bushingDtoList = new ArrayList<>();
        List<Bushing> bushings = bushingRepository.findByLocationIdAndIsDeleted(id, false);
        for (Bushing bushing : bushings){
            BushingDto bushingDto = bushingMapper.bushingToBushingDto(bushing);
            bushingDtoList.add(bushingDto);
        }
        return bushingDtoList;
    }

    @Override
    public List<BushingDto> getAllBushingsByAssetid(UUID id) {
        Asset asset = assetRepository.findByMridAndIsDeleted(id, false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        List<Bushing> bushings = bushingRepository.findAllByAssetAndIsDeleted(asset,false);
        List<BushingDto> bushingDtoList = new ArrayList<>();
        for (Bushing bushing : bushings) {
            BushingDto bushingDto = bushingMapper.bushingToBushingDto(bushing);
            bushingDtoList.add(bushingDto);
        }
        return bushingDtoList;
    }
}
