package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.OwnerUserMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserMessageDto;
import vn.automationandtesting.atproject.entity.Group;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.exception.GroupNotFoundException;
import vn.automationandtesting.atproject.exception.UserNotFoundException;
import vn.automationandtesting.atproject.repository.GroupRepository;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.RoleRepository;
import vn.automationandtesting.atproject.service.OwnerUserService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OwnerUserServiceImpl implements OwnerUserService {

    @Autowired
    private SharedListIdService sharedListIdService;
    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private OwnerUserMapper ownerUserMapper;

    @Autowired
    private GroupRepository groupRepository;

    /**
     * @param id
     * @return
     */
    @Override
    public int countOwnerUserByCreated(UUID id) {
        return ownerUserRepository.countOwnerUserByCreated(id);
    }

    /**
     * @param id
     */
    @Override
    public void removeOwnerUserById(UUID id) {
        OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        if(!ownerUser.getRole().getRoleName().toString().equals("ADMIN")) {
            List<OwnerUser> ownerUserList = ownerUserRepository.findAllOwnerUserByCreated(id);
            for (OwnerUser ownerUserdata : ownerUserList) {
                ownerUserdata.setCreatedBy(ownerUser.getCreatedBy());
            }
            ownerUserRepository.saveAll(ownerUserList);
            ownerUserRepository.delete(ownerUser);
        }
    }

    /**
     * @param id
     */
    @Override
    public void deleteOwnerUserById(UUID id) {
        OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        ownerUser.setIsDeleted(true);
        ownerUserRepository.save(ownerUser);
    }

    /**
     * @param ownerUserDto
     * @param id
     * @return
     */
    @Override
    public OwnerUserDto updateOwnerUserAllInfo(OwnerUserDto ownerUserDto, UUID id) {
        OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        BeanUtils.copyProperties(ownerUserDto, ownerUser, "id", "groups", "password");
        ownerUser.setIsDeleted(ownerUserDto.isDelete());
        if(!ownerUser.getGroups().isEmpty()) {
            ownerUser.setGroups(checkValidGroupName(ownerUserDto.getGroups()));
        }

        if(ownerUserDto.getPassword() != null && !ownerUserDto.getPassword().isEmpty()) {
            ownerUser.setPassword(passwordEncoder.encode(ownerUserDto.getPassword()));
        }
        OwnerUser savedOwnerUser = ownerUserRepository.save(ownerUser);
        return ownerUserMapper.userOwnerToUserOwnerReqDto(savedOwnerUser);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public OwnerUserDto getOwnerUserById(UUID id) {
        OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        return ownerUserMapper.userOwnerToUserOwnerReqDto(ownerUser);
    }

    /**
     * @param created_by
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerUserDto> findOwnerUserByCreated(UUID created_by, int first, int limit) {
        List<OwnerUser> ownerUsers = ownerUserRepository.findOwnerUserByCreated(created_by, first, limit);
        List<OwnerUserDto> ownerUserDtos = ownerUsers.stream().map(ownerUser ->
                ownerUserMapper.userOwnerToUserOwnerReqDto(ownerUser)
        ).collect(Collectors.toList());
        return ownerUserDtos;
    }

    /**
     * @param ownerUserDto
     * @return
     */
    @Override
    public ResponseObject saveNewOwnerUser(OwnerUserDto ownerUserDto) {
        //check existing username
        if (ownerUserRepository.existsByUsername(ownerUserDto.getUsername())) {
            return ResponseObject.returnWithError("Username already existed");
        }
        int role = RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
        int roleCreate = RoleEnum.getIntOfRole(ownerUserDto.getRole());
        if(role < roleCreate) {
            OwnerUser ownerUser = ownerUserMapper.userOwnerReqDtoToUserOwner(ownerUserDto);
            ownerUser.setPassword(passwordEncoder.encode(ownerUser.getPassword()));
            ownerUser.setId(UUID.randomUUID());
            ownerUserRepository.save(ownerUser);
            return ResponseObject.returnWithData(true);
        } else {
            return ResponseObject.returnWithError("Do not permission");
        }
    }

    /**
     * @param userID
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @Override
    public ResponseObject changePassword(UUID userID, String oldPassword, String newPassword) {
        OwnerUser ownerUser = ownerUserRepository.findById(userID).orElse(null);
        if (ownerUser == null) {
            return ResponseObject.returnWithError("Invalid owner user");
        }
        if (!passwordEncoder.matches(oldPassword, ownerUser.getPassword())) {
            return ResponseObject.returnWithError("Invalid Password");
        }

        ownerUser.setPassword(passwordEncoder.encode(newPassword));
        ownerUserRepository.save(ownerUser);
        return ResponseObject.returnWithData(true);
    }

    /**
     * @param username
     * @return
     */
    @Override
    public OwnerUser getOwnerUserByUsername(String username) {
        return ownerUserRepository.findAllOwnerUserByUserName(username);
    }

    /**
     * @param ownerUserDto
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerUserDto> getDataByIncludes(OwnerUserDto ownerUserDto, int first, int limit) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<OwnerUser> ownerUserList = ownerUserRepository.findAllOwnerUserByField(ownerUserDto, first, limit, ids);
        return ownerUserList.stream().map(ownerUser ->
                ownerUserMapper.userOwnerToUserOwnerReqDto(ownerUser)
        ).collect(Collectors.toList());
    }

    /**
     * @param ownerUserDto
     * @return
     */
    @Override
    public Long countAllOwnerUserByField(OwnerUserDto ownerUserDto) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        Long count = ownerUserRepository.countAllOwnerUserByField(ownerUserDto, ids);
        return count;
    }

    /**
     * @param createdBy
     * @param Role
     * @return
     */
    @Override
    public List<UUID> getAllCreatedIdByRoleAndCreated(String createdBy, String Role) {
        if(RoleEnum.getIntOfRole(Role) >= RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName())) {
            List<UUID> ids = new ArrayList<>();
            ids.add(UUID.fromString(createdBy));
            List<UUID> idsLocal = new ArrayList<>();
            idsLocal.add(UUID.fromString(createdBy));
            int i = RoleEnum.getIntOfRole(Role) - RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
            for(int j=0 ; j <i ; j++) {
                List<UUID> data = ownerUserRepository.findAllIdByCreated(idsLocal);
                if (data.isEmpty()) {
                    break;
                } else {
                    idsLocal = data;
                    ids.addAll(data);
                }

            }
            return ids;
        } else {
            return null;
        }
    }

    /**
     * @param data
     * @return
     */
    @Override
    public List<OwnerUserMessageDto> searchMessageUser(String data) {
        List<OwnerUser> ownerUserList = ownerUserRepository.findAllUserByEmailOrName(data);
        List<String> fields = new ArrayList<>();
        List<OwnerUserMessageDto> ownerUserMessageDtoList = new ArrayList<>();
        fields.add("id");
        fields.add("username");
        fields.add("email");
        for(OwnerUser ownerUser : ownerUserList) {
            OwnerUserMessageDto ownerUserMessageDto = new OwnerUserMessageDto();
            BeanUtilsCustom.copyProperties(ownerUser, ownerUserMessageDto, fields);
            ownerUserMessageDtoList.add(ownerUserMessageDto);
        }
        return ownerUserMessageDtoList;
    }

    private String checkValidGroupName(List<String> groups) {
        List<Group> groupList = groupRepository.findAll();
        List<String> groupListString = groupList.stream().map(group -> group.getGroupName()).collect(Collectors.toList());
        groups.forEach(s -> {
            if (!groupListString.contains(s.toUpperCase())) {
                throw new GroupNotFoundException("Group name " + s + " not found.");
            }
        });
        return String.join(",", groups.stream().map(String::toUpperCase).collect(Collectors.toList()));
    }
}
