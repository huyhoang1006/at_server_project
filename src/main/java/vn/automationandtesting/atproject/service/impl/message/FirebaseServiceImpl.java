package vn.automationandtesting.atproject.service.impl.message;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.controller.dto.message.MessageDto;
import vn.automationandtesting.atproject.service.message.FirebaseService;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


@Service
public class FirebaseServiceImpl implements FirebaseService {

    @Autowired
    private Firestore firestore;


    /**
     *
     */
    @Override
    public void insertNotification(String name, String data, String title, String type, Boolean sign, MessageDto messageDto, BearerContext bearerContext) {
        CollectionReference messagesRef = firestore.collection("notifications")
                .document(name)
                .collection("messages");
        String id = messageDto.getId().toString();
        Map<String, Object> notificationData = new HashMap<>();
        notificationData.put("title", title); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("id", id); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("groupId", messageDto.getGroupId().toString()); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("content", data);
        notificationData.put("timestamp", Timestamp.from(Instant.now()));
        notificationData.put("isRead", sign); // Mảng trống, chưa có user nào đã đọc
        notificationData.put("type", type); // Loại thông báo, có thể tuỳ chỉnh theo yêu cầu
        notificationData.put("senderId", bearerContext.getUserId());
        notificationData.put("senderName", bearerContext.getUserName());

        try {
            // Thêm document vào sub-collection messages
            DocumentReference docRef = messagesRef.document(id);
            docRef.set(notificationData).get();
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Error adding document: " + e.getMessage());
            Thread.currentThread().interrupt(); // Để giữ trạng thái ngắt
        }
    }

    /**
     * @param name
     * @param data
     * @param title
     * @param type
     * @param updatedId
     * @param stagingId
     * @param id
     * @param sign
     * @param bearerContext
     */
    @Override
    public void insertNotificationNoMessage(String name, String data, String title, String type, String updatedId, String stagingId, String id, Boolean sign, BearerContext bearerContext) {
        CollectionReference messagesRef = firestore.collection("notifications")
                .document(name)
                .collection("messages");
        Map<String, Object> notificationData = new HashMap<>();
        notificationData.put("title", title); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("id", id); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("updatedId", updatedId); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("stagingId", stagingId); // Bạn có thể tuỳ chỉnh tiêu đề này
        notificationData.put("content", data);
        notificationData.put("timestamp", Timestamp.from(Instant.now()));
        notificationData.put("isRead", sign); // Mảng trống, chưa có user nào đã đọc
        notificationData.put("type", type); // Loại thông báo, có thể tuỳ chỉnh theo yêu cầu
        notificationData.put("senderId", bearerContext.getUserId());
        notificationData.put("senderName", bearerContext.getUserName());

        try {
            // Thêm document vào sub-collection messages
            DocumentReference docRef = messagesRef.document(id);
            docRef.set(notificationData).get();
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Error adding document: " + e.getMessage());
            Thread.currentThread().interrupt(); // Để giữ trạng thái ngắt
        }
    }

    /**
     * @param collectionName
     */
    @Override
    public Boolean checkCollection(String collectionName) {
        try {
            Iterable<CollectionReference> collections = firestore.listCollections();
            for (CollectionReference collection : collections) {
                if(collection.getId().equals(collectionName)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param name
     */
    @Override
    public void insertCollection(String name) {

    }

    /**
     * @param id
     */
    @Override
    public void markAsRead(String name, String id) {
        DocumentReference docRef = firestore.collection("notifications")
                .document(name)
                .collection("messages")
                .document(id);

        try {
            // Cập nhật trường isRead thành true
            docRef.update("isRead", true).get();
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Error updating document: " + e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    /**
     * @param name
     * @param ids
     */
    @Override
    public void markAsReadArr(String name, List<String> ids) {
        WriteBatch batch = firestore.batch();
        for (String id : ids) {
            DocumentReference docRef = firestore.collection("notifications")
                    .document(name)
                    .collection("messages")
                    .document(id);
            batch.update(docRef, "isRead", true); // Cập nhật trường isRead thành true
        }
        ApiFuture<List<WriteResult>> future = batch.commit(); // Giữ nguyên kiểu trả về
        try {
            future.get(); // Lấy danh sách WriteResult
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace(); // In lỗi ra để dễ dàng theo dõi
        }
    }
}
