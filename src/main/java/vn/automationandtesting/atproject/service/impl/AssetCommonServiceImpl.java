package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.repository.AssetCommonRepository;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.service.AssetCommonService;
import vn.automationandtesting.atproject.service.AssetService;
import vn.automationandtesting.atproject.service.circuit.CircuitService;
import vn.automationandtesting.atproject.service.current.CurrentService;
import vn.automationandtesting.atproject.service.disconnector.DisconnectorService;
import vn.automationandtesting.atproject.service.power.PowerCableService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;
import vn.automationandtesting.atproject.service.surge.SurgeService;
import vn.automationandtesting.atproject.service.voltage.VoltageService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AssetCommonServiceImpl implements AssetCommonService {

    @Autowired
    private SharedListIdService sharedListIdService;
    @Autowired
    private AssetService assetService;

    @Autowired
    private CircuitService circuitService;

    @Autowired
    private CurrentService currentService;

    @Autowired
    private VoltageService voltageService;

    @Autowired
    private DisconnectorService disconnectorService;

    @Autowired
    private SurgeService surgeService;

    @Autowired
    private PowerCableService powerCableService;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AssetCommonRepository assetCommonRepository;

    /**
     * @return
     */
    @Override
    public JSONObject countAsset() {
        JSONObject jsonObject = new JSONObject();
        List<UUID> ids = sharedListIdService.getCachedIds();
        int dataTransformer = assetCommonRepository.countAsset("asset", BearerContextHolder.getContext().getUserId(), ids);
        int dataCircuit = assetCommonRepository.countAsset("circuit_breaker", BearerContextHolder.getContext().getUserId(), ids);
        int dataCurent = assetCommonRepository.countAsset("current_voltage", BearerContextHolder.getContext().getUserId(), ids);
        int dataVoltage = assetCommonRepository.countAsset("voltage_trans", BearerContextHolder.getContext().getUserId(), ids);
        int dataDisconnector = assetCommonRepository.countAsset("disconnector", BearerContextHolder.getContext().getUserId(), ids);
        int dataSurge = assetCommonRepository.countAsset("surge_arrester", BearerContextHolder.getContext().getUserId(), ids);
        int dataPower = assetCommonRepository.countAsset("power_cable", BearerContextHolder.getContext().getUserId(), ids);
        try {
            jsonObject.put("Transformer", dataTransformer);
            jsonObject.put("Circuit breaker", dataCircuit);
            jsonObject.put("Current transformer", dataCurent);
            jsonObject.put("Voltage transformer", dataVoltage);
            jsonObject.put("Disconnector", dataDisconnector);
            jsonObject.put("Surge arrester", dataSurge);
            jsonObject.put("Power cable", dataPower);
            jsonObject.put("All", dataTransformer + dataCircuit + dataCurent + dataVoltage + dataDisconnector + dataSurge + dataPower);
            return jsonObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param asset
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public Object getAsset(String asset, int stt, int sl) {
        if(asset.equals("Transformer")) {
            List<AssetDto> assetDtoList = assetService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Circuit breaker")) {
            List<CircuitDto> assetDtoList = circuitService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Current transformer")) {
            List<CurrentDto> assetDtoList = currentService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Voltage transformer")) {
            List<VoltageDto> assetDtoList = voltageService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Disconnector")) {
            List<DisconnectorDto> assetDtoList = disconnectorService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Surge arrester")) {
            List<SurgeDto> assetDtoList = surgeService.findAssetList(stt, sl);
            return assetDtoList;
        } else if(asset.equals("Power cable")) {
            List<PowerCableDto> assetDtoList = powerCableService.findAssetList(stt, sl);
            return assetDtoList;
        } else {
            return null;
        }
    }

    /**
     * @param data
     * @param first
     * @param limit
     * @return
     */
    @Override
    public Object findAllAssetByField(Object data, int first, int limit) {
        HashMap<String, String> dataAsset = (HashMap<String, String>) data;
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        if(dataAsset.get("asset").isEmpty()) {
            return null;
        } else {
            return assetCommonRepository.getAssetInclude(data,userId,ids, first, limit);
        }
    }

    /**
     * @param data
     * @return
     */
    @Override
    public JSONObject countAllAssetByField(Object data) {
        HashMap<String, String> dataAsset = (HashMap<String, String>) data;
        if (dataAsset.get("asset").equals("Transformer")) {
            dataAsset.replace("asset", "asset");
        } else if (dataAsset.get("asset").equals("Circuit breaker")) {
            dataAsset.replace("asset", "circuit_breaker");
        } else if (dataAsset.get("asset").equals("Current transformer")) {
            dataAsset.replace("asset", "current_voltage");
        } else if (dataAsset.get("asset").equals("Voltage transformer")) {
            dataAsset.replace("asset", "voltage_trans");
        } else if (dataAsset.get("asset").equals("Disconnector")) {
            dataAsset.replace("asset", "disconnector");
        } else if (dataAsset.get("asset").equals("Surge arrester")) {
            dataAsset.replace("asset", "surge_arrester");
        } else if (dataAsset.get("asset").equals("Power cable")) {
            dataAsset.replace("asset", "power_cable");
        }
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        JSONObject object = assetCommonRepository.countAssetInclude(dataAsset, userId, ids);
        return object;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public Object findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<Object> data = new ArrayList<>();
        return null;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public JSONObject countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        JSONObject object = assetCommonRepository.countAssetListOffset(location_id, userId, ids);
        return object;
    }
}
