package vn.automationandtesting.atproject.service.impl.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.message.GroupListMapper;
import vn.automationandtesting.atproject.controller.dto.message.GroupListDto;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.message.GroupList;
import vn.automationandtesting.atproject.repository.message.GroupListRepository;
import vn.automationandtesting.atproject.service.OwnerUserService;
import vn.automationandtesting.atproject.service.UserService;
import vn.automationandtesting.atproject.service.message.GroupListService;
import com.google.cloud.firestore.Firestore;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class GroupListServiceImpl implements GroupListService {

    @Autowired
    private GroupListMapper groupListMapper;

    @Autowired
    private GroupListRepository groupListRepository;

    @Autowired
    private Firestore firestore;

    @Autowired
    private UserService userService;

    @Autowired
    private OwnerUserService ownerUserService;


    /**
     * @param groupListDto
     * @return
     */
    @Override
    public GroupListDto saveGroupList(GroupListDto groupListDto) {
        GroupList groupList = new GroupList();
        groupList = groupListMapper.copyDtoToEntity(groupListDto, groupList);
        groupListRepository.save(groupList);
        return groupListDto;
    }

    /**
     * @param user_id
     * @param first
     * @param sl
     * @return
     */
    @Override
    public List<GroupListDto> getGroupList(UUID user_id, int first, int sl) {
        List<GroupList> groupLists = groupListRepository.findGroupByCreatedIdAndMember(user_id, user_id.toString(), first, sl);
        return groupLists.stream().map(groupList -> groupListMapper.copyEntityToDto(groupList, new GroupListDto())).collect(Collectors.toList());
    }

    /**
     * @param id
     * @return
     */
    @Override
    public List<GroupListDto> getGroupListById(UUID id) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(id);
        return groupLists.stream().map(groupList -> groupListMapper.copyEntityToDto(groupList, new GroupListDto())).collect(Collectors.toList());
    }

    /**
     * @param type
     * @param user_id
     * @param other_id
     * @return
     */
    @Override
    public List<GroupListDto> getSingleGroupListByTypeAndId(String type, String user_id, String other_id) {
        List<GroupList> groupLists = groupListRepository.findSingleGroupListByTypeAndId(type, BearerContextHolder.getContext().getUserId(), other_id);
        return groupLists.stream().map(groupList -> groupListMapper.copyEntityToDto(groupList, new GroupListDto())).collect(Collectors.toList());
    }

    /**
     * @param type
     * @param user_id
     * @return
     */
    @Override
    public List<GroupListDto> getGroupListByTypeAndId(String type, String user_id) {
        List<GroupList> groupLists = groupListRepository.findGroupListByTypeAndId(type, BearerContextHolder.getContext().getUserId());
        return groupLists.stream().map(groupList -> groupListMapper.copyEntityToDto(groupList, new GroupListDto())).collect(Collectors.toList());
    }

    /**
     * @param group_id
     * @param user_id
     * @param lastMessage
     * @param timestamp
     * @return
     */
    @Override
    public GroupListDto updateGroup(UUID group_id, UUID user_id, String lastMessage, Timestamp timestamp) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(group_id);
        if(!groupLists.isEmpty()) {
            String member = groupLists.get(0).getMember();
            if(member.contains(user_id.toString())) {
                groupLists.get(0).setLastMessage(lastMessage);
                groupLists.get(0).setLastMessageAt(timestamp);
                groupListRepository.save(groupLists.get(0));
                return groupListMapper.copyEntityToDto(groupLists.get(0), new GroupListDto());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param group_id
     * @param user_id
     * @return
     */
    @Override
    public Boolean deleteGroup(UUID group_id, UUID user_id) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(group_id);
        if(!groupLists.isEmpty()) {
            GroupList groupList = groupLists.get(0);
            if(groupList.getCreatedBy().equals(user_id)) {
                groupListRepository.delete(groupList);
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    /**
     * @param group_id
     * @param user_id
     * @param name
     * @return
     */
    @Override
    public GroupListDto renameGroup(UUID group_id, String user_id, String name) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(group_id);
        if(!groupLists.isEmpty()) {
            GroupList groupList = groupLists.get(0);
            String member = groupList.getMember();
            if(member.contains(user_id)) {
                groupList.setGroupName(name);
                groupListRepository.save(groupList);
                return groupListMapper.copyEntityToDto(groupList, new GroupListDto());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param groupId
     * @return
     */
    @Override
    public List<Object> getMemberOfGroupById(UUID groupId, UUID userId) {
        try {
            List<GroupList> groupLists = groupListRepository.findGroupListById(groupId);
            if (!groupLists.isEmpty()) {
                GroupListDto groupListDto = new GroupListDto();
                groupListDto = groupListMapper.copyEntityToDto(groupLists.get(0), groupListDto);
                List<String> memberList = groupListDto.getMember();
                if (memberList == null) {
                    return null;
                } else {
                    if(memberList.contains(BearerContextHolder.getContext().getUserId())) {
                        List<Object> dataMember = new ArrayList<>();
                        for (String member : memberList) {
                            try {
                                OwnerUserDto ownerUserDto = ownerUserService.getOwnerUserById(UUID.fromString(member));
                                dataMember.add(ownerUserDto);
                            } catch (Exception e) {
                                try {
                                    UserReqDto user = userService.getUserById(UUID.fromString(member));
                                    dataMember.add(user);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                        return dataMember;
                    } else {
                        return null;
                    }
                }
            } else {
                return null;
            }
        } catch (Exception exception) {
            return null;
        }
    }

    /**
     * @param ids
     * @param userId
     * @param groupId
     * @return
     */
    @Override
    public GroupListDto addMemberToGroup(List<UUID> ids, UUID userId, String groupId) {
        try {
            List<GroupList> groupLists = groupListRepository.findGroupListById(UUID.fromString(groupId));
            if (!groupLists.isEmpty()) {
                GroupList groupList = groupLists.get(0);
                GroupListDto groupListDto = new GroupListDto();
                groupListDto = groupListMapper.copyEntityToDto(groupList, groupListDto);
                List<String> member = groupListDto.getMember();
                if (groupListDto.getCreatedBy().equals(userId)) {
                    List<String> idsStr = ids.stream().map(UUID::toString).collect(Collectors.toList());
                    member.addAll(idsStr);
                    groupListDto.setMember(member);
                    GroupList groupList1 = new GroupList();
                    groupList1 = groupListMapper.copyDtoToEntity(groupListDto, groupList1);
                    groupListRepository.save(groupList1);
                    return groupListDto;
                } else {
                    if (member == null) {
                        return null;
                    } else if (member.contains(userId.toString())) {
                        List<String> idsStr = ids.stream().map(UUID::toString).collect(Collectors.toList());
                        member.addAll(idsStr);
                        groupListDto.setMember(member);
                        GroupList groupList1 = new GroupList();
                        groupList1 = groupListMapper.copyDtoToEntity(groupListDto, groupList1);
                        groupListRepository.save(groupList1);
                        return groupListDto;
                    } else {
                        return null;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param id
     * @param userId
     * @param groupId
     * @return
     */
    @Override
    public GroupListDto deleteMember(UUID id, UUID userId, UUID groupId) {
        try {
            List<GroupList> groupLists = groupListRepository.findGroupListById(groupId);
            if (!groupLists.isEmpty()) {
                GroupList groupList = groupLists.get(0);
                GroupListDto groupListDto = new GroupListDto();
                groupListDto = groupListMapper.copyEntityToDto(groupList, groupListDto);
                List<String> member = groupListDto.getMember();
                if (groupListDto.getCreatedBy().equals(userId)) {
                    member.remove(id.toString());
                    groupListDto.setMember(member);
                    GroupList groupList1 = new GroupList();
                    groupList1 = groupListMapper.copyDtoToEntity(groupListDto, groupList1);
                    groupListRepository.save(groupList1);
                    return groupListDto;
                } else {
                    return null;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

}
