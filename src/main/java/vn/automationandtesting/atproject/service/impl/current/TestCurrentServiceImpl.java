package vn.automationandtesting.atproject.service.impl.current;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.current.TestCurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.current.TestCurrentMapper;
import vn.automationandtesting.atproject.entity.current.TestsCurrent;
import vn.automationandtesting.atproject.repository.current.TestCurrentRepository;
import vn.automationandtesting.atproject.service.current.TestCurrentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestCurrentServiceImpl implements TestCurrentService {

    @Autowired
    private TestCurrentRepository repository;

    @Autowired
    private TestCurrentMapper mapper;


    @Override
    public List<TestCurrentDto> findAllTestByJobId(UUID job_id) {
        List<TestsCurrent> tests = repository.findAllTestByJobId(job_id);
        List<TestCurrentDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsCurrent item : tests) {
                TestCurrentDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCurrentDto> findTestById(UUID id) {
        List<TestsCurrent> tests = repository.findTestById(id);
        List<TestCurrentDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsCurrent item : tests) {
                TestCurrentDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCurrentDto> findTestByType(UUID type_id) {
        List<TestsCurrent> tests = repository.findTestByType(type_id);
        List<TestCurrentDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsCurrent item : tests) {
                TestCurrentDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCurrentDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestsCurrent> tests = repository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestCurrentDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsCurrent item : tests) {
                TestCurrentDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestsCurrent> tests) {
        repository.saveAll(tests);
    }

    @Override
    public void deleteAll(List<TestsCurrent> tests) {
        repository.deleteAll(tests);
    }
}
