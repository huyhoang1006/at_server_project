package vn.automationandtesting.atproject.service.impl.power;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.power.PowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.repository.power.PowerRepository;
import vn.automationandtesting.atproject.service.power.PowerCableService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PowerCableServiceImpl implements PowerCableService {

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private PowerRepository repository;

    @Autowired
    private PowerCableMapper mapper;


    @Override
    public List<PowerCableDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<PowerCable> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<PowerCable> powerCableList = repository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<PowerCableDto> powerCableDtoList = new ArrayList<>();
        if(!powerCableList.isEmpty()) {
            for(PowerCable powerCable : powerCableList) {
                PowerCableDto powerCableDto = mapper.assetToAssetDto(powerCable);
                powerCableDtoList.add(powerCableDto);
            }
        }
        return powerCableDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return repository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<PowerCableDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<PowerCable> assets = repository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<PowerCableDto> assetDtoList = new ArrayList<>();
        for(PowerCable asset : assets) {
            PowerCableDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<PowerCableDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<PowerCable> assets = repository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<PowerCableDto> assetDtoList = new ArrayList<>();
        for(PowerCable asset : assets) {
            PowerCableDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = repository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    @Override
    public List<PowerCableDto> findAssetById(UUID id) {
        List<PowerCable> list = repository.findAssetById(id);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findBySerial(String serial) {
        List<PowerCable> list = repository.findBySerial(serial);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<PowerCable> list = repository.findBySerialAndLocation(serial, location_id);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findAll() {
        List<PowerCable> list = repository.findAll();
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<PowerCable> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<PowerCable> list) {
        repository.saveAll(list);
    }

    /**
     * @param powerCableDto
     * @param id
     * @return
     */
    @Override
    public PowerCableDto updateAssetLite(PowerCableDto powerCableDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            PowerCable asset = repository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            PowerCable temp = mapper.copyAssetDtoToAssetLite(powerCableDto, asset, acceptProperties);
            repository.save(temp);
            return powerCableDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteAll(List<PowerCable> list) {
        repository.deleteAll(list);
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public PowerCableDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            PowerCable asset = repository.findAssetById(asset_id, ids, user_id);
            return mapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }
}
