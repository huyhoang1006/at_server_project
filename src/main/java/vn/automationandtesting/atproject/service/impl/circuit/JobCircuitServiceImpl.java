package vn.automationandtesting.atproject.service.impl.circuit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.JobDto;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.JobCircuitMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;
import vn.automationandtesting.atproject.repository.circuit.JobCircuitRepository;
import vn.automationandtesting.atproject.service.circuit.JobCircuitService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobCircuitServiceImpl implements JobCircuitService {

    @Autowired
    private JobCircuitRepository jobCircuitRepository;

    @Autowired
    private JobCircuitMapper jobCircuitMapper;

    @Override
    public List<JobCircuitDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Jobdata> jobs = jobCircuitRepository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCircuitDto> findJobById(UUID id) {
        List<Jobdata> jobs = jobCircuitRepository.findJobById(id);
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCircuitDto> findJobByName(String name) {
        List<Jobdata> jobs = jobCircuitRepository.findJobByName(name);
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCircuitDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<Jobdata> jobs = jobCircuitRepository.findJobByNameAndAsset(name, asset_id);
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCircuitDto> findAll() {
        List<Jobdata> jobs = jobCircuitRepository.findAll();
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCircuitDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<Jobdata> jobs = jobCircuitRepository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobCircuitDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(Jobdata item : jobs) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<Jobdata> jobdataList) {
        jobCircuitRepository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<Jobdata> jobdataList) {
        jobCircuitRepository.deleteAll(jobdataList);
    }

    /**
     * @param asset_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<JobCircuitDto> findJobByAssetId(UUID asset_id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Jobdata> jobList = jobCircuitRepository.findJobByAssetId(asset_id, user_id, userId, first, limit );
        List<JobCircuitDto> jobDtoList = new ArrayList<>();
        if(!jobList.isEmpty()) {
            for(Jobdata job : jobList) {
                JobCircuitDto jobDto = jobCircuitMapper.JobToJobDto(job);
                jobDtoList.add(jobDto);
            }
        }
        return jobDtoList;
    }

    /**
     * @param asset_id
     * @return
     */
    @Override
    public int countJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = jobCircuitRepository.countJobByAssetId(asset_id, user_id, userId);
        return count;
    }
}
