package vn.automationandtesting.atproject.service.impl.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.DisconnectorMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.owner.OwnerMapper;
import vn.automationandtesting.atproject.controller.dto.owner.OwnerDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.entity.owner.Owner;
import vn.automationandtesting.atproject.exception.LocationNotFoundException;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.RoleRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.repository.owner.OwnerRepository;
import vn.automationandtesting.atproject.service.OwnerUserService;
import vn.automationandtesting.atproject.service.owner.OwnerService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OwnerServiceImpl implements OwnerService {
    @Autowired
    private SharedListIdService sharedListIdService;
    @Autowired
    private OwnerRepository repository;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private OwnerMapper mapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OwnerUserService ownerUserService;

    @Override
    public OwnerDto findById(UUID ownerId) {
        Owner owner = repository.findById(ownerId).orElseThrow(() -> new IllegalArgumentException("Location not found."));
        return mapper.assetToAssetDto(owner);
    }

    @Override
    public List<OwnerDto> findAll(UUID userId) {
        boolean check = true;
        List<User> users = userRepository.findAllUserById(userId);
        List<User> dataUser = users;
        while (check) {
            List<User> data = new ArrayList<>();
            for(User user : dataUser) {
                if(!user.getRole().toString().equals("OWNER5")) {
                    List<User> usersTemp = userRepository.findAllUserByCreated(user.getId());
                    data.addAll(usersTemp);
                }
            }

            if(data.isEmpty()) {
                check = false;
            } else {
                dataUser = data;
                users.addAll(data);
            }
        }

        List<OwnerDto> ownerDtos = new ArrayList<OwnerDto>();
        List<Owner> owners = new ArrayList<Owner>();

        for(User user : users) {
            List<Owner> owners1 = repository.findByCreated(user.getId());
            owners.addAll(owners1);
        }
        for(Owner owner : owners) {
            OwnerDto ownerDto = mapper.assetToAssetDto(owner);
            ownerDto.setIsDeleted(owner.getIsDeleted());
            ownerDto.setCreated_by(owner.getCreatedBy());
            ownerDtos.add(ownerDto);
        }
        return ownerDtos;
    }

    @Override
    public List<Owner> saveOwner(List<OwnerDto> ownerDtoList) {
        try {
            List<Owner> owners = new ArrayList<>();
            for (OwnerDto ownerDto : ownerDtoList) {
                Owner owner = mapper.assetDtoToAsset(ownerDto);
                int roleCurrent = RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
                int roleSave = RoleEnum.getIntOfRole(owner.getMode());
                if(roleCurrent <= roleSave) {
                    if(roleSave == 2) {
                        Owner root  = repository.findByUserIdAndName(UUID.fromString("00000000-0000-0000-0000-000000000000"), "root");
                        owner.setMrid(UUID.randomUUID());
                        owner.setRef_id(root.getMrid());
                        owner.setUser_id(UUID.fromString(BearerContextHolder.getContext().getUserId()));
                        owner.setPre("1");
                        owners.add(owner);
                    } else {
                        owner.setMrid(UUID.randomUUID());
                        owner.setUser_id(UUID.fromString(BearerContextHolder.getContext().getUserId()));
                        UUID ref_id = owner.getRef_id();
                        Owner ownerParent = repository.findByMrid(ref_id);
                        UUID idParent = ownerParent.getCreatedBy();
                        if(!idParent.toString().equals(BearerContextHolder.getContext().getUserId())) {
                            owner.setPre("1");
                        } else {
                            owner.setPre("");
                        }
                        owners.add(owner);
                    }
                } else {
                    return new ArrayList<>();
                }
            }
            repository.saveAll(owners);
            return owners;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Owner> getAllOwnerAndParent(UUID id) {
        List<Owner> owners = repository.findByCreated(id);
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        int roleCurrent = RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
        if(roleCurrent != 1) {
            User userParent = userRepository.findById(user.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("User not found"));
            List<Owner> ownersParent = repository.findByCreated(userParent.getId());
            owners.addAll(ownersParent);
        }
        return owners;
    }

    @Override
    public Boolean removeOwner(OwnerDto ownerDto) {
        int countByRef = repository.countByRef(ownerDto.getId());
        if(countByRef == 0) {
            Owner owner = mapper.assetDtoToAsset(ownerDto);
            try {
                repository.delete(owner);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Owner updateOwner(OwnerDto ownerDto) {
        Owner data = repository.findByMrid(ownerDto.getId());
        Owner owner = mapper.updateAssetDtoToAsset(ownerDto, data);
        repository.save(owner);
        processPreOwner(owner.getCreatedBy());
        return owner;
    }

    @Override
    public Owner deleteOwner(OwnerDto ownerDto, Boolean sign) {
        Owner data = repository.findByMrid(ownerDto.getId());
        data.setIsDeleted(sign);
        repository.save(data);
        return data;
    }

    @Override
    public List<Object> findOwnerParentById(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        User userParent = userRepository.findById(user.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("User not found"));
        return Collections.singletonList(repository.findByCreated(userParent.getId()));
    }

    @Override
    public List<Object> findOwnerParentById(UUID id, String mode, int first, int limit) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        User userParent = userRepository.findById(user.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("User not found"));
        return Collections.singletonList(repository.findByCreated(userParent.getId(), mode, first, limit));
    }

    public List<Owner> findOwnerParentAndPreById(UUID id, String pre, int first, int limit) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        User userParent = userRepository.findById(user.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("User not found"));
        return repository.findByCreatedAndPre(userParent.getId(), pre, first, limit);
    }

    public void processPreOwner(UUID id) {
        List<Owner> owners = repository.findByCreated(id);
        for(Owner owner : owners) {
            UUID ref_id = owner.getRef_id();
            Owner OwnerParent = repository.findByMrid(ref_id);
            if(OwnerParent != null) {
                if (OwnerParent.getCreatedBy().toString().equals(id.toString())) {
                    if (owner.getPre().equals("1")) {
                        owner.setPre("");
                        repository.save(owner);
                    }
                } else {
                    if (owner.getPre().isEmpty() || owner.getPre().equals("null")) {
                        owner.setPre("1");
                        repository.save(owner);
                    }
                }
            }
        }
    }

    @Override
    public void changeOwner(UUID owner_id, UUID user_id) {
        Owner owner = repository.findByMrid(owner_id);
        User user = userRepository.findById(user_id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        if(RoleEnum.getIntOfRole(owner.getMode()) >= RoleEnum.getIntOfRole(user.getRole().toString())) {
            UUID ref_owner_id = owner.getRef_id();
            Owner ownerParent = repository.findByMrid(ref_owner_id);
            if(ownerParent != null) {
                User UserParent = userRepository.findById(user.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("User not found"));
                if (ownerParent.getCreatedBy().equals(user.getId()) || ownerParent.getCreatedBy().equals(UserParent.getId())) {
                    if (ownerParent.getCreatedBy().equals(UserParent.getId())) {
                        owner.setPre("1");
                    }
                    owner.setUser_id(user_id);
                    owner.setCreatedBy(user_id);
                    repository.save(owner);
                    processPreOwner(user_id);
                }
            } else {
                owner.setUser_id(user_id);
                owner.setCreatedBy(user_id);
                repository.save(owner);
                processPreOwner(user_id);
            }

        }

    }

    /**
     * @param id
     * @param pre
     * @return
     */
    @Override
    public int countByCreatedAndPre(UUID id, String pre) {
        OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Owner not found"));
        int count = repository.countByCreatedAndPre(ownerUser.getId(), pre);
        return count;
    }

    /**
     * @param ref_id
     * @param user_id
     * @return
     */
    @Override
    public int countByRef(UUID ref_id, UUID user_id) {
        User user = userRepository.findById(user_id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        int count = repository.countByRef(ref_id, user.getCreatedBy());
        return count;
    }

    /**
     * @param ref_id
     * @param user_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<Owner> getOwnerByRefAndCreated(UUID ref_id, UUID user_id, int first, int limit) {
        User user = userRepository.findById(user_id).orElseThrow(() -> new IllegalArgumentException("User not found"));
        return repository.getOwnerByRefAndCreated(ref_id, user.getCreatedBy(), first, limit);
    }

    /**
     * @param created_by
     * @return
     */
    @Override
    public int countByCreated(UUID created_by) {
        return repository.countByCreated(created_by);
    }

    /**
     * @param created_by
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerDto> findByCreated(UUID created_by, int first, int limit) {
        List<Owner> ownerList = repository.findByCreated(created_by, first, limit);
        if(!ownerList.isEmpty()) {
            return ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    /**
     * @param ref_id
     * @return
     */
    @Override
    public int countByRefId(UUID ref_id) {
        return repository.countByRef(ref_id);
    }

    /**
     * @param ref_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerDto> findByRef(UUID ref_id, int first, int limit) {
        List<Owner> ownerList = repository.findByRef(ref_id, first, limit);
        if(!ownerList.isEmpty()) {
            return ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    /**
     * @param created_by
     * @param first
     * @param limit
     * @param pre
     * @return
     */
    @Override
    public List<OwnerDto> findByCreatedAndPre(UUID created_by, int first, int limit, String pre) {
        List<Owner> ownerList = repository.findByCreatedAndPre(created_by, pre, first, limit);
        return ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
    }

    /**
     * @param id
     * @param role
     * @return
     */
    @Override
    public List<OwnerDto> findOwnerParent(UUID id, String role) {
        int roleRequest = RoleEnum.getIntOfRole(role);
        int roleOwner = RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
        int roleNumberParent = RoleEnum.getIntOfRole(role) - 1;
        String roleParent = RoleEnum.getRoleOfString(roleNumberParent);
        if(roleOwner < roleRequest) {
            List<UUID> ids = ownerUserService.getAllCreatedIdByRoleAndCreated(id.toString(), role);
            OwnerUser ownerUser = ownerUserRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Owner not found"));
            OwnerUser ownerUserParent = ownerUserRepository.findById(ownerUser.getCreatedBy()).orElseThrow(() -> new IllegalArgumentException("Owner parent not found"));
            ids.add(ownerUserParent.getId());
            List<Owner> ownerList = repository.getAllParentByRole(ids, roleParent);
            return ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
        } else {
            throw new RuntimeException();
        }

    }

    /**
     * @param ownerDto
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerDto> getDataByIncludes(OwnerDto ownerDto, int first, int limit) {
        List<UUID> ids = ownerUserService.getAllCreatedIdByRoleAndCreated(BearerContextHolder.getContext().getUserId(), "OWNER5");
        List<Owner> ownerList = repository.findAllOwnerByField(ownerDto, first, limit, ids);
        return ownerList.stream().map(owner ->
                mapper.assetToAssetDto(owner)
        ).collect(Collectors.toList());
    }

    /**
     * @param ownerDto
     * @return
     */
    @Override
    public Long countAllOwnerUserByField(OwnerDto ownerDto) {
        List<UUID> ids = ownerUserService.getAllCreatedIdByRoleAndCreated(BearerContextHolder.getContext().getUserId(), "OWNER5");
        Long count = repository.countAllOwnerByField(ownerDto, ids);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAllOwnerByIds() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        int count = repository.countAllOwnerByIds(ids);
        return count;
    }

    /**
     * @param mode
     * @return
     */
    @Override
    public int countAllOwnerByIdsAndMode(String mode) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        int count = repository.countAllOwnerByIdsAndMode(ids, mode);
        return count;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public List<OwnerDto> getOwnerByParentId(UUID id) {
        List<Owner> ownerList = repository.getOwnerByParentId(id);
        if(!ownerList.isEmpty()) {
            List<OwnerDto> ownerDtoList = ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
            return ownerDtoList;
        }
        return null;
    }


    /**
     * @param role
     * @return
     */
    @Override
    public int countOwnerByRole(String role) {
        int count = repository.countOwnerByRole(role);
        return count;
    }

    /**
     * @param role
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<OwnerDto> getOwnerByRole(String role, int first, int limit) {
        List<Owner> ownerList = repository.getOwnerByRole(role, first, limit);
        if(!ownerList.isEmpty()) {
            List<OwnerDto> ownerDtoList = ownerList.stream().map(owner -> mapper.assetToAssetDto(owner)).collect(Collectors.toList());
            return ownerDtoList;
        }
        return null;
    }

}
