package vn.automationandtesting.atproject.service.impl.current;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.current.CurrentMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.repository.current.CurrentRepository;
import vn.automationandtesting.atproject.service.current.CurrentService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CurrentServiceImpl implements CurrentService {

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private CurrentRepository repository;

    @Autowired
    private CurrentMapper mapper;


    @Override
    public List<CurrentDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Current> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Current> currentList = repository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<CurrentDto> currentDtoList = new ArrayList<>();
        if(!currentList.isEmpty()) {
            for(Current current : currentList) {
                CurrentDto currentDto = mapper.assetToAssetDto(current);
                currentDtoList.add(currentDto);
            }
        }
        return currentDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return repository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<CurrentDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Current> assets = repository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<CurrentDto> assetDtoList = new ArrayList<>();
        for(Current asset : assets) {
            CurrentDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<CurrentDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Current> assets = repository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<CurrentDto> assetDtoList = new ArrayList<>();
        for(Current asset : assets) {
            CurrentDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = repository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }


    @Override
    public List<CurrentDto> findAssetById(UUID id) {
        List<Current> list = repository.findAssetById(id);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findBySerial(String serial) {
        List<Current> list = repository.findBySerial(serial);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Current> list = repository.findBySerialAndLocation(serial, location_id);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findAll() {
        List<Current> list = repository.findAll();
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Current> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Current> list) {
        repository.saveAll(list);
    }

    /**
     * @param currentDto
     * @param id
     * @return
     */
    @Override
    public CurrentDto updateAssetLite(CurrentDto currentDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Current asset = repository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Current temp = mapper.copyAssetDtoToAssetLite(currentDto, asset, acceptProperties);
            repository.save(temp);
            return currentDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteAll(List<Current> list) {
        repository.deleteAll(list);
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public CurrentDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Current asset = repository.findAssetById(asset_id, ids, user_id);
            return mapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }
}
