package vn.automationandtesting.atproject.service.impl.circuit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.CircuitMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.repository.circuit.CircuitRepository;
import vn.automationandtesting.atproject.service.circuit.CircuitService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CircuitServiceImpl implements CircuitService {

    @Autowired
    private CircuitRepository circuitRepository;

    @Autowired
    private CircuitMapper circuitMapper;

    @Autowired
    private SharedListIdService sharedListIdService;

    @Override
    public List<CircuitDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Circuit> circuitList = circuitRepository.findAllByLocationId(location_id, user_id, userId);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    /**
     * @param id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<CircuitDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Circuit> circuitList = circuitRepository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    /**
     * @param locationId
     * @return
     */
    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = circuitRepository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    @Override
    public List<CircuitDto> findAssetById(UUID id) {
        List<Circuit> circuitList = circuitRepository.findAssetById(id);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findBySerial(String serial) {
        List<Circuit> circuitList = circuitRepository.findBySerial(serial);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    /**
     * @param circuitDto
     * @param id
     * @return
     */
    @Override
    public CircuitDto updateAssetLite(CircuitDto circuitDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Circuit asset = circuitRepository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Circuit temp = circuitMapper.copyAssetDtoToAssetLite(circuitDto, asset, acceptProperties);
            circuitRepository.save(temp);
            return circuitDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<CircuitDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Circuit> circuitList = circuitRepository.findBySerialAndLocation(serial, location_id);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findAll() {
        List<Circuit> circuitList = circuitRepository.findAll();
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Circuit> circuitList = circuitRepository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public void saveAll(List<Circuit> circuits) {
        circuitRepository.saveAll(circuits);
    }

    @Override
    public void deleteAll(List<Circuit> circuits) {
        circuitRepository.deleteAll(circuits);
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return circuitRepository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<CircuitDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Circuit> assets = circuitRepository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<CircuitDto> assetDtoList = new ArrayList<>();
        for(Circuit asset : assets) {
            CircuitDto assetDto = circuitMapper.circuitToCircuitDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<CircuitDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Circuit> assets = circuitRepository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<CircuitDto> assetDtoList = new ArrayList<>();
        for(Circuit asset : assets) {
            CircuitDto assetDto = circuitMapper.circuitToCircuitDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = circuitRepository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public CircuitDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Circuit asset = circuitRepository.findAssetById(asset_id, ids, user_id);
            return circuitMapper.circuitToCircuitDto(asset);
        } catch (Exception e) {
            return null;
        }
    }

}
