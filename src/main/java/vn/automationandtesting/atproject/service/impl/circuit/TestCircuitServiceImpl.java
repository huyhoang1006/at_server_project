package vn.automationandtesting.atproject.service.impl.circuit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.circuit.TestCircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.TestCircuitMapper;
import vn.automationandtesting.atproject.entity.circuit.TestCircuit;
import vn.automationandtesting.atproject.repository.circuit.TestCircuitRepository;
import vn.automationandtesting.atproject.service.circuit.TestCircuitService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestCircuitServiceImpl implements TestCircuitService {

    @Autowired
    private TestCircuitRepository testCircuitRepository;

    @Autowired
    private TestCircuitMapper testCircuitMapper;

    @Override
    public List<TestCircuitDto> findAllTestByJobId(UUID job_id) {
        List<TestCircuit> tests = testCircuitRepository.findAllTestByJobId(job_id);
        List<TestCircuitDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestCircuit item : tests) {
                TestCircuitDto testDto = testCircuitMapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCircuitDto> findTestById(UUID id) {
        List<TestCircuit> tests = testCircuitRepository.findTestById(id);
        List<TestCircuitDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestCircuit item : tests) {
                TestCircuitDto testDto = testCircuitMapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCircuitDto> findTestByType(UUID type_id) {
        List<TestCircuit> tests = testCircuitRepository.findTestByType(type_id);
        List<TestCircuitDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestCircuit item : tests) {
                TestCircuitDto testDto = testCircuitMapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestCircuitDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestCircuit> tests = testCircuitRepository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestCircuitDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestCircuit item : tests) {
                TestCircuitDto testDto = testCircuitMapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestCircuit> testCircuits) {
        testCircuitRepository.saveAll(testCircuits);
    }

    @Override
    public void deleteAll(List<TestCircuit> testCircuits) {
        testCircuitRepository.deleteAll(testCircuits);
    }
}
