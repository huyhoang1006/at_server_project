package vn.automationandtesting.atproject.service.impl.surge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.SurgeMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.repository.surge.SurgeRepository;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;
import vn.automationandtesting.atproject.service.surge.SurgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SurgeServiceImpl implements SurgeService {
    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private SurgeRepository repository;

    @Autowired
    private SurgeMapper mapper;


    @Override
    public List<SurgeDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Surge> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Surge> surgeList = repository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<SurgeDto> surgeDtoList = new ArrayList<>();
        if(!surgeList.isEmpty()) {
            for(Surge surge : surgeList) {
                SurgeDto surgeDto = mapper.assetToAssetDto(surge);
                surgeDtoList.add(surgeDto);
            }
        }
        return surgeDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return repository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<SurgeDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Surge> assets = repository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<SurgeDto> assetDtoList = new ArrayList<>();
        for(Surge asset : assets) {
            SurgeDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<SurgeDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Surge> assets = repository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<SurgeDto> assetDtoList = new ArrayList<>();
        for(Surge asset : assets) {
            SurgeDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = repository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    @Override
    public List<SurgeDto> findAssetById(UUID id) {
        List<Surge> list = repository.findAssetById(id);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findBySerial(String serial) {
        List<Surge> list = repository.findBySerial(serial);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Surge> list = repository.findBySerialAndLocation(serial, location_id);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findAll() {
        List<Surge> list = repository.findAll();
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Surge> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Surge> list) {
        repository.saveAll(list);
    }

    /**
     * @param surgeDto
     * @param id
     * @return
     */
    @Override
    public SurgeDto updateAssetLite(SurgeDto surgeDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Surge asset = repository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Surge temp = mapper.copyAssetDtoToAssetLite(surgeDto, asset, acceptProperties);
            repository.save(temp);
            return surgeDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteAll(List<Surge> list) {
        repository.deleteAll(list);
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public SurgeDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Surge asset = repository.findAssetById(asset_id, ids, user_id);
            return mapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }
}
