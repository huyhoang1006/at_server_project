package vn.automationandtesting.atproject.service.impl.fileUpload;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.service.fileUpload.FileUploadService;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    String Working_Directory = System.getProperty("user.dir");
    String pathData = Working_Directory + FileSystems.getDefault().getSeparator() + "FileUpload";

    /**
     * @param files
     * @param deleteFile
     * @param uuid
     */
    @Override
    public boolean updateFile(MultipartFile[] files, List<String> deleteFile, String uuid) {
        int indexDelete = 0;
        try {
            if(deleteFile != null) {
                for (String nameFile : deleteFile) {
                    Files.deleteIfExists(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + nameFile));
                    indexDelete = indexDelete + 1;
                }
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return false;
                    }
                }
            } else {
                int index = 0;
                if(files != null) {
                    try {
                        Path path = Paths.get(pathData + System.getProperty("file.separator") + uuid);
                        if(!Files.exists(path) || !Files.isDirectory(path)) {
                            File theDir = new File(String.valueOf(path));
                            theDir.mkdirs();
                        }
                        for (MultipartFile file : files) {
                            byte[] bytes = file.getBytes();
                            Files.write(Paths.get(pathData + System.getProperty("file.separator") + uuid + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
                            index = index + 1;
                        }
                    } catch (Exception ex) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
