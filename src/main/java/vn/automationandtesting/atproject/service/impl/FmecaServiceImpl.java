package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.FmecaDto;
import vn.automationandtesting.atproject.controller.dto.mapper.FmecaMapper;
import vn.automationandtesting.atproject.entity.Fmeca;
import vn.automationandtesting.atproject.exception.FmecaNotFoundException;
import vn.automationandtesting.atproject.repository.FmecaRepository;
import vn.automationandtesting.atproject.service.FmecaService;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@Service
public class FmecaServiceImpl implements FmecaService {
    @Autowired
    private FmecaRepository fmecaRepository;
    @Autowired
    private FmecaMapper fmecaMapper;

    @Override
    public FmecaDto createFmeca(FmecaDto fmecaDto) {
        Fmeca fmeca = new Fmeca();
        BeanUtils.copyProperties(fmecaDto, fmeca);
        if (fmeca.getId() == null) {
            fmeca.setId(UUID.randomUUID());
        }
        Fmeca savedFmeca = fmecaRepository.save(fmeca);
        fmecaDto.setId(savedFmeca.getId());
        return fmecaDto;
    }

    @Override
    public FmecaDto updateFmeca(UUID id, FmecaDto fmecaDto) {
        Fmeca savedFmeca = fmecaRepository.findByIdAndIsDeleted(id, false)
                .orElseThrow(() -> new FmecaNotFoundException("Resource not found."));
        savedFmeca.setTableCalculate(fmecaDto.getTable_calculate());
        savedFmeca.setTableFmeca(fmecaDto.getTable_fmeca());
        savedFmeca.setTotal(fmecaDto.getTotal());
        Fmeca updatedFmeca = fmecaRepository.save(savedFmeca);
        fmecaDto.setId(updatedFmeca.getId());
        return fmecaDto;
    }

    @Override
    public Fmeca getFmecaTableById(UUID fmecaId) {
        return fmecaRepository.findByIdAndIsDeleted(fmecaId, false)
                .orElseThrow(() -> new FmecaNotFoundException("Resource not found"));
    }

    @Override
    public ResponseObject getOrInit() {
        List<Fmeca> listFmeca = fmecaRepository.findAll();
        if (listFmeca.size() == 0) {
            Fmeca initFmeca = Fmeca.initFmeca();
            return ResponseObject.returnWithData(fmecaMapper.fmceToFmceDto(fmecaRepository.save(initFmeca)) );
        } else {
            return ResponseObject.returnWithData( fmecaMapper.fmceToFmceDto(listFmeca.get(0)));
        }
    }
}
