package vn.automationandtesting.atproject.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.constant.Constants;
import vn.automationandtesting.atproject.controller.dto.*;
import vn.automationandtesting.atproject.controller.dto.cim.*;
import vn.automationandtesting.atproject.controller.dto.mapper.*;
import vn.automationandtesting.atproject.entity.Fmeca;
import vn.automationandtesting.atproject.entity.Monitoring;
import vn.automationandtesting.atproject.entity.cim.*;
import vn.automationandtesting.atproject.entity.enumm.TestTypeEnum;
import vn.automationandtesting.atproject.exception.AssetNotFoundException;
import vn.automationandtesting.atproject.exception.InvalidTestException;
import vn.automationandtesting.atproject.exception.JobNotFoundException;
import vn.automationandtesting.atproject.exception.LocationLockedException;
import vn.automationandtesting.atproject.repository.*;
import vn.automationandtesting.atproject.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author tund on 5/9/2022
 * @project at-project-server
 */

@Service
@Slf4j
public class JobServiceImpl implements JobService {

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private TestMapper testMapper;
    @Autowired
    private AssetMapper assetMapper;
    @Autowired
    private LocationMapper locationMapper;
    @Autowired
    private AssetRepository assetRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private LocationService locationService;
    @Autowired
    private AssetService assetService;
    @Autowired
    private TestService testService;
    @Autowired
    private TestTypeService testTypeService;
    @Autowired
    private TapChangerMapper tapChangerMapper;

    @Autowired
    private BushingMapper bushingMapper;
    @Autowired
    private TapChangerRepository tapChangerRepository;
    @Autowired
    private BushingRepository bushingRepository;
    @Autowired
    private FmecaRepository fmecaRepository;

    @Autowired
    private MonitoringRepository monitoringRepository;


    @Override
    public JobDto createNewJob(JobDto jobDto) {
        Asset asset = assetRepository.findByMridAndIsDeleted(jobDto.getAsset_id(), false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        Job job = jobMapper.jobDtoToJob(jobDto);
        if (jobDto.getId() == null) {
            job.setId(UUID.randomUUID());
        } else {
            job.setId(jobDto.getId());
        }
        job.setAsset(asset);
        job = jobRepository.save(job);
        return jobMapper.jobToJobDto(job);
    }

    @Override
    public JobDto updateJob(JobDto jobDto, UUID id) {
        String userId = BearerContextHolder.getContext().getUserId();
        Job job = getJobByIdAndLoggedInUserID(id, UUID.fromString(userId));

        if (job.isLocked()) {
            if (!job.getLocked_by().toString().equals(userId)) {
                throw new LocationLockedException("Location already locked by another user.");
            }
        } else {
            if (jobDto.isLocked()) {
                job.setLocked_by(UUID.fromString(userId));
            }
        }
        job = jobMapper.copyJobDtoToJob(jobDto, job, "id", "asset_id", "collabs");
        Job updateJob = jobRepository.save(job);
        return jobMapper.jobToJobDto(updateJob);
    }

    @Override
    public void deleteJob(UUID id) {
        String userId = BearerContextHolder.getContext().getUserId();
        Job job = getJobByIdAndLoggedInUserID(id, UUID.fromString(userId));
        // Không lock mới được phép xóa
        if (!job.isLocked()) {
            jobRepository.delete(job);
        } else {
            throw new LocationLockedException("Please unlock job");
        }
    }

    @Override
    public List<JobDto> getAllJobs() {
        List<JobDto> jobDtoList = new ArrayList<>();
        List<Job> jobList = jobRepository.findAllByIsDeleted(false);

        String currentUserId = BearerContextHolder.getContext().getUserId();
        List<Job> jobs = jobList.stream()
                .filter(job -> job.getCreatedBy().toString().equals(currentUserId) || job.getCollabs().contains(currentUserId))
                .collect(Collectors.toList());

        for (Job job : jobs) {
            JobDto jobDto = jobMapper.jobToJobDto(job);
            jobDtoList.add(jobDto);
        }
        return jobDtoList;
    }

    @Override
    public List<JobDto> findJobsByAssetSerialNo(String assetSerialNo) {
        List<JobDto> jobDtoList = new ArrayList<>();
        List<Job> jobList = jobRepository.findByAssetSerialNo(assetSerialNo, false);

        String currentUserId = BearerContextHolder.getContext().getUserId();
        List<Job> jobs = jobList.stream()
                .filter(job -> job.getCreatedBy().toString().equals(currentUserId) || job.getCollabs().contains(currentUserId))
                .collect(Collectors.toList());

        for (Job job : jobs) {
            JobDto jobDto = jobMapper.jobToJobDto(job);
            jobDtoList.add(jobDto);
        }
        return jobDtoList;
    }

    @Override
    public List<JobDto> findJobsByAssetId(UUID id) {
        List<JobDto> jobDtoList = new ArrayList<>();
        List<Job> jobList = jobRepository.findByAssetId(id, false);

        String currentUserId = BearerContextHolder.getContext().getUserId();
        List<Job> jobs = jobList.stream()
                .filter(job -> job.getCreatedBy().toString().equals(currentUserId) || job.getCollabs().contains(currentUserId))
                .collect(Collectors.toList());

        for (Job job : jobs) {
            JobDto jobDto = jobMapper.jobToJobDto(job);
            jobDtoList.add(jobDto);
        }
        return jobDtoList;
    }

    @Override
    public List<JobDto> findJobsByName(String jobName) {
        List<JobDto> jobDtoList = new ArrayList<>();
        List<Job> jobList = jobRepository.findAllByNameAndIsDeleted(jobName, false);

        String currentUserId = BearerContextHolder.getContext().getUserId();
        List<Job> jobs = jobList.stream()
                .filter(job -> job.getCreatedBy().toString().equals(currentUserId) || job.getCollabs().contains(currentUserId))
                .collect(Collectors.toList());

        for (Job job : jobs) {
            JobDto jobDto = jobMapper.jobToJobDto(job);
            jobDtoList.add(jobDto);
        }
        return jobDtoList;
    }

    @Override
    public boolean checkJobExisted(UUID id) {
//        Optional<Job> job = jobRepository.findByIdAndIsDeleted(id, false);
        String userId = BearerContextHolder.getContext().getUserId();
        Job job = getJobByIdAndLoggedInUserID(id, UUID.fromString(userId));
        return job != null;
    }

    @Override
    public Job getJobWithHealthIndex(UUID jobId, String fmecaTableCalculate) {
        try {
            Job job = jobRepository.findByIdAndIsDeleted(jobId, false)
                    .orElseThrow(() -> new JobNotFoundException("Job not existed"));
            List<Monitoring> monitoringList = monitoringRepository.findByAssetAndIsDeleted(job.getAsset(), false);
            Monitoring monitoring = null;
            if (monitoringList != null && !monitoringList.isEmpty()) {
                monitoring = monitoringList.get(0);
            }

            float averageHealthIndex = 0;
            float worstHealthIndex = 0;

            JSONObject jsonObject = new JSONObject(fmecaTableCalculate);
            List<Test> tests = job.getTests();

            //Descending sort by created time to get the latest one of the same test of job
            if (tests != null && !tests.isEmpty()) {
                try {
                    tests.sort((o1, o2) -> o2.getCreateAtClient().compareTo(o1.getCreateAtClient()));
                } catch (Exception e) {
                }
                tests = tests.stream().distinct().collect(Collectors.toList());
                tests = tests.stream().distinct().collect(Collectors.toList());
            }

            int bushing_prim_c1 = 0;
            int bushing_prim_c2 = 0;

            int dc_winding_prim = 0;
            int dc_winding_sec = 0;

            for (Test test : tests) {
                if (TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    bushing_prim_c1 = bushing_prim_c1 + 1;
                } else if (TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    bushing_prim_c2 = bushing_prim_c2 + 1;
                } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    dc_winding_prim = dc_winding_prim + 1;
                } else if (TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    dc_winding_sec = dc_winding_sec + 1;
                }
            }


            for (Test test : tests

            ) {
                if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    float weightingFactorC = handleNullValue(jsonObject.getJSONObject(testTypeCMappingToFmecaString(test.getTestType().getCode()))
                            .getString(Constants.WEIGHTING_FACTOR));
                    float weightingFactorDF = handleNullValue(jsonObject.getJSONObject(testTypeDFMappingToFmecaString(test.getTestType().getCode()))
                            .getString(Constants.WEIGHTING_FACTOR));
                    test.setWeightingFactorDF(weightingFactorDF);
                    test.setWeightingFactorC(weightingFactorC);
                    averageHealthIndex += weightingFactorC * test.getAverageScoreC() + weightingFactorDF * test.getAverageScoreDF();
                    worstHealthIndex += weightingFactorC * test.getWorstScoreC() + weightingFactorDF * test.getWorstScoreDF();
                } else if (TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(test.getTestType().getCode())
                        || TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    float weightingFactorC = handleNullValue(jsonObject.getJSONObject(testTypeCMappingToFmecaString(test.getTestType().getCode()))
                            .getString(Constants.WEIGHTING_FACTOR));
                    float weightingFactorDF = handleNullValue(jsonObject.getJSONObject(testTypeDFMappingToFmecaString(test.getTestType().getCode()))
                            .getString(Constants.WEIGHTING_FACTOR));
                    test.setWeightingFactorDF(weightingFactorDF);
                    test.setWeightingFactorC(weightingFactorC);

                    float averageScoreC = test.getAverageScoreC();
                    float averageScoreDF = test.getAverageScoreDF();
                    float worstScoreC = test.getAverageScoreC();
                    float worstScoreDF = test.getAverageScoreDF();

                    // Compare BushingPrimC1 with Monitoring
                    if (monitoring != null
                            && TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(test.getTestType().getCode())
                    ) {
                        float monitoringAverageC = Float.parseFloat(monitoring.getBushing_c_average());
                        float monitoringAverageDF = Float.parseFloat(monitoring.getBushing_df_average());
                        float monitoringWorstC = Float.parseFloat(monitoring.getBushing_c_worst());
                        float monitoringWorstDF = Float.parseFloat(monitoring.getBushing_df_worst());

                        worstScoreC = Math.min(worstScoreC, monitoringWorstC);
                        worstScoreDF = Math.min(worstScoreDF, monitoringWorstDF);
                        averageScoreC = Math.min(averageScoreC, monitoringAverageC);
                        averageScoreDF = Math.min(averageScoreDF, monitoringAverageDF);
                    }

                    if (bushing_prim_c1 > 0 && bushing_prim_c2 > 0) {
                        averageHealthIndex += (weightingFactorC * averageScoreC + weightingFactorDF * averageScoreDF) / 2;
                        worstHealthIndex += (weightingFactorC * worstScoreC + weightingFactorDF * worstScoreDF) / 2;
                    } else {
                        averageHealthIndex += weightingFactorC * averageScoreC + weightingFactorDF * averageScoreDF;
                        worstHealthIndex += weightingFactorC * worstScoreC + weightingFactorDF * worstScoreDF;
                    }

                } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(test.getTestType().getCode())
                        || TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(test.getTestType().getCode())) {
                    if (dc_winding_prim > 0 && dc_winding_sec > 0) {
                        float weightingFactor = handleNullValue(jsonObject.getJSONObject(testTypeMappingToFmecaString(test.getTestType().getCode()))
                                .getString(Constants.WEIGHTING_FACTOR));
                        test.setWeightingFactor(weightingFactor);
                        averageHealthIndex += (weightingFactor * test.getAverageScore()) / 2;
                        worstHealthIndex += (weightingFactor * test.getWorstScore()) / 2;
                    } else {
                        float weightingFactor = handleNullValue(jsonObject.getJSONObject(testTypeMappingToFmecaString(test.getTestType().getCode()))
                                .getString(Constants.WEIGHTING_FACTOR));
                        test.setWeightingFactor(weightingFactor);
                        averageHealthIndex += weightingFactor * test.getAverageScore();
                        worstHealthIndex += weightingFactor * test.getWorstScore();
                    }

                } else {
                    float weightingFactor = handleNullValue(jsonObject.getJSONObject(testTypeMappingToFmecaString(test.getTestType().getCode()))
                            .getString(Constants.WEIGHTING_FACTOR));
                    test.setWeightingFactor(weightingFactor);
                    averageHealthIndex += weightingFactor * test.getAverageScore();
                    worstHealthIndex += weightingFactor * test.getWorstScore();
                }
            }
            job.setAverageHealthIndex(averageHealthIndex);
            job.setWorstHealthIndex(worstHealthIndex);
            return jobRepository.save(job);
        } catch (JSONException e) {
            log.error(e.toString());
            throw new InvalidTestException("Invalid test data");
        }
    }

    private String testTypeCMappingToFmecaString(String testTypeCode) {
        if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(testTypeCode)) {
            return "wc";
        } else if (TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(testTypeCode)
                || TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(testTypeCode)
        ) {
            return "b_C1_c";
        }
        return "wc";
    }

    private String testTypeDFMappingToFmecaString(String testTypeCode) {
        if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(testTypeCode)) {
            return "w_pf_df";
        } else if (TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(testTypeCode)
                || TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(testTypeCode)
        ) {
            return "b_pf_df";
        }
        return "w_pf_df";
    }

    private String testTypeMappingToFmecaString(String testTypeCode) {
        if (TestTypeEnum.DGA.name().equalsIgnoreCase(testTypeCode)) {
            return "dmt";
        } else if (TestTypeEnum.MEASUREMENTOFOIL.name().equalsIgnoreCase(testTypeCode)) {
            return "otmt";
        } else if (TestTypeEnum.INSULATIONRESISTANCE.name().equalsIgnoreCase(testTypeCode)) {
            return "ir";
        } else if (TestTypeEnum.RATIOPRIMSEC.name().equalsIgnoreCase(testTypeCode)) {
            return "rt";
        } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(testTypeCode)
                || TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(testTypeCode)
        ) {
            return "dcwr";
        } else if (TestTypeEnum.EXCITINGCURRENT.name().equalsIgnoreCase(testTypeCode)
        ) {
            return "ec";
        }
        return "dmt";
    }

    @Override
    public void updateJobCollabs(String jobId, ListIdDto listIdDto) {
        String userId = BearerContextHolder.getContext().getUserId();
        Job job = getJobByIdAndLoggedInUserID(UUID.fromString(jobId), UUID.fromString(userId));
        List<String> userIdList = listIdDto.getListId().stream().map(UUID::toString).collect(Collectors.toList());
        String commaSeperatedUserIdString = String.join(",", userIdList);
        job.setCollabs(commaSeperatedUserIdString);
        job.getAsset().setCollabs(job.getAsset().getCollabs() + "," + commaSeperatedUserIdString);
        job.getAsset().getLocation().setCollabs(job.getAsset().getLocation().getCollabs() + "," + commaSeperatedUserIdString);

        jobRepository.save(job);
    }

    /**
     * @param asset_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<JobDto> findJobByAssetId(UUID asset_id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Job> jobList = jobRepository.findJobByAssetId(asset_id, user_id, userId, first, limit );
        List<JobDto> jobDtoList = new ArrayList<>();
        if(!jobList.isEmpty()) {
            for(Job job : jobList) {
                JobDto jobDto = jobMapper.jobToJobDto(job);
                jobDtoList.add(jobDto);
            }
        }
        return jobDtoList;
    }

    /**
     * @param asset_id
     * @return
     */
    @Override
    public int countJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = jobRepository.countJobByAssetId(asset_id, user_id, userId);
        return count;
    }

    private Job getJobByIdAndLoggedInUserID(UUID jobId, UUID loggedInUserId) {
        Optional<Job> job = jobRepository.findByIdAndIsDeletedAndCreatedBy(jobId, false, loggedInUserId);
        if (job.isEmpty()) {
            job = jobRepository.findByIdAndIsDeletedAndCollabsContains(jobId, false, loggedInUserId.toString());
        }
        return job.orElseThrow(() -> new JobNotFoundException("Job not found."));
    }

    @Override
    public ResponseObject lock(Boolean locked, List<UUID> listId) {
        String userID = BearerContextHolder.getContext().getUserId();
        List<Job> jobs = jobRepository.findAllById(listId);
        for (Job job : jobs) {
            if (!(job.isLocked() && !job.getLocked_by().toString().equals(userID))) {
                job.setLocked(locked);
                if (locked) {
                    job.setLocked_by(UUID.fromString(userID));
                }
            }
        }
        jobRepository.saveAll(jobs);

        return ResponseObject.returnWithData(true);
    }

    @Override
    public ResponseObject upload(ResourceFullDto resourceFullDto) {
        List<LocationDto> listLocationDto = resourceFullDto.getListLocation();
        Location newLocation = locationService.insertIfNotExist(listLocationDto.get(0));
        for(int i=1; i < listLocationDto.size(); i ++) {
            locationService.insertIfNotExist(listLocationDto.get(i));
        }

        List<AssetFullDto> listFullAssetDto = resourceFullDto.getListAsset();
        Asset newAsset = assetService.insertIfNotExist(listFullAssetDto.get(0));

        List<JobFullDto> listJobFullDto = resourceFullDto.getListJob();
        List<UUID> jobIdDto = listJobFullDto.stream().map(job -> job.getJob().getId()).collect(Collectors.toList());

        List<Job> listJobExist = jobRepository.findAllById(jobIdDto);
        List<UUID> listJobIdExist = listJobExist.stream().map(job -> job.getId()).collect(Collectors.toList());

        List<JobFullDto> listJobFullDtoInsert = listJobFullDto.stream()
                .filter(jobFullDto -> !listJobIdExist.contains(jobFullDto.getJob().getId()))
                .collect(Collectors.toList());

        List<Job> listJob = new ArrayList<>();
        for (int i = 0; i < listJobFullDtoInsert.size(); i++) {
            JobDto jobDto = listJobFullDtoInsert.get(i).getJob();
            Job job = jobMapper.jobDtoToJob(jobDto);
            job.setAsset(newAsset);
            listJob.add(job);
        }

        List<Test> listTestInsert = new ArrayList<>();
        for (JobFullDto jobFullDto : listJobFullDtoInsert) {
            List<TestDto> listTestDto = jobFullDto.getListTest();

            List<Test> listTest = listTestDto.stream()
                    .map(testDto -> testMapper.testDtoToTest(testDto))
                    .collect(Collectors.toList());

            List<TestType> listTestType = listTest.stream()
                    .map(test -> test.getTestType())
                    .collect(Collectors.toList());

            Job job = jobMapper.jobDtoToJob(jobFullDto.getJob());
            for (int i = 0; i < listTest.size(); i++) {
                Test test = listTest.get(i);
                test.setJob(job);

                TestType testType = listTestType.get(i);
                TestType newTestType = testTypeService.insertIfNotExist(testType);
                test.setTestType(newTestType);

                try {
                    testService.addMetaData(test);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
            listTestInsert.addAll(listTest);
        }

        // job không bị lock
        List<Job> listJobUpdate = listJobExist.stream()
                .filter(job -> !job.isLocked())
                .collect(Collectors.toList());
        // id job không bị lock
        List<UUID> listJobIdUpdate = listJobUpdate.stream()
                .map(job -> job.getId())
                .collect(Collectors.toList());
        // list job full dto
        List<JobFullDto> listJobFullDtoUpdate = listJobFullDto.stream()
                .filter(jobFullDto -> listJobIdUpdate.contains(
                        jobFullDto.getJob().getId()))
                .collect(Collectors.toList());

        // update
        List<Job> _listJobUpdate = new ArrayList<>();
        List<Test> listTestUpdate = new ArrayList<>();
        for (JobFullDto jobFullDtoUpdate : listJobFullDtoUpdate) {
            Job newJob = jobRepository.findById(jobFullDtoUpdate.getJob().getId()).get();
            newJob = jobMapper.copyJobDtoToJob(jobFullDtoUpdate.getJob(), newJob, "id", "asset_id", "collabs",
                    "locked");
            _listJobUpdate.add(newJob);

            // xóa test cũ
            List<Test> listDeletedTest = testRepository.findAllByJobId(jobFullDtoUpdate.getJob().getId());
            testRepository.deleteAll(listDeletedTest);

            // cập nhật test mới
            List<TestDto> listTestDto = jobFullDtoUpdate.getListTest();
            for (TestDto testDto : listTestDto) {
                Test newTest = testRepository.findById(testDto.getId()).orElse(null);
                if (newTest == null) {
                    newTest = testMapper.testDtoToTest(testDto);
                    newTest.setJob(newJob);

                    TestType testType = newTest.getTestType();
                    TestType newTestType = testTypeService.insertIfNotExist(testType);
                    newTest.setTestType(newTestType);
                } else {
                    newTest = testMapper.copyTestDtoToTest(testDto, newTest, "id", "job_id", "testTypeId");
                }

                try {
                    testService.addMetaData(newTest);
                } catch (Exception e) {
                    // TODO: handle exception
                }

                listTestUpdate.add(newTest);
            }
        }

        jobRepository.saveAll(_listJobUpdate);
        testRepository.saveAll(listTestUpdate);

        jobRepository.saveAll(listJob);
        testRepository.saveAll(listTestInsert);
        return ResponseObject.returnWithData(true);
    }

    @Override
    public ResponseObject download(List<UUID> listId) {
        List<Job> jobs = jobRepository.findAllById(listId);
        List<ResourceFullDto> res = new ArrayList<>();

        for (Job job : jobs) {
            UUID jobId = job.getId();

            List<Test> listTest = testRepository.findAllByJobId(jobId);
            List<TestDto> listTestDto = listTest.stream()
                    .map(test -> testMapper.testToTestDto(test))
                    .collect(Collectors.toList());

            float averageHealthIndex = 0;
            float worstHealthIndex = 0;
            List<Fmeca> listFmeca = fmecaRepository.findAll();
            if (listFmeca.size() != 0) {

                Job job_1 = getJobWithHealthIndex(jobId, listFmeca.get(0).getTableCalculate());
                averageHealthIndex = job_1.getAverageHealthIndex();
                worstHealthIndex = job_1.getWorstHealthIndex();

                job.setAverageHealthIndex(averageHealthIndex);
                job.setWorstHealthIndex(worstHealthIndex);
                List<TestDto> listTestDto_1 = job_1.getTests().stream()
                        .map(test -> testMapper.testToTestDto(test))
                        .collect(Collectors.toList());
                listTestDto = listTestDto_1;

//                try {
//                    JSONObject jsonObject = new JSONObject(listFmeca.get(0).getTableCalculate());
//                    for (TestDto testDto : listTestDto) {
//                        float weightingFactor = Float
//                                .parseFloat(jsonObject.getJSONObject(testDto.getTestTypeCode().toLowerCase())
//                                        .getString(Constants.WEIGHTING_FACTOR));
//                        testDto.setWeighting_factor(weightingFactor);
//
//                        Float totalAverageHealthIndex = weightingFactor * testDto.getAverage_score();
//                        averageHealthIndex += totalAverageHealthIndex;
//                        testDto.setTotal_average_score(totalAverageHealthIndex);
//
//                        Float totalWorstHealthIndex = weightingFactor * testDto.getWorst_score();
//                        worstHealthIndex += totalWorstHealthIndex;
//                        testDto.setTotal_worst_score(totalWorstHealthIndex);
//                    }
//                } catch (Exception e) {
//                    // TODO: handle exception
//                }
            }

            job.setAverageHealthIndex(averageHealthIndex);
            job.setWorstHealthIndex(worstHealthIndex);


            JobDto jobDto = jobMapper.jobToJobDto(job);

            JobFullDto jobFullDto = new JobFullDto(jobDto, listTestDto);

            Asset asset = job.getAsset();
            UUID assetId = asset.getMrid();

            TapChanger tapChanger = tapChangerRepository.findFirstByAssetId(assetId);
            Bushing bushing = bushingRepository.findFirstByAssetId(assetId);

            AssetDto assetDto = assetMapper.assetToAssetDto(asset);
            TapChangerDto tapChangerDto = tapChangerMapper.tapChangerToTapChangerDto(tapChanger);
            BushingDto bushingDto = bushingMapper.bushingToBushingDto(bushing);

            AssetFullDto assetFullDto = new AssetFullDto(assetDto, bushingDto, tapChangerDto);

            Location location = asset.getLocation();
            LocationDto locationDto = locationMapper.locationToLocationDto(location);
            res.add(new ResourceFullDto(null, null, null, locationDto, assetFullDto, jobFullDto));
        }

        return ResponseObject.returnWithData(res);
    }

    private Float handleNullValue(@NotNull String value) {
        if (value.isEmpty()) {
            return Float.NaN;
        } else {
            return Float.valueOf(value);
        }
    }
}
