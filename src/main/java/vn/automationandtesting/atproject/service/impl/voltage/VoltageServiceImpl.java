package vn.automationandtesting.atproject.service.impl.voltage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.VoltageMapper;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.entity.voltage.Voltage;
import vn.automationandtesting.atproject.repository.voltage.VoltageRepository;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;
import vn.automationandtesting.atproject.service.voltage.VoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class VoltageServiceImpl implements VoltageService {

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private VoltageRepository repository;

    @Autowired
    private VoltageMapper mapper;


    @Override
    public List<VoltageDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Voltage> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Voltage> voltageList = repository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<VoltageDto> voltageDtoList = new ArrayList<>();
        if(!voltageList.isEmpty()) {
            for(Voltage voltage : voltageList) {
                VoltageDto voltageDto = mapper.assetToAssetDto(voltage);
                voltageDtoList.add(voltageDto);
            }
        }
        return voltageDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return repository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<VoltageDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Voltage> assets = repository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<VoltageDto> assetDtoList = new ArrayList<>();
        for(Voltage asset : assets) {
            VoltageDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<VoltageDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Voltage> assets = repository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<VoltageDto> assetDtoList = new ArrayList<>();
        for(Voltage asset : assets) {
            VoltageDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = repository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    @Override
    public List<VoltageDto> findAssetById(UUID id) {
        List<Voltage> list = repository.findAssetById(id);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findBySerial(String serial) {
        List<Voltage> list = repository.findBySerial(serial);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Voltage> list = repository.findBySerialAndLocation(serial, location_id);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findAll() {
        List<Voltage> list = repository.findAll();
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Voltage> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Voltage> list) {
        repository.saveAll(list);
    }

    /**
     * @param voltageDto
     * @param id
     * @return
     */
    @Override
    public VoltageDto updateAssetLite(VoltageDto voltageDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Voltage asset = repository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Voltage temp = mapper.copyAssetDtoToAssetLite(voltageDto, asset, acceptProperties);
            repository.save(temp);
            return voltageDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteAll(List<Voltage> list) {
        repository.deleteAll(list);
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public VoltageDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Voltage asset = repository.findAssetById(asset_id, ids, user_id);
            return mapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }
}
