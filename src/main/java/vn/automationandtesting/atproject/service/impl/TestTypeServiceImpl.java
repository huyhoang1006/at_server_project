package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.cim.TestTypeDto;
import vn.automationandtesting.atproject.controller.dto.mapper.TestTypeMapper;
import vn.automationandtesting.atproject.entity.cim.TestType;
import vn.automationandtesting.atproject.entity.enumm.TestTypeEnum;
import vn.automationandtesting.atproject.repository.TestTypeRepository;
import vn.automationandtesting.atproject.service.TestTypeService;

@Service
public class TestTypeServiceImpl implements TestTypeService {

    @Autowired
    private TestTypeRepository testTypeRepository;

    @Autowired
    private TestTypeMapper testTypeMapper;

    @Override
    public TestTypeDto findTestTypesByCode(String testTypeCode) {
        TestType testType = testTypeRepository.findByCodeAndIsDeleted(TestTypeEnum.valueOf(testTypeCode).name(), false);
        TestTypeDto testTypeDto = testTypeMapper.testTypeToTestTypeDto(testType);
        return testTypeDto;
    }
    @Override
    public TestType insertIfNotExist(TestType testType) {
        String testTypeCode = testType.getCode();
        TestType testTypeExist = testTypeRepository.findByCode(testTypeCode);
        if (testTypeExist == null) {
            return testTypeRepository.save(testType);
        }
        
        return testTypeExist;
    }
}
