package vn.automationandtesting.atproject.service.impl.surge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.current.JobCurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.JobSurgeMapper;
import vn.automationandtesting.atproject.controller.dto.surge.JobSurgeDto;
import vn.automationandtesting.atproject.entity.current.JobsCurrent;
import vn.automationandtesting.atproject.entity.surge.JobsSurge;
import vn.automationandtesting.atproject.repository.surge.JobSurgeRepository;
import vn.automationandtesting.atproject.service.surge.JobSurgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobSurgeServiceImpl implements JobSurgeService {

    @Autowired
    private JobSurgeRepository repository;

    @Autowired
    private JobSurgeMapper mapper;

    @Override
    public List<JobSurgeDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsSurge> jobs = repository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobSurgeDto> findJobById(UUID id) {
        List<JobsSurge> jobs = repository.findJobById(id);
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobSurgeDto> findJobByName(String name) {
        List<JobsSurge> jobs = repository.findJobByName(name);
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobSurgeDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<JobsSurge> jobs = repository.findJobByNameAndAsset(name, asset_id);
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobSurgeDto> findAll() {
        List<JobsSurge> jobs = repository.findAll();
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobSurgeDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<JobsSurge> jobs = repository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobSurgeDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsSurge item : jobs) {
                JobSurgeDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<JobsSurge> jobdataList) {
        repository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<JobsSurge> jobdataList) {
        repository.deleteAll(jobdataList);
    }

    /**
     * @param asset_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<JobSurgeDto> findJobByAssetId(UUID asset_id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsSurge> jobList = repository.findJobByAssetId(asset_id, user_id, userId, first, limit );
        List<JobSurgeDto> jobDtoList = new ArrayList<>();
        if(!jobList.isEmpty()) {
            for(JobsSurge job : jobList) {
                JobSurgeDto jobDto = mapper.JobToJobDto(job);
                jobDtoList.add(jobDto);
            }
        }
        return jobDtoList;
    }

    /**
     * @param asset_id
     * @return
     */
    @Override
    public int countJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countJobByAssetId(asset_id, user_id, userId);
        return count;
    }
}
