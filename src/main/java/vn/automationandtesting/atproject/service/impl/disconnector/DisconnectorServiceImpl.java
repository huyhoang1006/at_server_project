package vn.automationandtesting.atproject.service.impl.disconnector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.DisconnectorMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;
import vn.automationandtesting.atproject.repository.disconnector.DisconnectorRepository;
import vn.automationandtesting.atproject.service.disconnector.DisconnectorService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DisconnectorServiceImpl implements DisconnectorService {

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private DisconnectorRepository repository;

    @Autowired
    private DisconnectorMapper mapper;


    @Override
    public List<DisconnectorDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Disconnector> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findAssetByLocationId(UUID id, int first, int limit) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Disconnector> disconnectorList = repository.findAssetByLocationId(id, user_id, userId, first, limit );
        List<DisconnectorDto> disconnectorDtoList = new ArrayList<>();
        if(!disconnectorList.isEmpty()) {
            for(Disconnector disconnector : disconnectorList) {
                DisconnectorDto disconnectorDto = mapper.assetToAssetDto(disconnector);
                disconnectorDtoList.add(disconnectorDto);
            }
        }
        return disconnectorDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = repository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return repository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<DisconnectorDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Disconnector> assets = repository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<DisconnectorDto> assetDtoList = new ArrayList<>();
        for(Disconnector asset : assets) {
            DisconnectorDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<DisconnectorDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Disconnector> assets = repository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<DisconnectorDto> assetDtoList = new ArrayList<>();
        for(Disconnector asset : assets) {
            DisconnectorDto assetDto = mapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = repository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    @Override
    public List<DisconnectorDto> findAssetById(UUID id) {
        List<Disconnector> list = repository.findAssetById(id);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findBySerial(String serial) {
        List<Disconnector> list = repository.findBySerial(serial);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Disconnector> list = repository.findBySerialAndLocation(serial, location_id);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findAll() {
        List<Disconnector> list = repository.findAll();
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Disconnector> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Disconnector> list) {
        repository.saveAll(list);
    }

    /**
     * @param disconnectorDto
     * @param id
     * @return
     */
    @Override
    public DisconnectorDto updateAssetLite(DisconnectorDto disconnectorDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Disconnector asset = repository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("serial_no");
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Disconnector temp = mapper.copyAssetDtoToAssetLite(disconnectorDto, asset, acceptProperties);
            repository.save(temp);
            return disconnectorDto;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteAll(List<Disconnector> list) {
        repository.deleteAll(list);
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public DisconnectorDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Disconnector asset = repository.findAssetById(asset_id, ids, user_id);
            return mapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }
}
