package vn.automationandtesting.atproject.service.impl.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.message.MessageMapper;
import vn.automationandtesting.atproject.controller.dto.message.MessageDto;
import vn.automationandtesting.atproject.entity.message.GroupList;
import vn.automationandtesting.atproject.entity.message.Message;
import vn.automationandtesting.atproject.repository.message.GroupListRepository;
import vn.automationandtesting.atproject.repository.message.MessageRepository;
import vn.automationandtesting.atproject.service.message.MessageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageMapper mapper;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private GroupListRepository groupListRepository;

    @Autowired
    private FirebaseServiceImpl firebaseServiceImpl;

    /**
     * @param messageDto
     * @return
     */
    @Override
    public MessageDto saveMessage(MessageDto messageDto) {
        Message message = new Message();
        message = mapper.copyDtoToEntity(messageDto, message);
        message.setStatus("sent");
        Message saveMessage = messageRepository.save(message);
        return mapper.copyEntityToDto(saveMessage, messageDto);
    }

    /**
     * @param user_id
     * @param group_id
     * @param first
     * @param sl
     * @return
     */
    @Override
    public List<MessageDto> getMessage(UUID user_id, UUID group_id, int first, int sl) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(group_id);
        if(!groupLists.isEmpty()) {
            if(groupLists.get(0).getMember().contains(user_id.toString())) {
                List<Message> messageList = messageRepository.findGroupByCreatedIdAndGroupId(group_id, first, sl);
                return messageList.stream().map(message -> mapper.copyEntityToDto(message, new MessageDto())).collect(Collectors.toList());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param user_id
     * @param group_id
     * @return
     */
    @Override
    public List<MessageDto> getAllMessageByUnread(UUID user_id, UUID group_id) {
        List<GroupList> groupLists = groupListRepository.findGroupListById(group_id);
        if(!groupLists.isEmpty()) {
            if(groupLists.get(0).getMember().contains(user_id.toString())) {
                List<Message> messageList = messageRepository.getAllMessageByUnread(group_id, user_id.toString());
                return messageList.stream().map(message -> mapper.copyEntityToDto(message, new MessageDto())).collect(Collectors.toList());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param groupId
     * @param id
     * @param userId
     * @return
     */
    @Override
    public MessageDto getMessageById(String groupId, String id, String userId) {
        List<GroupList> groupList = groupListRepository.findGroupListById(UUID.fromString(groupId));
        if(!groupList.isEmpty()) {
            if(groupList.get(0).getMember().contains(userId)) {
                List<Message> messageList = messageRepository.findMessageByGroupIdAndId(UUID.fromString(groupId), UUID.fromString(id));
                return mapper.copyEntityToDto(messageList.get(0), new MessageDto());
            }
        }
        return null;
    }

    /**
     * @param ids
     */
    @Override
    public void markAsSeenArr(List<UUID> ids) {
        List<Message> messageList = messageRepository.getAllMessageById(ids);
        List<Message> messageArrayList = new ArrayList<>();
        for(Message message: messageList) {
            MessageDto messageDto = new MessageDto();
            messageDto = mapper.copyEntityToDto(message, messageDto);
            if(messageDto.getReceiverMember() == null || !messageDto.getReceiverMember().contains(BearerContextHolder.getContext().getUserId())) {
                List<String> receiverMember = messageDto.getReceiverMember();
                if(receiverMember == null) {
                    receiverMember = new ArrayList<>();
                }
                receiverMember.add(BearerContextHolder.getContext().getUserId());
                messageDto.setReceiverMember(receiverMember);
                Message message1 = new Message();
                message1 = mapper.copyDtoToEntity(messageDto, message1);
                messageArrayList.add(message1);
            }
        }
        messageRepository.saveAll(messageArrayList);
    }
}
