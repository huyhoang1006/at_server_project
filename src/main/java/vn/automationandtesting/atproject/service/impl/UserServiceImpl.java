package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.BeanUtilsCustom;
import vn.automationandtesting.atproject.controller.dto.request.OwnerUserDto;
import vn.automationandtesting.atproject.controller.dto.request.UserMessageDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.Group;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.Role;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.entity.owner.Owner;
import vn.automationandtesting.atproject.exception.GroupNotFoundException;
import vn.automationandtesting.atproject.exception.UserNotFoundException;
import vn.automationandtesting.atproject.repository.GroupRepository;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.RoleRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.service.OwnerUserService;
import vn.automationandtesting.atproject.service.UserService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private SharedListIdService sharedListIdService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    private OwnerUserService ownerUserService;

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found."));
    }

    @Override
    public ResponseObject changePassword(UUID userID, String oldPassword, String newPassword) {
        User user = userRepository.findById(userID).orElse(null);
        if (user == null) {
            return ResponseObject.returnWithError("Invalid User");
        }
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            return ResponseObject.returnWithError("Invalid Password");
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
        return ResponseObject.returnWithData(true);
    }

    @Override
    public ResponseObject saveNewUser(UserReqDto userReqDto) {
        //check existing username
        if (userRepository.existsByUsername(userReqDto.getUsername())) {
            return ResponseObject.returnWithError("Username already existed");
        }
        int role = RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
        int roleCreate = RoleEnum.getIntOfRole(userReqDto.getRole());
        if(role < roleCreate) {
            User user = userMapper.userReqDtoToUser(userReqDto);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setId(UUID.randomUUID());
            userRepository.save(user);
            return ResponseObject.returnWithData(true);
        } else {
            return ResponseObject.returnWithError("Do not permission");
        }
    }

    @Override
    public List<UserReqDto> getAllUsers() {
        boolean check = true;
        UUID id = UUID.fromString(BearerContextHolder.getContext().getUserId());
        List<User> users = userRepository.findAllUserById(id);
        List<User> dataUser = users;
        while (check) {
            List<User> data = new ArrayList<>();
            for(User user : dataUser) {
                List<User> usersTemp = userRepository.findAllUserByCreated(user.getId());
                data.addAll(usersTemp);
            }

            if(data.isEmpty()) {
                check = false;
            } else {
                dataUser = data;
                users.addAll(data);
            }

        }
//        if (RoleEnum.USER.name().equalsIgnoreCase(BearerContextHolder.getContext().getRoleName())) {
//            users = users.stream().filter(user -> RoleEnum.USER.equals(user.getRole().getRoleName()))
//                    .collect(Collectors.toList());
//        }
        List<UserReqDto> userReqDtos = users.stream().map(user ->
                userMapper.userToUserReqDto(user)
        ).collect(Collectors.toList());
        return userReqDtos;
    }

    /**
     * @param created_by
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<UserReqDto> findUserByCreated(UUID created_by, int first, int limit) {
        List<User> users = userRepository.findUserByCreated(created_by, first, limit);
        List<UserReqDto> userReqDtos = users.stream().map(user ->
                userMapper.userToUserReqDto(user)
        ).collect(Collectors.toList());
        return userReqDtos;
    }

    @Override
    public UserReqDto getUserById(UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        return userMapper.userToUserReqDto(user);
    }

    @Override
    public UserReqDto updateUser(UserReqDto userReqDto, UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        BeanUtils.copyProperties(userReqDto, user, "id", "username", "password", "role", "groups");
        user.setGroups(checkValidGroupName(userReqDto.getGroups()));
        User savedUser = userRepository.save(user);
        return userMapper.userToUserReqDto(savedUser);
    }

    @Override
    public UserReqDto updateUserAllInfo(UserReqDto userReqDto, UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        BeanUtils.copyProperties(userReqDto, user, "id", "groups", "password");
        user.setIsDeleted(userReqDto.isDelete());
        if(!user.getGroups().equals("")) {
            user.setGroups(checkValidGroupName(userReqDto.getGroups()));
        }

        if(userReqDto.getPassword() != null && !userReqDto.getPassword().equals("")) {
            user.setPassword(passwordEncoder.encode(userReqDto.getPassword()));
        }
        User savedUser = userRepository.save(user);
        return userMapper.userToUserReqDto(savedUser);
    }

    @Override
    public UserReqDto updateUserFull(UserReqDto userReqDto, UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        BeanUtils.copyProperties(userReqDto, user, "id");
        user.setGroups(checkValidGroupName(userReqDto.getGroups()));
        User savedUser = userRepository.save(user);
        return userMapper.userToUserReqDto(savedUser);
    }

    @Override
    public UserReqDto changeGroupUser(List<String> groupName, UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        user.setGroups(checkValidGroupName(groupName));
        User savedUser = userRepository.save(user);
        return userMapper.userToUserReqDto(savedUser);
    }

    @Override
    public void deleteUserById(UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        user.setIsDeleted(true);
        userRepository.save(user);
    }

    @Override
    public void removeUserById(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User with the ID not found."));
        if(!user.getRole().getRoleName().toString().equals("ADMIN")) {
            List<User> userList = userRepository.findAllUserByCreated(id);
            for (User userdata : userList) {
                userdata.setCreatedBy(user.getCreatedBy());
            }
            userRepository.saveAll(userList);
            userRepository.delete(user);
        }
    }

    @Override
    public UserReqDto updateUserGroupsForAdmin(List<String> groupNames, UUID id) {
        User user = userRepository.findByIdAndIsDeleted(id, false).orElseThrow(() -> new UsernameNotFoundException("User's ID not existed."));
        String groupNamesString = checkValidGroupName(groupNames);
        user.setGroups(groupNamesString);
        User savedUser = userRepository.save(user);
        return userMapper.userToUserReqDto(savedUser);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public int countUserByCreated(UUID id) {
        return userRepository.countUserByCreated(id);
    }

    /**
     * @param userReqDto
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<UserReqDto> getDataByIncludes(UserReqDto userReqDto, int first, int limit) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<User> userList = userRepository.findAllUserByField(userReqDto, first, limit, ids);
        return userList.stream().map(user1 ->
                userMapper.userToUserReqDto(user1)
        ).collect(Collectors.toList());

    }

    /**
     * @param userReqDto
     * @return
     */
    @Override
    public Long countAllUserByField(UserReqDto userReqDto) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        Long count = userRepository.countAllUserByField(userReqDto, ids);
        return count;
    }

    /**
     * @param data
     * @return
     */
    @Override
    public List<UserMessageDto> searchMessageUser(String data) {
        List<User> userList = userRepository.findAllUserByEmailOrName(data);
        List<String> fields = new ArrayList<>();
        List<UserMessageDto> userMessageDtoList = new ArrayList<>();
        fields.add("id");
        fields.add("username");
        fields.add("email");
        for(User user : userList) {
            UserMessageDto userMessageDto = new UserMessageDto();
            BeanUtilsCustom.copyProperties(user, userMessageDto, fields);
            userMessageDtoList.add(userMessageDto);
        }
        return userMessageDtoList;
    }

    private String checkValidGroupName(List<String> groups) {
        List<Group> groupList = groupRepository.findAll();
        List<String> groupListString = groupList.stream().map(group -> group.getGroupName()).collect(Collectors.toList());
        groups.forEach(s -> {
            if (!groupListString.contains(s.toUpperCase())) {
                throw new GroupNotFoundException("Group name " + s + " not found.");
            }
        });
        return String.join(",", groups.stream().map(String::toUpperCase).collect(Collectors.toList()));
    }
}
