package vn.automationandtesting.atproject.service.impl.disconnector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.disconnector.TestDisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.TestDisconnectorMapper;
import vn.automationandtesting.atproject.entity.disconnector.TestsDisconnector;
import vn.automationandtesting.atproject.repository.disconnector.TestDisconnectorRepository;
import vn.automationandtesting.atproject.service.disconnector.TestDisconnectorService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestDisconnectorServiceImpl implements TestDisconnectorService {

    @Autowired
    private TestDisconnectorRepository repository;

    @Autowired
    private TestDisconnectorMapper mapper;


    @Override
    public List<TestDisconnectorDto> findAllTestByJobId(UUID job_id) {
        List<TestsDisconnector> tests = repository.findAllTestByJobId(job_id);
        List<TestDisconnectorDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsDisconnector item : tests) {
                TestDisconnectorDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestDisconnectorDto> findTestById(UUID id) {
        List<TestsDisconnector> tests = repository.findTestById(id);
        List<TestDisconnectorDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsDisconnector item : tests) {
                TestDisconnectorDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestDisconnectorDto> findTestByType(UUID type_id) {
        List<TestsDisconnector> tests = repository.findTestByType(type_id);
        List<TestDisconnectorDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsDisconnector item : tests) {
                TestDisconnectorDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestDisconnectorDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestsDisconnector> tests = repository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestDisconnectorDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsDisconnector item : tests) {
                TestDisconnectorDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestsDisconnector> tests) {
        repository.saveAll(tests);
    }

    @Override
    public void deleteAll(List<TestsDisconnector> tests) {
        repository.deleteAll(tests);
    }
}
