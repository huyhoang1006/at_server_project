package vn.automationandtesting.atproject.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.AttachmentMapper;
import vn.automationandtesting.atproject.entity.Attachment;
import vn.automationandtesting.atproject.repository.AttachmentRepository;
import vn.automationandtesting.atproject.service.AttachmentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private AttachmentMapper attachmentMapper;

    @Override
    public List<AttachmentDto> findAllById_foreign(String id_foreign) {
        UUID id = UUID.fromString(id_foreign);
        List<Attachment> attachmentList = attachmentRepository.findAllById_foreign(id);
        List<AttachmentDto> attachmentDtoList = new ArrayList<>();
        for(Attachment attachment : attachmentList) {
            attachmentDtoList.add(attachmentMapper.AttachmentToAttachmentDto(attachment));
        }
        return attachmentDtoList;

    }

    public void insertAttachment(Attachment attachment) {
        attachmentRepository.save(attachment);
    }

    @Override
    public void updateNameById_foreign(Attachment attachment) {
        String name = attachment.getName();
        UUID id_foreign = attachment.getId_foreign();
        attachmentRepository.updateNameById_foreign(name, id_foreign);
    }

    @Override
    public Boolean findById(UUID id) {
        List<Attachment> data = attachmentRepository.findAllById_foreign(id);
        if(data.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String findNamebyId_foreign(UUID id_foreign) {
        String data = attachmentRepository.findNamebyId_foreign(id_foreign);
        return data;
    }

    /**
     * @param attachmentDtos
     */
    @Override
    public boolean insertAttachment(List<AttachmentDto> attachmentDtos) {
        try {
            for (AttachmentDto attachmentDto : attachmentDtos) {
                Attachment attachment = attachmentMapper.AttachmentDtoToAttachment(attachmentDto);
                Boolean check = findById(attachment.getId_foreign());
                if (check) {
                    updateNameById_foreign(attachment);
                } else {
                    insertAttachment(attachment);
                }
            }
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public void deleteAttachmentById_foreign(String id_foreign) {
       attachmentRepository.deleteAttachmentById_foreign(UUID.fromString(id_foreign));
    }

}
