package vn.automationandtesting.atproject.service.impl.objectLog.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.controller.dto.mapper.objectLog.location.AuditLocationLogMapper;
import vn.automationandtesting.atproject.controller.dto.objectLog.location.AuditLocationLogDto;
import vn.automationandtesting.atproject.entity.AuditLocationLog;
import vn.automationandtesting.atproject.repository.objectLog.location.AuditLocationLogRepository;
import vn.automationandtesting.atproject.service.objectLog.location.AuditLocationLogService;

import java.util.UUID;

@Repository
public class AuditLocationLogServiceImpl implements AuditLocationLogService {

    @Autowired
    private AuditLocationLogRepository auditLocationLogRepository;

    @Autowired
    private AuditLocationLogMapper auditLocationLogMapper;

    /**
     * @return
     */
    @Override
    public AuditLocationLogDto insert(AuditLocationLogDto auditLocationLogDto) {
        AuditLocationLog auditLocationLog = new AuditLocationLog();
        auditLocationLog = auditLocationLogMapper.objectDtoToObject(auditLocationLogDto, auditLocationLog);
        if(auditLocationLog.getId() == null) {
            auditLocationLog.setId(UUID.randomUUID());
        }
        auditLocationLogRepository.save(auditLocationLog);
        return auditLocationLogDto;
    }
}
