package vn.automationandtesting.atproject.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.common.LocationOwnerDto;
import vn.automationandtesting.atproject.controller.dto.mapper.LocationMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.OwnerUserMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.objectLog.location.AuditLocationLogMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.staging.location.LocationStagingMapper;
import vn.automationandtesting.atproject.controller.dto.objectLog.location.AuditLocationLogDto;
import vn.automationandtesting.atproject.controller.dto.staging.location.LocationStagingDto;
import vn.automationandtesting.atproject.entity.Attachment;
import vn.automationandtesting.atproject.entity.LocationStaging;
import vn.automationandtesting.atproject.entity.OwnerUser;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.cim.Location;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;
import vn.automationandtesting.atproject.entity.owner.Owner;
import vn.automationandtesting.atproject.exception.LocationLockedException;
import vn.automationandtesting.atproject.exception.LocationNotFoundException;
import vn.automationandtesting.atproject.exception.LocationSaveException;
import vn.automationandtesting.atproject.exception.UserNotFoundException;
import vn.automationandtesting.atproject.repository.CommonRepositoryCustom;
import vn.automationandtesting.atproject.repository.LocationRepository;
import vn.automationandtesting.atproject.repository.OwnerUserRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.repository.owner.OwnerRepository;
import vn.automationandtesting.atproject.service.AttachmentService;
import vn.automationandtesting.atproject.service.LocationService;
import vn.automationandtesting.atproject.service.fileUpload.FileUploadService;
import vn.automationandtesting.atproject.service.objectLog.location.AuditLocationLogService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;
import vn.automationandtesting.atproject.service.staging.location.LocationStagingService;
import vn.automationandtesting.atproject.util.common.JsonProcessing;
import vn.automationandtesting.atproject.util.search.LocationSearchSpecification;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private CommonRepositoryCustom commonRepositoryCustom;

    @Autowired
    private SharedListIdService sharedListIdService;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LocationStagingMapper locationStagingMapper;

    @Autowired
    private LocationStagingService locationStagingService;

    @Autowired
    private OwnerUserRepository ownerUserRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LocationMapper locationMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OwnerUserMapper ownerUserMapper;

    @Autowired
    private JsonProcessing jsonProcessing;

    @Autowired
    private AuditLocationLogService auditLocationLogService;

    @Autowired
    private AuditLocationLogMapper auditLocationLogMapper;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private OwnerRepository ownerRepository;

    @Override
    public LocationDto createLocation(LocationDto locationDto) {
        Location location = locationMapper.locationDtoToLocation(locationDto);
        String currentUserId = BearerContextHolder.getContext().getUserId();
        if (locationDto.getId() != null) {
            location.setMrid(locationDto.getId());
            Optional<Location> locationDataOptional = locationRepository.findById(locationDto.getId());
            if(locationDataOptional.isPresent()) {
                Location locationData = locationDataOptional.get();
                try {
                    LocationStaging locationStaging = locationStagingMapper.LocationToStaging(locationData);
                    LocationStagingDto locationStagingDto = locationStagingMapper.objectToObjectDto(locationStaging, new LocationStagingDto());
                    if(locationData.getVersion() != null) {
                        int version = Integer.parseInt(locationData.getVersion());
                        version = version + 1;
                        locationStagingDto.setVersion(String.valueOf(version));
                    } else {
                        locationData.setVersion("1");
                        locationRepository.save(locationData);
                        locationStagingDto.setVersion("2");
                    }
                    locationStagingService.insert(locationStagingDto);
                    return locationDto;
                } catch (Exception e) {
                    return null;
                }
            } else {
                location.setVersion("1");
            }
        } else {
            location.setMrid(UUID.randomUUID());
            location.setVersion("1");
        }
        if(location.getVersion() == null) {
            location.setVersion("1");
        }
        if(Objects.equals(BearerContextHolder.getContext().getRoleName(), "USER")) {
            User user = userRepository
                    .findByIdAndIsDeleted(UUID.fromString(currentUserId), false)
                    .orElseThrow(() -> new UserNotFoundException("Invalid user's id."));
            location.setUser(user);
        }
        Location savedLocation = locationRepository.save(location);
        return locationMapper.locationToLocationDto(savedLocation);
    }

    @Override
    public Location insertIfNotExist(LocationDto locationDto) {
        Location location = locationMapper.locationDtoToLocation(locationDto);
        UUID locationId = location.getMrid();

        Location locationExist = locationRepository.findById(locationId).orElse(null);
        if (locationExist == null) {
            UUID userId = location.getUser_id();
            User user = userRepository.findById(userId).get();
            location.setUser(user);
            return locationRepository.save(location);
        }
        else {
            if (!locationExist.isLocked()) {
                locationExist = locationMapper.copyLocationDtoToLocation(locationDto,
                        locationExist, "id", "user_id",
                        "collabs");
                return locationRepository.save(locationExist);
            }
            else{ 
                return locationExist;
            }
        }

    }

    @Override
    public void updateJobCollabs(String locationId, ListIdDto listIdDto) {
        String userId = BearerContextHolder.getContext().getUserId();
        Location location = getLocationByIdAndLoggedInUserID(UUID.fromString(locationId), UUID.fromString(userId));
        List<String> userIdList = listIdDto.getListId().stream().map(UUID::toString).collect(Collectors.toList());
        String commaSeperatedUserIdString = String.join(",", userIdList);
        location.setCollabs(commaSeperatedUserIdString);
        locationRepository.save(location);
    }

    /**
     * @param ref_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<LocationDto> findLocationByRefId(UUID ref_id, int first, int limit) {
        List<Location> locationList = locationRepository.findLocationByRefId(ref_id.toString(), first, limit);
        return locationList.stream().map(location -> locationMapper.locationToLocationDto(location)).collect(Collectors.toList());
    }

    /**
     * @param id
     * @return
     */
    @Override
    public int countLocationByRefId(UUID id) {
        int count = locationRepository.countLocationByRefId(id.toString());
        return count;
    }

    /**
     * @param ref_id
     * @param first
     * @param limit
     * @return
     */
    @Override
    public List<LocationDto> findLocationByRefIdAndCreatedAndCollab( String ref_id, int first, int limit) {
        UUID user_id = UUID.fromString(BearerContextHolder.getContext().getUserId());
        List<Location> locationList = locationRepository.findLocationByRefIdAndCreatedAndCollab(ref_id, user_id, first, limit);
        return locationList.stream().map(location -> locationMapper.locationToLocationDto(location)).collect(Collectors.toList());
    }

    /**
     * @param ref_id
     * @return
     */
    @Override
    public int countLocationByRefIdAndCreatedAndCollab(String ref_id) {
        UUID user_id = UUID.fromString(BearerContextHolder.getContext().getUserId());
        return locationRepository.countLocationByRefIdAndCreatedAndCollab(ref_id, user_id);
    }

    /**
     * @param mode
     * @param user_id
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public List<LocationDto> findAllLocationByCreatedAndCollab(String mode, UUID user_id, int offset, int limit) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Location> locationList = locationRepository.findAllLocationByCreatedAndCollab(mode, ids, user_id, offset, limit);
        return locationList.stream().map(location -> locationMapper.locationToLocationDto(location)).collect(Collectors.toList());
    }

    /**
     * @param mode
     * @param user_id
     * @return
     */
    @Override
    public int countAllLocationByCreatedAndCollab(String mode, UUID user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return locationRepository.countAllLocationByCreatedAndCollab(mode, ids, user_id);
    }

    /**
     * @param locationDto
     * @param first
     * @param limit
     * @param user_id
     * @return
     */
    @Override
    public List<LocationDto> findAllLocationByField(LocationDto locationDto, int first, int limit, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Location> locationList = locationRepository.findAllLocationByField(locationDto, first, limit, ids, user_id);
        return locationList.stream().map(location -> locationMapper.locationToLocationDto(location)).collect(Collectors.toList());

    }

    /**
     * @param locationDto
     * @param user_id
     * @return
     */
    @Override
    public Long countAllLocationByField(LocationDto locationDto, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        Long count = locationRepository.countAllLocationByField(locationDto, ids, user_id);
        return count;
    }

    /**
     * @param locationDto
     * @return
     */
    @Override
    public LocationDto insertLocation(LocationDto locationDto) {
        Location location = new Location();
        location = locationMapper.copyLocationDtoToLocation(locationDto, location);
        locationRepository.save(location);
        return locationDto;
    }



    /**
     * @param locationId
     * @param locationDto
     * @param attachmentDto
     * @param deleteFile
     * @param files
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public LocationDto insertLocationData(UUID locationId, LocationDto locationDto, List<AttachmentDto> attachmentDto, List<String> deleteFile, MultipartFile[] files) {
        boolean fileSign = fileUploadService.updateFile(files, deleteFile, locationId.toString());
        if(!fileSign) {
            return null;
        } else {
            boolean attachmentSign = attachmentService.insertAttachment(attachmentDto);
            if(!attachmentSign) {
                return null;
            } else {
                insertLocation(locationDto);
                return locationDto;
            }
        }
    }

    /**
     * @param mode
     * @param userId
     * @return
     */
    @Override
    public int countLocationsByRefIdOwnerAndModeAndCreatedBy(String mode, UUID userId) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        int count = locationRepository.countLocationsByRefIdOwnerAndModeAndCreatedBy(mode, ids, userId);
        return count;
    }

    @Override
    public int countLocationsModeByCreatedByAndCollab(String mode, UUID userId) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        int count = locationRepository.countLocationsModeByCreatedByAndCollab(mode, ids, userId);
        return count;
    }

    /**
     * @param locationOwnerDtoList
     * @return
     */
    @Override
    @Transactional
    public List<LocationOwnerDto> addLocationByOwnerFullName(List<LocationOwnerDto> locationOwnerDtoList) {
        List<Location> arr = new ArrayList<>();
        List<Object[]> foreign_key = commonRepositoryCustom.findReferencingTables("location", "mrid");
        for(int i=0; i<locationOwnerDtoList.size(); i++) {
            LocationOwnerDto locationOwnerDto = locationOwnerDtoList.get(i);
            String nameOwner = locationOwnerDto.getFull_name();
            List<Owner> ownerList = ownerRepository.findByFullName(nameOwner);
            if(!ownerList.isEmpty()) {
                List<Location> locationList = locationRepository.findLocationByName(locationOwnerDto.getName());
                if(locationList.isEmpty()) {
                    Location location = new Location();
                    location = locationMapper.copyLocationOwnerDtoToLocation(locationOwnerDto, location);
                    if (location.getMrid() == null || location.getMrid().toString().isEmpty()) {
                        location.setMrid(UUID.randomUUID());
                    }
                    location.setVersion("1");
                    location.setRefId(ownerList.get(0).getMrid().toString());
                    location.setCreatedBy(ownerList.get(0).getCreatedBy());
                    location.setMode("location");
                    arr.add(location);
                } else {
                    Location locationOld = locationList.get(0);
                    Location location = new Location();
                    location = locationMapper.copyLocationOwnerDtoToLocation(locationOwnerDto, location);
                    if (location.getMrid() == null || location.getMrid().toString().isEmpty()) {
                        location.setMrid(UUID.randomUUID());
                    }
                    location.setVersion(locationOld.getVersion());
                    location.setRefId(locationOld.getRefId());
                    location.setCreatedBy(locationOld.getCreatedBy());
                    location.setMode(locationOld.getMode());
                    if(location.getMrid().equals(locationOld.getMrid())) {
                        locationRepository.save(location);
                    } else {
                        locationRepository.save(location);
                        for(Object[] row : foreign_key) {

                        }
                    }
//                    commonRepositoryCustom.updateLocationRef(location.getMrid());
                }
            }
        }
        locationRepository.saveAll(arr);
        return locationOwnerDtoList;
    }

    /**
     * @param userId
     * @return
     */
    @Override
    public int countLocationsByCreatedByAndCollab(UUID userId) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        int count = locationRepository.countLocationsByCreatedByAndCollab(ids, userId);
        return count;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> updateLocation(UUID id, LocationDto locationDto) {
        String userID = BearerContextHolder.getContext().getUserId();
        Location savedLocation = locationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Location not found"));
        if (savedLocation.isLocked()) {
            if (!savedLocation.getLocked_by().toString().equals(userID)) {
                throw new LocationLockedException("Location already locked by another user");
            } else {
                throw new LocationLockedException("Location already locked");
            }
        } else {
//            savedLocation = locationMapper.copyLocationDtoToLocation(locationDto, savedLocation, "id", "user_id",
//                    "collabs", "locked");
            try {
                LocationStaging locationStaging = locationStagingMapper.LocationDtoToStaging(locationDto);
                LocationStagingDto locationStagingDto = locationStagingMapper.objectToObjectDto(locationStaging, new LocationStagingDto());
                AuditLocationLogDto auditLocationLogDto = new AuditLocationLogDto();
                if(savedLocation.getVersion() != null) {
                    int version = Integer.parseInt(savedLocation.getVersion());
                    version = version + 1;
                    locationStagingDto.setVersion(String.valueOf(version));
                    auditLocationLogDto.setVersion(String.valueOf(version));
                } else {
                    savedLocation.setVersion("1");
                    savedLocation = locationRepository.save(savedLocation);
                    locationStagingDto.setVersion("2");
                    auditLocationLogDto.setVersion("2");
                }
                auditLocationLogDto.setLocation_staging_id(locationStagingDto.getId());
                auditLocationLogDto.setId(UUID.randomUUID());
                auditLocationLogDto.setStatus("waiting");
                auditLocationLogDto.setSender_id(UUID.fromString(userID));
                auditLocationLogDto.setSender_name(BearerContextHolder.getContext().getUserName());

                LocationDto locationDto1 = locationMapper.locationToLocationDto(savedLocation);
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString1 = objectMapper.writeValueAsString(locationDto1);
                String jsonString2 = locationStagingDto.getContent();

                ObjectNode differences = jsonProcessing.getJsonDiff(jsonString1, jsonString2);
                auditLocationLogDto.setContent(differences.toString());
                locationStagingDto.setStatus("waiting");
                locationStagingService.insert(locationStagingDto);
                auditLocationLogService.insert(auditLocationLogDto);
                Map<String, Object> map = new HashMap<>();
                map.put("location", locationDto1);
                map.put("location_staging", locationStagingDto);
                map.put("audit_location_log", auditLocationLogDto);
                return map;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
//            Location updatedLocation = locationRepository.save(savedLocation);
//            return locationMapper.locationToLocationDto(savedLocation);
        }
    }

    /**
     * @param id
     * @param locationDto
     * @return
     */
    @Override
    public LocationDto updateLocationLite(UUID id, LocationDto locationDto) {
        Location location = locationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Location not found"));
        String name = location.getName();
        if (location.isLocked()) {
            if (!location.getLocked_by().toString().equals(BearerContextHolder.getContext().getUserId())) {
                throw new LocationLockedException("Location already locked by another user");
            } else {
                throw new LocationLockedException("Location already locked");
            }
        } else {
            try {
                List<String> accceptProperties = new ArrayList<>();
                accceptProperties.add("refId");
                accceptProperties.add("name");
                accceptProperties.add("address");
                accceptProperties.add("state_province");
                accceptProperties.add("city");
                accceptProperties.add("country");
                accceptProperties.add("person_phone_no1");
                accceptProperties.add("person_email");
                Location updateLocation = locationMapper.copyLocationDtoToLocation(locationDto, location, accceptProperties, "id", "user_id",
                        "collabs", "locked");

                locationRepository.save(updateLocation);
                return locationDto;
            } catch (Exception e) {
                throw new LocationSaveException("Cannot save location");
            }
        }
    }

    @Override
    public ResponseObject lock(Boolean locked, List<UUID> listId) {
        List<Location> locations = locationRepository.findAllById(listId);
        String userID = BearerContextHolder.getContext().getUserId();
        List<UUID> ids = sharedListIdService.getCachedIds();
        for (Location location : locations) {
            if(ids.contains(location.getCreatedBy())) {
                if (!(location.isLocked() && !location.getLocked_by().toString().equals(userID))) {
                    location.setLocked(locked);
                    if (locked) {
                        location.setLocked_by(UUID.fromString(userID));
                    }
                }
            }
        }
        locationRepository.saveAll(locations);

        return ResponseObject.returnWithData(true);
    }

    @Override
    public ResponseObject download(List<UUID> listId) {
        List<Location> locations = locationRepository.findAllById(listId);
        List<ResourceFullDto> res = locations.stream()
                .map(location -> new ResourceFullDto(null,null,null, locationMapper.locationToLocationDto(
                        location), null,null) )
                .collect(Collectors.toList());
        return ResponseObject.returnWithData(res);
    }

    @Override
    public ResponseObject upload(ResourceFullDto resourceFullDto) {
        String currentUserId = BearerContextHolder.getContext().getUserId();
        User user = userRepository.findById(UUID.fromString(currentUserId)).get();

        List<LocationDto> listLocation = resourceFullDto.getListLocation();
        List<UUID> locationId = listLocation.stream()
                .map(location -> location.getId())
                .collect(Collectors.toList());

        List<Location> listLocationExist = locationRepository.findAllById(locationId);
        List<UUID> locationIdExist = listLocationExist.stream()
                .map(location -> location.getMrid())
                .collect(Collectors.toList());

        List<LocationDto> listLocationInsert = listLocation.stream()
                .filter(location -> !locationIdExist.contains(location.getId()))
                .collect(Collectors.toList());

        List<Location> locations = listLocationInsert.stream()
                .map(location -> locationMapper.locationDtoToLocation(location))
                .collect(Collectors.toList());

        for (Location location : locations) {
            location.setUser(user);
        }
        
        // location không bị lock
        List<Location> listLocationUpdate = listLocationExist.stream()
                .filter(location -> !location.isLocked())
                .collect(Collectors.toList());
        // id location không bị lock
        List<UUID> listIdLocationUpdate = listLocationUpdate.stream()
                .map(location -> location.getMrid())
                .collect(Collectors.toList());
        // location dto
        List<LocationDto> listLocationDtoUpdate = listLocation.stream()
                .filter(locationDto -> listIdLocationUpdate.contains(locationDto.getId()))
                .collect(Collectors.toList());
        // update
        List<Location> listUpdate = new ArrayList<>();
        for (LocationDto locationDto : listLocationDtoUpdate) {
            Location newLocation = locationRepository.findById(locationDto.getId()).get();
            newLocation = locationMapper.copyLocationDtoToLocation(locationDto,
                    newLocation, "id", "user_id", "collabs", "locked");
            listUpdate.add(newLocation);
        }

        locationRepository.saveAll(listUpdate);
        locationRepository.saveAll(locations);
        
        return ResponseObject.returnWithData(true);
    }

    @Override
    public void deleteLocation(UUID id) {
        String userID = BearerContextHolder.getContext().getUserId();
        Location location = getLocationByIdAndLoggedInUserID(id, UUID.fromString(userID));
        // Không lock mới được phép xóa
        if (!location.isLocked()) {
            locationRepository.delete(location);
        }
        else {
            throw new LocationLockedException("Please unlock location");
        }
    }

    /**
     * @param id
     */
    @Override
    public void deleteLocationData(UUID id) {
        Location location = locationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("location not found"));
        List<UUID> ids = sharedListIdService.getCachedIds();
        if(ids.contains(location.getCreatedBy())) {
            List<Location> locationList = locationRepository.findAllByRefId(location.getMrid().toString());
            if(locationList.isEmpty()) {
                if(!location.isLocked()) {
                    locationRepository.delete(location);
                } else {
                    throw new LocationNotFoundException("location locked");
                }
            } else {
                throw new LocationNotFoundException("location have child");
            }
        } else {
            throw new LocationNotFoundException("location not found in user list");
        }
    }

    @Override
    public LocationDto getLocationById(UUID id) {
        Location location = locationRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Location not found."));
        return locationMapper.locationToLocationDto(location);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public LocationDto getListLocationById(UUID id) {
        try {
            List<Location> location = locationRepository.getLocationById(id);
            if (location.isEmpty()) {
                return null;
            } else {
                return locationMapper.locationToLocationDto(location.get(0));
            }
        } catch (Exception e) {
            throw new RuntimeException("Cannot load location");
        }
    }

    /**
     * @param ids
     * @return
     */
    @Override
    public List<Object> getListLocation(List<UUID> ids) {
        if(!Objects.equals(BearerContextHolder.getContext().getRoleName(), "USER")) {
            List<Object> idsList = getAllCreatedByIdByRoleAndCreated(BearerContextHolder.getContext().getUserId(), "USER");
            List<Object> common = new ArrayList<>();
            for(Object data : idsList) {
                if (data instanceof User) {
                    User user = (User) data;
                    if(ids.contains(user.getId())){
                        common.add(userMapper.userToUserReqDto(user));
                    }
                } else {
                    OwnerUser ownerUser = (OwnerUser) data;
                    if(ids.contains(ownerUser.getId())){
                        OwnerUser ownerUser1 = (OwnerUser) data;
                        common.add(ownerUserMapper.userOwnerToUserOwnerReqDto(ownerUser1));
                    }
                }
            }
            return common;
        } else {
            List<Object> common = new ArrayList<>();
            User user = userRepository.findById(UUID.fromString(BearerContextHolder.getContext().getUserId())).orElseThrow(() -> new IllegalArgumentException("User parent not found"));
            List<User> users = userRepository.findAllUserByCreated(user.getCreatedBy());
            for(User user1: users) {
                if(ids.contains(user1.getId())){
                    common.add(userMapper.userToUserReqDto(user1));
                }
            }
            return common;
        }
    }

    @Override
    public List<LocationDto> getLocationByRefId(UUID id) {
        List<Location> locations = locationRepository.findAllByRefId(id.toString());
        List<LocationDto> locationDtoList = new ArrayList<>();
        for(Location item : locations) {
            LocationDto locationDto = locationMapper.locationToLocationDto(item);
            locationDtoList.add(locationDto);
        }
        return locationDtoList;
    }

    @Override
    public List<LocationDto> searchLocations(UUID id, Pageable pageable) {
        User user = null;
        if (id != null) {
            user = new User();
            user.setId(id);
        }
        String userID = BearerContextHolder.getContext().getUserId();
        LocationSearchSpecification locationSearchSpecification = new LocationSearchSpecification(user, false,
                UUID.fromString(userID), userID);
        List<Location> locations = locationRepository.findAll(locationSearchSpecification);
        List<LocationDto> locationDtoList = locations.stream()
                .map(location -> locationMapper.locationToLocationDto(location))
                .collect(Collectors.toList());
        return locationDtoList;
    }

    private Location getLocationByIdAndLoggedInUserID(UUID locationId, UUID loggedInUserId) {
        Optional<Location> location = locationRepository.findByMridAndIsDeletedAndCreatedBy(locationId, false,
                loggedInUserId);
        if (location.isEmpty()) {
            location = locationRepository.findByMridAndIsDeletedAndCollabsContains(locationId, false,
                    loggedInUserId.toString());
        }
        return location.orElseThrow(() -> new LocationNotFoundException("Location not found."));
    }

    public List<UUID> getAllCreatedIdByRoleAndCreated(String createdBy, String Role) {
        if(RoleEnum.getIntOfRole(Role) >= RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName())) {
            List<UUID> ids = new ArrayList<>();
            ids.add(UUID.fromString(createdBy));
            List<UUID> idsLocal = new ArrayList<>();
            idsLocal.add(UUID.fromString(createdBy));
            int i = RoleEnum.getIntOfRole(Role) - RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
            for(int j=0 ; j <i ; j++) {
                List<UUID> data = ownerUserRepository.findAllIdByCreated(idsLocal);
                if (data.isEmpty()) {
                    break;
                } else {
                    idsLocal = data;
                    ids.addAll(data);
                }

            }
            for(int k = 0; k<ids.size(); k++) {
                List<User> userList = userRepository.findAllUserByCreated(ids.get(k));
                if(!userList.isEmpty()) {
                    List<UUID> idU = userList.stream().map(user -> user.getId()).collect(Collectors.toList());
                    ids.addAll(idU);
                }
            }
            return ids;
        } else {
            return null;
        }
    }
    public List<Object> getAllCreatedByIdByRoleAndCreated(String createdBy, String Role) {
        if(RoleEnum.getIntOfRole(Role) >= RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName())) {
            List<UUID> ids = new ArrayList<>();
            List<Object> dataArr = new ArrayList<>();

            OwnerUser ownerUser = ownerUserRepository.findById(UUID.fromString(createdBy)).orElseThrow(() -> new IllegalArgumentException("Owner user not found"));

            dataArr.add(ownerUser);
            ids.add(UUID.fromString(createdBy));

            List<UUID> idsLocal = new ArrayList<>();
            idsLocal.add(UUID.fromString(createdBy));

            int i = RoleEnum.getIntOfRole(Role) - RoleEnum.getIntOfRole(BearerContextHolder.getContext().getRoleName());
            for(int j=0 ; j <i ; j++) {
                List<OwnerUser> dataOwner = ownerUserRepository.findAllOwnerUserByCreated(idsLocal);
                List<UUID> data = dataOwner.stream().map(user -> user.getId()).collect(Collectors.toList());
                if (data.isEmpty()) {
                    break;
                } else {
                    idsLocal = data;
                    ids.addAll(data);
                    dataArr.addAll(dataOwner);
                }
            }
            for(int k = 0; k<ids.size(); k++) {
                List<User> userList = userRepository.findAllUserByCreated(ids.get(k));
                if(!userList.isEmpty()) {
                    dataArr.addAll(userList);
                    List<UUID> idU = userList.stream().map(user -> user.getId()).collect(Collectors.toList());
                    ids.addAll(idU);
                }
            }
            return dataArr;
        } else {
            return null;
        }
    }
}
