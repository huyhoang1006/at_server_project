package vn.automationandtesting.atproject.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.AssetFullDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.cim.BushingDto;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.controller.dto.mapper.AssetMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.BushingMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.LocationMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.TapChangerMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Bushing;
import vn.automationandtesting.atproject.entity.cim.Location;
import vn.automationandtesting.atproject.entity.cim.TapChanger;
import vn.automationandtesting.atproject.exception.AssetLockedException;
import vn.automationandtesting.atproject.exception.AssetNotFoundException;
import vn.automationandtesting.atproject.exception.LocationNotFoundException;
import vn.automationandtesting.atproject.repository.AssetRepository;
import vn.automationandtesting.atproject.repository.BushingRepository;
import vn.automationandtesting.atproject.repository.LocationRepository;
import vn.automationandtesting.atproject.repository.TapChangerRepository;
import vn.automationandtesting.atproject.service.AssetService;
import vn.automationandtesting.atproject.service.LocationService;
import vn.automationandtesting.atproject.service.sharedListId.SharedListIdService;
import vn.automationandtesting.atproject.util.search.AssetSearchSpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
@Service
public class AssetServiceImpl implements AssetService {
    Logger logger = LoggerFactory.getLogger(AssetServiceImpl.class);

    @Autowired
    private AssetRepository assetRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private AssetMapper assetMapper;
    @Autowired
    private TapChangerRepository tapChangerRepository;
    @Autowired
    private BushingRepository bushingRepository;
    @Autowired
    private TapChangerMapper tapChangerMapper;
    @Autowired
    private BushingMapper bushingMapper;
    @Autowired
    private LocationService locationService;
    @Autowired
    private LocationMapper locationMapper;

    @Autowired
    private SharedListIdService sharedListIdService;

    @Override
    public List<AssetDto> getAllAssets() {
        List<Asset> assets = assetRepository.findAllByIsDeleted(false);
        List<AssetDto> assetDtoList = assets.stream().map(asset -> assetMapper.assetToAssetDto(asset))
                .collect(Collectors.toList());
        return assetDtoList;
    }

    @Override
    public AssetDto createNewAsset(AssetDto assetDto) {
        Asset asset = assetMapper.assetDtoToAsset(assetDto);
        if(assetDto.getId() != null) {
            asset.setMrid(assetDto.getId());
        } else {
            asset.setMrid(UUID.randomUUID());
        }
        Location location = locationRepository.findByMridAndIsDeleted(asset.getLocation_id(), false)
                .orElseThrow(() -> new LocationNotFoundException("Location id not existed."));
        asset.setLocation(location);
        Asset savedAsset = assetRepository.save(asset);
        return assetMapper.assetToAssetDto(savedAsset);
    }

    @Override
    public Asset insertIfNotExist(AssetFullDto assetFullDto) {
        AssetDto assetDto = assetFullDto.getAsset();
        BushingDto bushingDto = assetFullDto.getBushing();
        TapChangerDto tapChangerDto = assetFullDto.getTapChanger();

        Asset asset = assetMapper.assetDtoToAsset(assetDto);
        Bushing bushing = bushingMapper.bushingDtoToBushing(bushingDto);
        TapChanger tapChanger = tapChangerMapper.tapChangerDtoToTapChanger(tapChangerDto);

        UUID assetId = asset.getMrid();
        Asset assetExist = assetRepository.findById(assetId).orElse(null);
        if (assetExist == null) {
            UUID locationId = asset.getLocation_id();
            Location location = locationRepository.findById(locationId).get();
            asset.setLocation(location);
            Asset newAsset = assetRepository.save(asset);

            bushing.setAsset(newAsset);
            bushingRepository.save(bushing);

            tapChanger.setAssets(newAsset);
            tapChangerRepository.save(tapChanger);

            return newAsset;
        }
        else {
            if (!assetExist.isLocked()) {
                assetExist = assetMapper.copyAssetDtoToAsset(assetDto, assetExist, "mrid", "location_id", "collabs", 
                        "locked");
                
                TapChanger tapChangerExist = tapChangerRepository.findById(tapChangerDto.getId()).get();
                tapChangerExist = tapChangerMapper.copyTapChangerDtoToTapChanger(tapChangerDto, tapChangerExist,"id");

                Bushing bushingExist = bushingRepository.findById(bushingDto.getId()).get();
                bushingExist = bushingMapper.copyBushingDtoToBushing(bushingDto, bushingExist, "id");

                bushingRepository.save(bushingExist);
                tapChangerRepository.save(tapChangerExist);
                return assetRepository.save(assetExist);
            }
            else {
                return assetExist;
            }
        }
    }

    @Override
    public AssetDto updateAsset(AssetDto assetDto, UUID id) {
        // Asset asset = assetRepository.findByMridAndIsDeleted(id, false)
        // .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        String userID = BearerContextHolder.getContext().getUserId();
        Asset asset = getAssetByIdAndLoggedInUserID(id, UUID.fromString(userID));
        if (asset.isLocked()) {
            if (!asset.getLocked_by().toString().equals(userID)) {
                throw new AssetLockedException("Asset already locked by another user.");
            }
        } else {
            if (assetDto.isLocked()) {
                asset.setLocked_by(UUID.fromString(userID));
            }
        }
        // do not allow to change "id", "location_id"
        asset = assetMapper.copyAssetDtoToAsset(assetDto, asset, "mrid", "location_id", "collabs");
        Asset updatedAsset = assetRepository.save(asset);
        return assetMapper.assetToAssetDto(updatedAsset);
    }

    /**
     * @param assetDto
     * @param id
     * @return
     */
    @Override
    public AssetDto updateAssetLite(AssetDto assetDto, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Asset asset = assetRepository.findAssetById(id, ids, userId);
            List<String> acceptProperties = new ArrayList<>();
            acceptProperties.add("manufacturer");
            acceptProperties.add("manufacturer_type");
            acceptProperties.add("manufacturing_year");
            acceptProperties.add("asset_system_code");
            acceptProperties.add("apparatus_id");
            acceptProperties.add("feeder");
            acceptProperties.add("comment");
            Asset temp = assetMapper.copyAssetDtoToAssetLite(assetDto, asset, acceptProperties);
            assetRepository.save(temp);
            return assetDto;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public void deleteAsset(UUID id) {
        String userID = BearerContextHolder.getContext().getUserId();
        Asset asset = getAssetByIdAndLoggedInUserID(id, UUID.fromString(userID));
        // Không lock mới được phép xóa
        if (!asset.isLocked()) {
            assetRepository.delete(asset);
        } else {
            throw new AssetLockedException("Please unlock asset");
        }
    }

    @Override
    public AssetDto getAssetByIdAndUserIdAndCollabId(UUID id, UUID loggedInUserId) {
        Asset asset = getAssetByIdAndLoggedInUserID(id, loggedInUserId);
        return assetMapper.assetToAssetDto(asset);
    }

    @Override
    public List<AssetDto> searchAssets(String locationId, String serialNo, Pageable pageable) {
        Location location = new Location();
        try {
            location.setMrid(UUID.fromString(locationId));
        } catch (Exception ex) {
            location = null;
        }
        String userID = BearerContextHolder.getContext().getUserId();

        AssetSearchSpecification assetSearchSpecification = new AssetSearchSpecification(serialNo, location, false,
                UUID.fromString(userID), userID);
        List<Asset> assets = assetRepository.findAll(assetSearchSpecification, pageable).toList();
        List<AssetDto> assetDtos = assets.stream().map(asset -> assetMapper.assetToAssetDto(asset))
                .collect(Collectors.toList());
        return assetDtos;
    }

    private Asset getAssetByIdAndLoggedInUserID(UUID assetId, UUID loggedInUserId) {
        Optional<Asset> asset = assetRepository.findByMridAndIsDeletedAndCreatedBy(assetId, false, loggedInUserId);
        if (asset.isEmpty()) {
            asset = assetRepository.findByMridAndIsDeletedAndCollabsContains(assetId, false, loggedInUserId.toString());
        }
        return asset.orElseThrow(() -> new AssetNotFoundException("Asset not found."));
    }

    @Override
    public ResponseObject lock(Boolean locked, List<UUID> listId) {
        String userID = BearerContextHolder.getContext().getUserId();
        List<Asset> assets = assetRepository.findAllById(listId);
        for (Asset asset : assets) {
            if (!(asset.isLocked() && !asset.getLocked_by().toString().equals(userID))) {
                asset.setLocked(locked);
                if (locked) {
                    asset.setLocked_by(UUID.fromString(userID));
                }
            }
        }
        assetRepository.saveAll(assets);

        return ResponseObject.returnWithData(true);
    }

    /**
     * @param locked
     * @return
     */
    @Override
    public Boolean changeLock(Boolean locked, UUID id) {
        try {
            List<UUID> ids = sharedListIdService.getCachedIds();
            String userId = BearerContextHolder.getContext().getUserId();
            Asset asset = assetRepository.findAssetById(id, ids, userId);
            asset.setLocked(locked);
            assetRepository.save(asset);
            return true;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ResponseObject download(List<UUID> listId) {
        List<Asset> assets = assetRepository.findAllById(listId);
        List<ResourceFullDto> res = new ArrayList<>();

        for (Asset asset : assets) {
            UUID assetId = asset.getMrid();
            TapChanger tapChanger = tapChangerRepository.findFirstByAssetId(assetId);
            Bushing bushing = bushingRepository.findFirstByAssetId(assetId);

            AssetDto assetDto = assetMapper.assetToAssetDto(asset);
            TapChangerDto tapChangerDto = tapChangerMapper.tapChangerToTapChangerDto(tapChanger);
            BushingDto bushingDto = bushingMapper.bushingToBushingDto(bushing);

            AssetFullDto assetFullDto = new AssetFullDto(assetDto, bushingDto, tapChangerDto);
            Location location = asset.getLocation();
            LocationDto locationDto = locationMapper.locationToLocationDto(location);
            res.add(new ResourceFullDto(null, null, null, locationDto, assetFullDto, null));
        }

        return ResponseObject.returnWithData(res);
    }

    @Override
    public ResponseObject upload(ResourceFullDto resourceFullDto) {
        List<LocationDto> listLocationDto = resourceFullDto.getListLocation();
        Location location = locationService.insertIfNotExist(listLocationDto.get(0));
        for(int i=1; i < listLocationDto.size(); i ++) {
            locationService.insertIfNotExist(listLocationDto.get(i));
        }

        List<AssetFullDto> listFullAssetDto = resourceFullDto.getListAsset();
        List<UUID> assetDtoId = listFullAssetDto.stream()
                .map(fullAssetDto -> fullAssetDto.getAsset().getId())
                .collect(Collectors.toList());

        List<Asset> listAssetExist = assetRepository.findAllById(assetDtoId);
        List<UUID> listAssetIdExist = listAssetExist.stream()
                .map(asset -> asset.getMrid())
                .collect(Collectors.toList());

        List<AssetFullDto> listFullAssetDtoInsert = listFullAssetDto.stream()
                .filter(fullAssetDto -> !listAssetIdExist.contains(fullAssetDto.getAsset().getId()))
                .collect(Collectors.toList());

        List<Asset> listAsset = listFullAssetDtoInsert.stream()
                .map(fullAssetDto -> assetMapper.assetDtoToAsset(fullAssetDto.getAsset()))
                .collect(Collectors.toList());
        List<Bushing> listBushing = listFullAssetDtoInsert.stream()
                .map(fullAssetDto -> bushingMapper.bushingDtoToBushing(fullAssetDto.getBushing()))
                .collect(Collectors.toList());

        logger.info(String.valueOf(resourceFullDto.getAsset()));

        List<TapChanger> listTapChanger = listFullAssetDtoInsert.stream()
                .map(fullAssetDto -> tapChangerMapper.tapChangerDtoToTapChanger(fullAssetDto.getTapChanger()))
                .collect(Collectors.toList());

        for (int i = 0; i < listAsset.size(); i++) {
            Asset asset = listAsset.get(i);
            Bushing bushing = listBushing.get(i);
            TapChanger tapChanger = listTapChanger.get(i);

            asset.setLocation(location);
            bushing.setAsset(asset);
            tapChanger.setAssets(asset);
        }

        // asset không bị lock
        List<Asset> listAssetUpdate = listAssetExist.stream()
                .filter(asset -> !asset.isLocked())
                .collect(Collectors.toList());
        // id asset không bị lock
        List<UUID> listIdAssetUpdate = listAssetUpdate.stream()
                .map(asset -> asset.getMrid())
                .collect(Collectors.toList());
        // asset full dto
        List<AssetFullDto> listAssetFullDtoUpdate = listFullAssetDto.stream()
                .filter(assetFullDto -> listIdAssetUpdate.contains(
                        assetFullDto.getAsset().getId()))
                .collect(Collectors.toList());
        // update
        List<Asset> _listAssetUpdate = new ArrayList<>();
        List<Bushing> listBushingUpdate = new ArrayList<>();
        List<TapChanger> listTapChangerUpdate = new ArrayList<>();
        for (AssetFullDto assetFullDto : listAssetFullDtoUpdate) {
            Asset newAsset = assetRepository.findById(assetFullDto.getAsset().getId()).get();
            newAsset = assetMapper.copyAssetDtoToAsset(
                    assetFullDto.getAsset(),
                    newAsset, "mrid", "location_id", "collabs", "locked");
            _listAssetUpdate.add(newAsset);
            System.out.println(assetFullDto.getBushing().getId());
            Bushing newBushing = bushingRepository.findById(assetFullDto.getBushing().getId()).get();
            newBushing = bushingMapper.copyBushingDtoToBushing(assetFullDto.getBushing(), newBushing, "id", "asset_id");
            listBushingUpdate.add(newBushing);

            TapChanger newTapChanger = tapChangerRepository.findById(assetFullDto.getTapChanger().getId()).orElse(null);
            newTapChanger = tapChangerMapper.copyTapChangerDtoToTapChanger(assetFullDto.getTapChanger(), newTapChanger,
                    "id", "asset_id");
            listTapChangerUpdate.add(newTapChanger);
        }

        assetRepository.saveAll(_listAssetUpdate);
        bushingRepository.saveAll(listBushingUpdate);
        tapChangerRepository.saveAll(listTapChangerUpdate);

        assetRepository.saveAll(listAsset);
        bushingRepository.saveAll(listBushing);
        tapChangerRepository.saveAll(listTapChanger);

        return ResponseObject.returnWithData(true);
    }

    @Override
    public List<AssetDto> getAssetByLocationIdAndUserIdAndCollabId(UUID loctionID, UUID loggedInUserId) {
        List<Asset> asset = getAssetByLocationIdAndLoggedInUserID(loctionID, loggedInUserId);
        List<AssetDto> assetDtos = new ArrayList<>();
        if(asset.size() != 0) {
            for(Asset data : asset) {
                AssetDto assetDto = assetMapper.assetToAssetDto(data);
                assetDtos.add(assetDto);
            }
        }
        return assetDtos;
    }

    /**
     * @param loctionID
     * @param first
     * @param sl
     * @return
     */
    @Override
    public List<AssetDto> findAssetByLocationId(UUID loctionID, int first, int sl) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Asset> assetList = assetRepository.findAssetByLocationId(loctionID, user_id, userId, first, sl );
        List<AssetDto> assetDtoList = new ArrayList<>();
        if(!assetList.isEmpty()) {
            for(Asset asset : assetList) {
                AssetDto assetDto = assetMapper.assetToAssetDto(asset);
                assetDtoList.add(assetDto);
            }
        }
        return assetDtoList;
    }

    @Override
    public int countAssetByLocationId(UUID locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        int count = assetRepository.countAssetByLocationId(locationId, user_id, userId);
        return count;
    }

    /**
     * @return
     */
    @Override
    public int countAssetList() {
        List<UUID> ids = sharedListIdService.getCachedIds();
        return assetRepository.countAssetList(ids, BearerContextHolder.getContext().getUserId());
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<AssetDto> findAssetList(int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Asset> assets = assetRepository.findAssetList(ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<AssetDto> assetDtoList = new ArrayList<>();
        for(Asset asset : assets) {
           AssetDto assetDto = assetMapper.assetToAssetDto(asset);
           assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param stt
     * @param sl
     * @return
     */
    @Override
    public List<AssetDto> findAssetListByLocationId(UUID location_id, int stt, int sl) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        List<Asset> assets = assetRepository.findAssetListByLocationId(location_id,ids, BearerContextHolder.getContext().getUserId(), stt, sl);
        List<AssetDto> assetDtoList = new ArrayList<>();
        for(Asset asset : assets) {
            AssetDto assetDto = assetMapper.assetToAssetDto(asset);
            assetDtoList.add(assetDto);
        }
        return assetDtoList;
    }

    /**
     * @param location_id
     * @return
     */
    @Override
    public int countAssetListByLocationId(UUID location_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        String userId = BearerContextHolder.getContext().getUserId();
        int count = assetRepository.countAssetListByLocationId(location_id, ids, userId);
        return count;
    }

    /**
     * @param asset_id
     * @param user_id
     * @return
     */
    @Override
    public AssetDto getAssetById(UUID asset_id, String user_id) {
        List<UUID> ids = sharedListIdService.getCachedIds();
        try {
            Asset asset = assetRepository.findAssetById(asset_id, ids, user_id);
            return assetMapper.assetToAssetDto(asset);
        } catch (Exception e) {
            return null;
        }
    }

    private List<Asset> getAssetByLocationIdAndLoggedInUserID(UUID locationId, UUID loggedInUserId) {
        List<Asset> assetList = new ArrayList<>();
        List<Asset> assets = assetRepository.findAllByLocation_idAndIsDeletedAndCreatedBy(locationId, false, loggedInUserId);
        List<Asset> assetCollabs = assetRepository.findAllByLocation_idAndIsDeletedAndCollabs(locationId, false, loggedInUserId.toString());
        assetList.addAll(assets);
        assetList.addAll(assetCollabs);
        return assetList;
    }
}
