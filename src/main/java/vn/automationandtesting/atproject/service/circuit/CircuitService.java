package vn.automationandtesting.atproject.service.circuit;

import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.entity.circuit.Circuit;

import java.util.List;
import java.util.UUID;

public interface CircuitService {
    List<CircuitDto> findAllByLocationId(UUID location_id);

    List<CircuitDto> findAssetByLocationId(UUID id, int first, int limit);

    int countAssetByLocationId(UUID locationId);

    List<CircuitDto> findAssetById(UUID id);

    List<CircuitDto> findBySerial(String serial);

    CircuitDto updateAssetLite(CircuitDto circuitDto, UUID id);

    List<CircuitDto> findBySerialAndLocation(String serial, UUID location_id);

    List<CircuitDto> findAll();

    List<CircuitDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Circuit> circuits);

    void deleteAll(List<Circuit> circuits);

    int countAssetList();
    List<CircuitDto> findAssetList(int stt, int sl);

    List<CircuitDto> findAssetListByLocationId(UUID location_id, int stt, int sl);
    int countAssetListByLocationId(UUID location_id);

    CircuitDto getAssetById(UUID asset_id, String user_id);
}
