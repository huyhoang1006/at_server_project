SET search_path to at_project_schema;
/*Association*/
--Asset
ALTER TABLE "asset"
    ADD Column "power_system_resource" UUID,
    ADD Column "activity_record" UUID,
    ADD Column "configuration_event" UUID,
    ADD Column "asset_info" UUID,
    ADD Column "location" UUID
;
ALTER TABLE "asset"
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("activity_record") REFERENCES "activity_record"("mrid"),
    ADD FOREIGN KEY ("configuration_event") REFERENCES "configuration_event"("mrid"),
    ADD FOREIGN KEY ("asset_info") REFERENCES "asset_info"("mrid"),
    ADD FOREIGN KEY ("location") REFERENCES "location"("mrid")
;
--TransformerTest
--ShortCircuiteTest
--assetInfo
ALTER TABLE "asset_info"
    ADD Column "power_system_resource" UUID,
    ADD Column "asset" UUID,
    ADD Column "asset_model" UUID
;
ALTER TABLE "asset_info"
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid"),
    ADD FOREIGN KEY ("asset_model") REFERENCES "asset_model"("mrid")
;
--assetModel
ALTER TABLE "asset_model"
    ADD Column "asset_info" UUID
;
ALTER TABLE "asset_model"
    ADD FOREIGN KEY ("asset_info") REFERENCES "asset_info"("mrid")
;
--Manufacturer
ALTER TABLE "manufacturer"
    ADD Column "product_asset_model" UUID,
    ADD Column "configuration_event" UUID
;
ALTER TABLE "manufacturer"
    ADD FOREIGN KEY ("product_asset_model") REFERENCES "product_asset_model"("mrid"),
    ADD FOREIGN KEY ("configuration_event") REFERENCES "configuration_event"("mrid")
;
--ProductassetModel
ALTER TABLE "product_asset_model"
    ADD Column "manufacturer" UUID
;
ALTER TABLE "product_asset_model"
    ADD FOREIGN KEY ("manufacturer") REFERENCES "manufacturer"("mrid")
;
-------------------------------
--TransformerEndInfo
ALTER TABLE "transformer_end_info"
    ADD Column "energised_end_no_load_test" UUID,
    ADD Column "open_end_open_circuit_test" UUID,
    ADD Column "energised_end_open_circuit_test" UUID,
    ADD Column "grounded_end_short_circuit_test" UUID,
    ADD Column "energised_end_short_circuit_test" UUID,
    ADD Column "transformer_tank_info" UUID
;
ALTER TABLE "transformer_end_info"
    ADD FOREIGN KEY ("energised_end_no_load_test") REFERENCES "no_load_test"("mrid"),
    ADD FOREIGN KEY ("open_end_open_circuit_test") REFERENCES "open_circuit_test"("mrid"),
    ADD FOREIGN KEY ("energised_end_open_circuit_test") REFERENCES "open_circuit_test"("mrid"),
    ADD FOREIGN KEY ("grounded_end_short_circuit_test") REFERENCES "short_circuit_test"("mrid"),
    ADD FOREIGN KEY ("energised_end_short_circuit_test") REFERENCES "short_circuit_test"("mrid"),
    ADD FOREIGN KEY ("transformer_tank_info") REFERENCES "transformer_tank_info"("mrid")
;
--PositionPoint
ALTER TABLE "position_point"
    ADD Column "location" UUID
;
ALTER TABLE "position_point"
    ADD FOREIGN KEY ("location") REFERENCES "location"("mrid")
;
--PowerTransformerInfo
ALTER TABLE "power_transformer_info"
    ADD Column "transformer_tank_info" UUID
;
ALTER TABLE "power_transformer_info"
    ADD FOREIGN KEY ("transformer_tank_info") REFERENCES "transformer_tank_info"("mrid"),
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid"),
    ADD FOREIGN KEY ("asset_model") REFERENCES "asset_model"("mrid")
;

--TransformerTankInfo
ALTER TABLE "transformer_tank_info"
    ADD Column "power_transformer_info" UUID,
    ADD Column "transformer_end_info" UUID
;
ALTER TABLE "transformer_tank_info"
    ADD FOREIGN KEY ("power_transformer_info") REFERENCES "power_transformer_info"("mrid"),
    ADD FOREIGN KEY ("transformer_end_info") REFERENCES "transformer_end_info"("mrid"),
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid"),
    ADD FOREIGN KEY ("asset_model") REFERENCES "asset_model"("mrid")
;
--ActivityRecord
ALTER TABLE "activity_record"
    ADD Column "asset" UUID
;
ALTER TABLE "activity_record"
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid")
;
--ConfigurationEvent
ALTER TABLE "configuration_event"
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid")
;
--CoordinateSystem
ALTER TABLE "coordinate_system"
    ADD Column "location" UUID
;
ALTER TABLE "coordinate_system"
    ADD FOREIGN KEY ("location") REFERENCES "location"("mrid")
;
--TapChangerInfo
ALTER TABLE "tap_changer_info"
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid"),
    ADD FOREIGN KEY ("asset_model") REFERENCES "asset_model"("mrid")
;
--Location
ALTER TABLE "location"
    ADD Column "configuration_event" UUID,
    ADD Column "coordinate_system" UUID,
    ADD Column "position_point" UUID,
    ADD Column "power_system_resource" UUID,
    ADD Column "asset" UUID,
    ADD Column "asset_model" UUID
;
ALTER TABLE "location"
    ADD FOREIGN KEY ("power_system_resource") REFERENCES "power_system_resource"("mrid"),
    ADD FOREIGN KEY ("configuration_event") REFERENCES "configuration_event"("mrid"),
    ADD FOREIGN KEY ("coordinate_system") REFERENCES "coordinate_system"("mrid"),
    ADD FOREIGN KEY ("position_point") REFERENCES "position_point"("mrid"),
    ADD FOREIGN KEY ("asset_model") REFERENCES "asset_model"("mrid"),
    ADD FOREIGN KEY ("asset") REFERENCES "asset"("mrid")
;