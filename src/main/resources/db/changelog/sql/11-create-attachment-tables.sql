SET search_path to at_project_schema;
CREATE TABLE "attachment"
(
    "id"      UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "id_foreign" UUID not null,
    "type"    text,
    "name"    text,
    "path"    text,
    "created_by"        UUID,
    "created_on"        TIMESTAMP default CURRENT_TIMESTAMP,
    "updated_by"        UUID,
    "updated_on"        TIMESTAMP,
    "is_deleted"      BOOL      default 'f'
);