SET search_path to at_project_schema;

CREATE TABLE "Location"
(
--     "id"                     UUID default public.uuid_generate_v4(),
    "user_id"                UUID NOT NULL,
--     "name"                   VARCHAR(255) NOT NULL unique,
    "region"                 VARCHAR(255),
    "division"               INTEGER,
    "area"                   VARCHAR(255),
    "plant"                  VARCHAR(255),
    "address"                VARCHAR(255),
    "city"                   VARCHAR(255),
    "state_province"         VARCHAR(255),
    "postal_code"            VARCHAR(255),
    "country"                VARCHAR(255),
--     "geo_coordinates"        VARCHAR(255),
    "location_system_code"   VARCHAR(255),
    "person_name"            VARCHAR(255),
    "person_phone_no1"       VARCHAR(255),
    "person_phone_no2"       VARCHAR(255),
    "person_fax_no"          VARCHAR(255),
    "person_email"           VARCHAR(255),
    "comment"                VARCHAR(255),
    "company_company"        VARCHAR(255),
    "company_department"     VARCHAR(255),
    "company_address"        VARCHAR(255),
    "company_city"           VARCHAR(255),
    "company_state_province" VARCHAR(255),
    "company_postal_code"    VARCHAR(255),
    "company_country"        VARCHAR(255),
    "company_phone_no"       VARCHAR(255),
    "company_fax_no"         VARCHAR(255),
    "company_email"          VARCHAR(255),
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Location.userID"
        FOREIGN KEY ("userID")
            REFERENCES "User" ("id")
);

CREATE TABLE "Asset"
(
--     "id"                           UUID default public.uuid_generate_v4(),
    "locationID"                   UUID not null,
    "asset"                        VARCHAR(255),
--     "asset_type"                   VARCHAR(255),
--     "serial_no"                    VARCHAR(255),
    "manufacturer"                 VARCHAR(255),
    "manufacturer_type"            VARCHAR(255),
    "manufacturing_year"           VARCHAR(255),
    "asset_system_code"            VARCHAR(255),
    "apparatus_id"                 VARCHAR(255),
    "feeder"                       VARCHAR(255),
    "date_of_warehouse_receipt"    VARCHAR(255),
    "date_of_delivery"             VARCHAR(255),
    "date_of_production_order"     VARCHAR(255),
    "comment"                      VARCHAR(255),
    "phases"                       VARCHAR(255),
    "vector_group"                 VARCHAR(255),
    "vector_group_custom"          VARCHAR(255),
    "unsupported_vector_group"     VARCHAR(255),
    "rated_frequency"              VARCHAR(255),
    "voltage_ratings"              VARCHAR(255),
    "voltage_regulation"           VARCHAR(255),
    "power_ratings"                VARCHAR(255),
    "current_ratings"              VARCHAR(255),
    "max_short_circuit_current_ka" VARCHAR(255),
    "max_short_circuit_current_s"  VARCHAR(255),
    "ref_temp"                     VARCHAR(255),
    "prim_sec"                     VARCHAR(255),
    "prim_tert"                    VARCHAR(255),
    "sec_tert"                     VARCHAR(255),
    "base_power"                   VARCHAR(255),
    "base_voltage"                 VARCHAR(255),
    "zero_percent"                 VARCHAR(255),
    "category"                     VARCHAR(255),
    "status"                       VARCHAR(255),
    "tank_type"                    VARCHAR(255),
    "insulation_medium"            VARCHAR(255),
    "insulation_weight"            VARCHAR(255),
    "insulation_volume"            VARCHAR(255),
    "total_weight"                 VARCHAR(255),
    "winding"                      VARCHAR(255),
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Asset.locationID"
        FOREIGN KEY ("locationID")
            REFERENCES "Location" ("id")
);

CREATE TABLE "Bushing"
(
    "id"                UUID default public.uuid_generate_v4(),
    "assetID"           UUID not null,
    "asset_type"        TEXT,
    "serial_no"         TEXT,
    "manufacturer"      TEXT,
    "manufacturer_type" TEXT,
    "manufacturer_year" TEXT,
    "insull_level"      TEXT,
    "voltage_gr"        TEXT,
    "max_sys_voltage"   TEXT,
    "rate_current"      TEXT,
    "df_c1"             TEXT,
    "cap_c1"            TEXT,
    "df_c2"             TEXT,
    "cap_c2"            TEXT,
    "insulation_type"   TEXT,
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Bushing.assetID"
        FOREIGN KEY ("assetID")
            REFERENCES "Asset" ("id")
);

CREATE TABLE "Tap_Changer"
(
--     "id"                UUID default public.uuid_generate_v4(),
    "assetID"           UUID         not null,
    "mode"              VARCHAR(255) not null,
    "serial_no"         VARCHAR(255) not null,
    "manufacturer"      VARCHAR(255),
    "manufacturer_type" VARCHAR(255),
    "winding"           VARCHAR(255),
    "tap_scheme"        VARCHAR(255),
    "no_of_taps"        INTEGER,
    "voltage_table"     VARCHAR(255),
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Tap_Changer.assetID"
        FOREIGN KEY ("assetID")
            REFERENCES "Asset" ("id")
);

CREATE TABLE "Job"
(
    "id"                UUID default public.uuid_generate_v4(),
    "assetID"           UUID not null,
    "name"              VARCHAR(255),
    "work_order"        VARCHAR(255),
    "creation_date"     VARCHAR(255),
    "execution_date"    VARCHAR(255),
    "tested_by"         VARCHAR(255),
    "approved_by"       VARCHAR(255),
    "approval_date"     VARCHAR(255),
    "summary"           VARCHAR(255),
    "ambient_condition" VARCHAR(255),
    "testing_method"    VARCHAR(255),
    "standard"          INTEGER,
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Job.assetID"
        FOREIGN KEY ("assetID")
            REFERENCES "Asset" ("id")
);

CREATE TABLE "test_type"
(
    "id"   UUID default public.uuid_generate_v4(),
    "code" varchar(255),
    "name" varchar(255)
);

CREATE TABLE "test"
(
    "id"      UUID default public.uuid_generate_v4(),
    "type_id" UUID not null,
    "name"    varchar(255),
    "data"    text,
    FOREIGN KEY ("type_id") REFERENCES "test_type" ("id") ON DELETE CASCADE
);
