SET search_path TO at_project_schema;

ALTER TABLE at_project_schema.location
    ADD COLUMN IF NOT EXISTS locked    boolean      default false,
    ADD COLUMN IF NOT EXISTS locked_by UUID         default NULL,
    ADD COLUMN IF NOT EXISTS collabs   varchar(255) default '';

ALTER TABLE at_project_schema.asset
    ADD COLUMN IF NOT EXISTS locked  boolean      default false,
    ADD COLUMN IF NOT EXISTS locked_by UUID         default NULL,
    ADD COLUMN IF NOT EXISTS collabs varchar(255) default '';

ALTER TABLE at_project_schema.job
    ADD COLUMN IF NOT EXISTS locked  boolean      default false,
    ADD COLUMN IF NOT EXISTS locked_by UUID         default NULL,
    ADD COLUMN IF NOT EXISTS collabs varchar(255) default '';
