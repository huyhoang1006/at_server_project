CREATE
EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;

ALTER TABLE "location"
    ADD COLUMN "user_id" UUID NOT NULL REFERENCES "user" ("id") ON DELETE CASCADE,
    ADD COLUMN "region"                 TEXT,
    ADD COLUMN "division"               INTEGER,
    ADD COLUMN "area"                   TEXT,
    ADD COLUMN "plant"                  TEXT,
    ADD COLUMN "address"                TEXT,
    ADD COLUMN "city"                   TEXT,
    ADD COLUMN "state_province"         TEXT,
    ADD COLUMN "postal_code"            TEXT,
    ADD COLUMN "country"                TEXT,
    ADD COLUMN "location_system_code"   TEXT,
    ADD COLUMN "person_name"            TEXT,
    ADD COLUMN "person_phone_no1"       TEXT,
    ADD COLUMN "person_phone_no2"       TEXT,
    ADD COLUMN "person_fax_no"          TEXT,
    ADD COLUMN "person_email"           TEXT,
    ADD COLUMN "comment"                TEXT,
    ADD COLUMN "company_company"        TEXT,
    ADD COLUMN "company_department"     TEXT,
    ADD COLUMN "company_address"        TEXT,
    ADD COLUMN "company_city"           TEXT,
    ADD COLUMN "company_state_province" TEXT,
    ADD COLUMN "company_postal_code"    TEXT,
    ADD COLUMN "company_country"        TEXT,
    ADD COLUMN "company_phone_no"       TEXT,
    ADD COLUMN "company_fax_no"         TEXT,
    ADD COLUMN "company_email"          TEXT,
    ADD COLUMN "mode"                   TEXT,
    ADD COLUMN "ref_id"                 TEXT
;

ALTER TABLE "asset"
    ADD COLUMN "location_id" UUID not null REFERENCES "location" ("mrid") ON DELETE CASCADE,
    ADD COLUMN "asset"                        TEXT,
    ADD COLUMN "manufacturer"                 TEXT,
    ADD COLUMN "manufacturer_type"            TEXT,
    ADD COLUMN "manufacturing_year"           TEXT,
    ADD COLUMN "asset_system_code"            TEXT,
    ADD COLUMN "apparatus_id"                 TEXT,
    ADD COLUMN "feeder"                       TEXT,
    ADD COLUMN "date_of_warehouse_receipt"    TEXT,
    ADD COLUMN "date_of_delivery"             TEXT,
    ADD COLUMN "date_of_production_order"     TEXT,
    ADD COLUMN "comment"                      TEXT,
    ADD COLUMN "phases"                       TEXT,
    ADD COLUMN "vector_group"                 TEXT,
    ADD COLUMN "vector_group_custom"          TEXT,
    ADD COLUMN "unsupported_vector_group"     TEXT,
    ADD COLUMN "rated_frequency"              TEXT,
    ADD COLUMN "voltage_ratings"              TEXT,
    ADD COLUMN "voltage_regulation"           TEXT,
    ADD COLUMN "power_ratings"                TEXT,
    ADD COLUMN "current_ratings"              TEXT,
    ADD COLUMN "max_short_circuit_current_ka" TEXT,
    ADD COLUMN "max_short_circuit_current_s"  TEXT,
    ADD COLUMN "ref_temp"                     TEXT,
    ADD COLUMN "prim_sec"                     TEXT,
    ADD COLUMN "prim_tert"                    TEXT,
    ADD COLUMN "sec_tert"                     TEXT,
    ADD COLUMN "base_power"                   TEXT,
    ADD COLUMN "base_voltage"                 TEXT,
    ADD COLUMN "zero_percent"                 TEXT,
    ADD COLUMN "category"                     TEXT,
    ADD COLUMN "asset_status"                 TEXT,
    ADD COLUMN "tank_type"                    TEXT,
    ADD COLUMN "insulation_medium"            TEXT,
    ADD COLUMN "insulation_weight"            TEXT,
    ADD COLUMN "insulation_volume"            TEXT,
    ADD COLUMN "total_weight"                 TEXT,
    ADD COLUMN "winding"                      TEXT,
    ADD COLUMN "date_of_warehouse_delivery"   TEXT,
    ADD COLUMN "progress"                     TEXT,
    ADD COLUMN "standard"                     TEXT,
    ADD COLUMN "oil_type"                     TEXT,
    ADD COLUMN "thermal_meter"                TEXT,
    ADD COLUMN "extend"                       TEXT
;

ALTER TABLE "tap_changer_info"
    ADD COLUMN "asset_id" UUID not null REFERENCES "asset" ("mrid") ON DELETE CASCADE,
    ADD COLUMN "mode"              TEXT not null,
    ADD COLUMN "serial_no"         TEXT not null,
    ADD COLUMN "manufacturer"      TEXT,
    ADD COLUMN "manufacturer_type" TEXT,
    ADD COLUMN "winding"           TEXT,
    ADD COLUMN "tap_scheme"        TEXT,
    ADD COLUMN "no_of_taps"        INTEGER,
    ADD COLUMN "voltage_table"     TEXT
;

CREATE TABLE "test_type"
(
    "id"   UUID default public.uuid_generate_v4() primary key,
    "code" TEXT,
    "name" TEXT
);

CREATE TABLE "test"
(
    "id"      UUID default public.uuid_generate_v4() primary key,
    "type_id" UUID not null REFERENCES "test_type" ("id"),
    "name"    text,
    "data"    text,
    "job_id"  UUID not null REFERENCES "job" ("id") ON DELETE CASCADE
);