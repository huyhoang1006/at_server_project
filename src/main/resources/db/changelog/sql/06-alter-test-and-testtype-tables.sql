ALTER TABLE at_project_schema.test
ADD COLUMN created_by uuid NOT NULL,
ADD COLUMN created_on timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN updated_by uuid,
ADD COLUMN updated_on timestamp without time zone,
ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;

ALTER TABLE at_project_schema.test_type
ADD COLUMN created_by uuid NOT NULL,
ADD COLUMN created_on timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN updated_by uuid,
ADD COLUMN updated_on timestamp without time zone,
ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;