SET search_path to at_project_schema;
-- create initial role
INSERT into at_project_schema."role"(role_name,created_by,description) VALUES('ADMIN', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', 'admin');
INSERT into at_project_schema."role"(role_name,created_by,description) VALUES('USER', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', 'user');
INSERT into at_project_schema."role"(role_name,created_by,description) VALUES('OWNER1', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', 'owner1');
INSERT into at_project_schema."role"(role_name,created_by,description) VALUES('OWNER2', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', 'owner2');
INSERT into at_project_schema."role"(role_name,created_by,description) VALUES('OWNER3', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', 'owner3');

-- create initial user
----admin/pass
INSERT INTO at_project_schema."user"(username, "password", first_name, last_name, gender, email, phone, role_id, created_by, "birth_date")
values('admin', '$2a$10$fMD40ijVdXD79fy9lbTDT.Ob77pnCJrZUl/fxeZ/T3gr2LlRJHRGu', 'admin', 'Do', 'MALE', 'tria3ltt@gmail.com', '012345678', (select "id" from at_project_schema."role" as r WHERE r."role_name" = 'ADMIN'), '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', '2000-01-01');
----user/pass
INSERT INTO at_project_schema."user"(username, "password", first_name, last_name, gender, email, phone, role_id, created_by, "birth_date")
values('tridv', '$2a$10$fMD40ijVdXD79fy9lbTDT.Ob77pnCJrZUl/fxeZ/T3gr2LlRJHRGu', 'Tri', 'Do', 'MALE', 'tria3ltt@gmail.com', '012345678', (select "id" from at_project_schema."role" as r WHERE r."role_name" = 'USER'), '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a', '2000-01-01');

-- Permission
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Creat location', 'CREATE_LOCATION', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Delete location', 'DELETE_LOCATION', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Update location', 'UPDATE_LOCATION', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('View location', 'VIEW_LOCATION', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');

INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Creat asset', 'CREATE_ASSET', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Delete asset', 'DELETE_ASSET', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Update asset', 'UPDATE_ASSET', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('View asset', 'VIEW_ASSET', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');

INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Creat job', 'CREATE_JOB', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Delete job', 'DELETE_JOB', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('Update job', 'UPDATE_JOB', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
INSERT INTO at_project_schema."permission"(description, permission_name, created_by)
VALUES('View job', 'VIEW_JOB', '3e0ce75b-e915-4cfb-b25e-bc5a23bb425a');
