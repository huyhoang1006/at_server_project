SET search_path TO at_project_schema;

ALTER TABLE "test"
    ADD COLUMN IF NOT EXISTS "weighting_factor" float default -1,
    ADD COLUMN IF NOT EXISTS "weighting_factor_df" float default -1,
    ADD COLUMN IF NOT EXISTS "weighting_factor_c" float default -1
;