CREATE EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;

SET search_path to at_project_schema;

CREATE TABLE "role"
(
    "id"          UUID      default public.uuid_generate_v4(),
    "role_name"   VARCHAR(255)                        NOT NULL unique,
    "description" VARCHAR(255)                        NOT NULL,

    "created_by"  uuid                                not null,
    "created_on"  TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"  uuid,
    "updated_on"  TIMESTAMP,
    "is_deleted"  BOOL      default 'f'               not null,
    PRIMARY KEY ("id")
) TABLESPACE pg_default;

CREATE TABLE "permission"
(
    "id"              UUID      default public.uuid_generate_v4(),
    "permission_name" VARCHAR(255)                        NOT NULL unique,
    "description"     VARCHAR(255)                        NOT NULL,

    "created_by"      uuid                                not null,
    "created_on"      TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"      uuid,
    "updated_on"      TIMESTAMP,
    "is_deleted"      BOOL      default 'f'               not null,
    PRIMARY KEY ("id")
) TABLESPACE pg_default;

CREATE TABLE "group"
(
    "id"          UUID      default public.uuid_generate_v4(),
    "group_name"  VARCHAR(255)                        NOT NULL unique,
    "description" VARCHAR(255)                        NOT NULL,

    "created_by"  uuid                                not null,
    "created_on"  TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"  uuid,
    "updated_on"  TIMESTAMP,
    "is_deleted"  BOOL      default 'f'               not null,
    PRIMARY KEY ("id")
) TABLESPACE pg_default;

CREATE TABLE "group_permission"
(
    "permission_id" UUID REFERENCES "permission" ("id") not null,
    "group_id"      UUID REFERENCES "group" ("id")      not null
) TABLESPACE pg_default;

CREATE TABLE "user"
(
    "id"         UUID         default public.uuid_generate_v4(),
    "username"   VARCHAR(255)                           NOT NULL unique,
    "password"   VARCHAR(255)                           NOT NULL,
    "enabled"    bool         default true,
    "first_name" VARCHAR(255)                           NOT NULL,
    "last_name"  VARCHAR(255)                           NOT NULL,
    "gender"     VARCHAR(255)                           NOT NULL,
    "email"      VARCHAR(255)                           NOT NULL,
    "phone"      VARCHAR(255),
    "role_id"    UUID                                   NOT NULL,
    "groups"     varchar(255) default '',
    "birth_date" DATE                                   NOT NULL,

    "created_by" uuid                                   not null,
    "created_on" TIMESTAMP    default CURRENT_TIMESTAMP not null,
    "updated_by" uuid,
    "updated_on" TIMESTAMP,
    "is_deleted" BOOL         default 'f'               not null,
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_User.roleID"
        FOREIGN KEY ("role_id")
            REFERENCES "role" ("id")
) TABLESPACE pg_default;

-- CREATE TABLE "user_group"
-- (
--     "user_id" UUID REFERENCES "user"("id") not null,
--     "group_id" UUID REFERENCES "group"("id") not null,
--     UNIQUE ("user_id", "group_id")
-- ) TABLESPACE pg_default;

CREATE TABLE "bushing"
(
    "id"                UUID      default public.uuid_generate_v4(),
    "asset_id"          UUID                                not null,
    "asset_type"        TEXT,
    "serial_no"         TEXT,
    "manufacturer"      TEXT,
    "manufacturer_type" TEXT,
    "manufacturer_year" TEXT,
    "insull_level"      TEXT,
    "voltage_gr"        TEXT,
    "max_sys_voltage"   TEXT,
    "rate_current"      TEXT,
    "df_c1"             TEXT,
    "cap_c1"            TEXT,
    "df_c2"             TEXT,
    "cap_c2"            TEXT,
    "insulation_type"   TEXT,

    "created_by"        uuid                                not null,
    "created_on"        TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"        uuid,
    "updated_on"        TIMESTAMP,
    "is_deleted"        BOOL      default 'f'               not null,
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Bushing.assetID"
        FOREIGN KEY ("asset_id")
            REFERENCES "asset" ("mrid") ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE "job"
(
    "id"                UUID      default public.uuid_generate_v4(),
    "asset_id"          UUID                                not null,
    "name"              VARCHAR(255),
    "work_order"        VARCHAR(255),
    "creation_date"     VARCHAR(255),
    "execution_date"    VARCHAR(255),
    "tested_by"         VARCHAR(255),
    "approved_by"       VARCHAR(255),
    "approval_date"     VARCHAR(255),
    "summary"           VARCHAR(255),
    "ambient_condition" VARCHAR(255),
    "testing_method"    VARCHAR(255),
    "standard"          INTEGER,

    "created_by"        uuid                                not null,
    "created_on"        TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"        uuid,
    "updated_on"        TIMESTAMP,
    "is_deleted"        BOOL      default 'f'               not null,
    PRIMARY KEY ("id"),
    CONSTRAINT "FK_Job.assetID"
        FOREIGN KEY ("asset_id")
            REFERENCES "asset" ("mrid") ON DELETE CASCADE
) TABLESPACE pg_default;